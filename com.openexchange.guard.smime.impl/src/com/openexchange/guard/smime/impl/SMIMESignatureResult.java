/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.math.BigInteger;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link SMIMESignatureResult}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SMIMESignatureResult implements SignatureVerificationResult {

    private static final String REASON = "error";
    private static final String DATE_PROPERTY = "signatureCreatedOn";
    private static final String ISSUER_USERIDS_PROPERTY = "issuerUserIds";
    private static final String ISSUER_KEY_ID_PROPERTY = "issuerKeyId";
    private static final String ISSUER_KEY_FINGERPRINT_PROPERTY = "issuerKeyFingerprint";

    private final String reason;

    private final boolean verified;
    private String email;
    private Long signedDate;
    private BigInteger serial;

    public static class SMIMESignatureResultBuilder {

        private final boolean verified;
        private String reason;
        private String email;
        private Long signedDate;
        private BigInteger serial;

        public SMIMESignatureResultBuilder(boolean verified) {
            this.verified = verified;

        }

        public SMIMESignatureResultBuilder setReason(String reason) {
            this.reason = reason;
            return this;
        }

        public SMIMESignatureResultBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public SMIMESignatureResultBuilder setSerial(BigInteger serial) {
            this.serial = serial;
            return this;
        }

        public SMIMESignatureResultBuilder setSignedDate(Long ticks) {
            this.signedDate = ticks;
            return this;

        }

        public SMIMESignatureResult build() {
            return new SMIMESignatureResult(verified, reason, email, signedDate, serial);
        }

    }

    public SMIMESignatureResult(boolean verified) {
        super();
        this.verified = verified;
        this.reason = null;
    }

    public SMIMESignatureResult(boolean verified, String reason) {
        super();
        this.verified = verified;
        this.reason = reason;
    }

    public SMIMESignatureResult(boolean verified, String reason, String email, Long date, BigInteger serial) {
        this.verified = verified;
        this.reason = reason;
        this.email = email;
        this.signedDate = date;
        this.serial = serial;
    }

    public void setDate(Long ticks) {
        this.signedDate = ticks;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSerial(BigInteger serial) {
        this.serial = serial;
    }

    @Override
    public boolean isVerified() {
        return verified;
    }

    @Override
    public String toHeader() {
        StringBuilder sb = new StringBuilder();
        addVerifiedToHeader(sb);
        sb.append("; ").append(TYPE).append("=SMIME");
        if (reason != null) {
            sb.append("; ").append(REASON).append("=").append(reason);
        }
        if (this.signedDate != null) {
            sb.append("; ").append(DATE_PROPERTY).append("=");
            sb.append(this.signedDate);
        }
        // Issuer Ids are supported as long values.  If BigInt > 16 chars, use last 16
        if (this.serial != null) {
            sb.append("; ").append(ISSUER_KEY_ID_PROPERTY).append("=");
            final String serialString = this.serial.toString();
            if (serialString.length() > 16) {
                sb.append(serialString.subSequence(serialString.length() - 15, serialString.length()));
            } else {
                sb.append(this.serial.longValue());
            }
            sb.append("; ").append(ISSUER_KEY_FINGERPRINT_PROPERTY).append("=");
            sb.append(this.serial);
        }
        if (this.email != null) {
            sb.append("; ").append(ISSUER_USERIDS_PROPERTY).append("=");
            sb.append(this.email);
        }
        return Base64.encode(sb.toString());
    }
}
