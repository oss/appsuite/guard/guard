/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import static com.openexchange.java.Autoboxing.I;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.crypto.io.InvalidCipherTextIOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.annotation.Nullable;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimeUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardChangePasswordExceptionCodes;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.SmimeKeysExport;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.smime.storage.SmimePrivateKeyStorage;
import com.openexchange.guard.smime.storage.SmimePublicKeyStorage;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link SmimeKeyServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class SmimeKeyServiceImpl implements SmimeKeyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmimeKeyServiceImpl.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link SmimeKeyServiceImpl}.
     *
     * @param services The {@link ServiceLookup}
     */
    public SmimeKeyServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Internal method to fetch the {@link SmimePrivateKeyStorage}
     *
     * @return The {@link SmimePrivateKeyStorage}
     * @throws OXException
     */
    private SmimePrivateKeyStorage getPrivateKeyStorage() throws OXException {
        return services.getServiceSafe(SmimePrivateKeyStorage.class);
    }

    /**
     * Internal method to fetch the {@link SmimePublicKeyStorage}
     *
     * @return The {@link SmimePublicKeyStorage}
     * @throws OXException
     */
    private SmimePublicKeyStorage getPublicKeyStorage() throws OXException {
        return services.getServiceSafe(SmimePublicKeyStorage.class);
    }

    /**
     * Internal method to fetch the {@link SmimeKeys} from the corresponding given {@link SmimePrivateKeys}
     *
     * @param keys The {@link SmimePrivateKeys} to get the {@link SmimeKeys} for
     * @param userId The ID of the user
     * @param contextId The ID of the context
     * @return The {@link SmimeKeys} for the given {@link SmimePrivateKeys}
     */
    private SmimeKeys getSmimeKeyFromPrivate(SmimePrivateKeys key, int userId, int cid) {
        return getSmimeKeysFromPrivate(Collections.singletonList(key), userId, cid).stream().findFirst().orElse(null);
    }

    /**
     * Internal method to fetch {@link SmimeKeys} instances from the corresponding given {@link SmimePrivateKeys}
     *
     * @param keys The {@link SmimePrivateKeys} to get the {@link SmimeKeys} for
     * @param userId The ID of the user
     * @param contextId The ID of the context
     * @return The {@link SmimeKeys} for the given {@link SmimePrivateKeys}
     */
    private List<SmimeKeys> getSmimeKeysFromPrivate(List<SmimePrivateKeys> keys, int userId, int cid) {
        ArrayList<SmimeKeys> sKeys = new ArrayList<SmimeKeys>();
        if (keys == null || keys.isEmpty()) {
            return sKeys;
        }
        keys.forEach((k) -> {
            SmimeKeys sKey;
            try {
                sKey = getPublicKeyStorage().getKey(k.getSerial(), userId, cid);
                if (sKey != null) {
                    sKey.setPrivateKey(k);
                    sKeys.add(sKey);
                }
            } catch (OXException e) {
                LOGGER.warn("Unable to get key", e);
            }
        });
        return sKeys;
    }

    /**
     * Will check if any remaining private keys, and if so, make the first the current key
     *
     * @param userId The ID of the user
     * @param contextId The ID of the context
     */
    private void updateCurrent(int userId, int contextId) {
        try {
            List<SmimePrivateKeys> privKeys = getPrivateKeys(userId, contextId);
            if (!privKeys.isEmpty()) {
                setCurrentKey(privKeys.get(0).getSerial(), userId, contextId);
            }
        } catch (OXException e) {
            LOGGER.debug("Unable to update", e);
            // No need to fail here
        }
    }

    private void checkMinPasswordLength(String password, int userId, int contextId) throws OXException {
        final GuardConfigurationService configService = services.getService(GuardConfigurationService.class);
        final int minPasswordLength = configService.getIntProperty(GuardProperty.minPasswordLength, userId, contextId);
        if (password.length() < minPasswordLength) {
            throw GuardChangePasswordExceptionCodes.BAD_LENGTH.create(I(minPasswordLength));
        }
    }

    private static SmimeKeys changePassword(SmimeKeys keys, String oldpass, String newpass, int userId, int cid) throws OXException {
        final SmimePrivateKeys smimePriv = keys.getPrivateKey();
        PrivateKey pKey = SmimeCryptoKeyUtil.decryptPrivateKey(smimePriv, oldpass);
        String encryptedPrivateKey = SmimeCryptoKeyUtil.encryptPrivateKey(pKey, newpass, smimePriv.getSalt());
        smimePriv.setEncryptedKeyData(encryptedPrivateKey);
        return new SmimeKeys(keys.getCertificate(), keys.getChain(), smimePriv, userId, cid, keys.getEmail(), new Date().getTime());
    }

    @Override
    public boolean existsPublicKey(String serial, String email, Date fromDate) throws OXException {
        return getPublicKeyStorage().keyExists(serial, email, fromDate);
    }

    @Override
    public List<SmimeKeys> getKeys(int userId, int contextId) throws OXException {
        return getSmimeKeysFromPrivate(getPrivateKeyStorage().getPrivateKeys(userId, contextId), userId, contextId);
    }

    @Override
    public List<SmimeKeys> getKeys(String email) throws OXException {
        List<SmimeKeys> keys = getPublicKeyStorage().getKeys(email);
        for (SmimeKeys key : keys) {
            SmimePrivateKeys privateKey = getPrivateKey(key.getSerial().toString(), key.getUserId(), key.getContextId());
            if (privateKey == null) {
                throw SmimeExceptionCodes.KEY_NOT_FOUND.create(key.getSerial());
            }
            key.setPrivateKey(privateKey);
        }
        return keys;
    }

    @Override
    public List<SmimeKeys> getPublicKeys(String email) throws OXException {
        return getPublicKeyStorage().getKeys(email);
    }

    @Override
    public List<SmimeKeys> getPublicKeys() throws OXException {
        return getPublicKeyStorage().getKeys();
    }

    @Override
    public SmimeKeys getKey(String serial, int userId, int cid) throws OXException {
        SmimePrivateKeys privateKey = getPrivateKeyStorage().getKeyById(serial, userId, cid);
        return privateKey != null ? getSmimeKeyFromPrivate(privateKey, userId, cid) : null;
    }

    @Override
    public List<SmimePrivateKeys> getPrivateKeys(int userId, int cid) throws OXException {
        return getPrivateKeyStorage().getPrivateKeys(userId, cid);
    }

    @Override
    public SmimePrivateKeys getPrivateKey(String serial, int userId, int cid) throws OXException {
        return getPrivateKeyStorage().getKeyById(serial, userId, cid);
    }

    @Override
    public SmimeKeys getCurrentKey(int userId, int cid) throws OXException {
        SmimePrivateKeys currentPrivate = getCurrentPrivateKey(userId, cid);
        return currentPrivate != null ? getSmimeKeyFromPrivate(currentPrivate, userId, cid) : null;
    }

    @Override
    public SmimePrivateKeys getCurrentPrivateKey(int userId, int cid) throws OXException {
        return getPrivateKeyStorage().getCurrentPrivateKey(userId, cid);
    }

    @Override
    public void setCurrentKey(String serial, int userId, int cid) throws OXException {
        SmimePrivateKeys key = getPrivateKeyStorage().getKeyById(serial, userId, cid);
        if (key != null) {
            if (!getPrivateKeyStorage().makeCurrent(serial, userId, cid)) {
                throw SmimeExceptionCodes.KEY_NOT_FOUND.create(serial);
            }
        } else {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create(serial);
        }
    }

    @Override
    public SmimeKeys createKey(X509Certificate certificate, List<X509Certificate> certificates, PrivateKey privateKey, String password, int userId, int cid, boolean current) throws OXException {
        final MasterKeyService masterKeyService = services.getServiceSafe(MasterKeyService.class);
        String email = getPrimaryEmail(certificate, userId, cid);
        //@formatter:off
        SmimePrivateKeys newKey = new SmimePrivateKeyFactory().createKey(
            certificate,
            privateKey,
            password,
            email,
            masterKeyService.getIndexForUser(userId, cid),
            current);
        //@formatter:on
        updateRecovery(newKey, password, userId, cid);
        return new SmimeKeys(certificate, certificates, newKey, userId, cid, email, new Date().getTime());
    }

    @Override
    public void storeKey(SmimeKeys keys, int userId, int cid, boolean replaceIfPresent) throws OXException {
        getPrivateKeyStorage().storePrivateKey(keys, userId, cid, replaceIfPresent);
    }

    @Override
    public void storePublicKey(int userId, int cid, SmimeKeys keys, Date fromDate) throws OXException {
        getPublicKeyStorage().storePublicKeys(userId, cid, keys, fromDate);
    }

    @Override
    public void deleteKey(String serial, int userId, int contextId, String password) throws OXException {
        SmimePrivateKeys pKey = getPrivateKeyStorage().getKeyById(serial, userId, contextId);
        if (pKey == null) {
            throw SmimeExceptionCodes.KEY_NOT_FOUND.create(serial);
        }
        //Verify password
        SmimeCryptoKeyUtil.decryptPrivateKey(pKey, password);

        if (getPrivateKeyStorage().deleteKeyById(serial, userId, contextId)) {
            // OK.  Remove the public key
            if (getPublicKeyStorage().deleteKeyById(serial, userId, contextId)) {
                if (pKey.isCurrent()) {
                    updateCurrent(userId, contextId);
                }
                return;
            }
            throw SmimeExceptionCodes.CRYPTO_ERROR.create("Failed to delete public key");
        }
        throw SmimeExceptionCodes.CRYPTO_ERROR.create("Failed to delete private key");
    }

    @Override
    public void deleteKeys(int userId, int contextId) throws OXException {
        getPublicKeyStorage().deleteForUser(userId, contextId);
        getPrivateKeyStorage().deleteForUser(userId, contextId);
    }

    @Override
    public void deleteKeys(int contextId) throws OXException {
        getPublicKeyStorage().deleteForContext(contextId);
        getPrivateKeyStorage().deleteForContext(contextId);
    }

    /**
     * Check that this private key does not exist in another account
     *
     * @param key
     * @throws OXException
     */
    private void checkDuplicate(SmimeKeys key, int userId, int cid) throws OXException {
        List<SmimeKeys> pubKeys = getPublicKeys(key.getEmail());
        for (SmimeKeys pubKey: pubKeys) {
            // If key is owned by this user, then continue
            if (pubKey.getUserId() == userId && pubKey.getContextId() == cid) {
                continue;
            }
            // Check if serial number matches
            if (pubKey.isLocal()) {
                // If there is a private key, then we have a duplicate and should reject
                SmimePrivateKeys privKey = getPrivateKey(pubKey.getSerial().toString(), pubKey.getUserId(), pubKey.getContextId());
                if (privKey != null) {
                    if (pubKey.getSerial().equals(key.getSerial())) {
                        throw SmimeExceptionCodes.PRIVATE_KEY_ALREADY_EXISTS.create();
                    } else {
                        throw SmimeExceptionCodes.PRIVATE_KEY_OTHER_USER.create();
                    }
                }
            }
        }
    }

    @Override
    public SmimeKeys importKey(InputStream keyData, int userId, int contextId, String password, String newPassword, boolean ignoreDate) throws OXException {
        //Parsing
        try {
            // Check new password OK
            checkMinPasswordLength(newPassword, userId, contextId);
            // Do import
            final KeyImporter importer = new KeyImporter(services);
            SmimeKeys keys = importer.readPKCS12File(keyData, password, userId, contextId);
            if (keys == null || keys.getPrivateKey() == null) {
                throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
            }
            final CertificateService certificateService = services.getServiceSafe(CertificateService.class);
            final Result result = certificateService.verify(keys.getCertificate(), new HashSet<>(keys.getChain()), ignoreDate ? null : new Date(), userId, contextId);
            if (!Result.VERIFIED.equals(result)) {
                throw SmimeExceptionCodes.CERTIFICATE_VERIFICATION_FAIL.create();
            }
            // Make sure this key isn't already in another account
            checkDuplicate(keys, userId, contextId);
            // Change to new password
            keys = changePassword(keys, password, newPassword, userId, contextId);
            // Mark this now as the current key as long as we aren't ignoring exp date
            keys.getPrivateKey().setCurrent(!ignoreDate);
            updateRecovery(keys.getPrivateKey(), newPassword, userId, contextId);
            // Store it
            final SmimePrivateKeys existing = getPrivateKey(keys.getSerial().toString(), userId, contextId);
            if (existing != null) {
                throw SmimeExceptionCodes.DUPLICATE_PRIVATE_KEY.create();
            }
            storeKey(keys, userId, contextId, false);
            return keys;
        } catch (final Exception e) {
            if (e.getCause() instanceof InvalidCipherTextIOException) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
            if (e instanceof OXException) {
                throw (OXException) e;
            }
            if (e instanceof java.security.cert.CertificateExpiredException) {
                throw SmimeExceptionCodes.CERTIFICATE_EXPIRED.create();
            }
            throw GuardCoreExceptionCodes.IO_ERROR.create(e.getMessage());
        }
    }

    @SuppressWarnings("resource")
    @Override
    public SmimeKeysExport exportKey(@Nullable String serial, int userId, int contextId, String password, String newPassword) throws OXException {
        SmimeKeys keyToExport = serial != null ? getKey(serial, userId, contextId) : getCurrentKey(userId, contextId);
        if (keyToExport != null) {
            return new SmimeKeysExport(PKCS12KeyExporter.exportPKCS12(keyToExport, password, newPassword), keyToExport);
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
    }

    @Override
    public boolean updateRecovery(SmimeKeys key, String password, int userId, int cid) throws OXException {
        if (!key.hasPrivateKey()) {
            throw new NullPointerException("The included private key must not be null");
        }
        return updateRecovery(key.getPrivateKey(), password, userId, cid);
    }

    @Override
    public boolean updateRecovery(SmimePrivateKeys privateKey, String password, int userId, int cid) throws OXException {
        return getPrivateKeyStorage().updateRecovery(privateKey, password, userId, cid);
    }

    @Override
    public boolean changePasswordWithRecovery(SmimePrivateKeys key, String newpass, int userId, int cid) throws OXException {
        return getPrivateKeyStorage().changePasswordWithRecovery(key, newpass, userId, cid);
    }

    /**
     * Return standardized file type for supported formats.
     *
     * @param filename
     * @return null if not found, otherwise p7 or cer
     */
    private String getStandardFileType(String filename) {
        if (filename.contains(".p7")) {
            return "p7";
        }
        if (filename.contains(".cer") || filename.contains(".crt")) {
            return "cer";
        }
        return null;
    }

    @Override
    public SmimeKeys importRecipKey(InputStream data, int userId, int contextId, String filename) throws OXException {
        final KeyImporter importer = new KeyImporter(services);
        SmimeKeys cert = null;
        final String type = getStandardFileType(filename);
        if (type == null) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create("filename");
        }
        // Get the certificates from the file
        try {
            if (type.equals("p7")) {
                cert = importer.getFromPKCS7(data, userId, contextId);
            }
            if (type.equals("cer")) {
                cert = importer.getFromCerFile(data, userId, contextId);
            }
        } catch (IOException | CertificateException | CMSException e) {
            throw SmimeExceptionCodes.CRYPTO_ERROR.create(e.getLocalizedMessage());
        }
        // Now, verify that the certificates are trusted
        if (cert != null) {
            final CertificateService certificateService = services.getServiceSafe(CertificateService.class);
            try {
                // Check to make sure we don't have existing user with private certs using this email address first
                List<SmimeKeys> existing = this.getKeys(cert.getEmail());
                long localKeys = existing.stream().filter((k) -> k.isLocal()).count();
                if (localKeys > 0) {
                    // We have local users, reject upload
                    throw SmimeExceptionCodes.PRIVATE_KEY_ALREADY_EXISTS.create();
                }
            } catch (OXException e) {
                if (!SmimeExceptionCodes.KEY_NOT_FOUND.equals(e)) {
                    throw e;
                }
            }
            Result result = null;
            try {
                result = certificateService.verify(cert.getCertificate(), new HashSet<>(cert.getChain()), new Date(), userId, contextId);
            } catch (Exception e) {
                throw SmimeExceptionCodes.CERTIFICATE_VERIFICATION_FAIL.create(e);
            }
            if (!Result.VERIFIED.equals(result)) {
                throw SmimeExceptionCodes.CERTIFICATE_VERIFICATION_FAIL.create();
            }
            // OK, verified.  Now do import
            SmimePublicKeyStorage storage = services.getServiceSafe(SmimePublicKeyStorage.class);
            storage.storePublicKeys(userId, contextId, cert, new Date());
            return cert;
        }

        return null;
    }

    @Override
    public String getPrimaryEmail(X509Certificate cert, int userId, int cid) throws OXException {
        List<String> emails = SmimeUtil.getEmails(cert);
        if (emails.isEmpty())
            return null;
        if (emails.size() == 1)
            return emails.get(0);
        UserService userService = services.getServiceSafe(UserService.class);
        User user = userService.getUser(userId, cid);
        if (user == null)
            return emails.get(0);
        String email = user.getMail();
        if (email == null)
            return emails.get(0);
        if (SmimeUtil.containsEmail(email, emails)) {
            return email;
        }
        return emails.get(0);
    }
}
