/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.smime.impl;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cms.CMSAbsentContent;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.util.CollectionStore;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;

/**
 * Class to export PKCS7 certificates
 * 
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v8.21
 * 
 */
public class PKCS7Exporter {

    /**
     * Export Certificate and chain from SmimeKeys
     *
     * @param key containing the certificates to export
     * @return Byte array containing PKCS7 encoded certificate and chain
     * @throws Exception
     */
    public static byte[] export(SmimeKeys key) throws Exception {

        CMSSignedDataGenerator gen = new CMSSignedDataGenerator();

        Collection<JcaX509CertificateHolder> x509CertificateHolder = new ArrayList<>();

        // Add user certificate
        x509CertificateHolder.add(new JcaX509CertificateHolder(key.getCertificate()));
        // Add chain
        for (X509Certificate certificate : key.getChain()) {
            x509CertificateHolder.add(new JcaX509CertificateHolder(certificate));
        }

        CollectionStore<JcaX509CertificateHolder> store = new CollectionStore<>(x509CertificateHolder);

        // The final stage.
        gen.addCertificates(store);
        CMSSignedData data = gen.generate(new CMSAbsentContent());
        return data.getEncoded();
    }
}
