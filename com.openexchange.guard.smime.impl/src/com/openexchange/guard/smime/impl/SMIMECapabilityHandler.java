/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.smime.impl;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.smime.SMIMECapabilities;
import org.bouncycastle.asn1.smime.SMIMECapability;
import com.openexchange.capabilities.Capability;
import com.openexchange.exception.OXException;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.smime.storage.SmimeCapabilityStorage;

/**
 * {@link SMIMECapabilityHandler} - provides functionality for determining S/MIME capabilities.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
class SMIMECapabilityHandler {

    //A list of technical supported/allowed symmetric algorithms by Guard for S/MIME
    //See RFC 8551: 2.7 ContentEncryptionAlgorithmIdentifier
    //@formatter:off
    private static List<ASN1ObjectIdentifier> supportedSymmetricAlgorithms = Arrays.asList(

        //AES GCM
        NISTObjectIdentifiers.id_aes128_GCM,
        NISTObjectIdentifiers.id_aes192_GCM,
        NISTObjectIdentifiers.id_aes256_GCM,

        //AES CBC
        NISTObjectIdentifiers.id_aes128_CBC,
        NISTObjectIdentifiers.id_aes192_CBC,
        NISTObjectIdentifiers.id_aes256_CBC,

        //Triple DES (S/MIME v3)
        PKCSObjectIdentifiers.des_EDE3_CBC

        //ChaCha20-Poly1305 [RFC7905]
        //not yet supported but should be in the future
    );
    //@formatter:on

    // A simple enumeration representing the SMIMEVersion
    private static enum SMIMEVersion {

        UNKNOWN(0f),
        S_MIME_VERSION_3_1(3.1f),
        S_MIME_VERSION_3_2(3.2f),
        S_MIME_VERSION_4_0(3.2f),

        ;

        private float versionNumber;

        private SMIMEVersion(float versionNumber) {
            this.versionNumber = versionNumber;
        }

        @SuppressWarnings("unused")
        public float getVersionNumber() {
            return versionNumber;
        }

    }

    private SmimeCapabilityStorage capabilityStorage;

    //The fallback to use if no other suitable algorithm was found/announced in one of the capabilities
    private ASN1ObjectIdentifier defaultSymmetricAlgorithm;

    /**
     * Initializes a new {@link SMIMECapabilityHandler}.
     */
    public SMIMECapabilityHandler() {
        //strongest S/MIME v4.0 default
        this.defaultSymmetricAlgorithm = NISTObjectIdentifiers.id_aes256_GCM;
    }

    /**
     * Gets the S/MIME version supported by the given recipient, if known
     *
     * @param recipient The recipient to get the supported S/MIME version for
     */
    @SuppressWarnings("unused")
    private SMIMEVersion getKnownSMIMEVersion(X509Certificate recipient) {
        //This is prepared for future use: Currently the SMIME version is unknown.
        return SMIMEVersion.UNKNOWN;
    }

    /**
     * Checks whether or not OX Guard is able to use the given algorithm identifier as symmetric S/MIME encryption/decryption algorithm
     *
     * @param algorithm The algorithm to check
     * @return <code>True</code> if the given identifier represents a supported symmetric encryption algorithm, <code>False</code> otherwise.
     */
    private boolean isSymmetricAlgorithmSupported(ASN1ObjectIdentifier algorithm) {
        return supportedSymmetricAlgorithms.contains(algorithm);
    }

    /**
     * Checks whether or not OX Guard is able to use the given algorithm identifier as symmetric S/MIME encryption/decryption algorithm
     *
     * @param capability The capability describing an symmetric encryption algorithm
     * @return <code>True</code> if the given capabilities' OID identifier represents a supported symmetric encryption algorithm, <code>False</code> otherwise.
     */
    private boolean isSymmetricAlgorithmSupported(SMIMECapability capability) {
        return isSymmetricAlgorithmSupported(capability.getCapabilityID());
    }

    /**
     * Returns The first {@link Capability} from the given {@link SMIMECapabilities} which is a known and supported symmetric encryption algorithm
     *
     * @param capabilities The {@link SMIMECapabilities} to get the first known {@link Capability} from
     * @return The first {@link Capability} from the given {@link SMIMECapabilities} which is a known and supported symmetric encryption algorithm, or null if not found
     * @throws OXException
     */
    @SuppressWarnings("unchecked")
    private SMIMECapability getFirstKnownSymmetricEncryptionCapability(SMIMECapabilities capabilities) throws OXException {
        if (capabilities != null) {
            try {
                for (SMIMECapability capability : (Vector<SMIMECapability>) capabilities.getCapabilities(null)) {
                    if (isSymmetricAlgorithmSupported(capability)) {
                        return capability;
                    }
                }
            } catch (IllegalArgumentException e) {
                throw SmimeExceptionCodes.CRYPTO_ERROR.create(e, "Unable to determine SMIMECapabilities");
            }
        }
        return null;
    }

    /**
     * If a {@link SmimeCapabilityStorage} is available this method gets the most appropriated symmetric algorithm stored for the the given recipient and also supported by Guard
     * <p>
     * This implements Rule 1: Known Capabilities (See rfc8551 section 2.7.1.1)
     *
     * @param user The certificate representing the user/recipient to get the known decryption capabilities for
     * @return The most preferred algorithm stored for the given user which is supported by Guard, or null if no stored capability/algorithm was found or supported
     * @throws OXException
     */
    private ASN1ObjectIdentifier getSymmetricAlgorithmFromStorage(X509Certificate recipient) throws OXException {
        if (this.capabilityStorage != null) {
            SMIMECapabilities storedCapabiliteis = this.capabilityStorage.get(recipient);
            SMIMECapability cap = getFirstKnownSymmetricEncryptionCapability(storedCapabiliteis);
            if (cap != null) {
                return cap.getCapabilityID();
            }
        }
        return null;
    }

    /**
     * Gets list of supported algorithms from storage
     *
     * @param recipient
     * @return List of supported algorithms
     */
    private List<ASN1ObjectIdentifier> getSymmetricAlgorithmsFromStorage(X509Certificate recipient) {
        ArrayList<ASN1ObjectIdentifier> capabilities = new ArrayList<ASN1ObjectIdentifier>();
        if (this.capabilityStorage != null) {
            SMIMECapabilities storedCapabiliteis = this.capabilityStorage.get(recipient);
            for (SMIMECapability capability : (Vector<SMIMECapability>) storedCapabiliteis.getCapabilities(null)) {
                if (isSymmetricAlgorithmSupported(capability)) {
                    capabilities.add(capability.getCapabilityID());
                }
            }
        }
        return capabilities;
    }

    /**
     * Gets the most appropriated symmetric algorithm from the given certificate's capabilities extension (as defined in RFC 4262) and also supported by Guard
     *
     * @param certificate The certificate
     * @return The most preferred algorithm defined in the given certificate's extension and supported by Guard, or null if no capability/algorithm was found or supported
     * @throws OXException
     */
    private ASN1ObjectIdentifier getSymmetricAlgorithmFromCertificate(X509Certificate certificate) throws OXException {
        SMIMECapabilities capabilitiesFromExtension = SMIMECapabilitiesParser.parseCapabilities(certificate);
        SMIMECapability capability = getFirstKnownSymmetricEncryptionCapability(capabilitiesFromExtension);
        return capability != null ? capability.getCapabilityID() : null;
    }

    /**
     * Gets the list of supported capabilities from the certificate
     *
     * @param certificate The certificate to evaluate
     * @return List of supported algorithms
     * @throws OXException
     */
    private List<ASN1ObjectIdentifier> getSymmetricAlgorithmsFromCertificate(X509Certificate certificate) throws OXException {
        SMIMECapabilities capabilitiesFromExtension = SMIMECapabilitiesParser.parseCapabilities(certificate);
        if (capabilitiesFromExtension == null) {
            return Collections.emptyList();
        }
        ArrayList<ASN1ObjectIdentifier> capabilities = new ArrayList<ASN1ObjectIdentifier>();
        for (SMIMECapability capability : (Vector<SMIMECapability>) capabilitiesFromExtension.getCapabilities(null)) {
            if (isSymmetricAlgorithmSupported(capability)) {
                capabilities.add(capability.getCapabilityID());
            }
        }
        return capabilities;
    }

    /**
     * Sets the symmetric fallback algorithm to if not other symmetric algorithm was determined
     *
     * @param defaultSymmetricAlgorithm The ID of the default symmetric algorithm to use
     * @return this
     */
    public SMIMECapabilityHandler setSymmetricFallback(ASN1ObjectIdentifier defaultSymmetricAlgorithm) {
        this.defaultSymmetricAlgorithm = defaultSymmetricAlgorithm;
        return this;
    }

    /**
     * Sets the optional {@link SmimeCapabilityStorage} to use for fetching {@link SMIMECapabilities} from persistent Messages / SignerInfos
     *
     * @param capabilityStorage The {@link SmimeCapabilityStorage} to use for fetching stored {@link SMIMECapabilities} from recent messages received
     * @return this
     */
    public SMIMECapabilityHandler setCapabilityStorage(SmimeCapabilityStorage capabilityStorage) {
        this.capabilityStorage = capabilityStorage;
        return this;
    }

    /**
     * Gets the first common, preferred and supported symmetric encryption algorithm for the given recipients if present, or the default algorithm otherwise
     *
     * @param recipients The recipients to get the algorithm for
     * @return The first common, preferred and supported symmetric encryption algorithm for the given recipients if present, or the default algorithm otherwise
     * @throws OXException due an error
     */
    public ASN1ObjectIdentifier getSymmetricEncryptionAlgorithm(ArrayList<X509Certificate> recipientCertificates) throws OXException {
        return getSymmetricEncryptionAlgorithm(recipientCertificates.toArray(new X509Certificate[recipientCertificates.size()]));
    }

    /**
     * Gets the first common, preferred and supported symmetric encryption algorithm for the given recipients if present, or the default algorithm otherwise
     *
     * @param recipients The recipients to get the algorithm for
     * @return The first common, preferred and supported symmetric encryption algorithm for the given recipients if present, or the default algorithm otherwise
     * @throws OXException due an error
     */
    public ASN1ObjectIdentifier getSymmetricEncryptionAlgorithm(X509Certificate... recipients) throws OXException {
        if (recipients != null) {
            // If we just have one recipient, just send preferred
            if (recipients.length == 1) {
                ASN1ObjectIdentifier alg = getSymmetricAlgorithmFromStorage(recipients[0]);
                if (alg != null) {
                    return alg;
                }
                alg = getSymmetricAlgorithmFromCertificate(recipients[0]);
                if (alg != null) {
                    return alg;
                }
                return defaultSymmetricAlgorithm;

            }

            // Get lists of all supported algs for each recipient
            ArrayList<List<ASN1ObjectIdentifier>> algLists = new ArrayList<List<ASN1ObjectIdentifier>>();
            for (X509Certificate recipient : recipients) {
                List<ASN1ObjectIdentifier> supported = getSymmetricAlgorithmsFromStorage(recipient);
                if (!supported.isEmpty()) {
                    algLists.add(supported);
                    continue;
                }
                supported = getSymmetricAlgorithmsFromCertificate(recipient);
                if (!supported.isEmpty()) {
                    algLists.add(supported);
                    continue;
                }
                // No supported algs lists, try based on version
                SMIMEVersion knownSMIMEVersion = getKnownSMIMEVersion(recipient);
                switch (knownSMIMEVersion) {
                    case S_MIME_VERSION_3_1:
                        algLists.add(Collections.singletonList(NISTObjectIdentifiers.id_aes128_CBC));
                        break;
                    case S_MIME_VERSION_3_2:
                        algLists.add(Collections.singletonList(NISTObjectIdentifiers.id_aes128_CBC));
                        break;
                    case S_MIME_VERSION_4_0:
                        algLists.add(Collections.singletonList(NISTObjectIdentifiers.id_aes256_GCM));
                        break;
                    default:
                        //Rule 2: Unknown Capabilities, Unknown Version of S/MIME
                }
            }

            if (algLists.size() != recipients.length) { // Not all recipients had algs specified
                return defaultSymmetricAlgorithm;
            }

            // Now, find first that matches all.
            List<ASN1ObjectIdentifier> firstRecip = algLists.get(0);
            for (int i = 0; i < firstRecip.size(); i++) {
                ASN1ObjectIdentifier alg = firstRecip.get(i);
                boolean match = true;
                for (int j = 1; j < algLists.size(); j++) {
                    match = match && (algLists.get(j).contains(alg));
                }
                if (match) {
                    return alg;
                }
            }

        }
        //No common preferred algorithm found
        return defaultSymmetricAlgorithm;
    }
}
