/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.testService;

import java.io.ByteArrayInputStream;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.List;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.smime.impl.KeyImporter;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link TestCAStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class TestCAStorage implements CaCertStorage {

    private static final String GUARD_SMIME_TEST_CA = "com.openexchange.smime.testCA";

    //  THIS KEY IS FOR TESTING ONLY
    private static final String TEST_CA = "MIIKNgIBAzCCCfwGCSqGSIb3DQEHAaCCCe0EggnpMIIJ5TCCBH8GCSqGSIb3DQEHBqCCBHAwggRs\n"
        + "AgEAMIIEZQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIHTJIeEXkeXECAggAgIIEOGc1Yr+B\n"
        + "ivUukC2K+GIwriK5tWIefk7x9nKmTdwa+w3jYBG7t3zMVFWZhQaGDjehDgxxcqUo+lASPiTa2jeG\n"
        + "HeIHBNLieBdyiiFIeeJp/LKCedg0gvM0HAiQa829z9M4G+X955Wl3V2eeroqqQZvzo3wzymaWfGB\n"
        + "eVHENA+OboW5wGd18f8gQXuKw3aHnFToViI5MppgCjawJJd3RDIgZ61ZiWH3gQrAr4ttxzQrR562\n"
        + "qlqVzgKna8pzbISAWhXA49szUWxuYoBv2BE0RS7hywDoZSzk6kF/GAa7sAThlkd+E+l3wKxXNbPD\n"
        + "uKKE5zHmb8l7JJm0TYWL9k1igELEi8VzK7Crb/UwzrPRqzb783HypZFdiopTvJLfZGj4VMNaGGZW\n"
        + "jJ/vs2aPhJaGdMG4Y1MANcmXHui26IwckfM+s4FTN4M9g9yvWS3QDci9YpKVUVUljExJboLUnH/2\n"
        + "kcnbf7k07VY7F0q4HL/tTcBIpRo/FGVrpzo1JbKRcqCyN5YTb5oLA0AJ1GBqxtqfPt+OZxYO6Jmt\n"
        + "JAGUhhTKnTWEDP1/pmf9Gph8hh2ctwS9JnOomYU+z1+o1+afEXj0/CX6QARHow4k1il1aTaGODNT\n"
        + "e4lvGF66aT2G+a/yO6/7o3pOsVpYCl4tdXCBS3o9Ype73J2FNgVeAGFoS/aWmgcO/TK6qfsoUSh+\n"
        + "y+Awsig3PMkIganiuu83fFFobNLbLYQwS+NjgHgn666uYsJ/yz+wHXt3Ud2O6a+lLMI9Kpj6DW97\n"
        + "6g5PRBSAOs+u4n9JPnPY1OqvH+of99JEsSLRlWrQuL8rzNsQ2aeC8feyTpkQsg07qQmCPfWAR3nR\n"
        + "cRuRBrYWcLL2gW6xYE2WpZAAJW34tksGe2ShbhmWLqM8JLwZs0p5re4WcKlO+YbuibWfdE+UZx+Y\n"
        + "9BGBSu8n96C7lyAkCN0DbnBW0ICB9GucUGwWxzkxZS39FLzUabWxKyphxqbt5Kt/fAabctQPgI4I\n"
        + "F5ACUHFZ4ySZh9SxYxr6h6a0RW8c9ZLMyUY80WuuJbgtg9Wkzfmx03Y/z8rkaV2s3Fatga0f+QaV\n"
        + "fb8dd+pnzX+VbLPvSm9aT+Yh2GY7RLp0Gn8Pw/N8oUaaVTNy4a7CIj0bfhNMUBPgBMFMyQo1iFdU\n"
        + "U6SqU3Go1D+fNGGIVjOS8YlkNP6Q5SMK25f0b344kipIZrhecF4olnjuuITHviRVqH9+11ybX8f9\n"
        + "skXVr3IF1NRfx6YgW/p7cei5BNywXjFs1qnfconBMZj8kPatHlYdExnsrq8H2WB9hWsV+7D8Smxi\n"
        + "Ht8/+im3WrDDEdppJxcgHyhjBFuJrkgOkvBOY2t3gePGwgLkILH+dEmdeUN0vXZb+3KN9dFiAQcL\n"
        + "Q3k/yhAC2cE4FIPwLVHBaVB6btVrM1pyz+IjP2KzRhfp8bwmnQj3VFgEQ3V1KvSgNDCCBV4GCSqG\n"
        + "SIb3DQEHAaCCBU8EggVLMIIFRzCCBUMGCyqGSIb3DQEMCgECoIIE7jCCBOowHAYKKoZIhvcNAQwB\n"
        + "AzAOBAg+s+RbK8CG/AICCAAEggTIihV33XXzGyF6ORFgHMv61TGgjpcgbtV9++/hnipqi4t8jM/Y\n"
        + "WrpGE728GDQ7cjemMKVed6PUmBEkOkBTlfq9CNDdyGSOBCjENfONxleZv8DoEyND7PC1zaE7k9Pk\n"
        + "sUiJsmTf8u1XwDL4CNGxj03vOraPZSZvx4DfQgCN2OYDIeUDtIzBDbaShsHtdFmAex6DM2WFBfnz\n"
        + "wk8UlPrHVCFzbFq1lPq4fhQPh2dHsZewWNdwxRlFx8m75azJlZ17cbaAS6dTmNjTJUrTRsZyqVX+\n"
        + "C7NaVO1R4+d8hgfU8hO497JLCuXxgl3omxSDtu5tgl0T7XR6PaTNQx/opkrF0ApcwYLB5xPrcp3l\n"
        + "tugsGzdtwLz1DIjpfwxVB5UzwYa9npbsTz4Aa5A55JNkJ72z///zPAWNsVMeEwzYD3Y7pt9yzSQN\n"
        + "2S6YCaGNcfT6gTzqHdd0YuKEg0AYp51h0R9C6JKMdGOkrQXuCctMw3JCvNKDrncP8FwJhCDsysQr\n"
        + "WZFc9XZH3/6++Gj8tW+cYv7n6iL59p57WI/Gg4Ux4ECVoRDhkCfDm8ZIilQBNLGihvp1mvlOe41+\n"
        + "WF4Dy0nyisxryEEzQcp37Y2cTkvZWh3B86cdQyVpcXXDxmAaf7aV66F3P4RHsznBdCb3W+BTWkX3\n"
        + "yMqfgBYNvqTiEr1AszncCguOtGQRdjX9Rl97RDQRFusJ6W5Jrz5HwqVLZRNWrOD0ZFhbNKspy5b6\n"
        + "3PVNmLjWuD7w4jnFHo3PtgLYKDMOgz/3ROXji8NOavap38hT3b1KnMbX8tYgVcS1FcowR/SN0ZOH\n"
        + "bUIoQHJDpPZ51FgNtaFosZV9zNKlxuclw1SZCTAhFt4E7s8fjJS7jmGyTDZmDS2dc5kWrzwX0M9W\n"
        + "bYFeQeyOwxORS6USrJgYFBwbVa7uVtk9R1C8zb7J6nYmFHHMQmIAtP1WW+Miwr3BI+puXdUZpPKT\n"
        + "9S3i2MQsLOoEvDe954KQOFgnnoZBa5uZjY8yZeYU+UQ4Rwf/0NBjWNlUJIsVuPp24bHq+dPsAd2/\n"
        + "pOHr8eFOWQax+bgJJK3DfW/7exnIEiDbgQBfkGYUmHqPFzGWTog++ZdJI8BVytO8eDXCiiqwzvUH\n"
        + "mblgO+5+k9nfdhpxxYk6xmZFPsXUQC028XK7XAhg58R+vYUQmjUbZxE3QrRCxFLe30mlHBhGnr4m\n"
        + "51CbCjL4HJ4wyKjR7Rsiy8Hvg70p4LKSwUX+8V8KlM1dGMR4IVRPwvFURYNyK3I+fLgDc4TVATqV\n"
        + "GkVb7HQ6Hf8nxNfw6ia24Xrh/HYVIDrXrYIfk+AxWTKjgjKSHu4Oa9AqZzym71cnTZlChElzvTSQ\n"
        + "ppZV9UdZKYn/SUxEXo82F9Y6w6y0w43sTetobySrazM9KlXJ+gQKyrlzHHdfvoljHGKUjAbwJbUB\n"
        + "lHpTjEBb3V2DC7Ps9wthpapb3B8J1aA5emGq3qzCmhp0gv9cnjTZKmbGdpFPUe/LshJsUH07kaKj\n"
        + "DrKv83giGlCpe6bimsHgLz7CE7CqEtUN5SJ1yDaLXNbFq98owgnPc9QH2A90WYgPNpRD0u5oa6v5\n"
        + "KFtkRv/whEKkDkOclgeudD5hgXs0V/ptm94LMczQPsa7vwoqFPi0y2RMWaNlCIrvMUIwGwYJKoZI\n"
        + "hvcNAQkUMQ4eDAB0AGUAcwB0AEMAQTAjBgkqhkiG9w0BCRUxFgQUG3t8T5ZQALtRW25mOrOCA2wk\n"
        + "IxgwMTAhMAkGBSsOAwIaBQAEFPvBdqEJt6wjeEwEQXktUj5eHpDBBAgXdliCbTotwwICCAA=";

    private final String SUB_CA = "MIITSQIBAzCCEw8GCSqGSIb3DQEHAaCCEwAEghL8MIIS+DCCCScGCSqGSIb3DQEHBqCCCRgwggkU\n"
        + "AgEAMIIJDQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIXt/BVkxG6SoCAggAgIII4MDQebOY\n"
        + "d+NEduVcaFUCSJhkiIyiLGmd9/+IedAsO3IjWdWy8m9ShoZlDZNI+JUpiFRCLTpnF8pwaJcEj+qI\n"
        + "x7D/8yUSVJwDGtEnl2OJV5l7UZbHdAy+TIfS/9ns7dzR7bNNuIDKUINW39Wpfy3E41MIxqCkB1Xf\n"
        + "cOschXa21XC6wUaM5Ul00h+rpLiKlCzAjPxT4017ZIGPnp/4XHjns7u0RkIp3GcN+Ttv/ZIolnZx\n"
        + "wbKTErp6ZwKQ/xrCoqSPPpGInvgf/ZMgRiGjRkh71P1f3TG6dKavCFej3tn0PMdxA7DWbJN8HHDL\n"
        + "nTykUyZPEiP1a8p2flH8cy0zITZvGXDBLZOdVe2ar7RSZdOgzkFPvQVaNuNJJ+DaeyiHOJdZVMeN\n"
        + "BNUlab5+39NSdaWJ5NmbqvZ0Aqlhspx6kYw7ZjCCCOBWJwwACkBn/PWUYE7Hr8xZQu5dKNBjPNk2\n"
        + "4WE8b7pPyXCzIX1ZDWR5prXM8Z1pwiHKddnYeYSRbsHRer2NZIv1EiOrV38quGMsRA/v29Nqs4qj\n"
        + "v7dCH70UCHIhjNqz/MI/Dgk+4mP2EPe7bkmlfIFHdpZMM72VksvXBMFVCT+v7/saFobSNfzqHaV9\n"
        + "eS17h4B8AyjQD0kIM0S2qfWDI8JQVdxdM4BfZEqzYsNiwRTBXOXTX8STcnucyfzEF0jQPz5n2a7I\n"
        + "FOG1bsD7hhFfZW1KpD/CIBvBm66fZDwlrqtIku/KDDY1iiIKeCnmqLVicz4o27y4hVuL5B4qwrw8\n"
        + "k5WCsjHI9BLZ9Sa9X3HhkmaQ50BQMZEXdmf98FiKWypKWW9Cprw/tMh250z0V39r5jIHCrqz8qmX\n"
        + "siDHhU1jnaGhJNTD+nJJEZhRV7X2dqtp9p1BAkwJDFQultmNBWb9dhiQDs8B8ES/ciOn1JMZaKLI\n"
        + "JLKcSUNd8jVvlgyiIowcKuit9OOlfODw5dSiJ9pF2fNzXHv/Sb8eTBk9CoiQJMOHwwyqZbeFiLa+\n"
        + "JZlSfrYXKqs2Y0qt5oKX0x4AD5c4GFAboIEJPh7Nj49r0U/WkM8/uBSVuFW7Yp28B21RwWW1Y4QX\n"
        + "MmhT1QIY0pVGPxrfN0tSzVKBCoOShmzUAxhJ4hMCkq2BfplYG0ngGeCa8vzibYTqNhWwOjYyUVrg\n"
        + "t34agZ/aH3WgB79D5jX+zET5S5gnGfYVYGIBDDKHIAGAQWSIoCID4oGpL+pSguPOzHu+gQ43uPLB\n"
        + "EIzopbRTFbghF+ZIhw0GxkAASVWlxexll5Rpx9idXBnaZapBH7ofynSIFc4OYtlRtlFwD41s0uzV\n"
        + "5v2VlBYEZjVhquWmR+RVMCt6Weu81YzcaFW5WH7trPOMkJp2YN1bbhdj4AsYOT1h8nZjoePtWcld\n"
        + "nqfDpCx9DHttDmPd2QdomDwv/Hup1S2EV24BSp76g5Ggc6o8DL0MdI16Zmi7Z7jSP3OjdzwL6Mbd\n"
        + "1bcFtvpydyKSviyCW7CesXLaHhMRbKYo7GDc9sNBnpN9SHQb3arOFQT4Wk3DxeBD/W3+Rrk9xfJ0\n"
        + "CWdyd3dQLzU/ir3Uu1IcPzIYpPyR3v0iHbnxt1ot6EiFnRw6ArekbFjP4jQ+SpSllsWJ6158E9Qp\n"
        + "3QNC8rHOwHZvgW1hbt9psy3Q+9FjMUjpgvnOgLBSL0p6NhFYkN2TAwLDLyugrv1ZJQeHugKRWRXR\n"
        + "vppQfA0CK9MSIV3C2/SJp5SrGPmbTVeik/xSxOLGHunh8+EGDOWx1Zl12t6C5y/lTf1I5xpKiZbX\n"
        + "DlXMB79VzQskWUGFbvNhAcT0ma2T8bvAurc1+Wy5XgRfW6/VBtdmrnnhRBl2pbeoTcLuxvoJx4OS\n"
        + "sO2OHynGGbzWPL5zwNmhJrGLGIDTql4Rb30niIXbm66YbT3PNg1mFWZrMR9PZC2iUSdYbJo9W8ui\n"
        + "MMBv55/9mzGQ4p0ebN7vypoBfACFuwrVy6HILLkaBzkPLJJSGDUD5hRhfYy+aYjXUMHHKXLJsBI5\n"
        + "Hg2Sgyen4Q2Nek93wQsxXOfaSPfkcLg/M8FgnVNRWKtEKaiBb2/vV08uMvqWCg7sBSdvboHGNwda\n"
        + "pNR74v6bmjMTW/cCrCByaxuNM7oaE6hKI0qkeckynO1vo5tvjalvs30B2AKeJ6VJGh4cBnoCgEHY\n"
        + "CuaATE6MMf0o+hEarGoiI5kX/aVZgx6y6xME3jMh0qxR6Pp+LHOMRlhPZSMKBUSjWSdHYJWjRit1\n"
        + "N1Z1jIvepUqGjxudP7ng0o11XaGdPiqz97mnuv/WseRCXo1WpIjNkU6kYhkAEUf5svXId1FoDViM\n"
        + "KnoKmkSftm5KSE5Knp+FlLWaKUzcIMh1HJO/EZl8tJZq+IipdGCiyuNYRJ6o3pCcZ7rYeA/bxJo+\n"
        + "z7ZU+y1RlE2oIfOPc4r5a2ZMEUy4XW2DPV5EFO50GGP+UqtvMuM8uR0y+Ta/cBFbRF0Gfn9m5rJy\n"
        + "1GMRmJiO3YiBGikIXhrE/94yqaAzoAjgKQYOGAPEx5U54nwwfNjihelFejVNBOM40mXgoPTYnJUJ\n"
        + "RYEwyfCEfg3D/XqT2VAVNk3AGaqo4dEwPYhTk5u+k0zH2ovb+KH4/+dujZLlEuV3sERJdW6X59I/\n"
        + "xckqDIpe13cPlMSjF1uFlVz++i8PychLvK8dSqvXWXdUs+9xbldELq4HXNh5819OJkIHwavBhfke\n"
        + "MOL00VObTymJsiU1lBq6RVR6r6yccsfy6XgHgmxRyXtz/Rky0M7sdnqEplBaecd2Vt3RKxMzoVn5\n"
        + "KsVipivTXCakrs3zU+2/HtIsS6utPAHvx3iuz28bj4onllaQOB6O9eUj6phEUTtSwep19u/MRyRC\n"
        + "fuccSeZxAqLsn1Zhfr9+hK2L+pw4A3Oi3Gr6SNENP6uVDoBWJVmLOJzlTLPLnzVX/pr3cDf7FmYz\n"
        + "XO8GdzEP6I7AYeTQH5JOVZbTKV4E8cMYgHldD7NadQ6keGQgOUAqkglW9Ee80y05yJTNmzk6qv3w\n"
        + "deuVoHGY1Tp2bBo96KeSZ6N0iaJ1pAqq/YWu1lBn+GcknlXUutGjay5uQikwggnJBgkqhkiG9w0B\n"
        + "BwGgggm6BIIJtjCCCbIwggmuBgsqhkiG9w0BDAoBAqCCCXYwgglyMBwGCiqGSIb3DQEMAQMwDgQI\n"
        + "jR7G7rU/T8ECAggABIIJUNeh0l3K6oM1yLEZW0Eq2GzN0EgZinz70yIIYh1/slnEcBlgUkuTMwi4\n"
        + "yo0jhlYUd2MIIrTIcqXimRBHYBkmmmkgXv4lNnDJ1P5QmjLkvGsoiruEJ0RySy2wFZX+dzOSbHpv\n"
        + "7RRa4a48e1aG9eRWtrUWcm20SS5J3MD8gY/wMFMhPdNzazKIvXO1ugudE1ePNXDYJVOtrB6Z4dnR\n"
        + "yoLLDQW2tGY1gkEAJagxi+0ZT6KAdeYZZdtXZKDCc07kE9C0yBMJotAf6ETeHfyRW5+cO+H3suOL\n"
        + "iXSExojQlTMakwZsVjT+FXuCgYOpdaF0U+8C5LvkAW2nNd1euKqj5BBa+n8ybV9ZvvR4LkFeXgSA\n"
        + "gU1oz038nTcKQbsAYBSm5urursADA1mIzBgys739klyrbxZztx2fFbAc0CkrzU7oU9u93oS45pu6\n"
        + "w4vJ5zjLsKEKM4ExSMancmGrmRhfmKMAnY17SLllqhhHtRifrmgEDvoyUUpnHggiRoLRjUg3j5gN\n"
        + "Y45fStCnEb29y8dN2/Te20a+2kO/+BajEcZFZYqlddHkEGWTLM50c+CkWplZ0TYxbpa9eJkiXdnZ\n"
        + "+FyRz/AaXgqc6YaxHRU9OMVy/7fRQUI4IgYZ45UX6J9s1B5lC1XuYjj2Z9Zy1znLwgY+GPEDeTS8\n"
        + "YCIx42nJI/HeyrYDvwxp8flXR5US0yOA78dgU5tK9D0IIPZgo+WqCobnt6IR4MaaTLJl0CB4QJn/\n"
        + "BR2aOWgxWoJVN8zHR9ICKwfTU4C5aTVHAGWWxMuLjulncrivggPQb9h+QcP3EIAgGB8R7IKojKNU\n"
        + "bvTN81OE81jOCU3jDfj26l5FhzJTZuGC+9Sfze+6iSkix5V97XlzaGN255jhcFhYDD6W7U8Zf1n4\n"
        + "mFylczzenh6TAyFgvDZ7cWmiJRROPifbVU3hr+ahivZl4VhNo3i5UXG6cdYhI1m40+p9is5KII6n\n"
        + "dV9AXZ085Ecf2LXqbSvaGOtl7MeXnUhxRiAM6pN0fGLW4BCRLFir3JWOO/MV9jJe4aUsU9SpwDT1\n"
        + "Qf5GWU+KKwwX6wqO8fa7+T2MfsqLCBAV7jmA7kuHIQ7e+0HJ0T9f3f/XG64v9Y6hQH7Nb7Sl6oVb\n"
        + "SAxZTgskATsoezD8TgHcfjH0uI5yPQJzKVfSTMxgIArGwtgtaaULmPbCLaDooSHYqB8XNghXKqMB\n"
        + "Rm76haLlojAaX3R3t8kLL3661iiHb+9Q/6qvXeVCfqxVQ+4XoTZ2F/17ZCuxNJMkT4jqtY90gkWr\n"
        + "+F4D5+SA4zxGg0dzbEGufLsYUnFCIPW/c/kR7BC7Y+A1PJkkShFCmUyAFKHJZNG2oIEPR5xGifEQ\n"
        + "q+PCHRqY9V/zL02PCsFiC5q0c8lYtIlu92WST+QYIQSsPAkOvITiyLphjz8soEPReZn30yZzmUAu\n"
        + "RRVN7bKzwsdSPVK+nhBFI+HSeVcD9/qm0nWk/jZprlGexpIqbA4lWvJz95Q4+Cnn7Qp5XhCgfXn+\n"
        + "eeBFBGZuiaTPf/a4ICX/GQs4EL78Z9CnM1WN1V5R41JY5357VeV0PjXuGlK2MFpQNW5Wf62MGzGN\n"
        + "Jm91RAiLob9S/ecsX+YV2Ypiar8r0CnWywGYdzGUXLKJqHFzY2FEBDZlB5/5T97Go1GZMWwvF4R1\n"
        + "5pCVfVUB2Jt25DKlQf2bYv0iMVMlDULGs2/f3PYBa82rJT/MwC2INXQhSfCxgFUSUnSiZ0YYCatN\n"
        + "B6mUg7srI3vJ/DVzQRucLTgt6FEEbrsmS45S8i4r8hH+iBMBTyKBerkmk+RTx4HBwgpSbzgIavwC\n"
        + "7r6aSgzBTf69BaMIwLVT6agymMSXn2B+RnpHljfrDRszCta5oFMPWm4EvVVPgOwgIam20dgqBRZF\n"
        + "daECKb3Qys6M/Qv2iWZ2J/+dz7y5eoBTxRE/JSL2V1nTyc4tOyjoWXs/IFqy4c3KB12fiwWcRjqj\n"
        + "3bxvJYyHOSNyzB1B9Ql1Lu10WBF2WCf3DKby+rrKpAGPluQM7ch05VlrxsrmaD6itzs/PTlUSnLT\n"
        + "rwC7dhXU20ntqwBEvgZHxxok3PNRk+2L5s+E+x9g60OvMWNfXsMrxcYvbLHXXriZ4daJLmcy46sH\n"
        + "OjfA/GlintiFbNt+bQJ8RO7icJcg7a+Yo5wCvgQ/FEODwZStfljGPH31BfPcGn7GaXKRafHq5HsG\n"
        + "RgqKWSk7eP2Vp3NgAN5+Qgo1PWNzHMST7zp5tey07e8Y2dotVuoc8Wk8E2mWEhEweX2KoPLARUHy\n"
        + "4NiMi37xHwLhID+XAE6q9YRegj8s1cdrPOzgdILoQddmITQR5RKgV2aXDH71pDUo2wTP1HRWP6Nm\n"
        + "0xxVEtKFqjNPDplKkw6hnADpwqX/V/AHBiYE4xhCSHdZhNiZHocQHkcVc9L1uCAvyr2Wjd9fjC5x\n"
        + "tb73sJy4HWHOpcaH82X4n9+j4nJ0uEv7Vvu4jSYayv+bfhGOZ3BdcC0kCmJXRiftx2OQ1sBgODWa\n"
        + "7KsIOpX6fhwuvDYuS0k87B2DSM6NAcDZ+UzQ5hwo3Eagg/upqDwV91+pYHQP6i2TWFCbu+XZL24x\n"
        + "1e7ZM887IdomkDi4lWcyQ9v7aORfYRsNn8eQ8nb/2jIRpwLrz0DzspJYe4EZn9HaSuGG8xeOhETw\n"
        + "ETXZOPTUVahcleDOTFW/wjTZNUqNeH5AYHkAe/igICustzlZMYAB14dLgvqf/caaxf52eYwAwiLM\n"
        + "u6qkQNHk2NSEia9A4hDptzw95dZHgXEmJxrrvLwv6I9MzNhc70oISIJDAIrxfn3UvwUZmOMNvyVA\n"
        + "3DYOwqTQ3YXgIHrO88vU6fszpo818nh2apKHEAkeXPOUp804C5C8hq6ppY+lYIGobOsqDkKCv7e0\n"
        + "dRU9y6PGUAo5gRqWRMWrPH7e8BkejfqHfwUrA0NGU/PHm8HIwI89VlvFW/yKxfo+tdav1GXe6Wm7\n"
        + "pvxmp+EypevEchM22D0deEVI+7U/AVzfgW37LpZEDwpREKRUvHh8QWXnpg8xiIVWxAwW4Ojb5NSj\n"
        + "ZepBKII8ghfarqk+QjfDmISnA766JQ65HeJWZOMP/HhXg27mUysHNx82CZ/FGS0yiPcO8SUrce4R\n"
        + "zjKqcDhqQdyNHYMZlCJ3z5gyCol1MuCLRCq2CoSFiGThBM4TXXXQIq2Bkglna0X9jEvtbJlvmLa3\n"
        + "YcV5s6qYMSUwIwYJKoZIhvcNAQkVMRYEFDBg2krXA8wyIVqmlfXFMEm7tnKTMDEwITAJBgUrDgMC\n"
        + "GgUABBQuO2UYfbg9FsAjXdLfaavdvSp+PwQInFB6tL5MmhsCAggA";

    private SmimeKeys caKeys;
    private SmimeKeys subKeys;
    private ServiceLookup services;

    public TestCAStorage(ServiceLookup services) throws Exception {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.decode(TEST_CA));
        ByteArrayInputStream subInputStream = new ByteArrayInputStream(Base64.decode(SUB_CA));
        caKeys = new KeyImporter(services).readPKCS12File(inputStream, "secret", 0, 0);
        subKeys = new KeyImporter(services).readPKCS12File(subInputStream, "secret", 0, 0);
        this.services = services;
    }

    public SmimeKeys getCaKeys() {
        return caKeys;
    }

    public SmimeKeys getSubCaKeys() {
        return subKeys;
    }

    @Override
    public List<X509Certificate> getCAs(int group, int userId, int cid) throws OXException {
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        String caPem = configService.getProperty(GUARD_SMIME_TEST_CA, userId, cid, null);
        if (caPem != null) {
            try {
                JcaX509CertificateConverter converter = new JcaX509CertificateConverter().setProvider("BC");
                return Collections.singletonList(converter.getCertificate(new X509CertificateHolder(Base64.decode(caPem))));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return Collections.singletonList(caKeys.getCertificate());
    }

    @Override
    public boolean addCertificate(String certString, int group) throws OXException {
        throw new UnsupportedOperationException("Importing certificates into the Test CA storage is not supported.");
    }
}
