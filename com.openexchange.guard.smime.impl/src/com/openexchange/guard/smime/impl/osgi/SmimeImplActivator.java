/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl.osgi;

import org.osgi.service.http.HttpService;
import org.slf4j.Logger;
import com.openexchange.capabilities.CapabilityService;
import com.openexchange.config.ConfigurationService;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.auth.AuthenticationService;
import com.openexchange.guard.auth.AuthenticationServiceRegistry;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.smime.impl.CertificateVerifier;
import com.openexchange.guard.smime.impl.SMIMESigatureVerificationService;
import com.openexchange.guard.smime.impl.SmimeKeyServiceImpl;
import com.openexchange.guard.smime.impl.SmimeMimeEncryptionService;
import com.openexchange.guard.smime.impl.authentication.SmimeAuthenticationService;
import com.openexchange.guard.smime.impl.passwordManagementServices.SmimePasswordManagementService;
import com.openexchange.guard.smime.impl.testService.TestCAStorage;
import com.openexchange.guard.smime.impl.testService.TestServlet;
import com.openexchange.guard.smime.storage.CaCertStorage;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.osgi.RankingAwareNearRegistryServiceTracker;
import com.openexchange.user.UserService;

/**
 * {@link SmimeImplActivator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimeImplActivator extends HousekeepingActivator {

    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(SmimeImplActivator.class);

    private static final String GUARD_SMIME_TEST = "com.openexchange.smime.test";
    private static final String SMIME_TEST_PATH = "/oxguard/smime";

    @Override
    protected Class<?>[] getNeededServices() {
        //@formatter:off
        return new Class<?>[] { HttpService.class, 
            ConfigurationService.class, 
            ResponseHandler.class, 
            CryptoManager.class, 
            GuardConfigurationService.class, 
            AuthenticationServiceRegistry.class, 
            UserService.class };
        //@formatter:on
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        //@formatter:off
        return new Class<?>[] { GuardAntiAbuseService.class, 
            GuardCipherFactoryService.class, 
            GuardSessionService.class, 
            CapabilityService.class, 
            MailCreatorService.class, 
            GuardNotificationService.class };
        //@formatter:on
    }

    @Override
    protected void startBundle() throws Exception {
        final SMIMESigatureVerificationService signatureVerificationService = new SMIMESigatureVerificationService(this);
        trackService(CaCertStorage.class);
        trackService(GuardRatifierService.class);
        trackService(MasterKeyService.class);
        registerService(AuthenticationService.class, new SmimeAuthenticationService(this));
        registerService(MimeSignatureVerificationService.class, signatureVerificationService);
        registerService(PasswordManagementService.class, new SmimePasswordManagementService(this));
        registerService(MimeEncryptionService.class, new SmimeMimeEncryptionService(signatureVerificationService, this));
        registerService(SmimeKeyService.class, new SmimeKeyServiceImpl(this));
        registerService(CertificateService.class, new CertificateVerifier(this));

        RankingAwareNearRegistryServiceTracker<CaCertStorage> caStorageTracker = new RankingAwareNearRegistryServiceTracker<CaCertStorage>(context, CaCertStorage.class);
        rememberTracker(caStorageTracker);
        ConfigurationService guardConfig = this.getService(ConfigurationService.class);
        if (guardConfig.getBoolProperty(GUARD_SMIME_TEST, false)) {
            try {
                LOG.error("STARTING GUARD SMIME TEST SERVICE. THIS SHOULD NOT BE ENABLED FOR PRODUCTION");
                HttpService httpService = this.getService(HttpService.class);
                TestCAStorage testCAStorage = new TestCAStorage(this);
                httpService.registerServlet(SMIME_TEST_PATH, new TestServlet(this, testCAStorage), null, null);
                // Register our test Certificate Authority
                registerService(CaCertStorage.class, testCAStorage, 10000);
            } catch (Exception e) {
                LOG.error("Unable to register S/MIME Test-Servlet: ", e.getMessage());
            }
        }

        openTrackers();
    }

}
