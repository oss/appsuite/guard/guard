/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.impl;

import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.Random;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;

/**
 * {@link SmimePrivateKeyFactory}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
class SmimePrivateKeyFactory {

    private static final Random RANDOM = new SecureRandom();

    /**
     * Create salt
     *
     * @return The salt
     */
    private String createSalt() {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }

    /**
     * Creates a new instance of {@link SmimePrivateKeys} from a given {@link X509Certificate}
     *
     * @param cert Certificate to use for the new key
     * @param key PrivateKey The {@link PrivateKey} related to the given certificate
     * @param password Password to use in order to protect the private keys
     * @param email Email address associated with the key
     * @param masterKeyIndex The master key index to use
     * @param current If the key is to be marked "current"
     * @return The new created instance of {@link SmimePrivateKeys}
     * @throws OXException
     */
    public SmimePrivateKeys createKey(X509Certificate cert, PrivateKey key, String password, String email, int masterKeyIndex, boolean current) throws OXException {
        String salt = createSalt();
        String recovery = null;
        String encryptedPrivateKeyData = SmimeCryptoKeyUtil.encryptPrivateKey(key, password, salt);

        //@formatter:off
        return new SmimePrivateKeys(cert.getSerialNumber().toString(), 
                encryptedPrivateKeyData, 
                salt, 
                recovery, 
                masterKeyIndex, 
                email, 
                null, 
                current);
        //@formatter:on
    }
}
