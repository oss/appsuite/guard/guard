/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.session.internal;

class GuardSessionSql {

    static final String SELECT_STMT = "SELECT sessionid, token, date, userid, cid FROM oxguard_session WHERE sessionid = ?;";
    static final String INSERT_STMT = "INSERT INTO oxguard_session (sessionid, token, date, userid, cid) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE sessionid = ?, token = ?, date = ?;";
    static final String DELETE_BY_ID_STMT = "DELETE FROM oxguard_session WHERE sessionid = ?;";
    static final String DELETE_OLD_STMT = "DELETE FROM oxguard_session WHERE DATE < NOW() - INTERVAL ? SECOND;";
}
