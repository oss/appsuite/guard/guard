/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.guest.metadata.storage.impl.GuardGuestEMailMetadataServiceImpl;
import com.openexchange.guard.guestupgrade.storage.GuestUpgradeStorageService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuestMetadataActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuestMetadataActivator extends HousekeepingActivator {

    private final static Logger logger = LoggerFactory.getLogger(GuestMetadataActivator.class);

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class[] {GuardDatabaseService.class, GuardShardingService.class, GuestUpgradeStorageService.class};
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        logger.info("Stating bundle {}", context.getBundle().getSymbolicName());
        registerService(GuardGuestEMailMetadataService.class, new GuardGuestEMailMetadataServiceImpl(
            getService(GuardDatabaseService.class),
            getService(GuardShardingService.class),
            getService(GuestUpgradeStorageService.class)));
    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    public void stopBundle() throws Exception {
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(GuardGuestEMailMetadataService.class);
        super.stopBundle();
    }
}
