/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.metadata.storage.impl;

/**
 * {@link GuestEmailMetaDataSql}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuestEmailMetaDataSql {

    public static final String SELECT_BY_ID                  = "SELECT itemID , version , folderId , contentType , mimeType, mailFrom, mailTo, mailCc, mailBcc, subject, messageFlags, size, sentDate, receivedDate, colorLabel FROM GuestEmailMetaData WHERE itemID = ?;";
    public static final String SELECT_BY_FOLDER              = "SELECT itemID , version , folderId , contentType , mimeType, mailFrom, mailTo, mailCc, mailBcc, subject, messageFlags, size, sentDate, receivedDate, colorLabel FROM GuestEmailMetaData WHERE ownerId = ? AND ownerCid= ? AND folderId = ?;";
    public static final String INSERT                        = "INSERT INTO GuestEmailMetaData (itemID , version , ownerId, ownerCid, folderId , contentType , mimeType, mailFrom, mailTo, mailCc, mailBcc, subject, messageFlags, size, sentDate, receivedDate, colorLabel) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    public static final String UPDATE_MESSAGE_FLAGS          = "UPDATE GuestEmailMetaData SET messageFlags = ? WHERE itemId = ?;";
    public static final String UPDATE_COLOR_LABEL            = "UPDATE GuestEmailMetaData SET colorLabel = ? where itemId = ?;";
    public static final String UPDATE_FOLDER_ID              = "UPDATE GuestEmailMetaData SET folderId = ? where itemId = ?;";
    public static final String UPDATE_ALLL_FOLDERS_FOR_GUEST = "UPDATE GuestEmailMetaData SET folderId = ? where folderId = ? AND ownerId = ? AND ownerCid = ?;";
    public static final String DELETE_BY_ID                  = "DELETE FROM GuestEmailMetaData where itemID = ?";
    public static final String DELETE_BY_USER                = "DELETE FROM GuestEmailMetaData where ownerId = ? and ownerCid = ?";
}
