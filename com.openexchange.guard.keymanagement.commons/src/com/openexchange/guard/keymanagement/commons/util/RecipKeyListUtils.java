/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.util;

import java.util.List;
import com.openexchange.guard.keymanagement.commons.RecipKey;

/**
 * {@link RecipKeyListUtils}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class RecipKeyListUtils {

    /**
     * Extracts all languages from the given {@link RecipKey} list
     *
     * @param recipKeys The recipients
     * @return The languages of the given recipients
     */
    public static String[] getLanguagesFor(List<RecipKey> recipKeys) {
        return recipKeys.stream().map(recip -> recip.getLang()).toArray(String[]::new);
    }

    /**
     * Checks if all recipients are local Guard users. I.E. confirms keys were
     * made by Guard
     *
     * @param recipKeys
     * @return True if all Guard keys. False if user uploaded/remote/hkp/etc
     */
    public static boolean areLocal(List<RecipKey> recipKeys) {
        return recipKeys.stream().allMatch(recip -> "GUARD_KEY_PAIR".equals(recip.getKeySource().getName()));
    }
}
