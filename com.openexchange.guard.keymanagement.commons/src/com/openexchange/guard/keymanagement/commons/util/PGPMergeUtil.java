/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.util;

import com.openexchange.pgp.keys.common.ModifyingPGPPublicKeyRing;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPUserAttributeSubpacketVector;

/**
 * {@link PGPMergeUtil} contains helper methods for merging PGP keys.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class PGPMergeUtil {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PGPMergeUtil.class);

    private static boolean hasSig(PGPSignature sig, ArrayList<PGPSignature> sigs) throws PGPException {
        for (int i = 0; i < sigs.size(); i++) {
            if (Arrays.equals(sig.getSignature(), sigs.get(i).getSignature())) {
                return (true);
            }
        }
        return (false);
    }

    /**
     * Merges two instances of {@link PGPPublicKey}. <code>newKey</code> will be merged into <code>existingKey</code>.
     *
     * @param newKey The {@link PGPPPublicKey} which will be merged into <code>existingKey</code>
     * @param existingKey The {@link PGPPublicKey} where <code>newKey</code> will get merged into.
     * @return true, if the merge was performed, false if both keys have equals key id and the merge was not performed.
     * @throws PGPException
     */
    private static PGPPublicKey mergeKeys(PGPPublicKey newKey, PGPPublicKey existingKey) throws PGPException {
        PGPPublicKey returnKey = newKey;

        if (existingKey.getKeyID() != newKey.getKeyID()) {
            return null;
        }

        ArrayList<PGPSignature> oldsigs = new ArrayList<PGPSignature>();
        Iterator oldsigI = existingKey.getSignatures();
        while (oldsigI.hasNext()) {
            oldsigs.add((PGPSignature) oldsigI.next());
        }

        Iterator<String> userids = existingKey.getUserIDs();
        // Loop through userids of the old certificate and see if there are any new signatures
        while (userids.hasNext()) {
            String uid = userids.next();
            Iterator<PGPSignature> newsigs = newKey.getSignaturesForID(uid);
            if (newsigs != null) {
                while (newsigs.hasNext()) {
                    PGPSignature newsig = newsigs.next();
                    if (!hasSig(newsig, oldsigs)) {
                        returnKey = PGPPublicKey.addCertification(existingKey, uid, newsig);
                    }
                }
            }

        }
        userids = newKey.getUserIDs();
        // Loop through userids of the NEW certificate and see if there are any new signatures
        while (userids.hasNext()) {
            String uid = userids.next();
            Iterator<PGPSignature> newsigs = newKey.getSignaturesForID(uid);
            if (newsigs != null) {
                while (newsigs.hasNext()) {
                    PGPSignature newsig = newsigs.next();
                    if (!hasSig(newsig, oldsigs)) {
                        returnKey = PGPPublicKey.addCertification(existingKey, uid, newsig);
                    }
                }
            }
        }
        // Loop through user attributes and see if any new signatures
        Iterator<PGPUserAttributeSubpacketVector> vectors = existingKey.getUserAttributes();
        while (vectors.hasNext()) {
            PGPUserAttributeSubpacketVector vector = vectors.next();
            Iterator<PGPSignature> newsigs = newKey.getSignaturesForUserAttribute(vector);
            if (newsigs != null) {
                while (newsigs.hasNext()) {
                    PGPSignature newsig = newsigs.next();
                    if (!hasSig(newsig, oldsigs)) {
                        returnKey = PGPPublicKey.addCertification(existingKey, vector, newsig);
                    }
                }
            }
        }
        // Loop through user attributes and see if any new signatures
        vectors = newKey.getUserAttributes();
        while (vectors.hasNext()) {
            PGPUserAttributeSubpacketVector vector = vectors.next();
            Iterator<PGPSignature> newsigs = newKey.getSignaturesForUserAttribute(vector);
            if (newsigs != null) {
                while (newsigs.hasNext()) {
                    PGPSignature newsig = newsigs.next();
                    if (!hasSig(newsig, oldsigs)) {
                        returnKey = PGPPublicKey.addCertification(existingKey, vector, newsig);
                    }
                }
            }
        }

        // Check for revoke signatures
        Iterator<PGPSignature> newsigs = newKey.getSignaturesOfType(PGPSignature.KEY_REVOCATION);
        while (newsigs.hasNext()) {
            PGPSignature newsig = newsigs.next();
            if (!hasSig(newsig, oldsigs)) {
                returnKey = PGPPublicKey.addCertification(existingKey, newsig);
            }
        }
        return returnKey;
    }

    public static PGPPublicKeyRing mergeKeyRings(PGPPublicKeyRing firstRing, PGPPublicKeyRing secondRing) {

        ModifyingPGPPublicKeyRing newRing = new ModifyingPGPPublicKeyRing(secondRing);
        try {
            Iterator<PGPPublicKey> keys = firstRing.getPublicKeys();
            while (keys.hasNext()) {
                PGPPublicKey keyToImport = keys.next();
                long importID = keyToImport.getKeyID();
                PGPPublicKey toMerge = secondRing.getPublicKey(importID);
                if (toMerge != null) {
                    newRing.removePublicKey(toMerge);
                    toMerge = mergeKeys(keyToImport, toMerge);
                    if (toMerge == null) {
                        return null;
                    }
                    newRing.addPublicKey(toMerge);
                } else {
                    newRing.addPublicKey(keyToImport);
                }
            }
        } catch (Exception e) {
            logger.error("Problem merging duplicate public keys, ", e);
        }
        return newRing.getRing();
    }
}
