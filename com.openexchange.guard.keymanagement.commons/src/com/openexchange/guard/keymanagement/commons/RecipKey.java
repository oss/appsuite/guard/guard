/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import java.security.PublicKey;
import java.util.Locale;
import javax.mail.Message.RecipientType;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.google.gson.JsonObject;
import com.openexchange.guard.common.util.LocaleUtil;
import com.openexchange.guard.keymanagement.commons.trust.GuardKeySources;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

public class RecipKey {

    private int userid;
    private int cid;
    private int senderUserId;
    private int senderCid;
    private PublicKey pubkey;
    private boolean guest;
    private String newGuestPass;
    private String name;
    private RecipientType type;
    private String lang;
    private String email;
    private boolean pgp;
    private boolean expired;
    private boolean inline;
    private PGPPublicKeyRing pgpPublicKeyRing;
    private PGPSecretKeyRing pgpSecretKeyRing;
    private JsonObject settings;
    private final KeySource keySource;
    private boolean isNewCreated;
    private boolean encryptIncoming;

    public RecipKey() {
        this(GuardKeySources.GUARD);
    }

    public RecipKey(KeySource keySource) {
        this.keySource = keySource;
        setSettings(new JsonObject());
    }

    public RecipKey(GuardKeys key) {
        this(key, GuardKeySources.GUARD);
    }

    public RecipKey(GuardKeys key, KeySource keySource) {
        this.keySource = keySource;
        setGuest(key.getContextid() < 0);
        setUserid(key.getUserid());
        setInline(key.isInline());
        setPubkey(key.getPublicKey());
        setNewGuestPass(null);
        setPgp(true);
        setPGPPublicKeyRing(key.getPGPPublicKeyRing());
        setEmail(key.getEmail());
        setCid(key.getContextid());
        setExpired(PGPKeysUtil.checkAllExpired(key.getPGPPublicKeyRing()));
        setSettings(key.getSettings());
        setLang(key.getLanguage());
        setEncryptIncoming(key.getEncryptIncoming());
        pgpSecretKeyRing = key.getPGPSecretKeyRing();
    }

    /**
     * A remote key is a key which does not belong to an OX user but to an external recipient
     *
     * @return Whether the key belongs to an external PGP user or not
     */
    public boolean isRemoteKey() {
        return getCid() == 0;
    }

    /**
     * Check if the user settings require inline
     *
     * @param settings
     * @return
     */
    public boolean reqInline() {
        if (isInline()) {
            return (true);
        }
        if (getSettings() == null) {
            return (false);
        }
        if (getSettings().has("inline")) {
            return (getSettings().get("inline").getAsBoolean());
        }
        return (false);
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public PublicKey getPubkey() {
        return pubkey;
    }

    public void setPubkey(PublicKey pubkey) {
        this.pubkey = pubkey;
    }

    public boolean isGuest() {
        return guest;
    }

    public void setGuest(boolean guest) {
        this.guest = guest;
    }

    public String getNewGuestPass() {
        return newGuestPass;
    }

    public void setNewGuestPass(String newGuestPass) {
        this.newGuestPass = newGuestPass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecipientType getType() {
        return type;
    }

    public void setType(RecipientType type) {
        this.type = type;
    }

    public String getLang() {
        return lang;
    }

    public Locale getLocale() {
        return LocaleUtil.getLocalFor(lang);
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPgp() {
        return pgp;
    }

    public void setPgp(boolean pgp) {
        this.pgp = pgp;
    }

    public boolean isExpired() {
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public boolean isInline() {
        return inline;
    }

    public void setInline(boolean inline) {
        this.inline = inline;
    }

    public void setPGPPublicKeyRing(PGPPublicKeyRing ring) {
        this.pgpPublicKeyRing = ring;
    }
    public PGPPublicKeyRing getPGPPublicKeyRing() {
        return pgpPublicKeyRing;
    }

    public PGPSecretKeyRing getPGPSecretKeyRing() {
        return pgpSecretKeyRing;
    }

    public JsonObject getSettings() {
        return settings;
    }

    public void setSettings(JsonObject settings) {
        this.settings = settings;
    }

    public KeySource getKeySource() {
        return this.keySource;
    }

    /**
     * Returns if Guard treats this key as "new" and _will_ create new key material for this recipient during next encryption.
     *
     * @return True, if guard treats this key as new and will create new key material, False otherwise
     * @see {@link RecipKey#isNewCreated} to determine if guard already created new key material.
     */
    public boolean isNewKey() {
        if ((this.pgpPublicKeyRing == null) && (this.newGuestPass == null) && (this.pubkey == null)) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if Guard created new key material.
     *
     * @return True, if guard created a new key, false otherwise
     * @see {@link RecipKey#isNewKey()} to determine if guard will create new key material for this recipient
     */
    public boolean isNewCreated() {
        return isNewCreated;
    }

    /**
     * Flags that Guard created new Key material
     *
     */
    public void setNewCreated() {
        this.isNewCreated = true;
    }

    /**
     * Gets the public key from the key ring which is suitable for encrypting data.
     *
     * @return The key which can be used for encrypting data
     */
    public PGPPublicKey getEncryptionKey() {
        if (pgpPublicKeyRing != null) {
            return PGPKeysUtil.getEncryptionKey(pgpPublicKeyRing);
        }
        return null;
    }

    public void setEncryptIncoming (boolean encrypt) {
        this.encryptIncoming = encrypt;
    }

    public boolean getEncryptIncoming () {
        return this.encryptIncoming;
    }

    public void setSenderCid(int cid) {
        this.senderCid = cid;
    }

    public int getSenderCid() {
        return this.senderCid;
    }

    public void setSenderUserId(int userId) {
        this.senderUserId = userId;
    }

    public int getSenderUserId() {
        return this.senderUserId;
    }
}
