/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 *
 * {@link KeysExceptionCodes} contains general exception codes related to keys
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public enum KeysExceptionCodes implements DisplayableOXExceptionCode {

    IO_ERROR("An I/O error occurred: %1$s", GuardKeysExceptionCodes.IO_ERROR, Category.CATEGORY_ERROR, 1),

    PGP_ERROR("A PGP error occurred: %1$s", GuardKeysExceptionCodes.PGP_ERROR, Category.CATEGORY_ERROR, 2),

    DB_ERROR("Unexpected database error: \"%1$s\"", GuardKeysExceptionCodes.DB_ERROR, Category.CATEGORY_ERROR, 3),

    SQL_ERROR("An SQL error occurred: %1$s", GuardKeysExceptionCodes.SQL_ERROR, CATEGORY_ERROR, 4),

    INVALID_KEY_SPECIFICATION("The provided key specification is invalid.", GuardKeysExceptionCodes.INVALID_KEY_SPECIFICATION, CATEGORY_ERROR, 5),

    NO_SUCH_ALGORITHM("The requested algorithm in not available.", GuardKeysExceptionCodes.NO_SUCH_ALGORITHM, CATEGORY_ERROR, 6),

    NO_RECOVERY_ERROR("Failed to reset the password because recovery is disabled.", GuardKeysExceptionCodes.NO_RECOVERY_ERROR, CATEGORY_ERROR, 7),

    NO_KEY_ERROR("No valid key found for email %1$s.", GuardKeysExceptionCodes.NO_KEY_ERROR, CATEGORY_ERROR, 8),

    PREVIOUSLY_CHANGED("Current password required for password change. Previously Changed.", GuardKeysExceptionCodes.PREVIOUSLY_CHANGED, CATEGORY_ERROR, 9),

    REVOKE_FAILED("Unable to revoke the keys: %1$s", GuardKeysExceptionCodes.REVOKE_FAILED, CATEGORY_ERROR, 10),

    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    private KeysExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private KeysExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-KEYS";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
