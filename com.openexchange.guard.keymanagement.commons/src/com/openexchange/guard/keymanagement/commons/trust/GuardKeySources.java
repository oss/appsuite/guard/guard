/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons.trust;

import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link GuardKeySources} defines a set of default Guard key sources ({@link KeySource}).
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GuardKeySources {

    /**
     * An Unknown source
     */
    public static KeySource UNKNOWN;

    /**
     * The {@link KeySource} for a regular user's key pair.
     */
    public static KeySource GUARD;

    /**
     * The {@link KeySource} for a public key uploaded by a user to {@link OGPGPKeysStorage}.
     */
    public static KeySource GUARD_USER_UPLOADED;

    /**
     * Key was uploaded by another user in the same context ID and shared
     */
    public static KeySource GUARD_USER_SHARED;

    /**
     * Key created from incoming AutoCrypt Header
     */
    public static KeySource AUTOCRYPT_KEY;

    /**
     * Key created from incoming AutoCrypt Header AND manually verified by user
     */
    public static KeySource AUTOCRYPT_KEY_USER_VERIFIED;

    /**
     * Initializes the key source values
     * @param keySourceFactory
     */
    public static void initialize(KeySourceFactory keySourceFactory) {
        UNKNOWN = keySourceFactory.create("UNKNOWN", null);
        GUARD = keySourceFactory.create("GUARD_KEY_PAIR", GuardProperty.trustLevelGuard);
        GUARD_USER_UPLOADED = keySourceFactory.create("GUARD_USER_UPLOADED",GuardProperty.trustLevelGuardUserUploaded);
        GUARD_USER_SHARED = keySourceFactory.create("GUARD_USER_SHARED", GuardProperty.trustLevelGuardUserShared);
        AUTOCRYPT_KEY = keySourceFactory.create("AUTOCRYPT_KEY", GuardProperty.trustLevelAutoCrypt);
        AUTOCRYPT_KEY_USER_VERIFIED = keySourceFactory.create("AUTOCRYPT_KEY_USER_VERIFIED", GuardProperty.trustLevelAutoCryptUserVerified);

    }
}
