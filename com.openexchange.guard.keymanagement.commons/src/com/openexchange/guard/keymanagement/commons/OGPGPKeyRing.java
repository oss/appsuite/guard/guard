/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.commons;

import org.bouncycastle.openpgp.PGPPublicKeyRing;

/**
 * {@link OGPGPKeyRing}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class OGPGPKeyRing {

    private PGPPublicKeyRing keyRing;
    private int shareLevel;  // If the user is sharing the key
    private boolean inline;
    private final String ids;
    private boolean guardKey;
    private boolean owned; //true, if this key is owned by the user/caller, false if it was shared by another user
    private boolean verified;  // user verified key, used in autocrypt

    /**
     * Initializes a new {@link OGPGPKeyRing}.
     *
     * @param ids The uniqe IDs string of the {@link OGPGPKeyRing}
     * @param publicKeyRing The PGP key ring
     * @param isInline Whether the key is marked as "inline" or not
     * @param shareLevel The share level of the key
     * @param owned Whether this key is owned by the caller/creater/context requesting it
     */
    public OGPGPKeyRing(String ids, PGPPublicKeyRing publicKeyRing, boolean isInline, int shareLevel, boolean owned) {
        this.ids = ids;
        this.keyRing = publicKeyRing;
        this.inline = isInline;
        this.shareLevel = shareLevel;
        this.guardKey = false;
        this.owned = owned;
    }

    public OGPGPKeyRing(String ids, PGPPublicKeyRing publicKeyRing, boolean guardKey) {
        this.ids = ids;
        this.keyRing = publicKeyRing;
        this.inline = false;
        this.shareLevel = 0;
        this.owned = true;
        this.guardKey = guardKey;
    }

    public OGPGPKeyRing(String ids, PGPPublicKeyRing publicKeyRing, boolean guardKey, boolean verified) {
        this.ids = ids;
        this.keyRing = publicKeyRing;
        this.inline = false;
        this.shareLevel = 0;
        this.owned = true;
        this.guardKey = guardKey;
        this.verified = verified;
    }

    /**
     * Gets the ids
     *
     * @return The ids
     */
    public String getIds() {
        return ids;
    }

    /**
     * Gets the {@link PGPPublicKeyRing}
     *
     * @return The {@link PGPPublicKeyRing}
     */
    public PGPPublicKeyRing getKeyRing() {
        return keyRing;
    }

    /**
     * Sets the {@link PGPPublicKeyRing}
     *
     * @param keyRing The {@link PGPPublicKeyRing} to set
     */
    public void setKeyRing(PGPPublicKeyRing keyRing) {
        this.keyRing = keyRing;
    }

    /**
     * Gets the share level
     *
     * @return The share level
     */
    public int getShareLevel() {
        return shareLevel;
    }

    /**
     * Sets the share level
     *
     * @param shareLevel The share level to set
     */
    public void setShareLevel(int shareLevel) {
        this.shareLevel = shareLevel;
    }

    /**
     * Gets whether the key is marked to be used as "inline" or not
     *
     * @return Tue, if the key is marked to be used as "inline", false otherwise.
     */
    public boolean isInline() {
        return inline;
    }

    /**
     * Sets whether the key is marked as inline or not.
     *
     * @param inline True in order to marke the key as inline, False to mark it for PGP-Mime.
     */
    public void setInline(boolean inline) {
        this.inline = inline;
    }

    /**
     * Returns if this key was shared from another user in the same context
     * @return
     */
    public boolean isShared() {
        return this.shareLevel > 0;
    }

    /**
     * Gets if key was a Guard key
     * @return
     */
    public boolean isGuardKey() {
        return guardKey;
    }

    public void setGuardKey (boolean guardKey) {
        this.guardKey = guardKey;
    }

    /**
     * Returns whether or not this key entity is owned by the caller
     *
     * @return True, if the key is owned by the caller, false if it owned by another user
     */
    public boolean isOwned() {
        return this.owned;
    }

    /**
     * Sets whether or not this key entity is owned by the calling user
     *
     * @param owned True, if the key is owned by the calling user, false otherwise
     * @return this
     */
    public OGPGPKeyRing isOwned(boolean owned) {
       this.owned = owned;
       return this;
    }


    public void setVerified (boolean verified) {
        this.verified = verified;
    }

    public boolean isVerified() {
        return this.verified;
    }
}
