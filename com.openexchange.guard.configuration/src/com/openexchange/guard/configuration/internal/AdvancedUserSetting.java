package com.openexchange.guard.configuration.internal;

import com.openexchange.exception.OXException;
import com.openexchange.groupware.contexts.Context;
import com.openexchange.groupware.userconfiguration.UserConfiguration;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.session.Session;
import com.openexchange.user.User;

/**
 * Register with JSLOB setting if user is to default to advanced
 * @author Greg Hill
 *
 */
public class AdvancedUserSetting extends AbstractGuardSetting<Boolean> {

    GuardConfigurationService guardConfigService;

    public AdvancedUserSetting(GuardConfigurationService guardConfigService) {
        super("defaultAdvanced");
        this.guardConfigService = guardConfigService;
    }

    /**
     * Get the boolean value if the user should default to advanced.  ConfigCascade value
     */
    @Override
    protected Boolean getSettingValue(Session session, Context ctx, User user, UserConfiguration userConfig) throws OXException {
        return guardConfigService.getBooleanProperty(GuardProperty.defaultAdvanced, user.getId(), ctx.getContextId());
    }

}
