/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.configuration.validation;

import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.inputvalidation.InputValidator;

/**
 * {@link ConfigurationValidator} validates configuration properties
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class ConfigurationValidator {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ConfigurationValidator.class);

    private final GuardConfigurationService configurationService;

    /**
     * Initializes a new {@link ConfigurationValidator}.
     * 
     * @param configurationService The configuration service
     */
    public ConfigurationValidator(GuardConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @SuppressWarnings("unchecked")
    private <T> T getConfigurationValue(GuardProperty property, Class<? extends T> clazz) {
        T value = null;
        if (clazz.isAssignableFrom(Integer.class)) {
            return (T) new Integer(configurationService.getIntProperty(property));
        }
        else if (clazz.isAssignableFrom(Boolean.class)) {
            return (T) new Boolean(configurationService.getBooleanProperty(property));
        }
        else if (clazz.isAssignableFrom(String.class)) {
            return (T) configurationService.getProperty(property);
        }

        throw new IllegalArgumentException("Unknown property class type");
    }

    /**
     * Validates that a given GuardProperty is valid
     * 
     * @param property The property to validate
     * @param clazz The type of the property value
     * @param validator The validator used for validation
     * @param invalidHandler A handler which is called if the validation failed
     */
    public <T> void validateValue(GuardProperty property, Class<? extends T> clazz, InputValidator<T> validator, InvalidValueHandler<T> invalidHandler) {
        T value = getConfigurationValue(property, clazz);
        if (!validator.isValid(value)) {
            invalidHandler.handle(value);
        }
    }

    /**
     * Validates that a given GuardProperty is valid and throws an exception if it is not valid.
     * 
     * @param property The property to validate
     * @param clazz The type of the property value
     * @param validator The validator used for validation
     * @param invalidHandler A handler which is called if the validation failed
     */
    public <T> void assertValue(GuardProperty property, Class<? extends T> clazz, InputValidator<T> validator, InvalidValueHandler<T> invalidValueHandler) throws OXException {
        T value = getConfigurationValue(property, clazz);
        try {
            validator.assertIsValid(value);
        } catch (OXException e) {
            invalidValueHandler.handle(value);
            throw e;
        }
    }
}
