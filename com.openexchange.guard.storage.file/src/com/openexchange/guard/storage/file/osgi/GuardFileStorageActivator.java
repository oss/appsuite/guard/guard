/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.file.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.database.AssignmentFactory;
import com.openexchange.filestore.DatabaseAccessProvider;
import com.openexchange.filestore.FileStorageService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.storage.cache.FileCacheStorage;
import com.openexchange.guard.storage.file.internal.FileStorageImpl;
import com.openexchange.guard.storage.file.internal.GuestSharedDatabaseAccessProvider;
import com.openexchange.guard.storage.file.internal.SproxydStorageImpl;
import com.openexchange.java.Strings;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardFileStorageActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardFileStorageActivator extends HousekeepingActivator {

    private static final String SPROXYD_FILESTORE = "com.openexchange.guard.storage.file.sproxydFilestore";

    /**
     * Initialises a new {@link GuardFileStorageActivator}.
     */
    public GuardFileStorageActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, FileCacheStorage.class,
            FileStorageService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardFileStorageActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);

        GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
        boolean registered = false;

        String fileStorageType = guardConfigService.getProperty(GuardProperty.fileStorageType);
        if(Strings.isEmpty(fileStorageType)) {
            logger.error("No filestorage type defined: Property com.openexchange.guard.storage.file.fileStorageType is not set");
        }

        switch (fileStorageType) {
            case "sproxyd":
                String fileStore = guardConfigService.getProperty(SPROXYD_FILESTORE);
                if (fileStore == null || fileStore.isEmpty()) {
                    fileStore = "guard";
                }
                registerService(Storage.class, new SproxydStorageImpl(this.getService(FileStorageService.class), fileStore));
                trackService(GuardDatabaseService.class);
                trackService(AssignmentFactory.class);
                registerService(DatabaseAccessProvider.class, new GuestSharedDatabaseAccessProvider(this));
                registered = true;
                break;
            case "file":
                final String directory = guardConfigService.getProperty(GuardProperty.uploadDirectory);
                if (directory.isEmpty()) {
                    logger.error("No filestorage directory defined: Property com.openexchange.guard.storage.file.uploadDirectory is not set");
                    break;
                }
                registerService(Storage.class, new FileStorageImpl(this.getService(FileStorageService.class), directory));
                registered = true;
                break;
            case "s3":
                registerService(DatabaseAccessProvider.class, new GuestSharedDatabaseAccessProvider(this));
            default:

        }

        if (registered) {
            logger.info("GuardFileStorageService registered.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardFileStorageActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        unregisterService(Storage.class);
        logger.info("GuardFileStorageService unregistered.");

        Services.setServiceLookup(null);
        super.stopBundle();
    }
}
