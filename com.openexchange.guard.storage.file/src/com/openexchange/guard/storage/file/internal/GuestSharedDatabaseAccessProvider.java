/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.file.internal;

import java.net.URI;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.openexchange.database.Assignment;
import com.openexchange.database.AssignmentFactory;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.DatabaseAccess;
import com.openexchange.filestore.DatabaseAccessProvider;
import com.openexchange.filestore.utils.AssignmentUsingDatabaseAccess;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link GuestSharedDatabaseAccessProvider}
 * Provides the database connection to the guest shards for s3 and sproxyd
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.4
 */

public class GuestSharedDatabaseAccessProvider implements DatabaseAccessProvider {
    
    private final ServiceLookup services;
    
    public GuestSharedDatabaseAccessProvider(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Pattern match for the file prefix. Should be ext-shardId-userId
     */
    private static final Pattern prefixPattern = Pattern.compile("ext[-_](\\d*)[-_](\\d*)");

    /**
     * Get the shard id from the file prefix
     * getGuestShardIdFromPrefix
     *
     * @param directoryPrefix
     * @return
     */
    private static Integer getGuestShardIdFromPrefix(String directoryPrefix) {
        Objects.requireNonNull(directoryPrefix, "directoryPrefix must not be null");
        Matcher matcher = prefixPattern.matcher(directoryPrefix);
        if (matcher.find()) {
           return Integer.valueOf(matcher.group(1));
        }
        return null;
    }

    @Override
    public DatabaseAccess getAccessFor(URI uri, String prefix) throws OXException {
        // Guest storage is specified by the prefix, not the URI.  fileStorageId is not needed
        return getAccessFor(0, prefix);
    }

    @Override
    public DatabaseAccess getAccessFor(int fileStorageId, String prefix) throws OXException {
        final Integer shardId = getGuestShardIdFromPrefix(prefix);
        if (shardId == null) {  // Not right format
            return null;
        }
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        String shardName = databaseService.getShardName(shardId.intValue());
        AssignmentFactory assignmentFact = services.getServiceSafe(AssignmentFactory.class);
        Assignment assignment = assignmentFact.get(shardName);
        if (assignment == null) {
            throw OXException.general("Unable to get database assignment for Guard shard");
        }
        return new AssignmentUsingDatabaseAccess(assignment, databaseService.getDatabaseService());

    }

}
