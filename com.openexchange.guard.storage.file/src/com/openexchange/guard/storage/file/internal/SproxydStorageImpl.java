/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.file.internal;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.filestore.FileStorageService;
import com.openexchange.guard.storage.AbstractCoreStorage;

/**
 * Implements sproxyd storage for Guard guest files
 *
 * {@link SproxydStorageImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.1
 */
public class SproxydStorageImpl extends AbstractCoreStorage {

    private static Logger LOG = LoggerFactory.getLogger(SproxydStorageImpl.class);
    private static String filestore;

    public SproxydStorageImpl (FileStorageService fileStorageService, String filestore) {
        this.fileStorageService = fileStorageService;
        this.filestore = filestore;
    }


    /**
     * Pull an file URI from the file location
     * @param location
     * @return
     */
    @Override
    protected URI uriFromLocation (String location) {
        String path = (location.indexOf("/") < 0) ? "" : location.substring(0, location.lastIndexOf("/"));
        try {
            return new URIBuilder().setScheme("sproxyd").setHost(filestore).setPath(path).build();
        } catch (URISyntaxException e) {
            LOG.error("Problem parsing file URI from file location", e);
        }
        return null;
    }

    /**
     * Pull an file URI from the directory
     * @param directory
     * @return
     */
    @Override
    protected URI uriFromDirectory (String directory) {
        try {
            return new URIBuilder().setScheme("sproxyd").setHost(filestore).setPath(directory).build();
        } catch (URISyntaxException e) {
            LOG.error("Problem parsing file URI from file directory", e);
            return null;
        }
    }



    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.Storage#objectExists(java.lang.String)
     */
    @Override
    public boolean objectExists(String objectId) throws OXException {
        InputStream in = getObjectStream(objectId);
        boolean exists = in != null;
        IOUtils.closeQuietly(in);
        return exists;
    }

}
