/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 *
 * {@link GuardAuthExceptionMessages}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public final class GuardAuthExceptionMessages implements LocalizableStrings {

    /** Returned when a user has been locked out due to providing the wrong password multiple times */
    public static final String LOCKOUT_MSG = "Your account has been locked out due to providing to many wrong passwords. Please try again later.";

    /** Returned when authentication is not possible due to invalid user/password combination */
    public static final String BAD_PASSWORD_MSG = "The user name or password isn't correct.";

    /** Returned when a session is not present but is required */
    public static final String MISSING_SESSION_MSG = "No valid session found";

    /** Returned when a user is authenticated but the requested operation is not permitted **/
    public static final String FORBIDDEN_MSG = "The operation is not permitted.";

    /** Retunred when a client is not sending authentication information **/
    public static final String AUTH_MISSING_MSG = "Authentication parameters are missing";

    /** Returned if error obtaining session token during authentication */
    public static final String TOKEN_ERROR_MSG = "Unable to obtain a session token.";

    /** Action blocked, as the user does not appear to be logged in */
    public static final String NOT_LOGGED_IN = "User does not appear to be logged in.";

    /** User tried to reset/recover their password, but no recovery is available */
    public static final String RECOVERY_MISSING = "No recovery available";

    /**
     * Prevent instantiation.
     */
    private GuardAuthExceptionMessages() {
        super();
    }
}
