/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link LongUtil}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public final class LongUtil {

    private static final Logger logger = LoggerFactory.getLogger(LongUtil.class);

    /**
     * Converts the specified String to a long value
     * 
     * @param string The string to convert to a long
     * @return The long value of the string
     */
    public static Long stringToLong(String string) {
        return new BigInteger(string, 16).longValue();
    }

    /**
     * Parses the specified string as a hex string
     * 
     * @param string the string to parse
     * @return The hex representation of the string
     */
    public static String getHex(String string) {
        try {
            return Long.toHexString(Long.parseLong(string)).substring(8);
        } catch (Exception ex) {
            logger.error("Error parsing long string " + string, ex);
            return null;
        }
    }

    /**
     * Parses the specified string as hex
     * 
     * @param id
     * @return
     */
    public static String getLongHex(String id) {
        try {
            return Long.toHexString(Long.parseLong(id));
        } catch (Exception ex) {
            logger.error("Error parsing long string " + id, ex);
            return null;
        }
    }

    /**
     * Long to Hex String
     * 
     * @param id
     * @return
     */
    public static String longToHexString(long id) {
        return Long.toHexString(id);
    }

    /**
     * Long to hex string, truncated
     * 
     * @param id
     * @return
     */
    public static String longToHexStringTruncated(long id) {
        if (Long.toHexString(id).length() < 8) {
            return (Long.toHexString(id));
        }
        return (Long.toHexString(id).substring(8));
    }
}
