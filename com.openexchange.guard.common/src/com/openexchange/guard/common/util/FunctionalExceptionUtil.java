/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * {@link FunctionalExceptionUtil} contains static helper methods which are useful for exception handling when using lambda expressions.
 * <br>
 * <br>
 * This allows creating a lambda expression calling code which is defined to throw a checked exception.
 * <br>
 * <br>
 * Example:
 * <br>
 * <code> <pre>
 *  Collection&lt;String&gt; collection = ...;
 *  collection.ForEach( s -> doSomething(s));
 * </pre> </code>
 * This code causes a compiler error if <code>doSomething()</code> is declared to throw an exception,
 * but there is no way to bubble the exception up to the caller.
 * <code> <pre>
 *      public void doSomething(String s) throws Exception{
 *       ...
 *      }
 * </pre> </code>
 * {@link FunctionalExceptionUtil} helps to work around this issue:
 * <br>
 * <code> <pre>
 *  public void example() throws Exception {
 *      //Exception is thrown and can be caught by the caller
 *      collection.forEach(FunctionalExceptionUtil.toExceptionAwareConsumer(s -> doSomething(s)));
 *  }
 * </pre> </code>
 * <strong>
 *  Although this pattern ("Sneaky Throw") is widely used in common public projects (like Android, or Project Lombok),
 *  this is more like a hack and should be used with caution!
 * </strong>
 * <br>
 * <br>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class FunctionalExceptionUtil {

    /**
     * Internal <b>hack</b> to "sneaky" throw a given exception. <b>(Sneaky Throw pattern)</b>.
     * <br>
     * <br>
     * Call this method with a generic type parameter of a non checked exceptions like {@link RuntimeException} or {@link Error}.
     * The code then casts the given Throwable to a {@link RuntimeException} or {@link Error} before throwing it.
     * <br>
     * For one thing, this pleases the compiler (because we act like throwing a non checked exception) and secondly,
     * generic types are erased during runtime so that the given Throwable is not casted and just thrown as it is.
     *
     * @see http://www.philandstuff.com/2012/04/28/sneakily-throwing-checked-exceptions.html
     *
     * @param <T> The type to cast the given Throwable to (Should be a non checked Exception type like {@link RuntimeException} or {@link Error}).
     * @param e The exception to "sneaky" throw.
     * @throws e Always throws the given Throwable e.
     */
    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void sneakyRethrow(Throwable e) throws T {
        //T is erased during runtime: No cast is performed and therefore no ClassCastException is thrown.
        throw (T) e;
    }

    /**
     * {@link ConsumerWithException} defines a functional consumer interface, like {@link Consumer} but with a checked exception.
     *
     * @param <T> The type to consume
     * @param <E> The type of the exception
     * @see {@link Consumer}
     */
    @FunctionalInterface
    public interface ConsumerWithException<T, E extends Exception> {

        void accept(T t) throws E;
    }

    /**
     * {@link FunctionWithException} defines a functional function interface like {@link Function} but with a checked exception.
     *
     * @param <T> The type of the function's parameter
     * @param <R> The return type of the function
     * @param <E> The type of the exception
     * @see {@link Function}
     */
    @FunctionalInterface
    public interface FunctionWithException<T, R, E extends Exception> {

        R apply(T t) throws E;
    }

    /**
     * Creates a Consumer which is able to throw checked Exceptions.
     * <br>
     * <br>
     * <strong>This method can "sneaky" throw an exception of type &lt;E&gt; although it is not declared in a throws clause.</strong>
     *
     * @param <T> The type of the consumer's parameter
     * @param <E> The type of exception which can be thrown by the given consumer
     * @param consumer The consumer to accept and to handle exceptions for
     * @return A consumer able to handle checked exceptions.
     */
    public static <T, E extends Exception> Consumer<T> toExceptionThrowingConsumer(ConsumerWithException<T, E> consumer) {
        return c -> {
            try {
                consumer.accept(c);
            } catch (Exception exception) {
                FunctionalExceptionUtil.<RuntimeException>sneakyRethrow(exception);
            }
        };
    }

    /**
     * Creates a Function which is able to throw checked Exceptions.
     * <br>
     * <br>
     * <strong>This method can "sneaky" throw an exception of type &lt;E&gt; although it is not declared in a throws clause.</strong>
     *
     * @param <T> The type of the function's parameter
     * @param <R> The return type of the function
     * @param <E> The type of exception which can be thrown by the given function
     * @param function The function to apply and to handle exceptions for.
     * @return A function able to handle checked exceptions.
     */
    public static <T, R, E extends Exception> Function<T, R> toExceptionThrowingFunction(FunctionWithException<T, R, E> function) {
        return f -> {
            try {
                return function.apply(f);
            } catch (Exception exception) {
                FunctionalExceptionUtil.<RuntimeException>sneakyRethrow(exception);
                return null; /* unreachable but required because the compiler does not know that the method does always throw an exception*/
            }
        };
    }
}
