/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.util.Locale;
import org.slf4j.Logger;
import org.apache.commons.lang3.LocaleUtils;

/**
 * {@link LocaleUtil}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class LocaleUtil {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(LocaleUtil.class);
    }

    /**
     * Parses a locale from string
     *
     * @param localeString The locale to parse
     * @return The parsed locale
     */
    public static Locale getLocalFor(String localeString) {
        if (localeString == null)
            return null;
        localeString = localeString.replace("-", "_");
        try {
            return LocaleUtils.toLocale(localeString);
        } catch (Exception e) {
            if (localeString.length() < 6) {  // Locales should only be 5 characters
                LoggerHolder.LOG.error("Unable to parse locale from string {}", localeString, e);
            } else {
                LoggerHolder.LOG.error("Invalid cookie for locale");  // Don't log the odd character string
            }
        }
        return null;
    }
}
