
package com.openexchange.guard.common.util;

import java.io.StringReader;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.stream.JsonReader;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * Utility class
 */
public final class JsonUtil {

    private static final Logger LOG = LoggerFactory.getLogger(JsonUtil.class);

    private static JsonObject toJsonObject(JSONObject jsonObject){
        JsonParser jsonParser = new JsonParser();
        return (JsonObject)jsonParser.parse(jsonObject.toString());
    }

    /**
     * Utility to extract JSON data from a web response
     *
     * @param webResponse the response to extract data from
     * @return The parsed JSON object
     */
    public static JsonObject extractObject(String webResponse) {
        if (webResponse == null) {
            return new JsonObject();
        }
        int i = webResponse.indexOf("{");
        int j = webResponse.lastIndexOf("}");
        if (i < 0 || j < 0 || i > j) {  // Not complete json string
            return new JsonObject();
        }
        String jsonString = webResponse.substring(i, j + 1);
        JsonParser parser = new JsonParser();
        JsonObject result = (JsonObject) parser.parse(jsonString);
        return result;
    }

    /**
     * Parses the specified data as JsonObject
     *
     * @param data
     * @return
     * @throws IllegalArgumentException if the specified data cannot be parsed as a JsonObject
     */
    public static JsonObject parseAsJsonObject(String data) {
        JsonParser jsonParser = new JsonParser();
        JsonElement element = jsonParser.parse(data.trim());
        if (!element.isJsonNull() && element.isJsonObject()) {
            return element.getAsJsonObject();
        }
        throw new IllegalArgumentException("Cannot parse the provided data as JsonObject: " + data);
    }

    /**
     * Checks if the data is a valid, parsable JSON
     *
     * @param data The data which should be checked
     * @return True, if the data is valid JSON, False otherwise
     */
    public static boolean isJson(String data) {
        try {
            if (data != null && !data.isEmpty() && data.startsWith("{") && data.endsWith("}")) {
                new JsonParser().parse(new JsonReader(new StringReader(data)));
                return true;
            }
            else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @param json
     * @param name
     * @return
     * @throws OXException
     */
    public static String getStringFromJson(JsonObject json, String name) throws OXException {
        return getStringFromJson(json, name, false);
    }

    /**
     * Get String from JsonObject
     *
     * @param json
     * @param name
     * @param mandatory
     * @return
     * @throws OXException
     */
    public static String getStringFromJson(JsonObject json, String name, boolean mandatory) throws OXException {
        if ((json == null) || (!json.has(name))) {
            if (mandatory) {
                throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create(name);
            } else {
                return null;
            }
        }
        try {
            return json.get(name).getAsString();
        } catch (Exception ex) {
            LOG.error("Problem getting string parameter " + name + "from json", ex);
            throw GuardCoreExceptionCodes.JSON_ERROR.create(ex, ex.getMessage());
        }
    }

    /**
     *
     * @param json
     * @param name
     * @return
     * @throws OXException
     */
    public static boolean getBooleanFromJson(JsonObject json, String name) throws OXException {
        return getBooleanFromJson(json, name, false);
    }

    public static boolean getBooleanFromJson(JSONObject json, String name) throws OXException{
        return getBooleanFromJson(toJsonObject(json), name);
    }

    /**
     *
     * @param json
     * @param name
     * @param mandatory
     * @return
     * @throws OXException
     */
    public static boolean getBooleanFromJson(JsonObject json, String name, boolean mandatory) throws OXException {
        if (!json.has(name)) {
            if (mandatory) {
                throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create(name);
            } else {
                return false;
            }
        }
        try {
            return json.get(name).getAsBoolean();
        } catch (Exception ex) {
            LOG.error("Problem getting string parameter " + name + "from json", ex);
            throw GuardCoreExceptionCodes.JSON_ERROR.create(ex, ex.getMessage());
        }
    }

    /**
     * Gets an integer parameter from the given JsonObject.
     *
     * @param json The JSON object.
     * @param name The name of the integer parameter.
     * @param mandatory true, to throw an exception if the parameter was not found, false to return null.
     * @return The parameter's value or null if the parameter was not found and mandatory is set to false.
     * @throws OXException If the parameter was not found and mandatory is set to true.
     */
    public static Integer getIntFromJson(JsonObject json, String name, boolean mandatory) throws OXException {
        if (!json.has(name)) {
            if (mandatory) {
                throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create(name);
            } else {
                return null;
            }
        }
        try {
            return json.get(name).getAsInt();
        } catch (Exception ex) {
            LOG.error("Problem getting int parameter " + name + "from json", ex);
            throw GuardCoreExceptionCodes.JSON_ERROR.create(ex, ex.getMessage());
        }
    }

    /**
     * Gets an integer parameter form the given JsonObject
     * @param json The JSON Object
     * @param name The nname of the integer parameter.
     * @return The parameter's value or null if the paramter was not found.
     * @throws OXException
     */
    public static Integer getIntFromJson(JsonObject json, String name) throws OXException {
        return getIntFromJson(json, name, false);
    }

    public static JsonArray getArrayFromJson(JsonObject json, String name) throws OXException {
        return getArrayFromJson(json,name,false);
    }

    public static JsonArray getArrayFromJson(JsonObject json, String name, boolean madatory) throws OXException {
        if(!json.has(name)) {
            if(madatory) {
                throw GuardCoreExceptionCodes.JSON_PARAMETER_MISSING.create(name);
            }
            else {
                return null;
            }
        }
        return json.getAsJsonArray(name);
    }

    public static String getError(String response) {
        try {
            int i = response.indexOf("{");
            int j = response.lastIndexOf("}");
            response = response.substring(i, j + 1);
            JsonObject resp = parseAsJsonObject(response);
            if (resp.has("error_desc")) {
                return resp.get("error_desc").getAsString();
            } else {
                return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * Maps an object to JSON and returns it as String
     *
     * @param object The object to map
     * @return The mapped JSON as String
     * @throws OXException
     */
    public static String toJsonString(Object object) throws OXException {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    public static String emptyJsonString() throws OXException {
        return "{}";
    }
}
