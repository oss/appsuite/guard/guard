/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.common.util;

import java.net.IDN;

/**
 * {@link IDNUtil} - Helper utilities for IDN encoding
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class IDNUtil {

    /**
     * Internal method to ASCII encode a domain part of an email
     *
     * @param email the email
     * @return the mail with ASCII encoded domain part if the domain part contains non ASCII characters or email if the domain part does only contain ASCII characters
     */
    public static String aceEmail(String email) {
        if (email.indexOf("@") == -1) {
            return email;
        }
        if (!isAllASCII(email)) {
            String sender = email.substring(0, email.indexOf("@"));
            String senderDomain = email.substring(email.indexOf("@") + 1);
            return sender + "@" + IDN.toASCII(senderDomain);
        }
        return email;
    }

    /**
     * Decode the ASCII encoding to Unicode
     * @param email
     * @return
     */
    public static String decodeEmail(String email) {
        if (email.indexOf("@") == -1) {
            return email;
        }
        String sender = email.substring(0, email.indexOf("@"));
        String senderDomain = email.substring(email.indexOf("@") + 1);
        return sender + "@" + IDN.toUnicode(senderDomain);
    }

    /**
     * Checks if a string contains only ASCII characters
     *
     * @param input the input to test
     * @return true if the string contains only ASCII characters (<128), false otherwise
     */
    public static boolean isAllASCII(String input) {
        boolean isASCII = true;
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (c > 0x7F) /* 128 */{
                isASCII = false;
                break;
            }
        }
        return isASCII;
    }
}
