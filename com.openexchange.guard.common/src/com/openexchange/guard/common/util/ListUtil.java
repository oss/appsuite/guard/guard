package com.openexchange.guard.common.util;

import java.util.List;


public class ListUtil {

    public final static String WHITE_SPACE_SEP = " ";
    public final static String COMMA_SEP = ",";

    /**
     * Helper method to convert a list into a white string separated by a given separator
     * @param list the list of keys to convert to a separated string
     * @return a separated string of items
     */
    public static <T> String  listToString(List<T> list, String separator) {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<list.size();i++) {

            //add the key id
            sb.append(list.get(i));

            //add whitespace if not last key id
            if(i < list.size() - 1) {
                sb.append(separator);
            }
        }
        return sb.toString();
    }

    /**
     * Will return a appropriated DB query variable string.
     * This string can be used to create queries which uses the IN keyword of SQL
     *
     * For a example ?,?,? would be returned for an input of 3
     * @param list a variable list containing as many variables as the list contains objects
     * @return
     */
    public static <T> String listToDbValuesList(int count) {
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<count;i++) {
            sb.append("?");
            if(i < count-1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
}
