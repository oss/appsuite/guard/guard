
package com.openexchange.guard.common.clients.utils;

import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.client.methods.HttpUriRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * {@link TimerUtils}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.5
 */
public class TimerUtils {

    private static final Logger LOG = LoggerFactory.getLogger(TimerUtils.class);

    /**
     * Function for hard timeout of a URIRequest
     * limitUriRequest
     *
     * @param request The HttpUriRequest to watch
     * @param timeout Timeout in ms for hard abort
     */
    public static TimerTask limitUriRequest(HttpUriRequest request, int timeout) {
        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                if (request != null) {
                    LOG.error("Closing http request {} due to timeout", request.getURI());
                    request.abort();
                }
            }
        };
        new Timer(true).schedule(task, timeout);
        return task;
    }

}
