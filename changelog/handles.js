module.exports = function (Handlebars) {

	Handlebars.registerHelper('ifOrArrayCond', function(v1, v2, options) {
		if((v1 && v1.length > 0) || (v2 && v2.length > 0)) {
		  return options.fn(this);
		}
		return options.inverse(this);
	  });

	Handlebars.registerHelper('getNeeded', (context, opt) => {
	  if (!context || !context.data.root || !context.data.root.releases || context.data.root.releases.length === 0) {
	    return ''
	  }
	  const releases = context.data.root.releases
	  const options = context.data.root.options

	  function capitalizeFirst(str) {
		return str.charAt(0).toUpperCase() + str.slice(1)
	  }

	  // Cleanup of messages.  Remove keywords (if any), capitalize, enhanced MW URL
	  function cleanup (toclean, toRemove) {
		return capitalizeFirst(toclean.replace(toRemove, '').replace(/  /g, ' ').replace(/^[\s,:]*/, '').replace(/MW-[0-9]+/, "['$&'](https://jira.open-xchange.com/browse/$&)"));
	  }

	  // Make sure items already added to fixed are also added to appropriate add/removed/etc
	  function duplicateEntries(issues, target, pattern) {
		issues.forEach(i => {
			if (pattern.test(i.commit.subject)) {
				i.commit.subject = capitalizeFirst(i.commit.subject)
				target.push(i.commit)
			}
		})
	  }
	  
	  for (var i = 0; i < releases.length; i++) {
		// Normalize releases
		let fixes = releases[i].fixes;
		for (var j = 0; j < fixes.length; j++) {
			fixes[j].commit.subject = cleanup(fixes[j].commit.subject, fixes[j].fixes[0].id);
			fixes[j].fixes[0].id = fixes[j].fixes[0].id.toUpperCase();

		}

		let release = releases[i];
		// Get added
		const addlist = release.commits
	    .filter(commit => {
	      const pattern = new RegExp('^add', 'mi')
	      if (pattern.test(commit.message)) {
			return true
	      }
	      return false 
	    })
		.map(commit => {
			commit.subject = capitalizeFirst(commit.subject)
			return commit
		})
		release.added = addlist
		duplicateEntries(release.fixes, release.added, new RegExp('^add', 'mi'))

		// Get addit fixed
		const fixed = release.commits
		.filter(commit => {
			const pattern = new RegExp('^fix', 'mi')
			if (pattern.test(commit.message)) {
			return true
			}
			return false 
		})
		.map(commit => {
			commit.subject = capitalizeFirst(commit.subject)
			return commit
		})
		release.fixed = fixed 

		// Get changed
		const changed = release.commits
		.filter(commit => {
			const pattern = new RegExp('^(change)|(MW-)', 'mi')
			if (pattern.test(commit.message)) {
			  return true
			}
			return false 
		  })
		  .map(commit => {
			commit.subject = cleanup(commit.subject, /^change[d]*[\s,:]*/im)
			return commit;
		  })
		release.changed = changed

		// Get removed
		const remlist = release.commits
	    .filter(commit => {
	      const pattern = new RegExp('^remove', 'mi')
	      if (pattern.test(commit.message)) {
			return true
	      }
	      return false 
	    })
		.map(commit => {
			commit.subject = cleanup(commit.subject, /^remove[d]*[\s,:]*/im)
			return commit;
		  })
		release.removed = remlist
		duplicateEntries(release.fixes, release.removed, new RegExp('^remove', 'mi'))

		// Get Security
		const seclist = release.commits
	    .filter(commit => {
	      const pattern = new RegExp("(^security)|(CVE-\\d+-\\d+)", 'mi')
	      if (pattern.test(commit.message)) {
			return true
	      }
	      return false 
	    })
		release.security = seclist

		// Get SCR
		const scrlist = release.commits
	    .filter(commit => {
	      const pattern = new RegExp('SCR-\\d+', 'm')
	      if (pattern.test(commit.message)) {
			const id = commit.message.match(pattern)[0]
			commit.scrid = id
			const url = options.scrUrl || options.issueUrl

			commit.scrUrl = url.replace('{id}', id)
			return true
	      }
	      return false 
	    })
		release.scr = scrlist
	  }
	})
	  
}
