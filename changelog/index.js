#!/usr/bin/env node

const { run } = require('auto-changelog/src/run')
var fs = require('fs')
var child_process = require('child_process');
const { exit } = require('process');
const prompt = require('prompt-sync')();
var spawn = child_process.spawn;

// Let's load up the current CHANGELOG.md
var oldChangelog = fs.readFileSync('../CHANGELOG.md', 'utf8')

const start = oldChangelog.indexOf('##')
if (start > 0) {
  oldChangelog = oldChangelog.substring(start - 1)
}

let args = process.argv
args.push('--output')
args.push('temp_changelog_dif.md')
args.push('--handlebars-setup')
args.push('handles.js')
let newlog = ''

function cleanup() {
  fs.unlink('./temp_changelog_dif.md', (err) => {
    if (err) {
      console.err(err);
    }
  })
  fs.unlink('./temp_merged.md', (err) => {
    if (err) {
      console.err(err);
    }
  })
}

run(args).catch(error => {
    console.log('\n')
    console.error(error)
    process.exit(1)
  }).then(() => {
    // Open an editor with the new
    var ed = spawn('code', ['-w', 'temp_changelog_dif.md'], {
    })
    ed.on('exit', function (e) {
      // Merge into temp file
      const newDiff = fs.readFileSync('temp_changelog_dif.md', 'utf8')
      const pattern = new RegExp(/[0-9]+\.[0-9]+\.[0-9]+/im, 'i')
      const lastVer = oldChangelog.match(pattern)[0]
      const version = args.filter((a) => pattern.test(a))
      const reference = '[' + version + ']: (https://gitlab.open-xchange.com/appsuite/guard/compare/' + lastVer + '...' + version + ')\r\n'
      if (!lastVer || version.length < 1) {
        cleanup()
        exit(1)
      }
      newlog = newDiff + oldChangelog + reference
      fs.writeFileSync('temp_merged.md', newlog)
      // Show the temp file for editing
      var ed2 = spawn('code', ['-w', 'temp_merged.md'], {
      })
      ed2.on('exit', function (e) {
        // Check if we should continue
        const a = prompt('Accept? (y/n)')
        if (a!="y") {
          cleanup()
          exit(1)
        }
        // Overwrite the CHANGELOG
        const newfile = fs.readFileSync('temp_merged.md', 'utf8')
        fs.writeFileSync('../CHANGELOG.md', newfile)
        cleanup()
        exit()
      })
    })
  })