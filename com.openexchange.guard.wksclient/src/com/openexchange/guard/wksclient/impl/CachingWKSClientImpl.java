/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient.impl;

import static com.openexchange.java.Autoboxing.I;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.CachedKey;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.wksclient.WKSClientService;
import com.openexchange.guard.wksclient.WKSResult;

/**
 * {@link CachingWKSClientImpl} - delegates the WKS search to another {@link WKSClient} but caches the result if trusted.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class CachingWKSClientImpl implements WKSClientService {

    private static final Logger logger = LoggerFactory.getLogger(CachingWKSClientImpl.class);
    private final WKSClientService delegate;
    private final RemoteKeyCacheStorage remoteKeyCacheStorage;

    /**
     * Initializes a new {@link CachingWKSClientImpl}.
     *
     * @param delegate The {@link WKSClientService} to query if no caching item was found
     */
    public CachingWKSClientImpl(WKSClientService delegate, RemoteKeyCacheStorage remoteKeyCacheStorage) {
        this.delegate = Objects.requireNonNull(delegate, "delegate must not be null");
        this.remoteKeyCacheStorage = Objects.requireNonNull(remoteKeyCacheStorage,"remoteKeyCacheStorage must not be null");
    }

    private CachedKey toCachedKey(WKSResult wksResult) {
        return new CachedKey(wksResult.getPublicKeyRing(), wksResult.getKeySource());
    }

    private WKSResult toWKSResult(CachedKey cachedKey) {
        return new WKSResult(cachedKey.getPublicKeyRing(), cachedKey.getKeySource());
    }

    private Optional<WKSResult> toWKSResult(Optional<CachedKey> cachedKey) {
        return cachedKey.map(ck -> new WKSResult(ck.getPublicKeyRing(), ck.getKeySource()));
    }

    private Collection<WKSResult> toWKSResults(Collection<CachedKey> cachedKeys) {
        return cachedKeys.stream().map( cachedKey -> toWKSResult(cachedKey)).collect(Collectors.toList());
    }

    private boolean writeToCache(WKSResult wksResult, int userId, int cid) throws OXException {
        if (wksResult.getKeySource() != null && wksResult.getKeySource().isTrusted(userId, cid)) { //Only save from trusted sources
            remoteKeyCacheStorage.insert(toCachedKey(wksResult));
            return true;
        }
        return false;
    }

    private Collection<WKSResult> findAllInCache(String email) throws OXException{
        return toWKSResults(remoteKeyCacheStorage.getByEmail(email));
    }

    private Optional<WKSResult> findFirstInCache(String email) throws OXException {
       return toWKSResult(remoteKeyCacheStorage.getByEmail(email).stream().findFirst());
    }

    private void cache(Collection<WKSResult> results, int userId, int cid) throws OXException {
        //Add keys from trusted sources to the cache
        if(results != null && !results.isEmpty()) {
            for (WKSResult result : results) {
                if (writeToCache(result, userId, cid)) {
                    logger.debug("Wrote {} found keys from trusted WKS server into cache", I(results.size()));
                }
            }
        }
    }

    private void cache(Optional<WKSResult> result, int userId, int cid) throws OXException {
        //Add key to cache if trusted
        if (result.isPresent()) {
            if (writeToCache(result.get(), userId, cid)) {
                logger.debug("Wrote found key from trusted WKS server into cache");
            }
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#findAll(java.lang.String, int)
     */
    @Override
    public Collection<WKSResult> findAll(String email, int userId, int cid, int timeout) throws OXException {

        //Search in the cache first
        Collection<WKSResult> results = findAllInCache(email);
        if(results.isEmpty()) {

            //delegate the search if the cache does not contain the key we are looking for
            results = delegate.findAll(email, userId, cid, timeout);
            cache(results, userId, cid);

        }
        else {
            logger.debug("{} Remote keys found in cache", I(results.size()));
        }
        return results;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#find(java.lang.String, int)
     */
    @Override
    public Optional<WKSResult> find(String email, int userId, int cid, int timeout) throws OXException {

        //Search in the cache first
        Optional<WKSResult> result = findFirstInCache(email);
        if(!result.isPresent()) {
            //delegate the search if the cache does not contain the key we are looking for
            result = delegate.find(email, userId, cid, timeout);
            cache(result, userId, cid);
        }

        return result;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#findAll(java.lang.String, int, java.lang.String, int)
     */
    @Override
    public Collection<WKSResult> findAll(String email, int userId, int cid, int timeout, String host, int port) throws OXException {

        //Search in the cache first
        Collection<WKSResult> results = findAllInCache(email);
        if(results.isEmpty()) {

            //delegate the search if the cache does not contain the key we are looking for
            results = delegate.findAll(email, userId, cid, timeout, host, port);
            cache(results, userId, cid);
        }
        else {
            logger.debug("{} Remote keys found in cache", I(results.size()));
        }
        return results;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.wksclient.WKSClientService#find(java.lang.String, int, java.lang.String, int)
     */
    @Override
    public Optional<WKSResult> find(String email, int userId, int cid, int timeout, String host, int port) throws OXException {
        //Search in the cache first
        Optional<WKSResult> result = findFirstInCache(email);
        if(!result.isPresent()) {
            //delegate the search if the cache does not contain the key we are looking for
            result = delegate.find(email, userId, cid, timeout, host, port);
            cache(result, userId, cid);
        }

        return result;
    }
}
