/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient.impl;

import static com.openexchange.java.Autoboxing.I;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.MessageConstraints;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.clients.utils.TimerUtils;
import com.openexchange.guard.common.streams.SizeLimitedInputStream;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.wksclient.WKSResult;
import com.openexchange.java.Strings;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.server.ServiceLookup;

/**
 * {@link WKSClient} Basic WKS client for searching and downloading public keys from WKS Servers.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class WKSClient {

    private static final Logger LOG = LoggerFactory.getLogger(WKSClient.class);

    private static String SCHEMA_PLAIN = "http://";
    private static String SCHEMA_TLS = "https://";
    private static String WELL_KNOWN = "/.well-known/openpgpkey/";
    private static String HU = "hu/";
    private static String ADVANCED_SUBDOMAIN_NAME = "openpgpkey";
    private static String LOCAL_PART_PARAM= "?l=";
    private static int PORT_PLAIN = 80;
    private static int TLS_PORT = 443;
    private final Mode mode;
    private final ServiceLookup services;

    public enum Mode {
        /**
         * HTTP
         */
        PLAIN_TEXT,

        /**
         * HTTPS
         */
        TLS
    }

    /**
     * Initializes a new {@link WKSClient}.
     */
    public WKSClient(ServiceLookup services) {
        this(Mode.TLS, services);

    }

    /**
     * Initializes a new {@link WKSClient}.
     *
     * @param mode The mode to use.
     */
    WKSClient(Mode mode, ServiceLookup services) {
        this.mode = mode;
        this.services = services;
    }

    private HttpClient getClient(int connectionTimeout) {
        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectTimeout(connectionTimeout)
            .setSocketTimeout(connectionTimeout)
            .setRedirectsEnabled(false)
            .build();
        return HttpClientBuilder.create()
            .setDefaultRequestConfig(requestConfig)
            .setDefaultConnectionConfig(
                ConnectionConfig.custom().setMessageConstraints(
                    MessageConstraints.custom()
                        .setMaxLineLength(10000)
                        .setMaxHeaderCount(200)
                        .build())
                    .build())
            .build();
    }

    /**
     * Internal method to extract the local part from the given email address
     *
     * @param email The email
     * @return The local part of the given email
     */
    private String getLocalPartFromEmail(String email) {
        return email.substring(0, email.indexOf("@"));
    }

    private String buildDirectUrl(String host, String hash, String localPart, int port) {
        if (host != null && host.endsWith(".")) {
            host = host.substring(0, host.length() - 1);
        }
        return new StringBuilder()
            .append(mode == Mode.TLS ? SCHEMA_TLS : SCHEMA_PLAIN)
            .append(host)
            .append(":")
            .append(Integer.toString(port))
            .append(WELL_KNOWN)
            .append(HU)
            .append(hash)
            .append(LOCAL_PART_PARAM)
            .append(localPart)
            .toString();
    }

    private String buildAdvancedUrl(String host, String hash, String localPart, int port) {
        Objects.requireNonNull(host, "host must not be null");
        if (host.endsWith(".")) {
            host = host.substring(0, host.length() - 1);
        }
        return new StringBuilder()
            .append(mode == Mode.TLS ? SCHEMA_TLS : SCHEMA_PLAIN)
            .append(ADVANCED_SUBDOMAIN_NAME)
            .append(".")
            .append(host)
            .append(":")
            .append(Integer.toString(port))
            .append(WELL_KNOWN)
            .append(host.toLowerCase())
            .append("/")
            .append(HU)
            .append(hash)
            .append(LOCAL_PART_PARAM)
            .append(localPart)
            .toString();
    }

    private List<PGPPublicKeyRing> parseKeys(InputStream keyData) throws IOException {
        return PGPPublicKeyRingFactory.createAll(keyData);
    }

    /**
     * Checks the size of the response.  Rejects if too large
     * checkSize
     *
     * @param response
     * @throws OXException  If too large
     */
    private int checkSize(HttpResponse response) throws IOException {
        GuardConfigurationService config = services.getService(GuardConfigurationService.class);
        if (config == null) {
            return 0;
        }
        int maxKeySize = config.getIntProperty(GuardProperty.maxRemoteKeySize);
        if (maxKeySize < response.getEntity().getContentLength()) {
            LOG.warn("Remote key rejected due to size");
            throw new IOException("Remote key rejected due to size");
        }
        return maxKeySize;

    }

    private List<PGPPublicKeyRing> fetchKeys(HttpUriRequest request, int timeout) throws UnknownHostException {
        HttpClient client = getClient(timeout);
        TimerTask timer = TimerUtils.limitUriRequest(request, timeout);
        HttpResponse response = null;
        try {
            response = client.execute(request);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                int maxKeySize = checkSize(response);
                try (InputStream keyData = new SizeLimitedInputStream(response.getEntity().getContent(), maxKeySize)) {
                    return parseKeys(keyData);
                }
            }
        }
        catch (UnknownHostException e) {
            // Throwing UnknownHostException because callers want to react if the (sub)-domain does not exist,
            // and try another query method (advanced vs direct).
            // See chapter 3.1 of the RFC Draft
            LOG.debug(e.getMessage(), e);
            throw e;
        } catch (IOException e) {
            if (e.getCause() != null && e.getCause().getClass().equals(java.net.SocketTimeoutException.class)) {
                LOG.warn("Timeout waiting for remote key server {} - time remaining in lookup queue was {} ms", request.getURI().getHost(), I(timeout));
            } else {
                LOG.info("Unable to retrieve keys from server {}, {}", request.getURI().getHost(), e.getMessage());
                LOG.debug(e.getMessage(), e);
            }
        } finally {
            if (response != null) {
                HttpClientUtils.closeQuietly(response);
            }
            HttpClientUtils.closeQuietly(client);
            if (timer != null) {
                timer.cancel();
            }
        }
        return Collections.emptyList();
    }

    private Collection<WKSResult> toWKSResult(Collection<PGPPublicKeyRing> keys) {
        List<WKSResult> ret = new ArrayList<WKSResult>();
        for (PGPPublicKeyRing key : keys) {
            ret.add(new WKSResult(key, WKSKeySources.PUBLIC_WKS));
        }
        return ret;
    }

    /**
     * Performs a "direct" WKS key search for the given hash
     *
     * @param host The host
     * @param email The email to search the key for
     * @param timeout The connection timeout
     * @return The search results or an empty list in case no key was found for the given hash.
     * @throws UnknownHostException If the given host does not exist
     */
    public Collection<WKSResult> findDirect(String host, String email, int timeout) {
        return findDirect(host, email, mode == Mode.TLS ? TLS_PORT : PORT_PLAIN, timeout);
    }

    /**
     * Performs a "direct" WKS key search for the given hash
     *
     * @param host The host
     * @param email The email to search the key for
     * @param port The port number to use
     * @param timeout The connection timeout
     * @return The search results or an empty list in case no key was found for the given hash.
     */
    public Collection<WKSResult> findDirect(String host, String email, int port, int timeout) {
        if (timeout <= 0) {
            return Collections.emptyList();
        }
        String hash = CipherUtil.getEmailUserHash(email);
        if(Strings.isEmpty(hash)) {
            return Collections.emptyList();
        }
        String localPart = getLocalPartFromEmail(email);

        HttpGet getRequest = new HttpGet(buildDirectUrl(host, hash, localPart, port));
        List<PGPPublicKeyRing> foundKeys = Collections.emptyList();
        try {
            foundKeys = fetchKeys(getRequest, timeout);
        } catch (UnknownHostException e) {
            LOG.info("Unable to retrieve keys from server {}, {}", host, e.getMessage());
            LOG.debug(e.getMessage(), e);
        }
        return toWKSResult(foundKeys);
    }

    /**
     * Performs an "advanced" WKS key search for the given hash
     *
     * @param host The host
     * @param email The email to search the key for
     * @param timeout The connection timeout
     * @return The search results or an empty list in case no key was found for the given hash.
     * @throws UnknownHostException If the given host does not exist
     */
    public Collection<WKSResult> findAdvanced(String host, String email, int timeout) throws UnknownHostException {
        return findAdvanced(host, email, mode == Mode.TLS ? TLS_PORT : PORT_PLAIN, timeout);
    }

    /**
     * Performs an "advanced" WKS key search for the given hash
     *
     * @param host The host
     * @param email The email to search the key for
     * @param port The port number to use
     * @param timeout The connection timeout
     * @return The search results or an empty list in case no key was found for the given hash.
     * @throws UnknownHostException If the given host does not exist
     */
    public Collection<WKSResult> findAdvanced(String host, String email, int port, int timeout) throws UnknownHostException {
        if (timeout <= 0) {
            return Collections.emptyList();
        }
        String hash = CipherUtil.getEmailUserHash(email);
        if(Strings.isEmpty(hash)) {
            return Collections.emptyList();
        }
        String localPart = getLocalPartFromEmail(email);

        HttpGet getRequest = new HttpGet(buildAdvancedUrl(host, hash, localPart, port));
        List<PGPPublicKeyRing> foundKeys = fetchKeys(getRequest, timeout);
        return toWKSResult(foundKeys);
    }
}
