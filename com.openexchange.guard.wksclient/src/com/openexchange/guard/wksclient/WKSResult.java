/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.wksclient;

import java.util.Iterator;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link WKSResult}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class WKSResult {

    private final PGPPublicKeyRing publicKeyRing;
    private KeySource keySource;

    /**
     * Initializes a new {@link WKSResult}.
     *
     * @param publicKeyRing The public key ring
     */
    public WKSResult(PGPPublicKeyRing publicKeyRing, KeySource keySource) {
        this.publicKeyRing = Objects.requireNonNull(publicKeyRing, "publicKeyRing must not be null");
        this.keySource = Objects.requireNonNull(keySource, "keySource must not be null");
    }

    /**
     * Gets the publicKeyRing
     *
     * @return The publicKeyRing
     */
    public PGPPublicKeyRing getPublicKeyRing() {
        return publicKeyRing;
    }

    /**
     * Returns whether or not the represented PGP key ring is expired.
     *
     * @return true if the represented PGP key is expired, false otherwise
     */
    public boolean isExpired() {
        Iterator<PGPPublicKey> iter = publicKeyRing.getPublicKeys();
        while (iter.hasNext()) {
            if (!PGPKeysUtil.isExpired(iter.next())) {
                return false;  // Check that we have at least one key not expired
            }
        }
        return true;
    }

    /**
     * Returns whether or not the represented PGP key ring is revoked.
     *
     * @return true if the represented PGP key is revoked, false otherwise
     */
    public boolean isRevoked() {
        Iterator<PGPPublicKey> iter = publicKeyRing.getPublicKeys();
        while (iter.hasNext()) {
            if (!iter.next().hasRevocation()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks that we have at least one valid encryption key
     *
     * @return true if a valid encryption key found
     */
    public boolean hasValidKey() {
        Iterator<PGPPublicKey> iter = publicKeyRing.getPublicKeys();
        while (iter.hasNext()) {
            PGPPublicKey key = iter.next();
            if (!key.hasRevocation() && !PGPKeysUtil.isExpired(key) && key.isEncryptionKey()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the source of the result
     *
     * @return The source of the result
     */
    public KeySource getKeySource() {
        return keySource;
    }

    /**
     * Sets the source of the result.
     *
     * @param source The source of the result
     */
    public void setKeySource(KeySource source) {
        this.keySource = source;
    }
}