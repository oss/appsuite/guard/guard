/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

/**
 * {@link MailUpdate}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class MailUpdate {

    private static final String ALL_IDS_STR   = "ALL_MAIL_IDS";
    private String[]            mailIds       = ALL_IDS;
    private Integer             mailFlags     = null;
    private MailFlagsMode       mailFlagsMode = null;
    private Integer             colorLabel    = null;

    public enum MailFlagsMode {
        /**
         * Sets a setting
         */
        SET,

        /**
         * Removes a setting
         */
        UNSET
    }

    /**
     * Constant for updating all known E-Mails
     */
    public static final String[] ALL_IDS = new String[] { ALL_IDS_STR };

    /**
     * Gets the IDs of email items which are getting updated.
     *
     * @return The mailIds The IDs of items to update.
     */
    public String[] getMailIds() {
        return mailIds;
    }

    /**
     * Sets the IDs of the items which are getting updated.
     *
     * @param mailIds the IDs of the items to update.
     */
    public void setMailIds(String[] mailIds) {
        this.mailIds = mailIds;
    }

    /**
     * Gets the mailFlags to update
     *
     * @return The mailFlags to update
     */
    public Integer getMailFlags() {
        return mailFlags;
    }

    /**
     * Sets the mailFlags to update
     *
     * @param mailFlags The mailFlags to update
     */
    public MailUpdate setMailFlags(int mailFlags) {
        this.mailFlags = mailFlags;
        return this;
    }

    /**
     * Gets the mode
     *
     * @return The mode
     */
    public MailFlagsMode getMailFlagsMode() {
        return mailFlagsMode;
    }

    /**
     * Sets the update mode
     *
     * @param mode The new update mode to set
     */
    public MailUpdate setMailFlagsMode(MailFlagsMode mode) {
        this.mailFlagsMode = mode;
        return this;
    }

    /**
     * Gets the color label
     *
     * @return The new color label
     */
    public Integer getColorLabel() {
        return this.colorLabel;
    }

    /**
     * Sets the new color label
     *
     * @param colorLabel The new color label
     * @return this
     */
    public MailUpdate setColorLabel(Integer colorLabel) {
        this.colorLabel = colorLabel;
        return this;
    }
}
