/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import com.openexchange.exception.OXException;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.oxapi.guestUser.OxLookup;
import com.openexchange.guard.oxapi.guestUser.UserEmails;

/**
 * {@link GuestLookupServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuestLookupServiceImpl implements GuestLookupService {

    private final EmailStorage emailStorage;

    public GuestLookupServiceImpl(EmailStorage emailStorage) {
        this.emailStorage = emailStorage;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.GuestLookupService#lookupGuardGuest(int, int)
     */
    @Override
    public Email lookupGuardGuest(int userId, int contextId) throws OXException {
        Email email = null;
        String userEmail = new UserEmails().getPrimaryEmail(userId, contextId);
        if (userEmail == null) return null;
        email = emailStorage.getByEmail(userEmail);
        return email;
    }

    @Override
    public int lookupOxUserId(String email, int contextId) throws OXException {
        return new OxLookup().getId(email, contextId);
    }

}
