/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl.comparators;

import java.util.Arrays;
import javax.mail.Message.RecipientType;
import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link RecipientComparator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class RecipientComparator extends GuardGuestEmailComparator {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5819097062455672037L;
    private final RecipientType type;

    /**
     * Initializes a new {@link RecipientComparator}.
     *
     * @param type The {@link RecipientType} to compare
     */
    public RecipientComparator(RecipientType type) {
        this.type = type;
    }

    private String[] getRecipients(GuardGuestEmail email, RecipientType type) {
        if (email.getMetaData() != null) {
            if (type == RecipientType.TO) {
                return email.getMetaData().getTo();
            } else if (type == RecipientType.CC) {
                return email.getMetaData().getCc();
            } else if (type == RecipientType.BCC) {
                return email.getMetaData().getBcc();
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.guest.impl.comparators.GuardGuestEmailComparator#compareInternal(com.openexchange.guard.guest.GuardGuestEmail, com.openexchange.guard.guest.GuardGuestEmail)
     */
    @Override
    public int compareEmails(GuardGuestEmail arg0, GuardGuestEmail arg1) {
        String[] recipients0 = getRecipients(arg0, type);
        String[] recipients1 = getRecipients(arg1, type);

        if (Arrays.equals(recipients0, recipients1)) {
            return 0;
        }

        if (checkForNull(recipients0, recipients1)) {
            return compareNull(recipients0, recipients1);
        }

        StringBuilder stringBuilderRecipients0 = new StringBuilder();
        StringBuilder stringBuilderRecipients1 = new StringBuilder();
        Arrays.stream(recipients0).forEach(a -> stringBuilderRecipients0.append(a));
        Arrays.stream(recipients1).forEach(a -> stringBuilderRecipients1.append(a));

        return stringBuilderRecipients0.toString().compareTo(stringBuilderRecipients1.toString());
    }
}
