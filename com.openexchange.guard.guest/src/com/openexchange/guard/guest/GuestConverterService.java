/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

import com.openexchange.exception.OXException;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuestConverterService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public interface GuestConverterService {

    /**
     * Method to upgrade a pre 2.10 Guard Guest account.  Creates needed database entries and file conversions
     * of all emails found in the cache
     * @param itemId   The ID of the email that the user link references.  Used to retrieve needed user information
     * @param userIdentity  The identity of the Guest user.  Must be fully authenticated
     * @return
     * @throws OXException
     */
    public String doUpgrade (String itemId, UserIdentity userIdentity, String language) throws OXException;

}
