/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

import com.openexchange.exception.OXException;
import com.openexchange.guard.email.storage.ogEmail.Email;

/**
 * {@link GuestLookupService}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public interface GuestLookupService {

    Email lookupGuardGuest (int userId, int contextId) throws OXException;

    /**
     * Search for an OX user id with the given email address and context
     * lookupOxUserId
     *
     * @param email
     * @param contextId
     * @return
     * @throws OXException
     */
    int lookupOxUserId(String email, int contextId) throws OXException;

}
