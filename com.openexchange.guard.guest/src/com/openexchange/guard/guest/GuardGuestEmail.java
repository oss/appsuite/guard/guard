/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.openexchange.guard.guest.metadata.GuardGuestEmailMetadata;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuardGuestEmail} represents an Email object owned by a guest user
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestEmail implements Closeable {

    private final String            itemId;
    private MimeMessage             message;
    private UserIdentity            owner;
    private GuardGuestEmailMetadata metaData;
    private InputStream             messageStream;

    /**
     * Initializes a new {@link GuardGuestEmail}.
     *
     * @param owner The owner
     * @param itemId The ID of the item
     */
    public GuardGuestEmail(UserIdentity owner, String itemId) {
        this(owner, itemId, null);
    }

    /**
     * Initializes a new {@link GuardGuestEmail}.
     *
     * @param owner The owner
     * @param itemId The ID of the item
     * @param message The message
     */
    public GuardGuestEmail(UserIdentity owner, String itemId, InputStream messageStream) {
        this.owner = Objects.requireNonNull(owner, "owner must not be null");
        this.itemId = Objects.requireNonNull(itemId, "itemId must not be null");
        this.messageStream = messageStream;
    }

    /**
     * Gets the item's unique ID
     *
     * @return The unique ID of the item
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Gets the owner of the guest email
     *
     * @return The owner of the guest email.
     */
    public UserIdentity getOwner() {
        return owner;
    }

    /***
     * Gets the related message
     *
     * @return The related message
     */
    public MimeMessage getMessage() {
        return message;
    }

    /**
     * Gets the content of the related message, or null if there is no message
     *
     * @return The content of the message as byte array, or null of there is no message
     * @throws IOException
     * @throws MessagingException
     */
    public byte[] getContent() throws IOException, MessagingException {
        if (message != null) {
            try (ByteArrayOutputStream bytes = new ByteArrayOutputStream()) {
                message.writeTo(bytes);
                return bytes.toByteArray();
            }
        }
        return null;
    }


    /**
     * Gets the {@link InputStream} for the whole message content
     *
     * @return The {@link InputStream} for the message content
     */
    public InputStream getMessgeStream() {
        return this.messageStream;
    }

    /**
     * Sets the message
     *
     * @param message The message to set
     * @return this
     */
    public GuardGuestEmail setMessage(MimeMessage message) {
        this.message = message;
        return this;
    }

    public GuardGuestEmail setMessageStream(InputStream messageSteam) {
        this.messageStream = messageSteam;
        return this;
    }

    /**
     * Gets the {@link GuardGuestEmailMetadata}
     *
     * @return The meta data, or null if no meta data are present
     */
    public GuardGuestEmailMetadata getMetaData() {
        return metaData;
    }

    /**
     * Sets the {@link GuardGuestEmailMetadata}
     *
     * @param metaData The meta data to set
     * @return this
     */
    public GuardGuestEmail setMetaData(GuardGuestEmailMetadata metaData) {
        this.metaData = metaData;
        return this;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        if (this.messageStream != null) {
            messageStream.close();
        }
    }
}
