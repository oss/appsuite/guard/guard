/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.notification;

import java.util.List;
import javax.mail.BodyPart;
import javax.mail.Message;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;

/**
 * {@link GuardNotificationService} defines an API to send mails without a related OX server session ('on behalf of')
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public interface GuardNotificationService {

    void send(JsonObject email, int senderUserId, int senderCid, String senderIp) throws OXException;

    /**
     * Sends the mail message
     *
     * @param email The e-mail message as JSONObject
     * @param attachments A list with attachments
     * @throws OXException if an error occurred and the mail couldn't get send
     */
    void send(JsonObject email, List<BodyPart> attachments, int senderUserId, int senderCid, String senderIp) throws OXException;

    void send(JsonObject email, int senderUserId, int senderCid, String lang, String senderIp) throws OXException;

    void send(JsonObject email, List<BodyPart> attachments, String sender, int senderUserId, int senderCid, String lang, String messageId, String senderIp) throws OXException;

    /**
     * Sends Mail message using SMTP Server
     *
     * @param msg The message
     * @param toAddr the destination address. If null, then addresses from msg used
     * @param sender The sender address
     * @param guardSenderUserId the Guard ID of the sender
     * @param guardSenderCid the Guard context ID of the sender
     * @param senderUserId the OX ID of the sender
     * @param senderUserCid the OX context ID of the sender
     * @param lang The language
     * @param senderIp IP address of the sender
     * @throws OXException if an error occurred and the mail couldn't get send
     */
    void sendMessage(Message msg, String toAddr, String sender, int guardSenderUserId, int guardSenderCid, int senderUserId, int senderCid, String lang, String senderIp) throws OXException;
}
