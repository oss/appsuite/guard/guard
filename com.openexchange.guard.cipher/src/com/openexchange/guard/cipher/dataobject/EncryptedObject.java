/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher.dataobject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link EncryptedObject}
 */
public class EncryptedObject {

    private static final Logger logger = LoggerFactory.getLogger(EncryptedObject.class);

    private String ItemID;

    private byte[] content;

    private String name;

    private String contentType;

    private String type;

    private int keyLength;

    private int length;

    /**
     * Initialises a new {@link EncryptedObject}.
     */
    public EncryptedObject() {
        super();
    }

    /**
     * Initialises a new {@link EncryptedObject}.
     * 
     * @param in
     * @throws IOException
     */
    public EncryptedObject(final InputStream in) throws IOException {
        parseInputStream(in);
    }

    /**
     * Initialises a new {@link EncryptedObject}.
     * 
     * @param in
     * @throws IOException
     */
    public EncryptedObject(final byte[] in) throws IOException {
        final InputStream inp = new ByteArrayInputStream(in);
        parseInputStream(inp);
        inp.close();
    }

    /**
     * Parses the input stream
     * 
     * @param in The input stream to parse
     * @throws IOException
     */
    private void parseInputStream(final InputStream in) throws IOException {
        int dat;
        boolean done = false;
        int total = 0;
        int pad = 0;
        final StringBuilder head = new StringBuilder();
        while (((dat = in.read()) != -1) && !done) {
            head.append((char) dat);
            if (dat == 45) {
                pad++;
            } else {
                pad = 0;
            }
            if (pad > 4) {
                done = true;
            }
            if (total++ > 200) {
                done = true;
            }
        }
        while (((dat = in.read()) != -1) && dat == 45) {
        }
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write((char) dat);
        IOUtils.copy(in, out);
        setContent(out.toByteArray());
        // this.ItemID = getID(head.toString());
        parseHeader(head.toString());
        out.flush();
        in.close();
        out.close();
    }

    /**
     * Returns the id from the specified header
     * 
     * @param head
     * @return
     */
    public String getID(final String head) {
        try {
            final Pattern p = Pattern.compile("\\bID: [A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-Z0-9]{12}\\b", Pattern.CASE_INSENSITIVE);
            final Matcher m = p.matcher(head);
            if (m.find()) {
                return (m.group().substring(3).trim());
            }
        } catch (final Exception e) {
            logger.error("Error getting id for " + head, e);
        }
        return (null);
    }

    /**
     * Parses the specified header
     * 
     * @param head The header to parse
     */
    public void parseHeader(final String head) {
        final String[] items = head.split(",");
        for (final String item : items) {
            if (item.contains(":")) {
                final String[] dat = item.split(":");
                switch (dat[0].trim().toLowerCase()) {
                    case "key":
                        setKeyLength(Integer.parseInt(dat[1].trim().replace("-", "")));
                        break;
                    case "id":
                        final Pattern p = Pattern.compile("\\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-Z0-9]{12}\\b", Pattern.CASE_INSENSITIVE);
                        final Matcher m = p.matcher(dat[1]);
                        if (m.find()) {
                            setItemID(m.group().trim());
                        }
                        break;
                    case "len":
                        setLength(Integer.parseInt(dat[1].trim()));
                        break;
                }
            }
        }
    }

    /**
     * Gets the itemID
     *
     * @return The itemID
     */
    public String getItemID() {
        return ItemID;
    }

    /**
     * Sets the itemID
     *
     * @param itemID The itemID to set
     */
    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    /**
     * Gets the content
     *
     * @return The content
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Sets the content
     *
     * @param content The content to set
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Gets the name
     *
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name
     *
     * @param name The name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the content_type
     *
     * @return The content_type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets the content_type
     *
     * @param content_type The content_type to set
     */
    public void setContentType(String content_type) {
        this.contentType = content_type;
    }

    /**
     * Gets the type
     *
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type
     *
     * @param type The type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets the keyLength
     *
     * @return The keyLength
     */
    public int getKeyLength() {
        return keyLength;
    }

    /**
     * Sets the keyLength
     *
     * @param keyLength The keyLength to set
     */
    public void setKeyLength(int keyLength) {
        this.keyLength = keyLength;
    }

    /**
     * Gets the length
     *
     * @return The length
     */
    public int getLength() {
        return length;
    }

    /**
     * Sets the length
     *
     * @param length The length to set
     */
    public void setLength(int length) {
        this.length = length;
    }
}
