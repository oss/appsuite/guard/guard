/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.cipher;

import java.security.Key;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardProperty;

/**
 * {@link GuardCipherService} to encrypt/decrypt data and generate random passwords and keys
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardCipherService {

    /**
     * Encrypts the specified data with the specified {@link Key}
     * 
     * @param data The data to encrypt
     * @param secretKey The secret {@link Key} to use for the encryption
     * @return The encrypted data
     */
    String encrypt(String data, Key secretKey);

    /**
     * Encrypts the specified data with the specified base64 encoded key
     * 
     * @param data The data to encrypt
     * @param base64Key The base 64 key
     * @return The encrypted data
     */
    String encrypt(String data, String base64Key);

    /**
     * Encrypts the specified data with the specified password and the specified salt
     * 
     * @param data The data to encrypt
     * @param password The password to encrypt with
     * @param salt The salt to use
     * @return The encrypted data
     */
    String encrypt(String data, String password, String salt) throws OXException;

    /**
     * Decrypts the specified data with the specified password and the specified salt
     * 
     * @param data The data to decrypt
     * @param password The password to encrypt with
     * @param salt The salt to use
     * @return The decrypted data
     */
    String decrypt(String data, String password, String salt);

    /**
     * Decrypts the specified data with the specified password, the specified salt and the specified key length
     * 
     * @param data The data to decrypt
     * @param password The password to encrypt with
     * @param salt The salt to use
     * @param keyLength The key length
     * @return The decrypted data
     */
    String decrypt(String data, String password, String salt, int keyLength);

    /**
     * Decrypts the specified data with the specified base64 encoded key
     * 
     * @param data The data to decrypt
     * @param base64Key The base 64 encoded key
     * @return The decrypted data
     */
    String decrypt(String data, String base64Key);

    /**
     * Decrypts the specified data with the specified {@link Key}
     * 
     * @param data The data to decrypt
     * @param secretKey The {@link Key} to decrypt the data with
     * @return The decrypted data
     */
    String decrypt(String data, Key secretKey);

    /**
     * Generates, encodes in to base 64 and returns a random key
     * 
     * @return the generated random key in base 64 encoding
     */
    String getRandomKey();

    /**
     * Generates and returns a random password. The length of the password is defines in the {@link GuardProperty}
     * 
     * @param userId  Id of the user
     * @param cid  The context of the user
     * @return The random password
     * @throws OXException if the password cannot be generated
     */
    String generateRandomPassword(int userId, int cid) throws OXException;

}
