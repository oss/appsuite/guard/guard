/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.TextParseException;
import org.xbill.DNS.Type;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.dns.DNSResult;
import com.openexchange.guard.dns.DNSService;
import com.openexchange.guard.dns.ValidationResult;
import com.openexchange.guard.dns.exception.DNSExceptionCodes;
import com.openexchange.guard.dns.internal.FlagTrustingDNSValidator.ValidationMode;

/**
 * {@link DNSServiceImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class DNSServiceImpl implements DNSService {

    private static final List<SRVRecord> EMPTY_LIST = Collections.emptyList();
    private static final ValidationResult VALIDATED = new ValidationResult(true);
    private static final ValidationResult NOT_VALIDATED = new ValidationResult(false);
    private final GuardConfigurationService guardConfigService;

    /**
     * Initializes a new {@link DNSServiceImpl}.
     *
     * @param validator A validator used for validating DNSSEC signatures of records, or null to not validate records
     */
    public DNSServiceImpl(GuardConfigurationService configService) {
        this.guardConfigService = configService;
    }

    /**
     * Get a list of SRV Records for PGP Servers for given domain
     *
     * @param email
     * @return
     * @throws TextParseException
     */
    @Override
    public DNSResult getSrvs(String serviceName, String email, int userId, int cid) throws OXException {
        List<SRVRecord> list = new ArrayList<SRVRecord>();
        ValidatingResolverDecorator validatingResolver = null;
        if (!email.contains("@")) {
            return new DNSResult(EMPTY_LIST, new ValidationResult(false));
        }
        String domain = email.substring(email.indexOf("@") + 1);
        try {
            Lookup lookup = new Lookup(serviceName + domain, Type.ANY);
            boolean allowUnsignedSRVRecords =
                guardConfigService.getBooleanProperty(GuardProperty.allowUnsignedSRVRecords, userId, cid);
            DNSValidator validator = new FlagTrustingDNSValidator(allowUnsignedSRVRecords ? ValidationMode.WARN : ValidationMode.FAIL);
            if (validator != null) {
                //Setting up a DNS validator.
                //The validating resolver  does _NOT_ get called if the DNS result is already in the local DNS cache.
                //Therefore the resolver marks a record as "validated" before it is put into the cache.
                //This allows us to know if the record was marked as validated when obtained from the cache.
                validatingResolver = new ValidatingResolverDecorator(Lookup.getDefaultResolver(), validator);
                //Decorating the resolver with decorators which...
                lookup.setResolver(
                    new HeaderFlagResolverDecorator( //...adds the AD flag to each query
                        validatingResolver, //..validates each DNS response with the given validator
                        Flags.AD));
            }
            Record[] records = lookup.run();
            if (records == null || records.length == 0) {
                //Nothing found
                return new DNSResult(EMPTY_LIST, NOT_VALIDATED);
            }

            //Looking for SRV records
            for (int i = 0; i < records.length; i++) {
                if(records[i] instanceof SRVRecord) {
                    SRVRecord srv = (SRVRecord) records[i];
                    list.add(srv);
                }
            }
            if(!list.isEmpty()){
                //returning found SRV records
                return new DNSResult(
                    list,
                    validatingResolver.getValidationResultFor(records) /* check if the resolver marked the entry as "validated*/);
            }
            //No SRV records found
            return new DNSResult(EMPTY_LIST, NOT_VALIDATED);
        } catch (TextParseException e) {
            throw DNSExceptionCodes.INVALID_NAME_ERROR.create(e, "_hkp._tcp." + domain);
        }
    }

    @Override
    public SRVRecord findNext(int current, List<SRVRecord> records) {
        int index = -1;
        int lowest = 0;
        for (int i = 0; i < records.size(); i++) {
            SRVRecord tocheck = records.get(i);
            if (tocheck.getPriority() > current) {
                if ((lowest == 0) || (tocheck.getPriority() < lowest)) {
                    lowest = tocheck.getPriority();
                    index = i;
                }
            }
        }
        if (index == -1) {
            return (null);
        }
        return (records.get(index));
    }

    @Override
    public String getUrl(String email) throws OXException {
        String url = null;
        try {
            Record[] records = new Lookup("_oxguard." + email, Type.TXT).run();
            if (records == null) {
                return url;
            }
            for (int i = 0; i < records.length; i++) {
                TXTRecord txt = (TXTRecord) records[i];
                if (txt.getName().toString().toLowerCase().contains("oxguard")) { // make sure this is oxguard record
                    url = txt.rdataToString().replace("\"", "").replace("\\", "");
                    if (!url.endsWith("/")) {
                        url = url + "/";
                    }
                    // make sure ssl
                    if (url.startsWith("http:")) {
                        url = url.replace("http:", "https:");
                    }
                    return url;
                }
            }
        } catch (TextParseException e) {
            throw DNSExceptionCodes.INVALID_NAME_ERROR.create(e, "_oxguard." + email);
        }
        return url;
    }
}
