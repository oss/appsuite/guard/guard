/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns.internal;

import org.xbill.DNS.Flags;
import org.xbill.DNS.Message;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import com.openexchange.guard.dns.ValidationResult;

/**
 * {@link FlagTrustingDNSValidator} just checks the present of the <i>AD(=Authorized Data)</i> flag
 * <br>
 * <p><b> PLEASE NOTE: This implementation does not validate the DNS Records by itself. It just trusts the given <i>AD</i> flag from the DNS resolver.</b></p>
 * <p>Please ensure you either run a local DNS resolver which performs DNSSEC validation, or the network from this OX Guard node to the DNS validating resolver is fully trusted.</p>
 * <br>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class FlagTrustingDNSValidator implements DNSValidator {

    /**
     * {@link ValidationMode} The validation mode to use
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since v2.6.0
     */
    public enum ValidationMode {
        /**
         * Only log a warning
         */
        WARN,

        /**
         * Fail on invalid response
         */
        FAIL,
    };

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FlagTrustingDNSValidator.class);
    private final ValidationMode validationMode;

    /**
     * Initializes a new {@link FlagTrustingDNSValidator}.
     *
     * @param validationMode The mode to use during validation
     */
    public FlagTrustingDNSValidator(ValidationMode validationMode) {
        this.validationMode = validationMode;
    }

    /**
     * Checks whether a message has a given flag
     *
     * @param message The message
     * @param flag The flag to check
     * @return True if the message contains the given flag, false otherwise
     */
    private boolean hasFlag(Message message, int flag) {
        return message.getHeader().getFlag(flag);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.dns.internal.dnssec.DNSSECValidator#isValid(org.xbill.DNS.Message)
     */
    @Override
    public ValidationResult isValid(Message answer) {
        boolean hasFlag = hasFlag(answer, Flags.AD);
        String name = "unknown domain";
        Record question = answer.getQuestion();
        if(question != null) {
            name = question.getName().toString();
        }
        if (!hasFlag) {
            Record[] answerSection = answer.getSectionArray(Section.ANSWER);
            if (validationMode == ValidationMode.WARN) {
                if (answerSection != null && answerSection.length > 0) /*only log if the we have got a real answer to prevent log spam*/ {
                    logger.debug("The header of the obtained SRV DNS record for the domain \"{}\" does not contain the AD (Authenticated Data) Flag. Please consider to enable DNSSEC for the domain.", name);
                }
                return null;
            }
            else if (validationMode == ValidationMode.FAIL) {
                if (answerSection != null && answerSection.length > 0) /*only log if the we have got a real answer to prevent log spam*/ {
                    logger.error("The header of the obtained SRV DNS record for the domain \"{}\" does not contain the AD (Authenticated Data) Flag and is discarded.", name);
                }
                return new ValidationResult(false);
            }
        }
        else {
            logger.debug("The header of the obtained SRV DNS record for the doman {} contains the AD (Authentication Data) Flag as expected.", name);
        }
        return new ValidationResult(true);
    }
}
