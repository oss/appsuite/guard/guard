/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.dns;

import java.util.List;
import org.xbill.DNS.SRVRecord;
import com.openexchange.exception.OXException;

/**
 * {@link DNSService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public interface DNSService {

    String getUrl(String email) throws OXException;

    /**
     * Tries to get a service record from the given email's domain
     *
     * @param serviceName The name of the service to fetch
     * @param email An email address containing the domain to search
     * @param userId the ID of the sender/user
     * @param cid fo the sender/user
     * @return A {@link DNSResult} the result of the search
     * @throws OXException
     */
    DNSResult getSrvs(String serviceName, String email, int userId, int cid) throws OXException;

    /**
     * Find the next in the priority list of SRV records
     *
     * @param current
     * @param records
     * @return
     */
    SRVRecord findNext(int current, List<SRVRecord> records);

}
