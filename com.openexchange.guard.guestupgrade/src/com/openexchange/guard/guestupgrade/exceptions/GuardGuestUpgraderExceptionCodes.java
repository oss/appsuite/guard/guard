
package com.openexchange.guard.guestupgrade.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

public enum GuardGuestUpgraderExceptionCodes implements DisplayableOXExceptionCode {

    /** Unable to find guest account for email %1$s'. */
    UNKNOWN_GUEST_ACCOUNT("Unable to find guest account for email '%1$s'.", GuardGuestUpgraderExceptionMessages.UNKNOWN_GUEST_ACCOUNT_MSG, CATEGORY_ERROR, 1),

    /** Already upgraded user with email '%1$s'. */
    ALREADY_UPGRADED("Already upgraded user with email '%1$s'.", GuardGuestUpgraderExceptionMessages.ALREADY_UPGRADED_MSG, CATEGORY_ERROR, 2),

    /** No OX account with email '%1$s'. */
    UNKNOWN_OX_ACCOUNT("No OX account with email '%1$s'.", GuardGuestUpgraderExceptionMessages.UNKNOWN_OX_ACCOUNT_MSG, CATEGORY_ERROR, 3),

    /** Error upgrading account for email '%1$s': '%2$s'. */
    UPGRADE_ERROR("Error upgrading account for email '%1$s': '%2$s'.", GuardGuestUpgraderExceptionMessages.UPGRADE_ERROR_MSG, CATEGORY_ERROR, 4),

    /** Resolved to OX Guest account */
    GUEST_ACCOUNT("Unable to upgrade by email address, email '%1$s' resolves to a guest account", GuardGuestUpgraderExceptionMessages.GUEST_ACCOUNT_MSG, CATEGORY_ERROR, 5),

    OX_ACCOUNT_ERROR("Unable to get account with user_id '%1$d' and context '%2$d'", CATEGORY_ERROR, 6)

    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    private GuardGuestUpgraderExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private GuardGuestUpgraderExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-GUEST-UPGRD";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }

}
