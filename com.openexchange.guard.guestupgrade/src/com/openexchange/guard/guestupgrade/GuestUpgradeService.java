package com.openexchange.guard.guestupgrade;

import com.openexchange.exception.OXException;

/**
 * {@link GuestUpgradeService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public interface GuestUpgradeService {

    /**
     * Upgrades a  guest user to an OX Guard user
     * @param email The email of the guest user
     * @param userid The id of the new OX user
     * @param cid The context of the new OX user
     * @throws OXException
     */
    public void upgrade(String email, String userid, String cid) throws OXException;

    /**
     * Upgrades a guest user to an OX Guard user
     * @param guestUserId The user id of the guest
     * @param guestContextId  The shard-id/context-id of the guest
     * @param userId The user id of the new user to migrate the data to
     * @param contextId The context of the new user to migrate the data to
     * @throws OXException
     */
    public void upgrade(int guestUserId, int guestContextId, int userId, int contextId) throws OXException;
}
