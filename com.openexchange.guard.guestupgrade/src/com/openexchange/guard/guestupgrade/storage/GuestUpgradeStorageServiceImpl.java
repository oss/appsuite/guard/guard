/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guestupgrade.storage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link GuestUpgradeStorageServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuestUpgradeStorageServiceImpl implements GuestUpgradeStorageService {

    GuardDatabaseService guardDatabaseService;

    public GuestUpgradeStorageServiceImpl(GuardDatabaseService guardDatabaseService) {
        this.guardDatabaseService = guardDatabaseService;
    }
    /* (non-Javadoc)
     * @see com.openexchange.guard.guestupgrade.storage.guestUpgradeStorageService#addRecord(int, int, int, int)
     */
    @Override
    public void addRecord(int guestId, int guestCid, int shard, int oxId, int oxCid) throws OXException {
        Connection connection = guardDatabaseService.getWritableForGuard();
        try ( PreparedStatement stmt = connection.prepareStatement(GuardUpgradeStorageServiceSQL.INSERT)) {
            stmt.setInt(1, oxId);
            stmt.setInt(2, oxCid);
            stmt.setInt(3, guestId);
            stmt.setInt(4, guestCid);
            stmt.setInt(5, shard);
            stmt.execute();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backWritableForGuard(connection);
        }

    }

    private Email parseResult(ResultSet rs) throws SQLException {
        if (rs.next()) {
            return new Email (null, rs.getInt("guestId"), rs.getInt("guestCid"), rs.getInt("guestShard"));
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guestupgrade.storage.guestUpgradeStorageService#getGuestRecord(int, int)
     */
    @Override
    public Email getGuestRecord(String email, int oxId, int oxCid) throws OXException {
        Connection connection = guardDatabaseService.getReadOnlyForGuard();
        try ( PreparedStatement stmt = connection.prepareStatement(GuardUpgradeStorageServiceSQL.GET_GUEST)){
            stmt.setInt(1, oxId);
            stmt.setInt(2, oxCid);
            return parseResult(stmt.executeQuery());
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            guardDatabaseService.backReadOnlyForGuard( connection);
        }
    }

}
