/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ldap.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.certificatemanagement.pki.PKIServiceHandler;
import com.openexchange.guard.certificatemanagement.pki.ldap.CachingLdapCertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.ldap.LdapCertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.ldap.LdapPkiService;
import com.openexchange.guard.certificatemanagement.pki.ldap.config.LdapPkiProp;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class Activator extends HousekeepingActivator {

    static final Logger LOGGER = LoggerFactory.getLogger(Activator.class);

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, GenericCacheFactory.class };
    }

    @Override
    protected void startBundle() throws Exception {

        GuardConfigurationService configService = this.getService(GuardConfigurationService.class);
        if (configService.getBooleanProperty(LdapPkiProp.enabled)) {
            LOGGER.info("Starting LDAP PKI lookup service");
            registerService(PKIServiceHandler.class, new LdapPkiService(this,
                new CachingLdapCertificateLookupService(this, new LdapCertificateLookupService(this))));
            trackService(CertificateService.class);
            openTrackers();
        } else {
            LOGGER.info("LDAP PKI lookup service disabled by configuration");
        }

    }

}
