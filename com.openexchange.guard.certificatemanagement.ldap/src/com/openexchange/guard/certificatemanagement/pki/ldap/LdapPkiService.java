/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ldap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.PKIServiceHandler;
import com.openexchange.guard.certificatemanagement.pki.ldap.config.LdapPkiProp;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.server.ServiceLookup;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class LdapPkiService extends PKIServiceHandler {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(LdapPkiService.class);
    }

    /**
     * Initializes a new {@link LdapPkiService}.
     *
     * @param services
     * @param certificateLookupService
     * @param certificateEnrollmentService
     */
    public LdapPkiService(ServiceLookup services, CertificateLookupService certificateLookupService) {
        super(services, certificateLookupService, null);
    }

    @Override
    public boolean handlesThisUser(int userId, int cid) {
        return false;  // No certificate management, only lookup
    }

    @Override
    public boolean enabledForLookup(int userId, int cid) {
        GuardConfigurationService config = getConfigurationService();
        if (config == null) {
            LoggerHolder.LOGGER.error("Missing Guard configuration service");
            return false;
        }
        // Check for configuration
        final String ids = config.getProperty(LdapPkiProp.clientId, userId, cid);
        return (ids != null && !ids.isEmpty());
    }

}
