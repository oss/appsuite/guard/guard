/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ldap;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupResponse;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.impl.CertificateUtil;
import com.openexchange.guard.certificatemanagement.pki.ldap.config.LdapPkiProp;
import com.openexchange.guard.common.session.FakeSession;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.ldap.common.LDAPConnectionProvider;
import com.openexchange.ldap.common.LDAPService;
import com.openexchange.server.ServiceLookup;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ReadOnlySearchRequest;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.util.Base64;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class LdapCertificateLookupService implements CertificateLookupService {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(LdapCertificateLookupService.class);
    }

    private static final String SOURCE_NAME = "LDAP";
    private final ServiceLookup services;

    /**
     *
     * Initializes a new {@link LdapCertificateLookupService}.
     *
     * @param services ServiceLookup
     */
    public LdapCertificateLookupService(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Gets the GuardConfigurationService from ServiceLookup
     *
     * @return GuardConfigurationService
     * @throws OXException Throws exception if not found
     */
    private GuardConfigurationService getConfigService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }

    /**
     * Gets a certificate from LDAP Attribute
     *
     * @param cert Certificate Attribute
     * @return X509Certificate
     * @throws OXException
     */
    private X509Certificate getCertificate(Attribute cert) throws OXException {
        String data;
        if (cert.needsBase64Encoding()) {
            data = Base64.encode(cert.getValueByteArray());
        } else {
            data = cert.getValue();
        }
        // Certificate base64 DER data should start with MI.  Sanity check before creating certificate from data
        if (data != null && data.startsWith("MI")) {
            return CertificateUtil.getCertificate(data, false);
        }

        LoggerHolder.LOGGER.error("Data from LDAP server not recognized as certificate");
        return null;
    }

    /**
     * Escape illegal characters for LDAP search such as () \\ / * null
     *
     * @param email Email address to escape
     * @return Encoded string with characters escaped.
     */
    private String ldapEncode(String email) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < email.length(); i++) {
            char c = email.charAt(i);
            switch (c) {
                case '(':
                    sb.append("\\28");
                    break;
                case ')':
                    sb.append("\\29");
                    break;
                case '\\':
                    sb.append("\\5c");
                    break;
                case '/':
                    sb.append("\\2f");
                    break;
                case '*':
                    sb.append("\\2a");
                    break;
                case '\0':
                    sb.append("\\00");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * Checks a LDAP specified by the id for the email address
     *
     * @param id ClientId to check
     * @param userId UserId of the sender
     * @param cid ContextId of the sender
     * @param email Email address to search
     * @return X509Certificate if found
     * @throws OXException
     */
    private X509Certificate checkLdap(String id, int userId, int cid, String email) throws OXException {
        LDAPConnectionProvider prov = services.getServiceSafe(LDAPService.class).getConnection(id);
        if (prov == null) {
            return null;
        }
        // See if we have a customized attributeName for the search, otherwise default to "certificate"
        String attributeName = getConfigService().getProperty(LdapPkiProp.certificateMapping.getFQPropertyNameWithId(id), userId, cid, "userCertificate");
        LDAPConnection con = prov.getConnection(new FakeSession(userId, cid));
        try {
            ReadOnlySearchRequest request = new SearchRequest(prov.getBaseDN(),
                SearchScope.SUB,
                "mail=" + ldapEncode(IDNUtil.aceEmail(email)),  // ? IDN Encoding
                new String[]
                { attributeName, "userCertificate;binary", "certificateSerialNumber" });
            SearchResultEntry entry = con.searchForEntry(request);
            if (entry != null) {
                // First check configured/default certificate attribute
                Attribute cert = entry.getAttribute(attributeName);
                if (cert != null) {
                    return getCertificate(cert);
                }
                // Fallback check to userCertificate;binary
                cert = entry.getAttribute("userCertificate;binary");
                if (cert != null) {
                    return getCertificate(cert);
                }
            }
        } catch (LDAPException e) {
            LoggerHolder.LOGGER.error("Error checking LDAP server for certificate", e);
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return null;
    }

    /**
     * Check that the certificate is still valid (not expired)
     *
     * @param cert Certificate to check
     * @param email Email address used for error messaging if expired
     * @return True if valid
     * @throws OXException
     */
    private boolean isValid(X509Certificate cert, String email) {
        try {
            cert.checkValidity(new Date());
        } catch (@SuppressWarnings("unused") CertificateNotYetValidException | CertificateExpiredException e) {
            LoggerHolder.LOGGER.error(
                String.format("Certificate retrieved from LDAP expired for %s after %s ", email, cert.getNotAfter().toString()));
            return false;
        }
        return true;
    }

    @Override
    public CertificateLookupResponse getCertificate(String email, int userId, int cid) throws OXException {
        // Check email before sending to LDAP servers
        GuardRatifierService ratifier = services.getServiceSafe(GuardRatifierService.class);
        ratifier.validate(email);
        // Load config service and get configured ldap lookups for the user
        String mapString = getConfigService().getProperty(LdapPkiProp.clientId, userId, cid);
        if (mapString.isEmpty()) {
            return null;  // Nothing configured
        }
        String[] configServices = mapString.split(",");
        // Loop through IDs to see if any have the certificate
        for (String id : configServices) {
            if (!id.isEmpty()) {
                X509Certificate cert = checkLdap(id, userId, cid, email);
                if (cert != null) {
                    // We aren't going to have full chain for the certificate, so can only check the date
                    if (isValid(cert, email)) {
                        return new CertificateLookupResponse(cert, SOURCE_NAME + ": " + id);
                    }
                }
                // Certificate expired/not valid.  Return null
                return null;
            }
        }
        return null;

    }

}
