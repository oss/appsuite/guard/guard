/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ldap;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.CertificateCacheItem;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.caching.GuardCacheModule;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupResponse;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupService;
import com.openexchange.server.ServiceLookup;

/**
 * Caching LDAP certificate lookup service
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.1.0
 */
public class CachingLdapCertificateLookupService implements CertificateLookupService {

    CertificateLookupService delegate;
    private final GenericCache<CertificateCacheItem> cache;
    private final String SOURCE = "S";

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CachingLdapCertificateLookupService.class);
    }

    /**
     * Initializes a new {@link CachingLdapCertificateLookupService}.
     *
     * @param services
     * @param delegate
     * @throws OXException
     */
    public CachingLdapCertificateLookupService(ServiceLookup services, CertificateLookupService delegate) throws OXException {
        GenericCacheFactory cacheFactory = services.getService(GenericCacheFactory.class);
        if (cacheFactory != null) {
            cache = cacheFactory.createCache(GuardCacheModule.LDAP_CERT_CACHE);
        } else {
            cache = null;
        }
        this.delegate = delegate;
    }

    @Override
    public CertificateLookupResponse getCertificate(String email, int userId, int cid) throws OXException {
        if (cache == null) {
            return delegate.getCertificate(email, userId, cid);
        }
        final Serializable cacheKey = cache.createKey(userId, userId, email);
        CertificateCacheItem hit = cache.get(cacheKey);
        if (hit != null) {
            String source;
            try {
                if (hit.getJson().has(SOURCE) && hit.getCertificate() != null) {
                    source = hit.getJson().getString(SOURCE);
                    return new CertificateLookupResponse(hit.getCertificate(), source);
                } else {  // Hit for not found
                    return null;
                }
            } catch (JSONException e) {
                LoggerHolder.LOGGER.error("Error adding cache item for LDAP certificate lookup");
            }

        }
        CertificateLookupResponse result = delegate.getCertificate(email, userId, cid);
        try {
            if (result != null) {
                cache.put(cacheKey, new CertificateCacheItem(result.getCertificate(), new JSONObject().put(SOURCE, result.getSourceName())));
            } else {
                cache.put(cacheKey, new CertificateCacheItem(null, new JSONObject()));
            }
        } catch (JSONException e) {
            LoggerHolder.LOGGER.error("Error adding cache item for LDAP certificate lookup");

        }  // Save result even if null;
        return result;
    }

}
