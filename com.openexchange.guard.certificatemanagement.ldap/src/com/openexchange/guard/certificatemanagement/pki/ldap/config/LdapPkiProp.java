/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ldap.config;

import com.openexchange.guard.configuration.GuardProp;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.1.0
 */
public enum LdapPkiProp implements GuardProp {

    /**
     * If LDAP bundle should be enabled. System-wide configuration.
     */
    enabled(Boolean.FALSE),

    /**
     * Client ID for the LDAP connection
     */
    clientId(""),

    /**
     * If the ldap server doesn't have the standard userCertificate or userCertificate;binary attribute
     * for the user certificate, then define the mapping here. Will be under the clientId, for example
     * com.openexchange.guard.pki.ldap.someserverId.certificateMapping=certificate
     */
    certificateMapping(),

    ;

    private static final String EMPTY = "";

    private static final String PREFIX = "com.openexchange.guard.pki.ldap.";

    private final String fqn;

    private final Object defaultValue;

    /**
     * Initializes a new {@link EjbcaProperty}.
     */
    private LdapPkiProp() {
        this(EMPTY);
    }

    /**
     * Initializes a new {@link EjbcaProperty}.
     *
     * @param fqn
     */
    private LdapPkiProp(Object defaultValue) {
        this(PREFIX, defaultValue);
    }

    /**
     * Initializes a new {@link EjbcaProperty}.
     *
     * @param fqn
     * @param defaultValue
     */
    private LdapPkiProp(String fqn, Object defaultValue) {
        this.fqn = fqn;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully qualified name of the property
     *
     * @return the fully qualified name of the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn + name();
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public <T extends Object> T getDefaultValue(Class<T> cls) {
        if (defaultValue.getClass().isAssignableFrom(cls)) {
            return cls.cast(defaultValue);
        }
        throw new IllegalArgumentException("The object cannot be converted to the specified type '" + cls.getCanonicalName() + "'");
    }

    public String getFQPropertyNameWithId(String id) {
        return fqn + id + "." + name();
    }

}
