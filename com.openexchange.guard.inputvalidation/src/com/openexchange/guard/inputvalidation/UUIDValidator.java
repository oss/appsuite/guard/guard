/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import com.openexchange.guard.inputvalidation.constants.RegexStrings;

/**
 * {@link UUIDValidator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UUIDValidator extends RegexInputValidator{

    private final String prefix;

    /**
     * Initializes a new {@link UUIDValidator}.
     */
    public UUIDValidator() {
        this (null);
    }

    /**
     * Initializes a new {@link UUIDValidator}.
     * @param prefix A prefix which must be in front of the UUID
     */
    public UUIDValidator(String prefix) {
        super(RegexStrings.UIID_UPPER_CASE);
        this.prefix = prefix;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.inputvalidation.InputValidator#isValid(java.lang.Object)
     */
    @Override
    public boolean isValid(String input) {
        if(input == null){
            return false;
        }

        if(prefix != null) {
            //Ensure that the prefix is in front of the UUID
            if(input.startsWith(prefix)) {
                //Cut the prefix in order for UUID regex validation
                input = input.substring(input.indexOf(prefix)+prefix.length());
            }
            else {
                return false;
            }
        }

        return super.isValid(input.toUpperCase());
    }
}
