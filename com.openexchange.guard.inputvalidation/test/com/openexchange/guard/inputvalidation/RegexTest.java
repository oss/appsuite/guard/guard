/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.inputvalidation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Test;
import com.openexchange.exception.OXException;

/**
 * {@link RegexTest}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class RegexTest {

    @Test
    public void testCheckForValidValueShouldReturnTrue() {
        String regex = "^.*?-.*?-.*?$";
        String testInput = "Start of the input - Middle of the input - End of the input";
        assertTrue(new RegexInputValidator(regex).isValid(testInput), "The input should be valid");
    }

    @Test
    public void testCheckForInvalidValueShouldReturnFalse() {
        String regex = "some other regex";
        String testInput = "Start of the input - Middle of the input - End of the input";
        assertFalse(new RegexInputValidator(regex).isValid(testInput), "The input should not be treated as valid");
    }

    @Test
    public void testAssertForValidValueShouldNotThrowException() throws Exception {
        String regex = "^.*?-.*?-.*?$";
        String testInput = "Start of the input - Middle of the input - End of the input :-)";
        new RegexInputValidator(regex).assertIsValid(testInput);
    }

    @Test
    public void testAssertForInvalidValueShouldThrowException() {

        try {
            String regex = "some other regex";
            String testInput = "Start of the input - Middle of the input - End of the input";
            new RegexInputValidator(regex).assertIsValid(testInput);
            fail("An OX exception was expected but not thrown.");
        } catch (OXException e) {
            assertTrue(e.getMessage().contains("GRD-INP-0001"), "Catched exception should contain specific error code");
        }
    }
}
