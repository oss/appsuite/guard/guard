/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mailcreator;

import java.util.List;
import javax.mail.Message.RecipientType;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;

/**
 * {@link MailCreatorService}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public interface MailCreatorService {

    JsonObject createBlankMail();

    JsonObject createBlankOutMail(String lang, int templId) throws OXException;

    JsonObject createBlankGuestMail(String lang, int templId, String host, int userid, int cid, String guestMessage, String from) throws OXException;

    JsonObject createBlankGuestMail(String lang, int templId, String messageID, String host, int userid, int cid, String guestMessage, String from) throws OXException;

    JsonObject createBlankMail(String lang, int templId, String host, int userid, int cid, String template, String guestMessage, String from) throws OXException;

    JsonObject addPlainText(JsonObject email, String data);

    JsonObject addURL(JsonObject email, String data);

    JsonObject addTo(JsonObject mail, String name, String email, RecipientType type);

    JsonObject addTo(JsonObject mail, String name, String email);

    JsonObject addCC(JsonObject mail, String name, String email);

    JsonObject addBCC(JsonObject mail, String name, String email);

    JsonObject addFrom(JsonObject mail, String name, String email);

    JsonObject addSubject(JsonObject mail, String subject);

    JsonObject setContent(JsonObject mail, String content);

    JsonObject noSave(JsonObject email);

    JsonObject getOxPasswordEmail(String to, String sendername, String from, String from2, String password, String lang, int templId, String host, int userid, int cid) throws OXException;

    JsonObject getPasswordEmail(String to, String sendername, String from, String from2, String password, String lang, int templId, String guestmessage, String host, int userid, int cid) throws OXException;

    JsonObject getResetEmail(String to, List<String> from, String password, String lang, int templId, String host, int userid, int cid) throws OXException;

    JsonObject getGuestResetEmail(String to, String resetId, String lang, int templId, String host, int userid, int cid) throws OXException;

    List<String> getFromAddress(String name, String email, int userid, int cid) throws OXException;

    /**
     * Creates Json Email from template
     * @param templ
     * @param to
     * @param from
     * @param host
     * @param userid
     * @param cid
     * @return
     * @throws OXException
     */
    JsonObject templToEmail(String templ, String to, List<String> from, String host, int userid, int cid) throws OXException;

}
