/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.streaming;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;

/**
 * Wraps an org.apache.commons.fileupload.FileItemStream
 */
public class FileItemInputStream extends InputStream {

    private final FileItemIterator fileItemIterator;
    private final String fieldName;
    private String itemName;
    private String contentType;
    private InputStream fileStream;

    /**
     * Constructor
     * 
     * @param fieldName The name of the file item
     * @param fileItemIterator The iterator to a collection of file items
     */
    public FileItemInputStream(String fieldName, FileItemIterator fileItemIterator) {
        this.fieldName = fieldName;
        this.fileItemIterator = fileItemIterator;
    }

    /**
     * Searches for the file item and opens a stream to it
     * 
     * @throws IOException
     * @throws FileUploadException
     */
    private void EnsureStreamIsLoaded() throws IOException, FileUploadException {
        if (fileStream == null) {
            while (fileItemIterator.hasNext()) {
                FileItemStream item = fileItemIterator.next();
                if (item.getFieldName().equals(this.fieldName)) {
                    fileStream = item.openStream();
                    this.itemName = item.getName();
                    this.contentType = item.getContentType();
                    break;
                }
            }
        }
    }

    /**
     * @return The item's name
     * @throws Exception
     */
    public String getItemName() throws Exception {
        EnsureStreamIsLoaded();
        return this.itemName;
    }

    /**
     * @return The item's content type
     * @throws Exception
     */
    public String getContentType() throws Exception {
        EnsureStreamIsLoaded();
        return this.contentType;
    }

    @Override
    public int read() throws IOException {
        try {
            EnsureStreamIsLoaded();
            return fileStream != null ? fileStream.read() : -1;
        } catch (FileUploadException e) {
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public int read(byte[] b) throws IOException {
        try {
            EnsureStreamIsLoaded();
            return fileStream != null ? fileStream.read(b) : -1;
        } catch (FileUploadException e) {
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        try {
            EnsureStreamIsLoaded();
            return fileStream != null ? fileStream.read(b, off, len) : -1;
        } catch (FileUploadException e) {
            throw new IOException(e.getMessage());
        }
    }

}
