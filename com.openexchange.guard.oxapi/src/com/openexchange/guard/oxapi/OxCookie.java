/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.util.ArrayList;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.common.java.Strings;


public class OxCookie {

    private static final Logger LOG = LoggerFactory.getLogger(OxCookie.class);

    private final ArrayList<Cookie> cList = new ArrayList<Cookie>();

    private String oxReaderID = "";

    public OxCookie(Cookie[] cookies) {
        try {
            if (cookies == null) {
                return;
            }
            if (cookies.length > 0) {// Make sure the cookies are present
                for (int i = 0; i < cookies.length; i++) {
                    String name = cookies[i].getName();
                    if (name.startsWith("OxReaderID")) {
                        setOxReaderID(Strings.removeCarriageReturn(cookies[i].getValue()));
                    } else {
                        cList.add(new Cookie(name, cookies[i].getValue()));
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error("Error handling cookie", ex);
        }
    }

    /**
     * Generate cookie header
     *
     * @return
     */
    public String getCookieHeader() {
        StringBuilder sb = new StringBuilder();
        for (Cookie ck : cList) {
            sb.append(ck.getName());
            sb.append("=");
            sb.append(ck.getValue());
            sb.append(";");
        }
        return sb.toString();
    }

    public String getOxReaderID() {
        return oxReaderID;
    }

    public void setOxReaderID(String oxReaderID) {
        this.oxReaderID = oxReaderID;
    }

}
