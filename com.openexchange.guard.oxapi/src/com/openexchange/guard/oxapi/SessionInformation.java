
package com.openexchange.guard.oxapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 *
 * {@link SessionInformation} - Client access for com.openexchange.rest.services.session.SessionRESTService
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class SessionInformation extends AbstractOXCalls {

    private static final String SESSION_REST_PATH = "/preliminary/session/v1/get/";
    private static final String MIME_JSON = "application/json";

    private JsonObject getJson(String url, Cookie[] cookies) throws OXException {

        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient http = poolService.getCookielessClient();
        HttpGet get = new HttpGet(url);
        // Try to add JSESSIONID for routing
        Cookie jsessionId = getJSessionId(cookies);
        if (jsessionId != null) {
            get.setHeader("Cookie", "JSESSIONID=" + jsessionId.getValue());
        }
        get.addHeader("accept", MIME_JSON);
        get.setHeader("User-Agent", Api.USER_AGENT);
        get.addHeader(getAuthenticationHeader());
        HttpResponse response = null;
        try {
            response = http.execute(get);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8))) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode != HttpServletResponse.SC_OK) {
                    throw ApiError.wrongErrorCode(response);
                }
                JsonParser parser = new JsonParser();
                return parser.parse(reader).getAsJsonObject();
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            get.releaseConnection();
        }
    }

    private static final String JSESSIONID = "JSESSIONID";

    /**
     * Find the JSESSION ID cookie from array
     *
     * @param cookies
     * @return Cookie if JSESSIONID present, otherwise null
     */
    private static Cookie getJSessionId(Cookie[] cookies) {
        for (Cookie cookie: cookies) {
            if (JSESSIONID.equals(cookie.getName())) {
                return cookie;
            }
        }
        return null;
    }

    private String getUrl(String sessionId) throws OXException {
        return getMainURI() + SESSION_REST_PATH + sessionId;
    }

    /**
     * Get data regarding a users session
     *
     * @param sessionId  SessionID of the current session
     * @param cookies   Cookies sent by the browser.  Used for routing
     * @return JsonObject containing data about the users session
     * @throws OXException
     */
    public JsonObject getSessionInformation(String sessionId, Cookie[] cookies) throws OXException {
        return getJson(getUrl(sessionId), cookies);
    }
}
