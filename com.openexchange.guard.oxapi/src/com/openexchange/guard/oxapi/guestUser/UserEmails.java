/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.guestUser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.AbstractOXCalls;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.ApiError;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * {@link UserEmails} - gets list of email addresses associated with user
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.0
 */
public class UserEmails extends AbstractOXCalls {

    private static Logger LOG = LoggerFactory.getLogger(UserEmails.class);
    private static final String SESSION_REST_PATH        = "/preliminary/guard/guest/v1/getEmails";
    private static final String MIME_JSON                = "application/json";
    private static final String RESULT_PRIMARY         = "primary";
    private static final String RESULT_ALIASES          = "aliases";

    /**
     * Internal method to create a share link/guest
     *
     * @param url The full URL
     * @param body The JSON body to put
     * @return The result object
     * @throws OXException
     * @throws UnsupportedEncodingException
     * @throws JSONException
     */
    private JSONObject getItem(String url) throws OXException, UnsupportedEncodingException, JSONException {
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();
        HttpGet get = new HttpGet(url);
        get.addHeader("Content-Type", MIME_JSON);
        get.addHeader("accept", MIME_JSON);
        get.setHeader("User-Agent", Api.USER_AGENT);
        get.addHeader(getAuthenticationHeader());
        HttpResponse response = null;
        try {
            response = httpClient.execute(get);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8))) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpServletResponse.SC_OK) {
                    return new JSONObject(reader);
                } else {
                    LOG.error(response.toString());
                    throw ApiError.wrongErrorCode(response);
                }
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            get.releaseConnection();
        }
    }


    /**
     * Creates the JSON body to sent
     *
     * @param sessionId The sender's session ID
     * @param guestEmail The receivers email address
     * @param emailItemId The ID of the item to share
     * @param language The preferred language of the recipient
     * @return A JSONObject ready to be used with the RESTful API
     * @throws JSONException
     */
    private String createURL(int userid, int cid) throws OXException {
        return getUrl() + "/" + cid + "/" + userid;
    }

    /**
     * Creates the full URL to call
     *
     * @return The URL to call
     * @throws OXException
     */
    private String getUrl() throws OXException {
        return getMainURI() + SESSION_REST_PATH;
    }

    /**
     * Internal method to parse the share link from the result object
     *
     * @param result The result object
     * @return The share link
     */
    private String parseResult(JSONObject result) {
        if(result.has(RESULT_PRIMARY)) {
            try {
                return result.getString(RESULT_PRIMARY);
            } catch (JSONException e) {
                return null;
            }
        }
        return null;
    }

    private String[] parseResults (JSONObject result) {
        ArrayList<String> list = new ArrayList<String>();
        if (result.has(RESULT_ALIASES)) {
            try {
                JSONArray array = result.getJSONArray(RESULT_ALIASES);
                Iterator<Object> it = array.iterator();
                while (it.hasNext()) {
                    String email = (String) it.next();
                    list.add(email);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return list.toArray(new String[list.size()]);
    }

    /**
     * Gets all email addresses of a user
     *
     * @param userID - ID of the user
     * @param cid - Context ID of the user
     * @return String array of all email aliases
     * @throws OXException
     */
    public String[] getAllEmails(int userId, int cid) throws OXException {
        try {
            return parseResults(getItem(createURL(userId, cid)));
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Gets all email addresses of a user
     *
     * @param userID - ID of the user
     * @param cid - Context ID of the user
     * @return the users primary email address
     * @throws OXException
     */
    public String getPrimaryEmail(int userId, int cid) throws OXException {
        try {
            return parseResult(getItem(createURL(userId, cid)));
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }
}