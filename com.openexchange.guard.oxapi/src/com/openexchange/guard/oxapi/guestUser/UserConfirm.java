/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.guestUser;

import com.openexchange.exception.OXException;
import com.openexchange.guard.oxapi.AbstractOXCalls;

/**
 * {@link UserConfirm} - Validates a user has an email address
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.0
 */
public class UserConfirm extends AbstractOXCalls {

    /**
     * Validates a user has an email address
     *
     * @param userId - id of the new user
     * @param cid - context of the new user
     * @param email The email address of the receiver
     * @return True if the email exists for the user
     * @throws OXException
     */
    public boolean validateEmail(int userId, int cid, String emailAddr) throws OXException {
        String[] emails = new UserEmails().getAllEmails(userId, cid);
        if (emails == null) return false;
        for (String email: emails) {
            if (email.toLowerCase().trim().equals(emailAddr.toLowerCase().trim())) return true;
        }
        return false;
    }
}