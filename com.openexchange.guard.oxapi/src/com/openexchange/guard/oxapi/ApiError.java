/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.oxapi;

import static com.openexchange.java.Autoboxing.I;
import org.apache.http.HttpResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.oxapi.exceptions.OXApiExceptionCodes;

/**
 * Error handling for API errors
 * 
 */
public class ApiError {

    private static class LoggerHolder {

        static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(ApiError.class);
    }

    /**
     * For wrong html error codes (500), throw error containing reason
     *
     * @param response The httpReponse containing the error
     * @throws OXException WrongError code error containing details if avail. Otherwise Handle_Server_Reponse_error
     */
    public static OXException wrongErrorCode(HttpResponse response) throws OXException {
        if (response != null && response.getStatusLine() != null) {
            try {
                if (response.getStatusLine().getReasonPhrase() != null && !response.getStatusLine().getReasonPhrase().isEmpty()) {
                    return OXApiExceptionCodes.WRONG_ERROR_CODE.create(I(response.getStatusLine().getStatusCode()), response.getStatusLine().getReasonPhrase());
                }
                return OXApiExceptionCodes.WRONG_ERROR_CODE.create(I(response.getStatusLine().getStatusCode()), response.toString());

            } catch (Exception ex) {
                LoggerHolder.LOG.error("Error getting error details from MW", ex);
            }
        }
        return OXApiExceptionCodes.HANDLE_SERVER_RESPONSE_ERROR.create();
    }
}
