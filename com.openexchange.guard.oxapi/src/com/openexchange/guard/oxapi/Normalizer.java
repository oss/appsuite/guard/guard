/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.nio.charset.StandardCharsets;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.guard.oxapi.context.CustomHttpClientContext;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.guard.oxapi.utils.ResponseHelper;

public class Normalizer extends AbstractOXCalls {

    private static Logger LOG = LoggerFactory.getLogger(Normalizer.class);

    public String normalize(String data) {
        try {
            JsonParser parser = new JsonParser();
            JsonObject json = (JsonObject) parser.parse(data);
            Gson gson = new Gson();
            return gson.toJson(normalize(json));
        } catch (Exception ex) {
            LOG.error("Error normalizing data", ex);
            return data;
        }
    }

    /**
     * Send string to backend for normalization. Primarily for PGP Inline
     *
     * @param data
     * @return
     * @throws Exception
     */
    public String normalizeString(String data) {
        try {
            JsonObject content = new JsonObject();
            content.addProperty("content", data);
            JsonObject newcontent = putRequest(content);
            String emailContent = newcontent.get("content").getAsString();
            return emailContent;
        } catch (Exception e) {
            LOG.error("Problem normalizing string ", e);
            return data;
        }
    }

    /**
     * Send email JSON to OX backend for normalization (strip scripts, etc)
     *
     * @param json
     * @return
     */
    public JsonObject normalize(JsonObject json) {
        if (json.has("attachments")) {
            JsonArray attachments = json.get("attachments").getAsJsonArray();
            for (int i = 0; i < attachments.size(); i++) {
                JsonObject attach = attachments.get(i).getAsJsonObject();
                if (attach.has("content")) {
                    if (!attach.get("content").isJsonNull()) {
                        try {
                            String data = attach.get("content").getAsString();
                            JsonObject content = new JsonObject();
                            content.addProperty("content", data);
                            JsonObject newcontent = putRequest(content);
                            String emailContent = newcontent.get("content").getAsString();
                            attach.remove("content");
                            attach.addProperty("content", emailContent);
                        } catch (Exception ex) {
                            LOG.error("Error normalizing data", ex);
                        }
                    }
                }
            }
        }
        return json;
    }

    /**
     * Actual send of data to OX Backend
     *
     * @param data String of JSON data to send
     * @param database The ID of the database
     * @param cid ContextId if pertinent. 0 if not
     * @param write Write or read ony
     * @return
     * @throws Exception
     */
    private JsonObject putRequest(JsonObject content) throws Exception {
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();
        HttpContext context = CustomHttpClientContext.create();

        String url = getMainURI() + "/preliminary/htmlproc/v1/sanitize";
        HttpPut putRequest = new HttpPut(url);

        String data = content.toString();
        StringEntity entity = new StringEntity(data, StandardCharsets.UTF_8);
        entity.setContentType("application/json;charset=UTF-8");
        putRequest.addHeader("accept", "application/json");
        putRequest.addHeader(getAuthenticationHeader());
        putRequest.setHeader("User-Agent", Api.USER_AGENT);
        putRequest.setEntity(entity);
        HttpResponse response = httpClient.execute(putRequest, context);
        JsonObject result = null;
        if (response.getStatusLine().getStatusCode() != 200) {
            try {
                result = ResponseHelper.getJson(response);
            } catch (Exception ex) {
                try {
                    EntityUtils.consume(response.getEntity());
                    putRequest.releaseConnection();
                    HttpClientUtils.closeQuietly(response);
                } catch (Exception e2) {
                    LOG.error("unable to close http stream after error", e2);
                }
                throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
            }
            HttpClientUtils.closeQuietly(response);
            putRequest.releaseConnection();
            return result;
        }
        result = ResponseHelper.getJson(response);
        EntityUtils.consume(response.getEntity());
        HttpClientUtils.closeQuietly(response);
        putRequest.releaseConnection();
        return result;
    }
}
