
package com.openexchange.guard.oxapi.exceptions;

import org.apache.http.Header;
import com.openexchange.exception.OXException;

/**
 * {@link OXApiException} represents an OX backend exception thrown while accessing the backend's HTTP API
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class OXApiException extends OXException {

    private static final long serialVersionUID = 6600807955323345168L;
    private final String backendErrorData;
    private final Header contentTypeHeader;
    private final String error;
    private final String[] errorParams;
    private final String categories;
    private final int category;
    private final String code;
    private final String errorId;
    private final String errorDescription;
    private final String[] errorStack;

    /**
     * Initializes a new {@link OXApiException}.
     *
     * @see {@link OXApiExceptionFactory} for instantiating
     * @param backendErrorData The data sent by the backend
     * @param contentTypeHeader The contentType sent by the backend
     * @param error The error message
     * @param errorParams error parameters
     * @param categories Categories
     * @param category First category id
     * @param code error code
     * @param errorId unique error id
     * @param errorDescription error description
     * @param errorStack the error's stack trace if present, or null if not present
     */
    protected OXApiException(String backendErrorData,
                          Header contentTypeHeader,
                          String error,
                          String[] errorParams,
                          String categories,
                          int category,
                          String code,
                          String errorId,
                          String errorDescription,
                          String[] errorStack) {
        this.backendErrorData = backendErrorData;
        this.contentTypeHeader = contentTypeHeader;
        this.error = error;
        this.errorParams = errorParams;
        this.categories = categories;
        this.category = category;
        this.code = code;
        this.errorId = errorId;
        this.errorDescription = errorDescription;
        this.errorStack = errorStack;
    }

    /**
     * @return original error data returned by the backend
     */
    public String getBackendErrorData() {
        return backendErrorData;
    }

    /**
     * @return The content type header received from the backend
     */
    public Header getContentTypeHader() {
        return contentTypeHeader;
    }

    /**
     * @return The error message
     */
    public String getError() {
        return error;
    }

    /**
     * @return Error parameters
     */
    public String[] getErrorParams() {
        return errorParams;
    }

    /**
     * @return Error categories
     */
    public String getApiErrorCategories() {
        return categories;
    }

    /**
     * @return Category ID
     */
    public int getApiErrorCategory() {
        return category;
    }

    /**
     * @return Error code
     */
    public String getApiErrorCode() {
        return code;
    }

    /**
     * @return Unique error ID
     */
    public String getErrorId() {
        return errorId;
    }

    /**
     * @return Error description
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * @return Error stack, or null if no error stack is available.
     */
    public String[] getErrorStack() {
        return errorStack;
    }
}
