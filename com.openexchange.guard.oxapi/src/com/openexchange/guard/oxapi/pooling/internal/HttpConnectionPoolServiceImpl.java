/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.pooling.internal;

import org.apache.http.client.HttpClient;
import com.openexchange.exception.OXException;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.guard.oxapi.pooling.HttpPoolingStats;

/**
 * Service providing access to a set of connection pools
 */
public class HttpConnectionPoolServiceImpl implements HttpConnectionPoolService {

    private static ConnectionPool cookielessPool;
    private static ConnectionPool cookiePool;
    private static HttpPoolingStats stats;

    /**
     * Initializes all connection pools
     * @throws OXException
     */
    public HttpConnectionPoolServiceImpl() throws OXException {
        cookielessPool = new ConnectionPool("User Auth", ConnectionPool.CookieHandlingMode.CookieHandligDissabled);
        cookiePool = new ConnectionPool("API", ConnectionPool.CookieHandlingMode.CookieHandlingEnabled);
        stats = new SummarizedHttpPoolingStats(cookielessPool.getPoolControl().getTotalStats(),
            cookiePool.getPoolControl().getTotalStats());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.server.connectionPooling.HttpConnectionPoolService#closeAll()
     */
    @Override
    public void closeAll() {
        cookielessPool.close();
        cookiePool.close();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.server.connectionPooling.HttpConnectionPoolService#getCookielessClient()
     */
    @Override
    public HttpClient getCookielessClient() {
        return cookielessPool.getHttpClient();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.server.connectionPooling.HttpConnectionPoolService#getClient()
     */
    @Override
    public HttpClient getClient() {
        return cookiePool.getHttpClient();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.server.connectionPooling.HttpConnectionPoolService#getStats()
     */
    @Override
    public HttpPoolingStats getStats() {
        return stats;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.server.connectionPooling.HttpConnectionPoolService#reset()
     */
    @Override
    public void reset() {
        cookielessPool.reset();
        cookiePool.reset();
    }
}
