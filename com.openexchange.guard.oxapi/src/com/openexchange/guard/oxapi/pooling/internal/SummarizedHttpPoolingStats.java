/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.pooling.internal;

import org.apache.http.pool.PoolStats;
import com.openexchange.guard.oxapi.pooling.HttpPoolingStats;

/**
 * Provides statistics for a set of PoolStats
 */
class SummarizedHttpPoolingStats implements HttpPoolingStats {

    private final PoolStats[] stats;

    /**
     * Constructor
     * 
     * @param stats the set of stats to summarize
     */
    SummarizedHttpPoolingStats(PoolStats... stats) {
        if (stats == null) {
            throw new IllegalArgumentException("parameter stats must not be null");
        }

        this.stats = stats;
    }

    @Override
    public int getAvailable() {
        int available = 0;
        for (PoolStats stat : stats) {
            available += stat.getAvailable();
        }
        return available;
    }

    @Override
    public int getLeased() {
        int leased = 0;
        for (PoolStats stat : stats) {
            leased += stat.getLeased();
        }
        return leased;
    }

    @Override
    public int getPending() {
        int pending = 0;
        for (PoolStats stat : stats) {
            pending += stat.getPending();
        }
        return pending;
    }
}
