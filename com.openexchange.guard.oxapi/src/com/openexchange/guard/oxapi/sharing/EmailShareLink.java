/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.sharing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.AbstractOXCalls;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.ApiError;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * {@link EmailShareLink} - creates a share link/guest for given email item
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class EmailShareLink extends AbstractOXCalls {

    private static Logger LOG = LoggerFactory.getLogger(EmailShareLink.class);
    private static final String SESSION_REST_PATH        = "/preliminary/guard/guest/v1/shareLink/";
    private static final String MIME_JSON                = "application/json";
    private static final String BODY_PARAM_SESSION_ID    = "session";
    private static final String BODY_PARAM_EMAIL         = "email";
    private static final String BODY_PARAM_EMAIL_ITEM_ID = "mailId";
    private static final String RESUL_PARAM_SHARE_LINK   = "shareLink";
    private static final String BODY_PARAM_USER_ID       = "userId";
    private static final String BODY_PARAM_CONTEXT_ID    = "cid";
    private static final String BODY_PARAM_LANGUAGE      = "language";

    /**
     * Internal method to create a share link/guest
     *
     * @param url The full URL
     * @param body The JSON body to put
     * @return The result object
     * @throws OXException
     * @throws JSONException
     */
    private JSONObject putShareItem(String url, JSONObject body) throws OXException, JSONException {
        LOG.debug("Creating new share link for guest: {}", url);
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();
        HttpPut put = new HttpPut(url);
        put.addHeader("Content-Type", MIME_JSON);
        put.addHeader("accept", MIME_JSON);
        put.setHeader("User-Agent", Api.USER_AGENT);
        put.addHeader(getAuthenticationHeader());
        put.setEntity(new StringEntity(body.toString(), StandardCharsets.UTF_8));
        HttpResponse response = null;
        try {
            response = httpClient.execute(put);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8))) {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpServletResponse.SC_OK) {
                    return new JSONObject(reader);
                }
                LOG.error(response.toString());
                throw ApiError.wrongErrorCode(response);
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            put.releaseConnection();
        }
    }

    /**
     * Creates the JSON body to sent
     *
     * @param sessionId The sender's session ID
     * @param guestEmail The receivers email address
     * @param emailItemId The ID of the item to share
     * @return A JSONObject ready to be used with the RESTful API
     * @throws JSONException
     */
    private JSONObject createJSONBody(String sessionId, String guestEmail, String emailItemId, String language) throws JSONException {
        return new JSONObject()
            .put(BODY_PARAM_SESSION_ID, sessionId)
            .put(BODY_PARAM_EMAIL, guestEmail)
            .putSafe(BODY_PARAM_LANGUAGE, language)
            .put(BODY_PARAM_EMAIL_ITEM_ID, emailItemId);
    }

    /**
     * Creates the JSON body to sent
     *
     * @param sessionId The sender's session ID
     * @param guestEmail The receivers email address
     * @param emailItemId The ID of the item to share
     * @param language The preferred language of the recipient
     * @return A JSONObject ready to be used with the RESTful API
     * @throws JSONException
     */
    private JSONObject createJSONBody(int userid, int cid, String guestEmail, String emailItemId, String language) throws JSONException {
        return new JSONObject()
            .put(BODY_PARAM_USER_ID, userid)
            .put(BODY_PARAM_CONTEXT_ID, cid)
            .put(BODY_PARAM_EMAIL, guestEmail)
            .putSafe(BODY_PARAM_LANGUAGE, language)
            .put(BODY_PARAM_EMAIL_ITEM_ID, emailItemId);
    }

    /**
     * Creates the full URL to call
     *
     * @return The URL to call
     * @throws OXException
     */
    private String getUrl() throws OXException {
        return getMainURI() + SESSION_REST_PATH;
    }

    /**
     * Internal method to parse the share link from the result object
     *
     * @param result The result object
     * @return The share link
     */
    private String parseResult(JSONObject result) {
        if(result.has(RESUL_PARAM_SHARE_LINK)) {
            return result.optString(RESUL_PARAM_SHARE_LINK);
        }
        return null;
    }

    /**
     * Creates a share/guest for the given emailItemId
     *
     * @param sessionId The session ID of the user who wants to create the share link (i.e. the sender)
     * @param guestEmail The email address of the receiver
     * @param emailItemId The ID of the item which should be shared with the receiver
     * @return A full link to the shared item
     * @throws OXException
     */
    public String createEmailShare(String sessionId, String guestEmail, String emailItemId, String language) throws OXException {
        try {
            return parseResult(putShareItem(getUrl(), createJSONBody(sessionId, guestEmail, emailItemId, language)));
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Creates a share/guest for the given emailItemId
     *
     * @param sessionId The session ID of the user who wants to create the share link (i.e. the sender)
     * @param guestEmail The email address of the receiver
     * @param emailItemId The ID of the item which should be shared with the receiver
     * @return A full link to the shared item
     * @throws OXException
     */
    public String createEmailShare(int userId, int cid, String guestEmail, String emailItemId, String language) throws OXException {
        try {
            return parseResult(putShareItem(getUrl(), createJSONBody(userId, cid, guestEmail, emailItemId, language)));
        } catch (JSONException e) {
            throw GuardCoreExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
    }
}