/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi.capabilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.oxapi.AbstractOXCalls;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.context.CustomHttpClientContext;
import com.openexchange.guard.oxapi.osgi.Services;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;

/**
 * Retrieve capabilities information from backend
 *
 * @author greg
 *
 */
public class Capabilities extends AbstractOXCalls {

    private static Logger LOG = LoggerFactory.getLogger(Capabilities.class);

    public static final String WEBMAIL_CAPABILITY = "webmail";
    public static final String GUARD_CAPABILITY = "guard";

    /**
     * Get Json Array of capabilities
     *
     * @param url
     * @return
     * @throws Exception
     */
    private JsonArray getQuietOgRequest(String url) throws Exception {
        HttpConnectionPoolService poolService = Services.getService(HttpConnectionPoolService.class);
        HttpClient httpClient = poolService.getClient();
        HttpContext context = CustomHttpClientContext.create();
        HttpGet getRequest = new HttpGet(url);
        getRequest.addHeader("accept", "application/json");
        getRequest.addHeader(getAuthenticationHeader());
        getRequest.setHeader("User-Agent", Api.USER_AGENT);
        HttpResponse response = httpClient.execute(getRequest, context);
        if (response.getStatusLine().getStatusCode() != 200) {
            try {
                EntityUtils.consume(response.getEntity());
                getRequest.releaseConnection();
                HttpClientUtils.closeQuietly(response);
            } catch (Exception e2) {
                LOG.error("unable to close http stream after error", e2);
            }
            String resp = response.getStatusLine().getReasonPhrase();
            LOG.debug("Unable to get user capabilities from backend " + response.getStatusLine().getReasonPhrase());
            LOG.error(resp);
            throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
        JsonParser parser = new JsonParser();
        JsonArray result = (JsonArray) parser.parse(reader);
        reader.close();
        EntityUtils.consume(response.getEntity());
        HttpClientUtils.closeQuietly(response);
        getRequest.releaseConnection();
        return result;
    }

    /**
     * Get a full JSON Array of all capabilities for a user
     *
     * @param id id of the user
     * @param cid context of the user
     * @return
     * @throws Exception
     */
    public JsonArray getUserCapabilities(int id, int cid) throws OXException  {
        try {
            String parameters = cid + "/" + id;
            String url = getMainURI() + "/preliminary/capabilities/v1/all/" + parameters;
            return getQuietOgRequest(url);
        }
        catch(OXException oxe) {
            throw oxe;
        }
        catch(Exception e) {
            LOG.error("Error while getting user capabilities: " + e.getMessage());
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Check if the user has a given capability
     *
     * @param capability String of the capability (the id)
     * @param id id of the user
     * @param cid context of the user
     * @return
     * @throws OXException
     * @throws Exception
     */
    public boolean hasUserCapability(String capability, int id, int cid) throws OXException {
        JsonArray capabilities = getUserCapabilities(id, cid);
        for (int i = 0; i < capabilities.size(); i++) {
            JsonObject cap = capabilities.get(i).getAsJsonObject();
            if (cap.has("id")) {
                if (cap.get("id").getAsString().equals(capability)) {
                    return true;
                }
            }
        }
        return false;
    }
}
