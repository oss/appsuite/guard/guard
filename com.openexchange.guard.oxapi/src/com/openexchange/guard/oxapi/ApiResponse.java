/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxapi;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.util.EntityUtils;

/**
 * {@link ApiResponse} - Helper wrapper for handling API response streams
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.1
 */
public class ApiResponse implements AutoCloseable{
    
    private final HttpResponse response;

    /**
     * Initializes a new {@link ApiResponse}.
     * @param response the response object
     */
    ApiResponse(HttpResponse response){
        this.response = response;
    }

    /**
     * Gets an InputStream for reading the content
     * @return an InputStream of the content
     * @throws IllegalStateException
     * @throws IOException
     */
    public InputStream getContent() throws IllegalStateException, IOException {
        return this.response.getEntity().getContent();
    }
    
    /**
     * Reads the whole content as byte array
     * @return the content
     * @throws IOException
     */
    public byte[] readContent() throws IOException {
        return EntityUtils.toByteArray(response.getEntity());
    }
    
    /**
     * Gets the response's content type
     * @return the content type header of the underlying response entity
     */
    public Header getContentType() {
        return response.getEntity().getContentType();
    }

    /* (non-Javadoc)
     * @see java.lang.AutoCloseable#close()
     */
    @Override
    public void close() {
        HttpClientUtils.closeQuietly(response);
    }
    
    
}
