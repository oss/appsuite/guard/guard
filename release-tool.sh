#!/bin/bash

while getopts v:c: flag
do 
  case "${flag}" in
    v) version=${OPTARG};;
    c) changelog=${OPTARG};;
  esac
done

if [ -z $version ]; then 
  echo "Version required.  Use -v"
  exit 1
fi

if [ -z $changelog ]; then changelog=$version
fi

cd "changelog"
yarn
getchangelog="node index.js --unreleased-only --latest-version $changelog"
$getchangelog
status=$?

if [ $status -eq 1 ]; then 
  echo "Failed to gen changelog"
  exit 1
fi

cd ..
sed -ri "s/^Version:\s[0-9]+\.[0-9]+\.[0-9]+/Version: $version/g" ./com.openexchange.guard/META-INF/MANIFEST.MF

read -p "Commit? (y/n) " yn

case $yn in 
	[yY] ) echo committing changelog...;
        git commit CHANGELOG.md -m "Update CHANGELOG";
	echo committing version bump....;
	git commit com.openexchange.guard/META-INF/MANIFEST.MF -m "Bump version";;

esac

read -p "Tag with version $version? (y/n) " yn

case $yn in 
	[yY] ) echo tagging...;
        git tag -a $version -m "$version release"
        tagged=true;;
esac

read -p "Push? (y/n) " yn

case $yn in
  [yY] ) echo pushing...;
       if [ "$tagged" = true ] ; then
          git push origin tag $version
       fi
       git push
esac

