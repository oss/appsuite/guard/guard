/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import java.util.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.impl.exceptions.AccountCreationExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;

public class AccountCreationServiceImpl implements AccountCreationService {

    private RecipKey createUser(RecipKey recipKey) throws OXException {

        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        KeyCreationService keyCreationService = Services.getService(KeyCreationService.class);
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        if (!recipKey.isNewKey()) {
            throw AccountCreationExceptionCodes.ACCOUNT_ALREADY_EXISTS.create();
        }

        // When creating a new key for a recipient, we will always create a recovery in case of lost email, etc
        final boolean createRecovery = true;
        final boolean markNewKeyAsCurrent = true;
        final String newRandomPassword = cipherService.generateRandomPassword(recipKey.getSenderUserId(), recipKey.getSenderCid());
        //Create
        GuardKeys keys = keyCreationService.create(recipKey.getUserid(),
            recipKey.getCid(),
            recipKey.getName(),
            recipKey.getEmail(),
            newRandomPassword,
            recipKey.getLocale(),
            markNewKeyAsCurrent,
            createRecovery,
            recipKey.getSenderUserId(),
            recipKey.getSenderCid());
        //Store
        final boolean isUserCreatedKey = false;
        keys = keyService.storeKeys(keys, isUserCreatedKey);
        keyService.setCurrentKey(keys);

        //Setting the created keys and return them
        recipKey.setPGPPublicKeyRing(keys.getPGPPublicKeyRing());
        recipKey.setPubkey(keys.getPublicKey());
        if (recipKey.isGuest()) {
            recipKey.setUserid(keys == null ? -1 : keys.getUserid());
            recipKey.setNewGuestPass(newRandomPassword);
        }
        recipKey.setPgp(true);
        //Mark the key as new created key
        recipKey.setNewCreated();
        return recipKey;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.AccountCreationService#createUserFor(com.openexchange.guard.keymanagement.commons.RecipKey)
     */
    @Override
    public RecipKey createUserFor(RecipKey recipKey) throws OXException {
        recipKey = Objects.requireNonNull(recipKey, "recipKey must not be null");
        return createUser(recipKey);
    }
}
