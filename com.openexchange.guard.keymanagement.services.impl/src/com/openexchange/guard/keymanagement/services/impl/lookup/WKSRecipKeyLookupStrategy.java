/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.AbstractRecipKeyLookupStrategy;
import com.openexchange.guard.wksclient.WKSClientService;
import com.openexchange.guard.wksclient.WKSResult;

/**
 * {@link WKSRecipKeyLookupStrategy} searches for a {@link RecipKey} by performing a WKS query.
 * <p>
 * WKS is the "OpenPGP Web Key Service" - a RFC draft by W.Koch
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class WKSRecipKeyLookupStrategy extends AbstractRecipKeyLookupStrategy {

    private int senderUserId, senderContextId;

    public WKSRecipKeyLookupStrategy(int userId, int cid) {
        this.senderContextId = cid;
        this.senderUserId = userId;
    }

    private boolean isValid(Optional<WKSResult> result) {
        return result.isPresent() && super.hasValidEncryptionKey(result.get().getPublicKeyRing());
    }

    private RecipKey toRecipKey(String email, WKSResult result) {
        RecipKey recipKey = new RecipKey(result.getKeySource());
        recipKey.setEmail(email);
        recipKey.setPgp(true);
        recipKey.setExpired(result.isExpired());
        recipKey.setPGPPublicKeyRing(result.getPublicKeyRing());
        return recipKey ;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.lookup.RecipKeyLookupStrategy#lookup(java.lang.String, int)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        WKSClientService wksClient = Services.getService(WKSClientService.class);
        Optional<WKSResult> result = wksClient.find(email, senderUserId, senderContextId, timeout);
        if(isValid(result)) {
            return toRecipKey(email, result.get());
        }
        return null;
    }
}
