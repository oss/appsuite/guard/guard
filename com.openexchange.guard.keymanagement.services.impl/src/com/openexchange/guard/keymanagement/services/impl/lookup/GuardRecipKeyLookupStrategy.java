/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.AbstractRecipKeyLookupStrategy;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link GuardRecipKeyLookupStrategy} searches for a {@link RecipKey} through regular OX Guard users
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GuardRecipKeyLookupStrategy extends AbstractRecipKeyLookupStrategy{

    private static final Logger logger = LoggerFactory.getLogger(GuardRecipKeyLookupStrategy.class);
    private int userId, cid;

    public GuardRecipKeyLookupStrategy () {
        this.userId = 0;
        this.cid = 0;
    }

    public GuardRecipKeyLookupStrategy(int senderUserId, int senderContextId) {
        this.userId = senderUserId;
        this.cid = senderContextId;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.impl.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        GuardKeys key = Services.getService(GuardKeyService.class).getKeys(email);
        if (key == null && cid != 0 && userId != 0) {  // Guard-286, handle sending to users own alias
            UserService userService = Services.getService(UserService.class);
            if (userService != null) {
                User user = userService.getUser(userId, cid);
                String[] aliases = user.getAliases();
                if (aliases != null) {
                    for (String alias : aliases) {  // check if email is alias of the sender
                        if (email.toLowerCase().equals(alias.toLowerCase())) {
                            key = Services.getService(GuardKeyService.class).getKeys(userId, cid);
                        }
                    }
                }
            }
        }
        return (key != null) && hasValidEncryptionKey(key) ? new RecipKey(key) : null;
    }

}
