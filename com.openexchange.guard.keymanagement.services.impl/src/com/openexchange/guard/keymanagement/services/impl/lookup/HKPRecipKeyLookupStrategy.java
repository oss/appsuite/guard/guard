/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.lookup;

import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.hkpclient.services.RemoteKeyResult;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.impl.osgi.Services;
import com.openexchange.guard.keymanagement.services.lookup.AbstractRecipKeyLookupStrategy;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link HKPRecipKeyLookupStrategy} performs a HKP lookup in order to find a {@link RecipKey}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class HKPRecipKeyLookupStrategy extends AbstractRecipKeyLookupStrategy {

    private final String hkpClientToken;
    private int senderUserId, senderContextId;

    /**
     * Initializes a new {@link HKPRecipKeyLookupStrategy}.
     * 
     * @param hkpClientToken The client token to use for outgoing HKP requests
     * @param senderUserId The id of the sender
     * @param senderContextId
     */
    public HKPRecipKeyLookupStrategy(String hkpClientToken, int senderUserId, int senderContextId) {
        this.hkpClientToken = hkpClientToken;
        this.senderUserId = senderUserId;
        this.senderContextId = senderContextId;
    }

    /**
     * Internal method to convert a HKP {@link RemoteKeyResult} into a {@link RecipKey}.
     *
     * @param email The email
     * @param remoteResult The results to convert to a {@link RecipKey}
     * @return The {@link RecipKey} constructed of the given {@link RemoteKeyResult}
     */
    private RecipKey toRecipKey(String email, RemoteKeyResult remoteResult) {
        PGPPublicKeyRing publicKeyRing = remoteResult.getRing();
        RecipKey recipKey = new RecipKey(remoteResult.getSource());
        recipKey.setPGPPublicKeyRing(publicKeyRing);
        recipKey.setEmail(email);
        recipKey.setPgp(true);
        recipKey.setExpired(PGPKeysUtil.checkAllExpired(publicKeyRing));
        return recipKey;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.keymanagement.services.impl.RecipKeyLookupStrategy#lookup(java.lang.String)
     */
    @Override
    public RecipKey lookup(String email, int timeout) throws OXException {
        Collection<RemoteKeyResult> results = Services.getService(HKPClientService.class).find(hkpClientToken, email, senderUserId, senderContextId, timeout);
        if(results != null && !results.isEmpty()) {
            Iterator<RemoteKeyResult> it = results.iterator();
            while (it.hasNext()) {
                RemoteKeyResult result = it.next();
                if (result != null && hasValidEncryptionKey(result.getRing())) {
                    return toRecipKey(email, result);
                }
            }
        }
        return null;
    }
}
