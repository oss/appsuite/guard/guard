/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link GuardMasterKeyExceptionMessages}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardMasterKeyExceptionMessages implements LocalizableStrings {

    // Technical error.  Malformed oxguardpass.  Unlikely to be displayed to user
    public static final String MALFORMED_MASTER_KEY_FILE_MSG = "Malformed 'oxguardpass' file in '%1$s'.";
    // Technical error.  Properties directory does not exist.  Unlikely to be displayed to user
    public static final String PROPS_DIR_NOT_EXIST_MSG = "The OX Guard properties directory does not exist.";

    private GuardMasterKeyExceptionMessages() { }
}
