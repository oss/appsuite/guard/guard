/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.impl.osgi;

import java.security.Security;
import java.util.ArrayList;
import java.util.Collection;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.contact.provider.composition.IDBasedContactsAccessFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.keysources.CompositeKeyPairSource;
import com.openexchange.guard.keymanagement.keysources.DbCachingKeyPairSource;
import com.openexchange.guard.keymanagement.keysources.RealtimeKeyPairSource;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.ContactKeyService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.ImportSystemPublicKey;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.keymanagement.services.KeyImportService;
import com.openexchange.guard.keymanagement.services.KeyPairSource;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.PasswordChangeService;
import com.openexchange.guard.keymanagement.services.PublicExternalKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.keymanagement.services.impl.AccountCreationServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.ContactKeyServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.GuardKeyServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.ImportSystemPublicKeyImpl;
import com.openexchange.guard.keymanagement.services.impl.KeyCreationServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.KeyImportServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.KeyRecoveryServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.PasswordChangeServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.PublicExternalKeyServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.RecipKeyServiceImpl;
import com.openexchange.guard.keymanagement.services.impl.exceptions.GuardMasterKeyExceptionCodes;
import com.openexchange.guard.keymanagement.services.impl.masterKey.CachingMasterKeyService;
import com.openexchange.guard.keymanagement.services.impl.masterKey.IndexMasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.keymanagement.storage.PGPKeysStorage;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.wksclient.WKSClientService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pgp.keys.parsing.PGPKeyRingParser;
import com.openexchange.user.UserService;

/**
 * {@link KeyManagementServicesImplActivator}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeyManagementServicesImplActivator extends HousekeepingActivator {

    private static final Logger LOG = LoggerFactory.getLogger(KeyManagementServicesImplActivator.class);
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardCipherFactoryService.class, GuardConfigurationService.class, GuardShardingService.class, GuardRatifierService.class,
                                KeyTableStorage.class, EmailStorage.class, PGPKeysStorage.class, OGPGPKeysStorage.class,
                                OXUserService.class, GuardAntiAbuseService.class, GuardSessionService.class, GuardTranslationService.class,
            HKPClientService.class, WKSClientService.class, PGPKeyRingParser.class, IDBasedContactsAccessFactory.class, SecondFactorService.class };
    }

    @Override
    protected Class<?>[] getOptionalServices() {
        return new Class<?>[] { AutoCryptService.class, UserService.class };
    }

    private KeyPairSource[] getKeyPairCreationStrategies(GuardConfigurationService guardConfigurationService){
        Collection<KeyPairSource >strategies = new ArrayList<KeyPairSource>();
        if(guardConfigurationService.getBooleanProperty(GuardProperty.rsaCache)) {
            //Using pre generated Key data from the key cache
            strategies.add(new DbCachingKeyPairSource(this));
        }
        strategies.add(new RealtimeKeyPairSource(guardConfigurationService));
        return strategies.toArray(new KeyPairSource[strategies.size()]);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        LOG.info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);
        Security.addProvider(new BouncyCastleProvider());

        final GuardConfigurationService guardConfigurationService = getService(GuardConfigurationService.class);
        final GuardCipherService guardCipherService = getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);

        // Register key services
        GuardKeyService keyService = new GuardKeyServiceImpl();
        registerService(GuardKeyService.class, keyService);
        trackService(GuardKeyService.class);

        //register the key recovery service
        KeyRecoveryService keyRecoveryService = new KeyRecoveryServiceImpl(this);
        registerService(KeyRecoveryService.class, keyRecoveryService);
        trackService(KeyRecoveryService.class);

        //create and registering the Key creation service
        KeyCreationService keyCreationService = new KeyCreationServiceImpl(this, guardCipherService);
        KeyPairSource keyPairSource = new CompositeKeyPairSource(getKeyPairCreationStrategies(guardConfigurationService));
        keyCreationService.setKeyPairSource(keyPairSource);
        registerService(KeyCreationService.class, keyCreationService);
        trackService(KeyCreationService.class);

        // Password change service
        PasswordChangeService passService = new PasswordChangeServiceImpl();
        registerService(PasswordChangeService.class, passService);
        trackService(PasswordChangeService.class);

        //Register the RecipKey lookup service
        registerService(RecipKeyService.class, new RecipKeyServiceImpl(keyService));
        trackService(RecipKeyService.class);

        //Service for external public key handling
        PublicExternalKeyService publicExternalKeyService = new PublicExternalKeyServiceImpl(this);
        registerService(PublicExternalKeyService.class, publicExternalKeyService);

        //Service for getting keys per contact
        registerService(ContactKeyService.class, new ContactKeyServiceImpl(this));

        //Service for importing client created, custom keys
        registerService(KeyImportService.class, new KeyImportServiceImpl(this));
        //Service for importing keys from command line
        registerService(ImportSystemPublicKey.class, new ImportSystemPublicKeyImpl());

        //Account creation Service
        registerService(AccountCreationService.class, new AccountCreationServiceImpl());

        // Master key service
        MasterKeyService mKeyService = new CachingMasterKeyService(new IndexMasterKeyService(this, guardCipherService), guardCipherService);
        registerService(MasterKeyService.class, mKeyService);
        trackService(MasterKeyService.class);
        openTrackers();
        try {
            MasterKey mKey = mKeyService.getMasterKey(0, true);  // confirm we can load database values
            mKeyService.getDecryptedClientKey(mKey);  // confirm oxguardpass file is correct
        } catch (OXException ex) {
            if (ex.getExceptionCode().equals(GuardMasterKeyExceptionCodes.INDEXED_MASTER_PASSWORD_FILE_MISSING)) {
                LOG.error("Missing oxguardpass file in etc directory.  Guard will not function properly");
            }
            // Keys not yet created
            if (ex.getExceptionCode().equals(GuardMasterKeyExceptionCodes.UNABLE_TO_RETIREVE_MASTER_PASSWORD)) {
                try {
                    mKeyService.createKey(0);
                    // Creating keys is very cpu demanding and takes time.  Possible rush condition in multi pod setup
                    // If duplicate key error, try again
                } catch (OXException exc) {
                    if (exc.getExceptionCode().equals(GuardCoreExceptionCodes.SQL_ERROR) && exc.getLogMessage().contains("Duplicate")) {
                        Thread.sleep(1000);  // Pause for database sync, other pod startup
                        MasterKey mKey = mKeyService.getMasterKey(0, true);  // try again
                        mKeyService.getDecryptedClientKey(mKey);  // confirm oxguardpass file is correct
                    } else {
                        throw exc;
                    }
                }
            }
            if (ex.getExceptionCode().equals(GuardAuthExceptionCodes.BAD_PASSWORD)) {  // Oxguardpass file bad
                throw GuardMasterKeyExceptionCodes.UNABLE_TO_RETIREVE_MASTER_PASSWORD.create();
            }
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.osgi.DeferredActivator#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        org.slf4j.LoggerFactory.getLogger(KeyManagementServicesImplActivator.class).info("Stopping bundle: {}", context.getBundle().getSymbolicName());
        unregisterService(KeyCreationService.class);
        Services.setServiceLookup(null);
    }
}
