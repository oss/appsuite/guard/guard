/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.creation;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus.Status;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EJBCAResponse;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;
import com.openexchange.guard.certificatemanagement.pki.impl.CertificateUtil;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class AbstractCsrResponse extends EJBCAResponse {

    /**
     * Initializes a new {@link AbstractCsrResponse}.
     *
     * @param json
     */
    public AbstractCsrResponse(JSONObject json) {
        super(json);
    }

    /**
     * Get the current status. Will be done if json has certificate
     * Fail if json is empty/null
     *
     * Will likely need override for specific calls
     *
     * @return
     */
    public Status getStatus() {
        if (json == null)
            return Status.Rejected;
        if (json.has("certificate") || json.has("error_message"))
            return Status.Done;
        return Status.Pending;
    }

    /**
     * Returns error message from json if present
     *
     * @return
     */
    public String getError() throws OXException {
        if (json != null && json.has("error_message")) {
            try {
                return json.getString("error_message");
            } catch (JSONException e) {
                throw PKIExceptionCodes.RESPONSE_ERROR.create(e.getMessage());
            }
        }
        return null;
    }

    /**
     * Pulls certificate from json response
     *
     * @return X509Certificate from json if present
     * @throws OXException
     */
    public X509Certificate getCertificate() throws OXException {
        if (!json.has("certificate"))
            return null;
        String certString;
        try {
            certString = json.getString("certificate");
        } catch (JSONException e) {
            throw PKIExceptionCodes.RESPONSE_ERROR.create(e.getMessage());
        }
        return CertificateUtil.getCertificate(certString, false);
    }

    /**
     * Gets the certificate chain from the response if present
     *
     * @return ArrayList of the certificate chain
     * @throws OXException
     */
    public List<X509Certificate> getChain() throws OXException {
        if (!json.has("certificate_chain")) {
            return Collections.emptyList();
        }
        ArrayList<X509Certificate> certs = new ArrayList<X509Certificate>();
        try {
            JSONArray chain = json.getJSONArray("certificate_chain");
            for (int i = 0; i < chain.length(); i++) {
                String cert = chain.getString(i);
                final X509Certificate chainObj = CertificateUtil.getCertificate(cert, false);
                certs.add(chainObj);
            }
        } catch (JSONException e) {
            throw PKIExceptionCodes.RESPONSE_ERROR.create(e.getMessage());
        }
        return certs;
    }

}
