/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup;

import java.security.cert.X509Certificate;
import java.util.Set;
import com.openexchange.exception.OXException;

public interface CertificateChainService {

    /**
     * Get chain of certificates for specified certificate associated with the certificate response
     *
     * @param cert Certificate to check
     * @param userId
     * @param cid
     * @return Set of certificates in chain
     * @throws OXException
     */
    Set<X509Certificate> getChain(X509Certificate cert, int userId, int cid) throws OXException;

    /**
     * Get chain of certificates for specified certificate associated with the certificate response
     *
     * @param resp Response that was used to generate the certificate
     * @param cert Certificate to check
     * @param userId
     * @param cid
     * @return Set of certificates in chain
     * @throws OXException
     */
    Set<X509Certificate> getChain(CertificateResponse resp, X509Certificate cert, int userId, int cid) throws OXException;
}
