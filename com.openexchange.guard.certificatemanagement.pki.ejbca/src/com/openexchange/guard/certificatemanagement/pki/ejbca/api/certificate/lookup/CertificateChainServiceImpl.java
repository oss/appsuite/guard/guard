/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup;

import java.io.Serializable;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.CertificateCacheItem;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.caching.GuardCacheModule;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.CertificateApi;
import com.openexchange.guard.certificatemanagement.pki.impl.CertificateUtil;
import com.openexchange.server.ServiceLookup;

/**
 * Implements CertificateChainService. Cached for improved performance
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateChainServiceImpl implements CertificateChainService {


    ServiceLookup services;
    private final GenericCache<CertificateCacheItem> responseCache;

    public CertificateChainServiceImpl(ServiceLookup services) throws OXException {
        this.services = services;
        GenericCacheFactory cacheFactory = services.getServiceSafe(GenericCacheFactory.class);
        responseCache = cacheFactory.createCache(GuardCacheModule.EJBCA);
    }

    @Override
    public Set<X509Certificate> getChain(CertificateResponse resp, X509Certificate cert, int userId, int cid) throws OXException {
        Set<X509Certificate> chain = new HashSet<X509Certificate>();
        boolean done = false;
        int i = 0;
        CertificateResponse toCheck = resp;
        CertificateApi api = services.getServiceSafe(CertificateApi.class);
        while (!done && i < 10) {
            i++;  // sanity check.  Won't be chains more than 10 deep
            if (toCheck.getCaFingerprint() == null)
                break;  // failing
            if (toCheck.getCaFingerprint().equals(toCheck.getFingerprint())) {  // We are at top CA
                done = true;
                break;
            }
            final String caFP = toCheck.getCaFingerprint();
            final String issuerSN = toCheck.getIssuerDN();

            final Serializable cacheKey = responseCache.createKey(caFP);
            CertificateCacheItem cacheItem = responseCache.get(cacheKey);
            if (cacheItem != null) {  // Cache hit.  Add to chain and continue
                chain.add(cacheItem.getCertificate());
                toCheck = new CertificateResponse(cacheItem.getJson());
                continue;
            }
            // Attempt to retrieve from server
            List<CertificateResponse> result = api.searchCert(issuerSN, userId, cid);
            boolean found = false;
            for (CertificateResponse c : result) {
                // Possible we might have more than one cert matching the subject.  Confirm fingerprint
                if (caFP.equals(c.getFingerprint())) {
                    toCheck = c;
                    X509Certificate caCert = CertificateUtil.getCertificate(c.getBase64Cert(), true);
                    chain.add(caCert);
                    responseCache.put(cacheKey, new CertificateCacheItem(caCert, c.getJson()));
                    found = true;
                    break;
                }
            }
            // Not found
            if (!found)
                done = true;

        }

        return chain;
    }

    @Override
    public Set<X509Certificate> getChain(X509Certificate cert, int userId, int cid) throws OXException {
        CertificateApi api = services.getServiceSafe(CertificateApi.class);
        List<CertificateResponse> result = api.searchCert(cert.getSerialNumber().toString(10), userId, cid);
        if (result != null) {
            for (CertificateResponse c : result) {
                // Serial number will match due to search.  Just confirm subject same.
                if (c.getSubjectDN().equals(cert.getSubjectX500Principal().getName())) {
                    return getChain(c, cert, userId, cid);
                }
            }
        }
        return Collections.emptySet();
    }

}
