/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EJBCARequest;

/**
 * Generates a request data for certificate search using the EJBCA search V2.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateSearchRequest extends EJBCARequest {

    JSONObject json;
    private boolean sortUp;
    private String subject;

    public CertificateSearchRequest() {
        json = new JSONObject();
        JSONObject pagination = new JSONObject();
        try {
            pagination.put("page_size", 100);
            pagination.put("current_page", 1);
            json.put("pagination", pagination);
        } catch (JSONException e) {
            // Not going to happen
        }
        sortUp = true;
    }

    /**
     * Sets the subject search string
     *
     * @param subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Order of sort
     *
     * @param up
     */
    public void setSortUp(boolean up) {
        this.sortUp = up;
    }

    @Override
    public String getString() throws OXException {
        JSONArray criteria = new JSONArray();
        JSONObject sub = new JSONObject();
        try {
            sub.put("property", "QUERY");
            sub.put("value", subject);
            sub.put("operation", "EQUAL");
            criteria.put(sub);
            JSONObject status = new JSONObject();
            status.put("property", "STATUS");
            status.put("value", "CERT_ACTIVE");
            status.put("operation", "EQUAL");
            criteria.put(status);
            json.put("criteria", criteria);

            JSONObject sort = new JSONObject();
            sort.put("property", "UPDATE_TIME");
            sort.put("operation", sortUp ? "ASC" : "DESC");
            json.put("sort", sort);
            return json.toString();
        } catch (JSONException e) {
            throw OXException.general("JSON Error with Search request", e);
        }

    }

}
