/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.creation;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateSigningRequest;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EJBCARequest;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;

/**
 * Certificate Signing Request for EJBCA call
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CsrCertificateRequest extends EJBCARequest {

    private String username;
    private String password;
    private String email;
    private CertificateConfig config;
    private CertificateSigningRequest csr;

    /**
     * Creates a certificate signing request
     * Initializes a new {@link CsrCertificateRequest}.
     *
     * @param username username for the user
     * @param password Password if needed for end user management in the PKI server. May be null
     * @param email email address of the user
     * @param config {@link CertificateConfig}
     * @param csr {@link CertificateSigningRequest}
     */
    public CsrCertificateRequest(String username, String password, String email, CertificateConfig config, CertificateSigningRequest csr) {
        super();
        this.username = username;
        this.password = password;
        this.email = email;
        this.config = config;
        this.csr = csr;
    }

    @Override
    public String getString() throws OXException {
        JSONObject json = new JSONObject();
        try {
            json.put("username", username);
            if (password != null) {
                json.put("password", password);
            }
            json.put("email", email);
            json.put("include_chain", true);
            json.put("certificate_authority_name", config.getCa());
            json.put("certificate_profile_name", config.getCert_profile());
            json.put("end_entity_profile_name", config.getEnd_entity_profile());
            json.put("certificate_request", csr.writePemString());
        } catch (JSONException e) {
            throw PKIExceptionCodes.ERROR_CSR.create(e.getMessage());
        }

        return json.toString();
    }

    /**
     * Return the certificate signing request that was sent
     *
     * @return
     */
    public CertificateSigningRequest getCSR() {
        return csr;
    }

}
