/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.pki.ejbca.configuration;

import com.openexchange.guard.configuration.GuardProp;

/**
 * {@link EjbcaProperty}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public enum EjbcaProperty implements GuardProp {

    /**
     * If EJBCA bundle should be enabled. System-wide configuration.
     */
    enabled(Boolean.FALSE),
    /**
     * If the user should use this EJBCA server to look up certificates
     */
    lookupAvail(Boolean.TRUE),
    /**
     * If this EJBCA manages the certificates for this user. I.E. certificate creation, renewal
     */
    managesUsersCertificates(Boolean.FALSE),
    /**
     * If the Certificate creation is simple, I.E. no approval process and expects an immediate return of signed cert
     */
    simpleCreation(Boolean.TRUE),
    /*
     * Base URL of the EJBCA server Rest Api. Example: http://10.10.10.10/ejbca/ejbca-rest-api/
     */
    serverBaseUrl(),
    /*
     * Certificate to use for authentication
     */
    certificate(),
    /*
     * The Certificate CA used for api
     */
    ca(),
    /*
     * Private key for authentication
     */
    key(),
    /*
     * Verify the EJBCA host name if connected through SSL
     */
    verifySSLHostname(Boolean.TRUE),

    /*
     * Request timeout when connecting to the EJBCA server
     */
    timeout(10000),

    /**
     * Trust all Certificate from this EBJCA server
     */
    trustAll(Boolean.FALSE),

    // NEW CERT CONFIGS
    /**
     * Crypto type of new certificates, allowed ECDSA EdDSA RSA
     */
    cryptoType("RSA"),
    /*
     * The certificate authority to use for new certs
     */
    certificateAuthority(),
    /**
     * The certificate profile to use for new certs
     */
    certificateProfile(),
    /**
     * The end entity profile to use for new certs
     */
    endEntityProfile(),

    /**
     * Custom message, if any, to display to the user before creating keys. Might specify
     * where the keys can be used (trust), usage restrictions, etc.
     *
     */
    customCreateMessage(),
    ;

    private static final String EMPTY = "";

    private static final String PREFIX = "com.openexchange.guard.pki.ejbca.";

    private final String fqn;

    private final Object defaultValue;

    /**
     * Initializes a new {@link EjbcaProperty}.
     */
    private EjbcaProperty() {
        this(EMPTY);
    }

    /**
     * Initializes a new {@link EjbcaProperty}.
     *
     * @param fqn
     */
    private EjbcaProperty(Object defaultValue) {
        this(PREFIX, defaultValue);
    }

    /**
     * Initializes a new {@link EjbcaProperty}.
     *
     * @param fqn
     * @param defaultValue
     */
    private EjbcaProperty(String fqn, Object defaultValue) {
        this.fqn = fqn;
        this.defaultValue = defaultValue;
    }

    /**
     * Returns the fully qualified name of the property
     *
     * @return the fully qualified name of the property
     */
    @Override
    public String getFQPropertyName() {
        return fqn + name();
    }

    /**
     * Returns the default value of this property
     *
     * @return the default value of this property
     */
    @Override
    public <T extends Object> T getDefaultValue(Class<T> cls) {
        if (defaultValue.getClass().isAssignableFrom(cls)) {
            return cls.cast(defaultValue);
        }
        throw new IllegalArgumentException("The object cannot be converted to the specified type '" + cls.getCanonicalName() + "'");
    }

}
