/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.net.ssl.KeyManagerFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.ejbca.configuration.EjbcaProperty;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.server.ServiceLookup;

/**
 * Performs API calls to configured EJBCA server
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class EjbcaApi {

    ServiceLookup services;
    CloseableHttpClient httpClient;
    private X509Certificate caCert;

    private static int MAX_CONNECTIONS = 20;  // Max connections for http pooling
    private static int MAX_CONENCTIONS_PER_ROUTE = 20; // Max connections per route

    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(EjbcaApi.class);
    }

    /**
     * Simple test strategy to make sure that SSL chain presented is signed by the CA certificate
     * as configured.
     *
     */
    public class TrustEJStrategy implements TrustStrategy {

        @Override
        public boolean isTrusted(X509Certificate[] x509Certificates, String authType) throws CertificateException {
            final int length = x509Certificates.length;
            try {
                for (int i = 0; i < length; i++) {
                    if (i < length - 1) {
                        x509Certificates[i].verify(x509Certificates[i + 1].getPublicKey());
                    } else {
                        x509Certificates[i].verify(caCert.getPublicKey());  // Last should be self signed CA key
                    }
                }
            } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | SignatureException e) {
                return false;
            }
            return true;
        }
    };

    /**
     * Constructor for EJBCA API. Configured certificates, SSL setup
     *
     * @param services
     * @throws OXException
     */
    public EjbcaApi(ServiceLookup services) throws OXException {
        this.services = services;
        try {
            // Set up empty keystore
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            // Load the client certificate
            X509Certificate clientCert = loadCertificate(EjbcaProperty.certificate);
            caCert = loadCertificate(EjbcaProperty.ca);
            ks.setCertificateEntry("EJBCA-cert", clientCert);
            // Load the client private key
            ks.setKeyEntry("EJBCA-key", loadPrivateKey(), null, new java.security.cert.Certificate[] { loadCertificate(EjbcaProperty.certificate) });
            // Load into key management factory
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(ks, null);
            // Set up the SSL Context.  We want to specifically verify the connection with the configured CA
            SSLContextBuilder sslContext = SSLContexts.custom()
                .loadKeyMaterial(ks, null)
                .loadTrustMaterial(ks, new TrustEJStrategy());

            final SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext.build(),
                getConfigService().getBooleanProperty(EjbcaProperty.verifySSLHostname) ? new DefaultHostnameVerifier() : NoopHostnameVerifier.INSTANCE);
            org.apache.http.config.Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("http", new PlainConnectionSocketFactory())  // Really, this is only for testing.  Should not have http connections
                .register("https", sslSocketFactory)
                .build();
            // Set up connection pooling
            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            cm.setMaxTotal(MAX_CONNECTIONS);
            cm.setDefaultMaxPerRoute(MAX_CONENCTIONS_PER_ROUTE);

            // Setup timeouts
            GuardConfigurationService configService = getConfigService();
            final int time_out = configService.getIntProperty(EjbcaProperty.timeout);
            RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(time_out)
                .setConnectionRequestTimeout(time_out)
                .setSocketTimeout(time_out)
                .build();

            // Finally, initialize the httpClient
            httpClient = HttpClients.custom()
                .setConnectionManager(cm)
                .evictExpiredConnections()
                .setDefaultRequestConfig(config)
                .setSSLSocketFactory(sslSocketFactory)
                .build();
        } catch (Exception e) {
            LoggerHolder.LOGGER.error("Error setup of EJBCA Api", e);
            throw PKIExceptionCodes.SETUP_ERROR.create(e.getMessage());
        }
    }

    public void close() {
        try {
            httpClient.close();
        } catch (IOException e) {
            LoggerHolder.LOGGER.error("Error closing httpClient", e);
        }
    }

    /**
     * Load a certificate from configuration
     *
     * @param prop Configuration Property to use
     * @return Certificate imported.
     * @throws IOException
     * @throws GeneralSecurityException
     * @throws OXException
     */
    public X509Certificate loadCertificate(EjbcaProperty prop) throws IOException, GeneralSecurityException, OXException {
        GuardConfigurationService configService = getConfigService();
        String key = configService.getProperty(prop);
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) factory.generateCertificate(new ByteArrayInputStream(key.getBytes()));
        return cert;
    }

    /**
     * Loads the configured private key
     *
     * @return
     * @throws IOException
     * @throws OXException
     */
    private PrivateKey loadPrivateKey() throws IOException, OXException {
        GuardConfigurationService configService = getConfigService();
        String key = configService.getProperty(EjbcaProperty.key);
        PEMParser pemParser = new PEMParser(new StringReader(key));
        PrivateKeyInfo pki = (PrivateKeyInfo) pemParser.readObject();
        JcaPEMKeyConverter conv = new JcaPEMKeyConverter();
        PrivateKey priv = conv.getPrivateKey(pki);
        return priv;
    }

    /**
     * Gets the GuardConfigurationService
     *
     * @return
     * @throws OXException
     */
    private GuardConfigurationService getConfigService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }

    /**
     * Go a Get call to EJBCA
     *
     * @param <R>
     * @param path Path to use
     * @param parameters Parameters if any
     * @param userId User Id of the user
     * @param cid Context Id of the user
     * @param clazz Class for the response type
     * @return
     * @throws OXException
     */
    public <R extends EJBCAResponse> R doCallGet(String path, Map<String, String> parameters, int userId, int cid, Class<R> clazz) throws OXException {
        HttpGet request = null;
        try {
            final URI uri = buildUri(path, toQueryString(parameters), userId, cid);
            request = new HttpGet(uri);
            LoggerHolder.LOGGER.debug("Executing GET using \"{}\"", uri);

            return handleHttpResponse(execute(request), clazz);
        } catch (UnsupportedOperationException | IllegalStateException | IOException e) {
            LoggerHolder.LOGGER.error("Error performing GET call ", e);
            throw PKIExceptionCodes.API_ERROR.create(e.getMessage());
        } finally {
            close(request);
        }
    }

    /**
     * Do post call to EJBCA
     *
     * @param <R>
     * @param path Path to use
     * @param parameters Any URl parameters
     * @param jsonReq JSON to send
     * @param userId UserId of the current user
     * @param cid cid of the current user
     * @param clazz Class type to return, should extend EJBCAResponse
     * @return Returns response of type clazz.
     * @throws OXException
     */
    public <R extends EJBCAResponse> R doCallPost(String path, Map<String, String> parameters, EJBCARequest jsonReq, int userId, int cid, Class<? extends R> clazz) throws OXException {
        HttpPost request = null;
        try {
            final URI uri = buildUri(path, toQueryString(parameters), userId, cid);
            request = new HttpPost(uri);
            StringEntity jsonEntity = new StringEntity(jsonReq.getString(), ContentType.APPLICATION_JSON);
            request.setEntity(jsonEntity);
            LoggerHolder.LOGGER.debug("Executing POST using \"{}\"", uri);
            return handleHttpResponse(execute(request), clazz);
        } catch (UnsupportedOperationException | IllegalStateException | IOException e) {
            LoggerHolder.LOGGER.error("Error performing POST call ", e);
            throw PKIExceptionCodes.API_ERROR.create(e.getMessage());
        } finally {
            close(request);
        }
    }

    /**
     * Handle the response from EJBCA and cast to EJBCAResponse
     *
     * @param <R>
     * @param httpResponse The HTTP Response
     * @param clazz clazz to cast. Should be an extension of EJBCAResponse
     * @return
     * @throws UnsupportedOperationException
     * @throws IOException
     * @throws OXException
     */
    @SuppressWarnings("unchecked")
    protected <R extends EJBCAResponse> R handleHttpResponse(HttpResponse httpResponse, Class<R> clazz) throws UnsupportedOperationException, IOException, OXException {
        final StatusLine statusLine = httpResponse.getStatusLine();
        final int statusCode = statusLine.getStatusCode();
        if (statusCode > 201) {
            if (httpResponse.getEntity() != null) {
                InputStream in = httpResponse.getEntity().getContent();
                try (in) {
                    JSONObject json = new JSONObject(new InputStreamReader(in, StandardCharsets.UTF_8));
                    LoggerHolder.LOGGER.error("Error communicating with EJBCA Service " + statusLine.toString() + json.toString());
                    final Constructor<?> constr = clazz.getConstructor(JSONObject.class);
                    return (R) constr.newInstance(json);
                } catch (JSONException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw PKIExceptionCodes.API_ERROR.create(e.getMessage());
                }
            }
            LoggerHolder.LOGGER.error("Error communicating with EJBCA Service " + statusLine.toString());
            throw PKIExceptionCodes.API_ERROR.create("Error communicating with EJBCA Service " + statusLine.toString());
        }
        try {
            if (httpResponse.getEntity() != null) {
                InputStream in = httpResponse.getEntity().getContent();
                try (in) {
                    JSONObject json = new JSONObject(new InputStreamReader(in, StandardCharsets.UTF_8));
                    final Constructor<?> constr = clazz.getConstructor(JSONObject.class);
                    return (R) constr.newInstance(json);
                }
            }
        } catch (final JSONException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            LoggerHolder.LOGGER.error("Error processing response from EJBCA", e);
            throw PKIExceptionCodes.API_ERROR.create(e.getMessage());
        } finally {
            close(httpResponse);
        }
        throw PKIExceptionCodes.API_ERROR.create("Error communicating with EJBCA Service");
    }

    protected HttpResponse execute(HttpRequestBase method) throws ClientProtocolException, IOException {
        return httpClient.execute(method);
    }

    /**
     * Builds the URI from given arguments
     *
     * @param path
     * @param queryString The query string parameters
     * @param userId userId of the user
     * @param cid contextId of the user
     * @return The built URI string
     * @throws OXException
     * @throws IllegalArgumentException If the given string violates RFC 2396
     */
    protected URI buildUri(String path, List<NameValuePair> queryString, int userId, int cid) throws OXException {
        try {
            GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
            final String base = configService.getProperty(EjbcaProperty.serverBaseUrl, userId, cid);
            StringBuilder sb = new StringBuilder();
            sb.append(base);
            if (!base.endsWith("/")) {
                sb.append("/");
            }
            sb.append(path);
            return new URIBuilder(sb.toString()).build();
        } catch (final URISyntaxException x) {
            throw new IllegalArgumentException("Failed to build URI", x);
        }
    }

    /**
     * Gets the appropriate query string for given parameters
     *
     * @param parameters The parameters
     * @return The query string
     */
    protected List<NameValuePair> toQueryString(Map<String, String> parameters) {
        if (null == parameters || parameters.isEmpty()) {
            return null;
        }
        final List<NameValuePair> l = new LinkedList<>();
        for (final Map.Entry<String, String> e : parameters.entrySet()) {
            l.add(new BasicNameValuePair(e.getKey(), e.getValue()));
        }
        return l;
    }

    /**
     * Close HttpResponse
     *
     * @param response
     */
    protected static void close(HttpResponse response) {
        if (null != response) {
            HttpEntity entity = response.getEntity();
            if (null != entity) {
                try {
                    EntityUtils.consumeQuietly(entity);
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
    }

    /**
     * Close HTTPRequest
     *
     * @param request
     */
    protected static void close(HttpRequestBase request) {
        if (null != request) {
            try {
                request.reset();
            } catch (Exception e) {
                // Ignore
            }
        }
    }
}
