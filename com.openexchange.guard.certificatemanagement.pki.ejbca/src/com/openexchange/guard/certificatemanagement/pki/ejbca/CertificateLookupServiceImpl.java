/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupResponse;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.CertificateApi;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateChainService;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateResponse;
import com.openexchange.guard.certificatemanagement.pki.ejbca.configuration.EjbcaProperty;
import com.openexchange.guard.certificatemanagement.pki.impl.CertificateUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.server.ServiceLookup;

public class CertificateLookupServiceImpl implements CertificateLookupService {

    private ServiceLookup services;
    private CertificateChainService certChainService;

    private static final String SOURCE_NAME = "EJBCA";

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CertificateLookupService.class);
    }

    public CertificateLookupServiceImpl(ServiceLookup services, CertificateChainService certChainService) {
        super();
        this.services = services;
        this.certChainService = certChainService;
    }

    /**
     * Confirm that this certificate is trusted by the user
     *
     * @param cert Certificate to check
     * @param userId userId of the user
     * @param cid Context ID
     * @return True if trusted and can be used.
     * @throws OXException
     */
    private boolean isValidAndTrusted(CertificateResponse resp, X509Certificate cert, int userId, int cid) throws OXException {
        try {
            cert.checkValidity();
        } catch (CertificateExpiredException | CertificateNotYetValidException e) {
            return false;
        }
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        if (configService.getBooleanProperty(EjbcaProperty.trustAll, userId, cid)) {
            LoggerHolder.LOGGER.debug("Certificate trusted by trustAll configuration");
            return true;
        }

        CertificateService certificateService = services.getServiceSafe(CertificateService.class);
        try {
            Result certResult = certificateService.verify(cert, certChainService.getChain(resp, cert, userId, cid), new Date(), userId, cid);
            if (certResult.equals(Result.VERIFIED))
                return true;
            LoggerHolder.LOGGER.info("Found certificate for user, but not trusted with users configured certificate authorities");
        } catch (Exception e) {
            throw OXException.general("Error checking certificate validity", e);
        }
        return false;

    }

    @Override
    public CertificateLookupResponse getCertificate(String email, int userId, int cid) throws OXException {
        CertificateApi api = services.getServiceSafe(CertificateApi.class);
        // validate email
        GuardRatifierService ratifier = services.getServiceSafe(GuardRatifierService.class);
        ratifier.validate(email);
        List<CertificateResponse> result = api.searchCert("CN=" + email, userId, cid);
        if (result == null || result.isEmpty()) {
            return null;
        }
        for (CertificateResponse cert : result) {
            try {
                X509Certificate x509Cert = CertificateUtil.getCertificate(cert.getBase64Cert(), true);
                if (isValidAndTrusted(cert, x509Cert, userId, cid)) {
                    LoggerHolder.LOGGER.info("Found certificate in EJBCA for " + email);
                    return new CertificateLookupResponse(x509Cert, SOURCE_NAME);
                }
            } catch (OXException e) {
                LoggerHolder.LOGGER.error("Error handling returned certificates", e);
                // We can continue searching for valid
            }
        }
        return null;
    }

}
