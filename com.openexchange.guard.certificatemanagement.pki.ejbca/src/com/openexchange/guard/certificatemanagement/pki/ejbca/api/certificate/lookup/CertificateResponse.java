/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup;

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Contains the certificate response from a certificate lookup call
 * 
 * @author Greg Hill<greg.hill@open-xchange.com>
 *
 */
public class CertificateResponse implements Serializable {

    /**
     * The CertificateResponse.java.
     */
    private static final long serialVersionUID = 1839908072617031961L;

    JSONObject json;
    private String fingerprint;
    private String caFingerprint;
    private String issuerDN;
    private String serialNumber;
    private int status;
    private int revocationReason;
    private String subjectDN;
    private String base64Cert;

    public CertificateResponse(JSONObject json) {
        this.json = json;
        this.fingerprint = getString("fingerprint");
        this.caFingerprint = getString("cAFingerprint");
        this.issuerDN = getString("issuerDN");
        this.serialNumber = getString("serialNumber");
        this.status = getInt("status");
        this.revocationReason = getInt("revocationReason");
        this.subjectDN = getString("subjectDN");
        this.base64Cert = getString("base64Cert");
    }

    /**
     * Get a string result from json
     *
     * @param name
     * @return
     */
    private String getString(String name) {
        return (String) getFromJson(name);
    }

    /**
     * Get int result from json. Return 0 if not present
     *
     * @param name
     * @return int with value, or 0 if absent
     */
    private int getInt(String name) {
        Object obj = getFromJson(name);
        if (obj == null)
            return 0;
        return (int) obj;
    }

    /**
     * Get an object from json
     *
     * @param name
     * @return
     */
    private Object getFromJson(String name) {
        if (json == null)
            return null;
        if (json.has(name)) {
            try {
                return json.get(name);
            } catch (JSONException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Return the json response
     *
     * @return
     */
    public JSONObject getJson() {
        return json;
    }

    /**
     * Gets the fingerprint of the certificate
     *
     * @return
     */
    public String getFingerprint() {
        return fingerprint;
    }

    /**
     * Gets the Fingerprint of the CA
     *
     * @return
     */
    public String getCaFingerprint() {
        return caFingerprint;
    }

    /**
     * Gets the issuer subject
     *
     * @return
     */
    public String getIssuerDN() {
        return issuerDN;
    }

    /**
     * Gets the serial number of the certificate
     *
     * @return
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Gets the EJBCA status of the certificate
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * Get the revokation reason. -1 if not revoked
     *
     * @return
     */
    public int getRevocationReason() {
        return revocationReason;
    }

    /**
     * Get the subject of the certificate
     *
     * @return
     */
    public String getSubjectDN() {
        return subjectDN;
    }

    /**
     * Gets the base64 representation of the certificate
     *
     * @return
     */
    public String getBase64Cert() {
        return base64Cert;
    }
}
