/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EjbcaApi;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.StatusResponse;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateResponse;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateSearchRequest;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateSearchResponse;
import com.openexchange.server.ServiceLookup;

/**
 * {@link CertificateApi} Contains the API calls for certificate search
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateApi {

    private ServiceLookup services;
    EjbcaApi api;

    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CertificateApi.class);
    }

    public CertificateApi(ServiceLookup services, EjbcaApi api) {
        this.services = services;
        this.api = api;

    }

    /**
     * Check the status of the certificate API
     *
     * @return
     * @throws OXException
     */
    public boolean checkStatus() throws OXException {
        StatusResponse resp = api.doCallGet("v2/certificate/status", null, 0, 0, StatusResponse.class);
        return resp.isOK();
    }

    /**
     * Search for certificates based on email address. Will check for subject
     *
     * @param subject Subject to check
     * @return List of CertificateResponse
     * @throws OXException
     */
    public List<CertificateResponse> searchCert(String subject, int userId, int contextId) throws OXException {
        CertificateSearchRequest req = new CertificateSearchRequest();
        req.setSubject(subject);
        try {
            CertificateSearchResponse resp = api.doCallPost("v2/certificate/search", null, req, userId, contextId, CertificateSearchResponse.class);
            if (resp == null) {
                return Collections.emptyList();
            }
            return resp.getCertificates();
        } catch (OXException e) {
            LoggerHolder.LOGGER.error("Error searching certificate", e);
        }
        return Collections.emptyList();
    }
}
