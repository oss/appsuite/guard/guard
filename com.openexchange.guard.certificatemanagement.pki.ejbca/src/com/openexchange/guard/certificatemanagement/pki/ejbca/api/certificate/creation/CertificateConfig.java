/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.creation;

import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.ejbca.configuration.EjbcaProperty;
import com.openexchange.guard.configuration.GuardConfigurationService;

/**
 * Contains configuration settings required to create new certificate with the EJBCA server
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateConfig {

    private String cryptoType;
    private String ca;
    private String cert_profile;
    private String end_entity_profile;

    /**
     * Initializes a new {@link CertificateConfig}. Loads the configurations for the user
     *
     * @param configService
     * @param userId
     * @param contextId
     * @throws OXException
     */
    public CertificateConfig(GuardConfigurationService configService, int userId, int contextId) throws OXException {
        if (configService == null)
            throw OXException.general("Missing configuration service");
        cryptoType = configService.getProperty(EjbcaProperty.cryptoType, userId, contextId);
        ca = configService.getProperty(EjbcaProperty.certificateAuthority, userId, contextId);
        cert_profile = configService.getProperty(EjbcaProperty.certificateProfile, userId, contextId);
        end_entity_profile = configService.getProperty(EjbcaProperty.endEntityProfile, userId, contextId);
    }

    /**
     * Gets the cryptoType
     *
     * @return The cryptoType
     */
    public String getCryptoType() {
        return cryptoType;
    }

    /**
     * Gets the ca
     *
     * @return The ca
     */
    public String getCa() {
        return ca;
    }

    /**
     * Gets the cert_profile
     *
     * @return The cert_profile
     */
    public String getCert_profile() {
        return cert_profile;
    }

    /**
     * Gets the end_entity_profile
     *
     * @return The end_entity_profile
     */
    public String getEnd_entity_profile() {
        return end_entity_profile;
    }

}
