/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.creation;

import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus.Status;
import com.openexchange.guard.certificatemanagement.commons.CertificateSigningRequest;
import com.openexchange.guard.certificatemanagement.commons.KeyPairGenerator;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.pki.CertificateEnrollmentService;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EjbcaApi;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateChainService;
import com.openexchange.guard.certificatemanagement.pki.ejbca.configuration.EjbcaProperty;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.guard.smime.CertificateVerificationResult.Result;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * Service for creating new certificates for the user.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateEnrollmentServiceImpl implements CertificateEnrollmentService {

    private ServiceLookup services;
    CertificateChainService certChainService;
    EjbcaApi api;

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CertificateEnrollmentServiceImpl.class);
    }

    public CertificateEnrollmentServiceImpl(ServiceLookup services, CertificateChainService certChainService, EjbcaApi api) {
        this.services = services;
        this.certChainService = certChainService;
        this.api = api;
    }

    @Override
    public String getCustomCreateMessage(int userId, int contextId) throws OXException {
        GuardConfigurationService config = services.getServiceSafe(GuardConfigurationService.class);

        // TODO   Consider if we need to send this through translation service or not
        // Configuration for the user may be already correct for language
        return config.getProperty(EjbcaProperty.customCreateMessage, userId, contextId);
    }

    @Override
    public CertificateEnrollmentStatus createKeys(int userId, int contextId, char[] password) throws OXException {
        CertificateEnrollmentStatus firstStatus = beginNewCertificateEnrollment(userId, contextId, password);
        // If we should immediately continue to continueCertification, do so now.
        if (firstStatus != null && firstStatus.getStatus().equals(Status.Continue)) {
            return continueCertificateEnrollment(userId, contextId, firstStatus);
        }
        // Otherwise, return status (done, rejected, pending)
        return firstStatus;
    }

    @Override
    public CertificateEnrollmentStatus beginNewCertificateEnrollment(int userId, int contextId, char[] password) throws OXException {
        if (password.length < 1) {
            throw OXException.mandatoryField("password");
        }
        final String passwordString = new String(password);
        // Get user email
        UserService userService = services.getServiceSafe(UserService.class);
        User user = userService.getUser(userId, contextId);

        // Get the singing request
        CsrCertificateRequest request = getCSR(userId, contextId, user);
        // ? Branch for different types?
        // Right now, only supported is pkcs10enroll which doesn't handle deferral/approval.  Will
        // Complete fully or fail
        return doPKCS10Call(request, userId, contextId, passwordString);
    }

    /**
     * Create a new certificate signing request for the user
     *
     * @param userId
     * @param contextId
     * @param user Appsuite user
     * @return {@link CsrCertificateRequest}
     * @throws OXException
     */
    private CsrCertificateRequest getCSR(int userId, int contextId, User user) throws OXException {
        String email = user.getMail();
        // Confirm valid email
        GuardRatifierService ratifier = services.getServiceSafe(GuardRatifierService.class);
        ratifier.isValid(email);
        CertificateConfig config = new CertificateConfig(services.getService(GuardConfigurationService.class), userId, contextId);

        // Create CSR
        CertificateSigningRequest csr = new CertificateSigningRequest.CertificateSigningRequestBuilder()
            .setKeyPairGenerator(new KeyPairGenerator(config.getCryptoType(), services))
            .setCommonName(email)
            .setEmail(email)
            .build();

        // Send it to EJBCA
        CsrCertificateRequest request = new CsrCertificateRequest(user.getImapLogin(), null, email, config, csr);
        return request;
    }

    /**
     * Calls the EJBCA server using the PKCS10ENROLL api call and resturns the status of the enrollment
     * Positive enrollment will contain new keys
     *
     * @param request {@link CsrCertificateRequest}
     * @param userId
     * @param contextId
     * @param passwordString Password to encrypt the new smime keys if returned with signed certificate
     * @return {@link CertificateEnrollmentStatus} Status with new keys, error, or rejection
     * @throws OXException
     */
    private CertificateEnrollmentStatus doPKCS10Call(CsrCertificateRequest request, int userId, int contextId, String passwordString) throws OXException {
        AbstractCsrResponse resp = api.doCallPost("v1/certificate/pkcs10enroll", null, request, userId, contextId, Pkcs10CsrCertificateResponse.class);
        X509Certificate cert = resp.getCertificate();

        // Handle response if done
        if (cert != null) {
            List<X509Certificate> chain = resp.getChain();
            if (chain.isEmpty()) {
                Set<X509Certificate> chainSet = certChainService.getChain(cert, userId, contextId);
                chain = new ArrayList<X509Certificate>(chainSet);
            }
            GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
            if (configService.getBooleanProperty(EjbcaProperty.trustAll, userId, contextId)) {
                LoggerHolder.LOGGER.info("Trust All configuration set for the user.  New certificate assumed trusted");
            } else {
                CertificateService certService = services.getServiceSafe(CertificateService.class);
                // Let's make sure that this certificate verifies locally.  If not, there is a problem
                // Log error and return failed.
                try {
                    Result result = certService.verify(cert, Set.copyOf(chain), new Date(), userId, contextId);
                    if (!result.equals(Result.VERIFIED)) {
                        LoggerHolder.LOGGER.error("Error verifying new certificate from EJBCA");
                        return new CertificateEnrollmentStatus(Status.Rejected, 0, "Verification Error");
                    }
                } catch (Exception e) {
                    LoggerHolder.LOGGER.error("Error verifying new certificate from EJBCA", e);
                }
            }

            SmimeKeyService smimeKeyService = services.getServiceSafe(SmimeKeyService.class);
            // Retrieve the new public/private key
            KeyPair keys = request.getCSR().getKeyPair();
            SmimeKeys newKeys = smimeKeyService.createKey(cert, chain, keys.getPrivate(), passwordString, userId, contextId, true);
            return new CertificateEnrollmentStatus(Status.Done, 0, newKeys);
        }
        // Check for error
        if (resp.getStatus().equals(Status.Done)) {
            final String error = resp.getError();
            if (error != null) {
                throw PKIExceptionCodes.ERROR_CSR.create(error);
            }
        }

        if (resp.getStatus().equals(Status.Rejected)) {
            // throw rejected
            throw PKIExceptionCodes.CERTIFICATE_REQUEST_REJECTED.create();
        }

        // Other failure
        throw PKIExceptionCodes.ERROR_CSR.create("Unknown failure");
    }

    @Override
    public CertificateEnrollmentStatus continueCertificateEnrollment(int userId, int contextId, CertificateEnrollmentStatus currentStatus) {
        // Currently nothing to do here for EJBCA pkcs10enroll implementation
        return null;
    }

}
