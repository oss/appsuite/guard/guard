/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EJBCAResponse;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;

/**
 * Contains the response (list of CertificateSearchResponse) from a certificate lookup call
 *
 * @author Greg Hill<greg.hill@open-xchange.com>
 *
 */
public class CertificateSearchResponse extends EJBCAResponse {

    ArrayList<CertificateResponse> certificates;

    public CertificateSearchResponse(JSONObject json) {
        super(json);
        if (json != null && json.has("certificates")) {
            JSONArray certificatesArray;
            try {
                certificatesArray = json.getJSONArray("certificates");
                certificates = new ArrayList<CertificateResponse>(certificatesArray.length());
                for (int i = 0; i < certificatesArray.length(); i++) {
                    CertificateResponse resp = new CertificateResponse(certificatesArray.getJSONObject(i));
                    certificates.add(resp);
                }
            } catch (JSONException e) {
                PKIExceptionCodes.RESPONSE_ERROR.create(e.getMessage());
            }

        }
    }

    /**
     * Return list of CertificateResponse
     *
     * @return
     */
    public List<CertificateResponse> getCertificates() {
        return certificates;
    }

}
