/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.ejbca.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.certificatemanagement.pki.CertificateEnrollmentService;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupService;
import com.openexchange.guard.certificatemanagement.pki.PKIServiceHandler;
import com.openexchange.guard.certificatemanagement.pki.ejbca.CertificateLookupServiceImpl;
import com.openexchange.guard.certificatemanagement.pki.ejbca.EJBCAPKIServiceImpl;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.EjbcaApi;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.CertificateApi;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.creation.CertificateEnrollmentServiceImpl;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateChainService;
import com.openexchange.guard.certificatemanagement.pki.ejbca.api.certificate.lookup.CertificateChainServiceImpl;
import com.openexchange.guard.certificatemanagement.pki.ejbca.configuration.EjbcaProperty;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.smime.CertificateService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.rest.client.httpclient.HttpClientService;
import com.openexchange.user.UserService;

/**
 * EJBCA implementation of PKI services
 * {@link Activator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class Activator extends HousekeepingActivator {

    Logger logger = LoggerFactory.getLogger(Activator.class);
    private EjbcaApi api;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, GenericCacheFactory.class };
    }

    @Override
    protected void startBundle() throws Exception {
        GuardConfigurationService configService = this.getServiceSafe(GuardConfigurationService.class);
        if (!configService.getBooleanProperty(EjbcaProperty.enabled)) {
            return;
        }
        logger.info("Starting EJBCA PKI bundle");

        // Quick sanity check
        String serverIp = configService.getProperty(EjbcaProperty.serverBaseUrl);
        String certPath = configService.getProperty(EjbcaProperty.certificate);
        String key = configService.getProperty(EjbcaProperty.key);
        if (serverIp == null || certPath == null || key == null) {
            logger.error("EJBCA minimum configuration not found.  Please configure serverIP, certificate path, and key path");
            return;
        }
        api = new EjbcaApi(this);

        final CertificateApi certApi = new CertificateApi(this, api);
        registerService(CertificateApi.class, certApi);

        CertificateChainService certChainService = new CertificateChainServiceImpl(this);
        CertificateLookupService lookup = new CertificateLookupServiceImpl(this, certChainService);
        CertificateEnrollmentService createService = new CertificateEnrollmentServiceImpl(this, certChainService, api);
        PKIServiceHandler ejbcaHandler = new EJBCAPKIServiceImpl(this, lookup, createService);
        registerService(PKIServiceHandler.class, ejbcaHandler);
        trackService(GuardRatifierService.class);
        trackService(CertificateService.class);
        trackService(HttpClientService.class);
        trackService(UserService.class);
        trackService(CertificateChainService.class);
        trackService(KeyCreationService.class);

        // Quick test all set up
        Thread checkAvail = new Thread() {

            @Override
            public void run() {
                try {
                    if (certApi.checkStatus()) {
                        logger.info("EJBCA PKI bundle started and server successfully contacted");
                    } else {
                        logger.warn("EJBCA PKI bundle started, but unable to get successful status from EJBCA server");
                    }
                } catch (OXException e) {
                    logger.error("Error checking EJBCA status", e);
                }
            }
        };
        checkAvail.start();

        openTrackers();

    }

    @Override
    protected void stopBundle() throws Exception {
        logger.info("Stopping EJBCA PKI bundle");
        if (api != null) {
            api.close();
        }
        unregisterService(CertificateApi.class);
        unregisterService(PKIServiceHandler.class);
        super.stopBundle();
    }

}
