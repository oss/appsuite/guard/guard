/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPDecryptionResult;
import com.openexchange.pgp.keys.common.PGPSymmetricKey;

/**
 * {@link PGPCryptoService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public interface PGPCryptoService {

    /**
     * Encrypts data
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the encrypted data to
     * @param armored True, if the encrypted data should be written ASCII-Armored, false if binary
     * @param recipients A collection of recipient identifier. The implementation is responsible for interpreting recipients.
     * @throws OXException
     */
    public void encrypt(InputStream input, OutputStream output, boolean armored, List<String> recipients) throws OXException;

    /**
     * Encrypts data
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the encrypted data to
     * @param armored True, if the encrypted data should be written ASCII-Armored, false if binary
     * @param recipientsKeys A collection of keys used for encryption
     * @throws OXException
     */
    public void encrypt(InputStream input, OutputStream output, boolean armored, RecipKey...recipientKeys) throws OXException;

    /**
     * Encrypts and signs data
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the encrypted data to
     * @param armored True, if the encrypted data should be written ASCII-Armored, false if binary
     * @param signerIdentity The Identity of the signer
     * @param recipients  A collection of recipient identifier. The implementation is responsible for interpreting recipients.
     * @throws OXException
     */
    public void encryptSigned(InputStream input, OutputStream output, boolean armored, UserIdentity signerIdentity, List<String> recipients) throws OXException;

    /**
     * Encrypts and signs data
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the encrypted data to
     * @param armored True, if the encrypted data should be written ASCII-Armored, false if binary
     * @param signingKey The password protected secret key to use for signing
     * @param password The password for the signignKey
     * @param recipientKeys A collection of keys used for encryption
     * @throws OXException
     */
    public void encryptSigned(InputStream input, OutputStream output, boolean armored, GuardKeys signingKey, String password, RecipKey...recipientKeys) throws OXException;

    /**
     * Decrypts data.
     *
     * The private key, used for decryption, is fetched by the concrete implementation based on the given {@link UserIdentity} and/or the data provided within the input data (key id).
     *
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the decoded data to
     * @param recipient The PGP user identity of the user who want's to decode the data
     * @return A result object containing information about signature verification
     * @throws OXException
     */
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, UserIdentity recipient) throws OXException;

    /**
     * Decrypts data with a specific key.
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the decoded data to
     * @param key The specific key used for decryption
     * @param password The password for the secret key
     * @return A result object containing information about signature verification
     * @throws OXException
     */
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, GuardKeys key, String password) throws OXException;

    /**
     *
     * Decrypts data with a PGP symmetric session key.
     *
     * @param input The input stream to read the data from
     * @param output The output stream to write the decoded data to
     * @param  symmetricKey The symmetric key for decrypting the PGP data
     * @param recipient The PGP user identity of the user who want's to decode the data
     * @return A list of Signature verification results, or an empty list, if the encrypted data was not signed
     */
    public PGPDecryptionResult decrypt(InputStream input, OutputStream output, PGPSymmetricKey symmetricKey, UserIdentity recipient) throws OXException;
}
