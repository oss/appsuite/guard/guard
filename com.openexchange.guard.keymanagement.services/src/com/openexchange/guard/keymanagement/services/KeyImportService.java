/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services;

import java.util.Collection;
import java.util.Locale;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;

/**
 * {@link KeyImportService} provides functionality for importing external created keys for users.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public interface KeyImportService {

    /**
     * Imports a set of {@link PGPPublicKeyRing} objects for a given user.
     *
     * @param userId The user's ID
     * @param contextId The context ID
     * @param email The user's primary email
     * @param locale The user's locale
     * @param publicKeyRings The set of {@link PGPPublicKeyRing} to import
     * @return A list of imported key rings
     * @throws OXException
     */
    Collection<GuardKeys> importPublicKeyRing(int userId, int contextId, String email, Locale locale, PGPPublicKeyRing... publicKeyRings) throws OXException;

    /**
     * Imports a set of {@link PGPSecretKeyRing} objects for a given user.
     *
     * @param userId The user's ID
     * @param contextId The context ID
     * @param email The user's primary email
     * @param locale The user's locale
     * @param password The password for the {@link PGPSecretKeyRing} objects
     * @param newPassword A new password which will be set for the imported secret keys, instead of <code>password</code> (This allows a user to choose a different password than the local one).
     * @param publicKeyRings The set of {@link PGPSecretKeyRing} to import
     * @return A list of imported key rings
     * @throws OXException
     */
    Collection<GuardKeys> importPrivateKeyRing(int userId, int contextId, String email, Locale locale, String password, String newPassword, PGPSecretKeyRing... privateKeyRings) throws OXException;

}
