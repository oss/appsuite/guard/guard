/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.services.lookup;

import org.bouncycastle.openpgp.PGPPublicKeyRing;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link AbstractRecipKeyLookupStrategy}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.1
 */
public abstract class AbstractRecipKeyLookupStrategy implements RecipKeyLookupStrategy {

    /**
     * Check the Guard Key has a valid encryption key
     * @param key
     * @return
     */
    protected boolean hasValidEncryptionKey(GuardKeys key) {
        return PGPKeysUtil.getEncryptionKey(key.getPGPPublicKeyRing()) != null;
    }

    /**
     * Check the keyring has a valid encryption key
     * @param ring
     * @return
     */
    protected boolean hasValidEncryptionKey(PGPPublicKeyRing ring) {
        return PGPKeysUtil.getEncryptionKey(ring) != null;
    }

}
