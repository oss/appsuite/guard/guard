/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import com.openexchange.pgp.core.PGPSymmetricDecrypter;
import com.openexchange.pgp.core.PGPSymmetricEncrypter;

/**
 * {@link AutoCryptCrypto}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptCrypto {

    private static final boolean ASCII_ARMORED = true;

    /**
     * Performs the decryption of symmetric encrypted PGP data
     *
     * @param in The symmetric encrypted PGP data
     * @param out The stream to write the decrypted data to
     * @param password The password required for decryption
     * @throws IOException
     * @throws PGPException
     */
    public static void decryptSymmetric(InputStream in, OutputStream out, char[] password) throws IOException, PGPException {
        new PGPSymmetricDecrypter().decrypt(in, out, password);
    }

    /**
     * Performs the decryption of one single, symmteric encrypted PGP Object
     *
     * @param in The symmetric encrypted PGP object data
     * @param password The password
     * @return The decrypted PGP object
     * @throws IOException
     * @throws PGPException
     */
    @SuppressWarnings("unchecked")
    public static <T> T decryptSymmetricPGPObject (InputStream in, char[] password) throws IOException, PGPException {
        ByteArrayOutputStream decrypted = new ByteArrayOutputStream();
        decryptSymmetric(in, decrypted, password);
        ByteArrayInputStream pgpDataInputStream = new ByteArrayInputStream(decrypted.toByteArray());
        PGPObjectFactory factory = new PGPObjectFactory(PGPUtil.getDecoderStream(pgpDataInputStream), new BcKeyFingerprintCalculator());
        try {
            return (T)factory.nextObject();
        }
        catch(ClassCastException c) {
           throw new PGPException("Unable to cast PGP object");
        }
    }

    /**
     * Performs symmetric PGP encryption of data using AES-256
     *
     * @param data The data to encrypt
     * @param out the stream to write the encrypted data to
     * @param key The symmetric key
     * @throws IOException
     * @throws PGPException
     */
    public static void encryptSymmetric(InputStream data, OutputStream out, char[] key) throws IOException, PGPException {
        encryptSymmetric(data, out, PGPEncryptedData.AES_256, key);
    }

    /**
     * Performs symmetric PGP encryption of data
     *
     * @param data The data to encrypt
     * @param out the stream to write the encrypted data to
     * @param algorithm the ID of the symmetric algorithm to use (See RFC-4880 - 9.2 Symmetric key algorithm)
     * @param key The symmetric key
     * @throws IOException
     * @throws PGPException
     */
    public static void encryptSymmetric(InputStream data, OutputStream out, int algorithm, char[] key) throws IOException, PGPException {
        new PGPSymmetricEncrypter(algorithm).encrypt(data, out, ASCII_ARMORED, key);

    }
}
