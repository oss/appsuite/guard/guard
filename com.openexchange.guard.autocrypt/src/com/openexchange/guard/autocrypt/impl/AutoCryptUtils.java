/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.idn.IDNA;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPUserAttributeSubpacketVector;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.database.AutoCryptKey;
import com.openexchange.guard.autocrypt.exceptions.AutocryptExceptionCodes;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.commons.trust.GuardKeySources;
import com.openexchange.pgp.keys.common.ModifyingPGPPublicKeyRing;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link AutoCryptUtils}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptUtils {

    private static Logger LOG = LoggerFactory.getLogger(AutoCryptUtils.class);

    /**
     * Wrap header to keep within 80 character limit for compliance
     * @param keydata
     * @return Wrapped data
     */
    public static String formatHeader(String keydata) {
        int length = keydata.length();
        int index = 0;
        StringBuilder sb = new StringBuilder();
        while (index < length) {
            sb.append("\r\n ");
            sb.append(keydata.substring(index, Math.min(index + 79, keydata.length())));
            index += 79;
        }
        return sb.toString();
    }

    /**
     * Strip the userIds out of a keyring with the exception of the one containing the specified email address.
     * Only include Master key and one subkey that is valid
     * 
     * @param ring
     * @param email Email address of the userId to maintain
     * @return
     */
    private static PGPPublicKeyRing removeOtherKeysAndUserIDs(PGPPublicKeyRing ring, String email) {
        ModifyingPGPPublicKeyRing newRing = new ModifyingPGPPublicKeyRing();
        Iterator<PGPPublicKey> keys = ring.getPublicKeys();
        boolean idFound = false;  // Found the email in userIds
        boolean done = false;  // Done finding master and encryption key
        boolean masterAdded = false;  // Master key was added
        while (keys.hasNext() && !done) {
            PGPPublicKey key = keys.next();
            Iterator<byte[]> ids = key.getRawUserIDs();
            while (ids.hasNext()) {
                byte[] id = ids.next();
                String strId = new String(id, StandardCharsets.UTF_8);
                try {
                    InternetAddress addr = new InternetAddress(IDNA.toACE(strId));
                    addr.validate();
                    if (strId != null && email.toLowerCase().equals(addr.getAddress().toLowerCase())) {
                        idFound = true;
                    } else {
                        key = PGPPublicKey.removeCertification(key, id);
                    }
                } catch (AddressException e) {
                    key = PGPPublicKey.removeCertification(key, id);  // invalid address
                }
            }
            if (key != null) {
                if (key.isMasterKey()) {  // This is the mater key.  Add it
                    newRing.addPublicKey(key);
                    masterAdded = true;
                }
                // Search for valid encryption key, not revoked, and not expired
                if (key.isEncryptionKey() && !key.isMasterKey() && !key.hasRevocation() && (!PGPKeysUtil.isExpired(key))) {
                    newRing.addPublicKey(key);
                    if (masterAdded) { // Assuming master key was added, then we are done
                        done = true;
                    } else {
                        LOG.info("Autocrypt key not added due to unexpected pgp keyring format");
                        return null;
                    }
                }
            }
        }
        if (!idFound || !done) {
            return null;  // No email matches, all IDs were stripped
        }
        return newRing.getRing();
    }

    /**
     * Remove all signatures and user attributes (like pictures) except the certification of the user ID and keys
     * 
     *
     * @param ring
     * @return Ring with signatures and attributes removed
     */
    private static PGPPublicKeyRing removeSigsAndAttributes(PGPPublicKeyRing ring) {

        ModifyingPGPPublicKeyRing newRing = new ModifyingPGPPublicKeyRing();
        Iterator<PGPPublicKey> keys = ring.getPublicKeys();
        Long masterKeyId = 0L;
        while (keys.hasNext()) {
            PGPPublicKey key = keys.next();
            if (key.isMasterKey()) {
                masterKeyId = key.getKeyID();
            }
            // Remove images and other userAttributes
            Iterator<PGPUserAttributeSubpacketVector> it = key.getUserAttributes();
            while (it.hasNext()) {
                PGPUserAttributeSubpacketVector sig = it.next();
                key = PGPPublicKey.removeCertification(key, sig);
            }
            // Go through signatures and remove all but the certifications of the keys and userid
            Iterator<PGPSignature> sigs = key.getSignatures();
            while (sigs.hasNext()) {
                PGPSignature sig = sigs.next();
                if (!sig.isCertification() && (sig.getSignatureType() != 24)) {
                    key = PGPPublicKey.removeCertification(key, sig);
                } else {
                    if (sig.getKeyID() != masterKeyId) {
                        key = PGPPublicKey.removeCertification(key, sig);
                    }
                }
            }
            newRing.addPublicKey(key);
        }
        return newRing.getRing();
    }

    /**
     * Base64 encode a pgp public key ring cleaned up per autocrypt specifications
     * Should include 5 objects
     * Master Key
     * UserId
     * Signature of UserId
     * Subkey valid for encryption
     * Signature of subkey by master
     * 
     * @param ring
     * @param email
     * @return
     * @throws OXException
     */
    public static String createEncodedKeyString (PGPPublicKeyRing ring, String email) throws OXException {
        if (ring == null) {
            return "";
        }
        if (email != null) {
            ring = removeOtherKeysAndUserIDs(ring, email);
        }
        if (ring == null) {
            return null;
        }
        ring = removeSigsAndAttributes(ring);
        try {
            return Base64.encodeBase64String(ring.getEncoded(true));
        } catch (IOException e) {
            throw OXException.general("Error encoding pgpKeyRing ", e);
        }
    }

    public static AutoCryptKey convertHeader (int userId, int cid, String header, long date) throws OXException {
        if (header == null) {
            return null;
        }
        String[] sections = header.split(";");
        String addr = "";
        String keydata = "";
        AutoCryptKey.Preference pref = AutoCryptKey.Preference.none;

        for (String section : sections) {
            String[] parts = section.split("=");
            if (parts.length >= 2) {
                switch (parts[0].trim()) {
                    case "addr":
                        addr = parts[1];
                        break;
                    case "prefer-encrypt":
                        if (parts[1].equals("mutual")) {
                            pref = AutoCryptKey.Preference.mutual;
                        }
                        break;
                    case "keydata":
                        keydata = parts[1];
                        break;
                }
            }
        }
        if (keydata.isEmpty() || addr.isEmpty()) {
            throw AutocryptExceptionCodes.MISSING_KEY_DATA.create();
        }
        byte[] keyByteData = Base64.decodeBase64(keydata);
        PGPPublicKeyRing keyRing;
        try {
            keyRing = new PGPPublicKeyRing(keyByteData, new BcKeyFingerprintCalculator());
        } catch (IOException e) {
            return null;
        }

        return new AutoCryptKey(userId, cid, addr, keyRing, date, pref);


    }

    /**
     * Check an autocrypt key to make sure the email address and key ids match
     * 
     * @param key
     * @return
     */
    public static boolean confirmEmail(AutoCryptKey key) {
        Iterator<PGPPublicKey> pubKeys = key.getPGPPublicKeyRing().getPublicKeys();
        final String keyEmail = IDNUtil.aceEmail(key.getEmail());
        while (pubKeys.hasNext()) {
            PGPPublicKey pubKey = pubKeys.next();
            Iterator<String> ids = pubKey.getUserIDs();
            while (ids.hasNext()) {
                String id = IDNUtil.aceEmail(ids.next());
                if (id.toLowerCase().contains(keyEmail)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getHash (AutoCryptKey key) {
        try {
            return Hex.encodeHexString(MessageDigest.getInstance("SHA-256").digest(key.getPGPPublicKeyRing().getEncoded()));
        } catch (NoSuchAlgorithmException | IOException e) {
            LOG.error("Error creating hash for autoCrypt key", e);;
        }
        return null;
    }

    public static RecipKey convertToRecipKey (AutoCryptKey key) {
        RecipKey recipKey = new RecipKey(key.getVerified() ?
                GuardKeySources.AUTOCRYPT_KEY_USER_VERIFIED :
                GuardKeySources.AUTOCRYPT_KEY
            );
        recipKey.setEmail(key.getEmail());
        recipKey.setPgp(true);
        recipKey.setPGPPublicKeyRing(key.getPGPPublicKeyRing());
        recipKey.setUserid(key.getUserId());
        recipKey.setCid(key.getCid());
        return recipKey;
    }


}
