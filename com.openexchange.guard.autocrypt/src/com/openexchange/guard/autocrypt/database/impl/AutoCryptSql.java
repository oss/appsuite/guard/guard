/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.autocrypt.database.impl;


/**
 * {@link AutoCryptSql}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class AutoCryptSql {

    public static final String SELECT_KEY = "SELECT pubkey, hash, lastUpdate, preference, verified FROM og_AutoCrypt WHERE id = ? AND cid = ? AND email =?;";
    public static final String SELECT_ALL_KEYS = "SELECT pubkey, hash, email, lastUpdate, preference, verified FROM og_AutoCrypt WHERE id = ? AND cid = ?;";
    public static final String SELECT_ALL_VERIFIED_KEYS = "SELECT pubkey, hash, email, lastUpdate, preference, verified FROM og_AutoCrypt WHERE id = ? AND cid = ? AND verified = b'1';";
    public static final String INSERT_KEY = "INSERT INTO og_AutoCrypt (id, cid, email, pubkey, hash, lastUpdate, preference, fingerprint, verified) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE pubkey = ?, hash = ?, lastUpdate = ?, preference = ?, fingerprint = ?, verified = ?";
    public static final String DELETE_KEY = "DELETE FROM og_AutoCrypt WHERE id = ? AND cid = ? AND fingerprint = ?";
    public static final String SET_VERIFIED = "UPDATE og_AutoCrypt SET verified = ? WHERE id = ? AND cid = ? AND fingerprint = ?";
    public static final String DELETE_FOR_USER = "DELETE FROM og_AutoCrypt WHERE id = ? AND cid = ?";

}
