/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.translation;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import com.openexchange.exception.OXException;

/**
 * {@link GuardTranslationService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardTranslationService {

    /**
     * Read help file using provided language
     *
     * @param language The language
     * @return The help file in the specified language
     * @throws OXException
     */
    String getHelpFile(String language) throws OXException;

    /**
     * Get the translation of the template based on the specified language
     *
     * @param template The template to translate
     * @param language The language to translate to
     * @param templateTransformations A list with the template transformations
     * @return The translated template
     * @throws OXException
     */
    String getTemplateTranslation(String template, String language, List<TemplateTransformation> templateTransformations) throws OXException;

    /**
     * Get the translation of the specified text in the specified language
     *
     * @param text The text to translate
     * @param language The language to translate to
     * @return The translated text
     */
    String getTranslation(String text, String language);

    /**
     * Get the translation of the specified text in the specified language and replace the specified variable
     *
     * @param text The text to translate
     * @param language The language to translate to
     * @param variable The variable to replace
     * @return The translated text
     * @throws OXException
     */
    String getTranslation(String text, String language, String variable);

    /**
     *
     * @param template
     * @param language
     * @param templateId
     * @return
     * @throws OXException
     */
    String getTranslation(String template, String language, int templateId) throws OXException;

    /**
     * Get translation of template, checking for custom based on template identifier
     *
     * @param template The template to translate
     * @param language The language to translate to
     * @param templateId The template identifier
     * @param templateTransformations A list with the template transformations
     * @return
     */
    String getTranslation(String template, String language, int templateId, List<TemplateTransformation> templateTransformations) throws OXException;

    /**
     * Verifies whether the specified template exists
     *
     * @param templateId The template identifier
     * @param template The template
     * @return true if the template exists; false otherwise
     * @throws OXException
     */
    boolean customTemplateExists(int templateId, String template) throws OXException;

    /**
     * Check if a language is available, and return its language code. If not, return the default code
     *
     * @param code The language code to check
     * @return the language code if it exists; otherwise return the default one
     */
    String getAvailableCode(String code);

    /**
     * Check if the language is available, and return its locale. If not, return the default locale
     *
     * @param code The language code to check
     * @return The locale of the code if it exists, or the default locale otherwise
     */
    Locale getAvailableLocale(String code);

    /**
     * Get a map with all available languages for the specified language code. If none available, an empty map will be returned
     *
     * @param code The language code
     * @return a map with all available languages for the specified language code.
     */
    Map<String, String> getAvailableLanguages(String code);
}
