/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.translation;

import java.util.Locale;

/**
 * {@link GuardTranslator} is a little helper class around {@link GuardTranslationService}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.2
 */
public class GuardTranslator {

    private final GuardTranslationService translationService;
    private final Locale                  locale;

    /**
     * Initializes a new {@link GuardTranslator}.
     *
     * @param translationService The translation service to wrap
     * @param locale The locale used for translation
     */
    public GuardTranslator(GuardTranslationService translationService, Locale locale) {
        this.translationService = translationService;
        this.locale = locale;
    }

    /**
     * Gets the translation for the given text
     *
     * @param text The text to translate
     * @return The translatet text, or the default text, if no such translation was found
     */
    public String getTranslation(String text) {
        return translationService.getTranslation(text, locale.toString());
    }
}
