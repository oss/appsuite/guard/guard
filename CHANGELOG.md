# OX Guard

## [8.32.0]- 2024-11-20

### Added

- [`GUARD-453`](https://jira.open-xchange.com/browse/Guard-453): Warn of expiring keys, clt to list expiring keys [`ed078bb`](https://gitlab.open-xchange.com/appsuite/guard/commit/ed078bb1a1614a20b12072d634f0d9b3a9980703)
- Add support for S3 chunking [`2d2ff75`](https://gitlab.open-xchange.com/appsuite/guard/commit/2d2ff75867267f82a4e0f94daccc77f86881a261)


### SCR

- [`SCR-1413`](https://jira.open-xchange.com/browse/SCR-1413) SCR-1413 Add expires column to og_KeyTable
- [`SCR-1400`](https://jira.open-xchange.com/browse/SCR-1400) SCR-1400 Add configuration to establish when Guard should warn users of expiring certificate

## [8.31.0]- 2024-10-25

### Fixed

- ['appsuite/platform/core#59'](https://gitlab.open-xchange.com/appsuite/platform/core/-/issues/59): Test env slow to startup in multi-pod env due to create key race condition. Add recheck for master keys on duplicate key error. [`f951374b`](https://gitlab.open-xchange.com/appsuite/guard/commit/f951374b950be21419f92c001be11f13a87f7687)

## [8.30.0]- 2024-09-27

### Changed

- ['MW-2354'](https://jira.open-xchange.com/browse/MW-2354): Use v2 caches in OX Guard [`ebf6cbf`](https://gitlab.open-xchange.com/appsuite/guard/commit/ebf6cbf4bb68364dbe45d3ffa08a77055b36899b)
- ['MW-2191'](https://jira.open-xchange.com/browse/MW-2191): update to Java 21 [`6067698`](https://gitlab.open-xchange.com/appsuite/guard/commit/6067698f2b4675208e0883d39d809cc165be809a)
- ['MW-2354'](https://jira.open-xchange.com/browse/MW-2354) Additional changes for using redis cache for Guard [`ce70223`](https://gitlab.open-xchange.com/appsuite/guard/commit/ce7022310b622add5778381f709c2a455910b7e9)
- ['MW-2191'](https://jira.open-xchange.com/browse/MW-2191): Switched to JUnit 5 [`1a8ba67`](https://gitlab.open-xchange.com/appsuite/guard/commit/1a8ba67579350bbb6a6fa0581d3f7aa07448e1b2)
- ['MW-2363'](https://jira.open-xchange.com/browse/MW-2363): Avoid using SessionStorageService [`a56d20a`](https://gitlab.open-xchange.com/appsuite/guard/commit/a56d20ac1ac7f667455db040c4ab5c1bc31f53cf)

## [8.29.0]- 2024-08-27

### Fixed

- [`GUARD-495`](https://jira.open-xchange.com/browse/Guard-495): SCR-1439, make smime_pub collate match smime_alt table. Fixes  [`1fe3ad0`](https://gitlab.open-xchange.com/appsuite/guard/commit/1fe3ad0ecd1f817191b4584505b6959430876764)

## [8.28.0]- 2024-07-31

### Fixed

- Minor documentation fix

### Changed

- Documentation deploy pipeline cleanup

## [8.27.0]- 2024-07-04

### Fixed

- [`GUARD-492`](https://jira.open-xchange.com/browse/Guard-492): Prevent upload of private certificate if cert with same email exists with other local user [`23f2eb9`](https://gitlab.open-xchange.com/appsuite/guard/commit/23f2eb9cffdd36220e39449ed70dae5a04d3f046)
- [`GUARD-491`](https://jira.open-xchange.com/browse/Guard-491): Allow upload of expired certificates to enable reading of old emails. [`3e73c12`](https://gitlab.open-xchange.com/appsuite/guard/commit/3e73c12462b9efba41f2683e0a1d7a57c567cde7)

## [8.26.0]- 2024-06-05

### Fixed

- [`GUARD-490`](https://jira.open-xchange.com/browse/Guard-490): Only attach help files to local Guard users unless otherwise configured [`cac0837`](https://gitlab.open-xchange.com/appsuite/guard/commit/cac0837771a8ec063f01083f8495032a2acb3a56)

### Added

- [`GUARD-489`](https://jira.open-xchange.com/browse/Guard-489): Allow import of cer and crt files for recipients. Some cleanup of KeyImporter [`2efd6e5`](https://gitlab.open-xchange.com/appsuite/guard/commit/2efd6e5d40ee7487576fd3ca8688a8afe9dab9a6)
- [`GUARD-460`](https://jira.open-xchange.com/browse/GUARD-460): Add support for multiple email addresses in SubjectAlternateName [`ecf589b`](https://gitlab.open-xchange.com/appsuite/guard/commit/ecf589be6cb13854eb14d7a4a54b16338919bb55)

### Removed

- Apache Commons Lang 2.6 [`8cf0255`](https://gitlab.open-xchange.com/appsuite/guard/commit/8cf02558f4fafadc1d885adcf303a4d3a2a75217)

### SCR

- [`SCR-1377`](https://jira.open-xchange.com/browse/SCR-1377): Add database table smime_alt to contain email aliases included in SubjectAltName extensions
- [`SCR-1384`](https://jira.open-xchange.com/browse/SCR-1384): Add c.ox.guard.attachHelpFileAll.  If true, Guard will attach help file to all outgoing PGP emails, otherwise, guard will only attach help file for local appsuite users.

## [8.25.0]- 2024-05-09

### Changed
- [`GUARD-482`](https://jira.open-xchange.com/browse/GUARD-482): Guard email templates updated to more modern feel. Reflects 8.0 UI

### Fixed

- [`MWB-2555`](https://jira.open-xchange.com/browse/MWB-2555): Check if authentication required for given security settings [`d731613`](https://gitlab.open-xchange.com/appsuite/guard/commit/d731613dc5fa06c78822b2493eea4fdef0283c4e)

### Added

- Add: translation updates for guard templates [`76ed14b`](https://gitlab.open-xchange.com/appsuite/guard/commit/76ed14bbf726fb756ab511142b27f2a8f0004f5d)
- Add gitlab pipeline steps to create and publish documentation [`450de32`](https://gitlab.open-xchange.com/appsuite/guard/commit/450de327deef6e0ebaaf2ba9dff7c958c023eb6c)
- Add custom variables to Guard templates. Update Guard email templates to better reflect 8.0 UI. [`5b1c458`](https://gitlab.open-xchange.com/appsuite/guard/commit/5b1c4584ba3dc5c768f0c244d24a1eeb58318d51)

## [8.24.0]- 2024-04-03

### Fixed

- [`GUARD-481`](https://jira.open-xchange.com/browse/Guard-481): Fix Guard Guests get error trying to read emails with non-current key [`ce00b51`](https://gitlab.open-xchange.com/appsuite/guard/commit/ce00b518c398599449f72c7cbc5b40036f597571)

### Changed

- [`MW-2195`](https://jira.open-xchange.com/browse/MW-2195): Align CHANGELOG [`92119eb`](https://gitlab.open-xchange.com/appsuite/guard/commit/92119ebba8215df23efc6a0a0b0091e5e0752904)


## [8.23.0] - 2024-03-08

### Fixed

-  [`GUARD-480`](https://jira.open-xchange.com/browse/GUARD-480): fix Resource leaking pulling Guest Emails.  Emails that were being filtered out by folder, flag, limits, were not being closed properly.  Now only loading streams after filtering done.
-  [`GUARD-476`](https://jira.open-xchange.com/browse/GUARD-476): fix issue sending to Guard guests if their key had expired.  New key userId was being assigned with adding key..
-  [`GUARD-476`](https://jira.open-xchange.com/browse/GUARD-476): possible fix for basicContraints issue.  Only check constraints when verifying specific certificate.  Issue if certs out of order.
-  [`GUARD-477`](https://jira.open-xchange.com/browse/GUARD-477): possible fix for duplicate key in guest mail account.  Use cid rather than pulling context first.
-  [`GUARD-475`](https://jira.open-xchange.com/browse/GUARD-475): change GuestEmailMetaData column from varchar to text.  SCR-1352
-  [`GUARD-474`](https://jira.open-xchange.com/browse/GUARD-474): fix Guard failing to login if locale invalid.  Use default if cookie string invalid
-  [`GUARD-456`](https://jira.open-xchange.com/browse/GUARD-456): add guard clt option to reset the users recovery email address
-  [`GUARD-473`](https://jira.open-xchange.com/browse/GUARD-473): return full key fingerprint with hkp index rather than the keyid
- Fix error message not displaying session id[`f872717`](https://gitlab.open-xchange.com/appsuite/guard/commit/f8727177741c1807ad4eaa75da23cb58152fc965)
- Fixed classpath[`b2367a3`](https://gitlab.open-xchange.com/appsuite/guard/commit/b2367a322d44fcd20c8292226337755e50a97173)

## [8.22.0] - 2024-02-08

### Added

- Users can now choose if draft Guard emails are encrypted
- Option to encrypt S/Mime draft emails added

### Fixed

- Fix authentication missing in parser check.  Don't add security json objects if null[`7dc72b6`](https://gitlab.open-xchange.com/appsuite/guard/commit/7dc72b6e7748b240e1c04399ce2e0ce068de34d8)
- Fix autocrypt flag check for non-encrypted emails[`cb53637`](https://gitlab.open-xchange.com/appsuite/guard/commit/cb536377817d30c64c306079dbadcaeada363839)

### Changed

- MW-2268: Change copyright headers back to Open-Xchange GmbH[`d4377d7`](https://gitlab.open-xchange.com/appsuite/guard/commit/d4377d74bbe62704ac58954476ff193187fec453)
- Use newer security settings from Middleware.  Implement parser[`275ede0`](https://gitlab.open-xchange.com/appsuite/guard/commit/275ede0ac3becb698e642afa942dbbbb13484983)
- MW-2125: RequestAnalyzer for Guard Support API[`e642462`](https://gitlab.open-xchange.com/appsuite/guard/commit/e642462da4dc0a63e8fa94a685f04b1d0b36bffb)

## [8.21.0] - 2024-01-11

### Fixed

-  [`GUARD-463`](https://jira.open-xchange.com/browse/GUARD-463): increase table size for public certificate chain

### Added

- Add PKCS7 exported.  Use base64 for PKCS12 exports[`c685fd7`](https://gitlab.open-xchange.com/appsuite/guard/commit/c685fd79fbe866b44ed38d7b400a53d7939c2fbd)

### SCR

- [`SCR-1334`](https://jira.open-xchange.com/browse/SCR-1334)Increase column size for public certificate and chain

### Removed

- Remove broken submodule[`565dbec`](https://gitlab.open-xchange.com/appsuite/guard/commit/565dbec47c2157cbe70cfb87c1787f69fb7cae86)

## [8.20.0] - 2023-12-20

### Fixed

-  [`GUARD-461`](https://jira.open-xchange.com/browse/GUARD-461): throw error on import if critical extension not supported
-  [`GUARD-462`](https://jira.open-xchange.com/browse/GUARD-462): Check BasicConstraints extension for path length when verifying signature
-  [`GUARD-457`](https://jira.open-xchange.com/browse/GUARD-457):  Review changes made, rename action.  Allow and handle PEM encoded pkcs7 certificates
-  [`GUARD-459`](https://jira.open-xchange.com/browse/GUARD-459): Use Subject-AlternativeName for email lookup in certificate
-  [`GUARD-457`](https://jira.open-xchange.com/browse/GUARD-457):   Check recipient certificate upload to confirm we don't have local user.  Add documentation

### Added

- Add:  Uploading of recipient certificate support added[`6ce04ce`](https://gitlab.open-xchange.com/appsuite/guard/commit/6ce04ceabecdbfcf601bdf24a477206d4b34876a)
- Add missing import[`3a2a3f6`](https://gitlab.open-xchange.com/appsuite/guard/commit/3a2a3f67f57ec974a670bd9fab120277de98b1a2)

## [8.19.0] - 2023-12-20

### Fixed

-  [`GUARD-315`](https://jira.open-xchange.com/browse/GUARD-315): show more detailed error message if attempting to encrypt item with revoked or expired key
-  [`GUARD-450`](https://jira.open-xchange.com/browse/GUARD-450): code review corrections.  Return source of certificate.  Impr error handling. Protect against LDAP injection.
-  [`GUARD-435`](https://jira.open-xchange.com/browse/GUARD-435): review.  Close inputstreams in EJBCAApi as well as throw error rather than null responses in failure.
-  [`GUARD-435`](https://jira.open-xchange.com/browse/GUARD-435): review, move PKIServiceRegistry into PKIService

### Added

- Add Certificate Signing request and Certificate Request Message[`0b2c95c`](https://gitlab.open-xchange.com/appsuite/guard/commit/0b2c95c96d3289dd561b8657a89423597205733a)
- Add user level CA configuration in test CA service[`8c90cfe`](https://gitlab.open-xchange.com/appsuite/guard/commit/8c90cfec6d3dcf9581c5a0acccaac52c03210801)
- Add request timeout to EJBCA server connections.  Do not block appsuite startup for testing connection[`3718f75`](https://gitlab.open-xchange.com/appsuite/guard/commit/3718f75523f71a3320126185dd00415b03088ca1)
- Add missing doc[`ce4a499`](https://gitlab.open-xchange.com/appsuite/guard/commit/ce4a4991fbf136ca2659b4dc224cc2199942a3e4)

### SCR

- [`SCR-1214`](https://jira.open-xchange.com/browse/SCR-1214): remove default filestorage setting for uploadDirectory and fileStorageType
- [`SCR-1214`](https://jira.open-xchange.com/browse/SCR-1214): Documentation changes for SCR-1214

## [8.14.0] - 2023-12-20

### Fixed

-  [`GUARD-342`](https://jira.open-xchange.com/browse/GUARD-342) Autocrypt:  Remove extra keys and image data.  Autocrypt should only pass master, one encryption key, and one useri
-  [`GUARD-337`](https://jira.open-xchange.com/browse/GUARD-337): update templates to use year variable for copyright. Add year, date, datetime as possible variables for guard templates

## [8.13.0] - 2023-12-20

### Fixed

-  [`GUARD-421`](https://jira.open-xchange.com/browse/GUARD-421): fix guests unable to reply to multiple if no GUARD-mail capabilities

## [8.12.0] - 2023-12-20

### Fixed

-  [`GUARD-409`](https://jira.open-xchange.com/browse/GUARD-409): Guard throws error when importing keys with certificates in non encrypted safebags.  Improve type of bag checks.
-  [`GUARD-396`](https://jira.open-xchange.com/browse/GUARD-396): prevent another user from uploading same S/Mime key.  Causes issue with public key lookup

### Added

- Added missing project files[`1438149`](https://gitlab.open-xchange.com/appsuite/guard/commit/1438149f97d796dde77b13ff59e8b2a660d57067)
- Added password reset destination[`dcc32b7`](https://gitlab.open-xchange.com/appsuite/guard/commit/dcc32b78a2d14d26a45e5838ad9eaf744093c7d4)

### Removed

- Removed deprecated OxDriveFileInputStream[`9798788`](https://gitlab.open-xchange.com/appsuite/guard/commit/9798788549e2d09cfaf56f162cbcf3f23bb012e2)
- Removed deprecated pgpmail "verify" action[`eac4692`](https://gitlab.open-xchange.com/appsuite/guard/commit/eac4692eecf096bdc3ab40ff866edd81c0d9d64d)
- Removed deprecated "create" action[`f2227be`](https://gitlab.open-xchange.com/appsuite/guard/commit/f2227be393c0ef61c3a59178959315bdd9b9fb88)
- Removed deprecated "decrypt" action[`7c28ad7`](https://gitlab.open-xchange.com/appsuite/guard/commit/7c28ad7a6ea4f5edf1ddd1df450f6a74085b4f99)
- Removed deprecated pgpmail "process_mime" action[`9274d54`](https://gitlab.open-xchange.com/appsuite/guard/commit/9274d5402359b2fe083d93c2853e4d4558bc03d6)
- Removed JSON from logic layer[`2a79822`](https://gitlab.open-xchange.com/appsuite/guard/commit/2a798222af1754376247cc62ad0e99f9645eb389)
- Removed deprecated "encrypt_mime" action[`a8b72e2`](https://gitlab.open-xchange.com/appsuite/guard/commit/a8b72e216b2455ee36ee0ae1b77312f649f8d30b)
- Removed deprecated GuestCacheService[`1b36d40`](https://gitlab.open-xchange.com/appsuite/guard/commit/1b36d40a9befd6c0d585a0502d0e07a55e4657f1)
- Removed interface[`f8bc2aa`](https://gitlab.open-xchange.com/appsuite/guard/commit/f8bc2aa7e6e3d10e329f1a82fbe329d400b05785)
- Removed deprecated constants[`2af8a61`](https://gitlab.open-xchange.com/appsuite/guard/commit/2af8a617618be0e35749e7cdd35bd76936c4903c)
- Removed dead code[`c667848`](https://gitlab.open-xchange.com/appsuite/guard/commit/c6678486833e22b9c0b1f38603410accf3349527)
- removed backend-mailfilter reference[`7459e7e`](https://gitlab.open-xchange.com/appsuite/guard/commit/7459e7e0432081b33fc7ea9629e0ae466332b149)
- Remove milter packaging in psf file[`d1340a0`](https://gitlab.open-xchange.com/appsuite/guard/commit/d1340a03c53e7e755c28a7df1b0dcc07e01eefd2)

## [8.10.0] - 2023-12-20

### Removed

- MW-1946 - removed org.apache.tika (and com.openexchange.textxtraction). The required functionality is now provided through the new bundle com.openexchange.tika.util[`a0cd6a4`](https://gitlab.open-xchange.com/appsuite/guard/commit/a0cd6a4566ed33f6cdedb4a1e5163f074b32a90b)

## [8.6.0] - 2023-12-20

### Added

- Add standard OK response with External Key sharing[`a3428ce`](https://gitlab.open-xchange.com/appsuite/guard/commit/a3428cecd095470633367d8dea8c25cee73a0400)

## [8.2.0] - 2023-12-20

### Fixed

-  [`GUARD-260`](https://jira.open-xchange.com/browse/GUARD-260): change missing upsell template to simple info log rather than error
-  [`GUARD-333`](https://jira.open-xchange.com/browse/GUARD-333): return signature failure if any errors.
-  [`GUARD-332`](https://jira.open-xchange.com/browse/GUARD-332) [`GUARD-314`](https://jira.open-xchange.com/browse/GUARD-314): partly revert commit for : restore Guard file tracking attached to the request.  Do not call cleanup after request complete, rather wait for GC
-  [`GUARD-331`](https://jira.open-xchange.com/browse/GUARD-331): fix out of memory error.  FileCleanupTracker thread was not closing
-  [`GUARD-330`](https://jira.open-xchange.com/browse/GUARD-330): allow import of public key for recipients without valid email address.
-  [`GUARD-297`](https://jira.open-xchange.com/browse/GUARD-297): Updated documentation for the ProxyPass
-  [`GUARD-314`](https://jira.open-xchange.com/browse/GUARD-314): remove temporary file tracker associated with request.  Was causing race issues during "multiple" requests from the UI.  Use apache FileCleaningTracker instead.
-  [`GUARD-264`](https://jira.open-xchange.com/browse/GUARD-264): Create and improve Documentation
-  [`GUARD-304`](https://jira.open-xchange.com/browse/GUARD-304): fix delete file errors with Guest emails.  Was not being recognized as hashed file structure
-  [`GUARD-291`](https://jira.open-xchange.com/browse/GUARD-291): Several java.sql.SQLException in o-x.log
-  [`GUARD-297`](https://jira.open-xchange.com/browse/GUARD-297): Update WKS to support RFC draft 12.  Provides for advanced URL format for WKD lookup.  Provides blank policy file.
-  [`GUARD-297`](https://jira.open-xchange.com/browse/GUARD-297): WKS: Added the local part parameter to the query
-  [`GUARD-297`](https://jira.open-xchange.com/browse/GUARD-297): WKS: Implemented the "advanced" mode for quering keys. Kept the "direct" mode as fallback.
-  [`GUARD-297`](https://jira.open-xchange.com/browse/GUARD-297): WKS: Drop the use of DNS SRV records as described in RFC draft v7
-  [`GUARD-295`](https://jira.open-xchange.com/browse/GUARD-295): check GUARD-mail capability for signing outgoing emails
-  [`GUARD-289`](https://jira.open-xchange.com/browse/GUARD-289): fix null error when adding autocrypt header
-  [`GUARD-286`](https://jira.open-xchange.com/browse/GUARD-286): fix Guard error when sending encrypted email to email alias of self.
-  [`GUARD-288`](https://jira.open-xchange.com/browse/GUARD-288): add CLT command to wipe Guard database caches
-  [`GUARD-237`](https://jira.open-xchange.com/browse/GUARD-237): guestSMTP* properties are not config cascade aware for guests
-  [`GUARD-283`](https://jira.open-xchange.com/browse/GUARD-283): add missing default value to documentation
-  [`GUARD-285`](https://jira.open-xchange.com/browse/GUARD-285): fix missing mKeyIndex in table creation
-  [`GUARD-284`](https://jira.open-xchange.com/browse/GUARD-284): fix lean property masterKeyPath not being added to security manager
-  [`GUARD-259`](https://jira.open-xchange.com/browse/GUARD-259): Improve configuration cascade for Guests
-  [`GUARD-223`](https://jira.open-xchange.com/browse/GUARD-223): cleanup
-  [`GUARD-282`](https://jira.open-xchange.com/browse/GUARD-282): "Save as (encrypted)" does not rename the file
-  [`GUARD-223`](https://jira.open-xchange.com/browse/GUARD-223): Handle versions with encrypting/decrypting file
-  [`GUARD-275`](https://jira.open-xchange.com/browse/GUARD-275): BCC recipients visible to Guest users
-  [`GUARD-274`](https://jira.open-xchange.com/browse/GUARD-274): fix unable to send to guest using bcc.  Wrong header id used.
-  [`GUARD-273`](https://jira.open-xchange.com/browse/GUARD-273): fix bcc not being saved to draft folder
-  [`GUARD-269`](https://jira.open-xchange.com/browse/GUARD-269): veryify normal upload doesn't have encrypt flag set
-  [`GUARD-271`](https://jira.open-xchange.com/browse/GUARD-271): Corrupt translation in email template for resetting guard guest password
-  [`MWB-952`](https://jira.open-xchange.com/browse/MWB-952): Attempt set proper charset in Content-Type inside .pot header section
-  [`GUARD-268`](https://jira.open-xchange.com/browse/GUARD-268): Parameter MAX_UPLOAD_SIZE in infostore.properties is ignored when trying to encrypt large files
-  [`MWB-952`](https://jira.open-xchange.com/browse/MWB-952): changed ascii to utf-8 in Bulgarian (no supported/maintained language)
-  [`GUARD-263`](https://jira.open-xchange.com/browse/GUARD-263): Remove Milter service
-  [`GUARD-233`](https://jira.open-xchange.com/browse/GUARD-233): Wizard: 'default/oxguard/createKeys' triggered twice
-  [`GUARD-234`](https://jira.open-xchange.com/browse/GUARD-234): Remove deprecated gt.format
-  [`GUARD-267`](https://jira.open-xchange.com/browse/GUARD-267): properly encode quotes in password before adding to form to download private
-  [`GUARD-266`](https://jira.open-xchange.com/browse/GUARD-266)keysAPI: introduce 'deleteExternalPublicKey' and streamline related dialogs
-  [`MWB-911`](https://jira.open-xchange.com/browse/MWB-911): Check if encryption-aware mail access is really needed by examining message if it is PGP or PGP/INLINE message
-  [`GUARD-261`](https://jira.open-xchange.com/browse/GUARD-261): fix max-height of pin dialog to avoid scrolling
-  [`GUARD-262`](https://jira.open-xchange.com/browse/GUARD-262): unable to send to Guest email address with Umlauts
-  [`GUARD-277`](https://jira.open-xchange.com/browse/GUARD-277): fix duplicate service notification when weakforced enabled
-  [`GUARD-254`](https://jira.open-xchange.com/browse/GUARD-254): Remove mail preview for PGP Inline messages
-  [`GUARD-255`](https://jira.open-xchange.com/browse/GUARD-255): Fixed http API version
-  [`GUARD-239`](https://jira.open-xchange.com/browse/GUARD-239): adjust for change in locale cookie name.  Was resetting Guard key locales to default (en) due to missing
-  [`GUARD-236`](https://jira.open-xchange.com/browse/GUARD-236): fix error message when finished guard repair
-  [`MWB-847`](https://jira.open-xchange.com/browse/MWB-847): fix one more path for old Guard reader package
-  [`GUARD-246`](https://jira.open-xchange.com/browse/GUARD-246): fix Guard guided tour broken
-  [`GUARD-245`](https://jira.open-xchange.com/browse/GUARD-245): Ensure setup from lock toggle before setting encrypt to true.  Causing loop
-  [`GUARD-243`](https://jira.open-xchange.com/browse/GUARD-243): fix extra password prompts on Guard replies
-  [`MWB-847`](https://jira.open-xchange.com/browse/MWB-847): make path names absolute in debian/conffiles (upcoming for Bullseye)
-  [`GUARD-228`](https://jira.open-xchange.com/browse/GUARD-228): add limits to http header size during get request, and limit stream size to protect against OOM attacks

### Added

- Remove Guest redirector[`b2baeee`](https://gitlab.open-xchange.com/appsuite/guard/commit/b2baeeeba3e285d177f01c06abb9594ee6aedd68)
- Additional refactoring to use guardModel[`329f7c5`](https://gitlab.open-xchange.com/appsuite/guard/commit/329f7c5a1ae824bd57af2d63b31bcb9508ab9be5)
- Add required changes for error management and UI refactoring[`f785bd6`](https://gitlab.open-xchange.com/appsuite/guard/commit/f785bd64d73ed5d02b23e5c57f0cca8289c1f988)
- Added missing import[`4c3ea85`](https://gitlab.open-xchange.com/appsuite/guard/commit/4c3ea8500563a1eab6f50cc09012b80a3c7f0d66)
- Added TODOs to accomplish MW-1535[`4c119d7`](https://gitlab.open-xchange.com/appsuite/guard/commit/4c119d7cc5c303e76d1506707fca38bb1573fb7d)
- Add database update task to test Script[`7e6ef6a`](https://gitlab.open-xchange.com/appsuite/guard/commit/7e6ef6a771aa7c7dc8d29ef262febcf3c5dd2f1c)

### Removed

- Remove oxguard-ui.  Moved to gitlab.ox/appsuite/guard-ui[`b7d41dc`](https://gitlab.open-xchange.com/appsuite/guard/commit/b7d41dc820b75aa26a52f40329ba5bbaf694dcf3)
- Remove guest-reader and packaging[`d6bd25b`](https://gitlab.open-xchange.com/appsuite/guard/commit/d6bd25bd2a647a2aece172f69782eeba3d38a4f1)
- Remove Guest redirector[`b2baeee`](https://gitlab.open-xchange.com/appsuite/guard/commit/b2baeeeba3e285d177f01c06abb9594ee6aedd68)
- remove exemplary implementation: pgp_local[`df3b41e`](https://gitlab.open-xchange.com/appsuite/guard/commit/df3b41e37ef111039097975d750c0a04a969469f)
- Remove help/user-guide from MW build[`f726e72`](https://gitlab.open-xchange.com/appsuite/guard/commit/f726e7276b2cd2c83d9798a3af1d6784123f4c0c)
- Remove old guest reset password, old temptoken storage.  Add new guest reset that uses share link[`09362df`](https://gitlab.open-xchange.com/appsuite/guard/commit/09362dfdba44af26a9816709bab362948ae5fd74)
- Remove legacy S3 storage[`2cc5f78`](https://gitlab.open-xchange.com/appsuite/guard/commit/2cc5f7846cc6d16be0e50356c3d6e99c9bb6999e)
- Removed obsolete build logic for integration tests[`ecf903e`](https://gitlab.open-xchange.com/appsuite/guard/commit/ecf903ee38e0358ef54a174487fa2bcb13482fa7)
- Remove legacy file storage for Guard[`41561ff`](https://gitlab.open-xchange.com/appsuite/guard/commit/41561ff0f6d23621d757093c19ac226f4d5a8835)
- Remove conflict checks for Guard storage file packages.  Require c.ox.guard.storage.file.fileStorageType config to specify s3 if s3 used.[`84f78db`](https://gitlab.open-xchange.com/appsuite/guard/commit/84f78db93c051631510f6576fbd96e68af32122b)
- Remove residual .project file[`3610eeb`](https://gitlab.open-xchange.com/appsuite/guard/commit/3610eebbef7d1ec02c2326526fa277f62f362fe8)
- Remove residual local decode code[`85f6a6b`](https://gitlab.open-xchange.com/appsuite/guard/commit/85f6a6bbbc919a261c0119cb823120f1b7000f3a)
- Remove frontened psf[`801e63f`](https://gitlab.open-xchange.com/appsuite/guard/commit/801e63f34a59341abd162af8d84761384f0be40b)
- Remove milter packaging in psf file[`4187298`](https://gitlab.open-xchange.com/appsuite/guard/commit/4187298c7d30114df97b98f07a06764b0f28b328)

### SCR

- [`SCR-914`](https://jira.open-xchange.com/browse/SCR-914): Improve handling of AES data.  Encode GSM vs CBC, IV, and data for flexible decryption.
- [`SCR-998`](https://jira.open-xchange.com/browse/SCR-998): post installation scripts for removing com.openexchange.guard.keySources.trustLevelWKSSRVDNSSECServer
- [`SCR-1074`](https://jira.open-xchange.com/browse/SCR-1074): commons-cli-1.4.jar =&gt; commons-cli-1.5.0.jar
- [`SCR-837`](https://jira.open-xchange.com/browse/SCR-837): Implement SCR-837

<!-- References -->
[8.2.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/2.10.7-9...8.2.0)
[8.6.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.5.0...8.6.0) 
[8.10.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.9.0...8.10.0)
[8.12.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.10.0...8.12.0)
[8.14.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.13.0...8.14.0)
[8.19.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.18.0...8.19.0)
[8.20.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.19.0...8.20.0)
[8.21.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.20.0...8.21.0)
[8.22.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.21.0...8.22.0)
[8.23.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.22.0...8.23.0) 
[8.24.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.23.0...8.24.0)
[8.25.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.24.0...8.25.0)
[8.26.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.25.0...8.26.0)
[8.27.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.26.0...8.27.0)
[8.29.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.28.0...8.29.0)
[8.30.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.29.0...8.30.0)
[8.32.0]: (https://gitlab.open-xchange.com/appsuite/guard/compare/8.31.0...8.32.0)
