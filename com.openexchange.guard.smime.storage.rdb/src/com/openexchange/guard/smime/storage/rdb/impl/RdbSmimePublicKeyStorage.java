/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.impl;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.slf4j.Logger;
import com.openexchange.database.Databases;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.smime.storage.SmimePublicKeyStorage;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.encoding.Base64;

/**
 * {@link RdbSmimePublicKeyStorage} - The DB implementations of {@link SmimePublicKeyStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class RdbSmimePublicKeyStorage implements SmimePublicKeyStorage {

    private static Logger LOG = org.slf4j.LoggerFactory.getLogger(RdbSmimePublicKeyStorage.class);
    private ServiceLookup services;

    /**
     * Initializes a new {@link RdbSmimePublicKeyStorage}.
     *
     * @param services The {@link ServiceLookup}
     */
    public RdbSmimePublicKeyStorage(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Parses a X509 certificate from base64 encoded string data
     *
     * @param data The base64 encoded string data which should be parsed
     * @return The {@link X509Certificate} instance parsed from the given string
     * @throws CertificateException
     * @throws IOException
     */
    private static X509Certificate parseCertificate(String data) throws CertificateException, IOException {
        JcaX509CertificateConverter converter = new JcaX509CertificateConverter().setProvider("BC");
        return converter.getCertificate(new X509CertificateHolder(Base64.decode(data)));
    }

    /**
     * Parses a comma separated list of base64 encoded certificates
     *
     * @param dataList The, comma separated, list of base64 encoded certificates
     * @return A list of parsed {@link X509Certificates}
     * @throws CertificateException
     * @throws IOException
     */
    private static List<X509Certificate> parseCertificates(String dataList) throws CertificateException, IOException {
        if (dataList != null && !dataList.isEmpty()) {
            final String[] certArray = dataList.split(",");
            final ArrayList<X509Certificate> parsedCertificates = new ArrayList<X509Certificate>(certArray.length);
            for (final String certImp : certArray) {
                if (!certImp.isEmpty()) {
                    parsedCertificates.add(parseCertificate(certImp));
                }
            }
            return parsedCertificates;
        }
        return Collections.emptyList();
    }

    /**
     * Encodes the given {@link X509Certificate} into it's base64 string representation
     *
     * @param certificate The certificate to encode
     * @return the Base64 encoded certificate
     * @throws CertificateEncodingException
     */
    private static String encodeCertificate(X509Certificate certificate) throws CertificateEncodingException {
        return Base64.encode(certificate.getEncoded());
    }

    /**
     * Encodes a given list og {@link X509Certificate}s into a comma separated, list where each certificate is base64 encoded
     *
     * @param certificates The list of certificates to encode
     * @return A comma separated list of base64 encoded certificates
     * @throws CertificateEncodingException
     */
    private static String encodeCertificates(List<X509Certificate> certificates) throws CertificateEncodingException {
        final StringBuilder sb = new StringBuilder();
        for (final X509Certificate cert : certificates) {
            sb.append(encodeCertificate(cert));
            sb.append(",");
        }
        return sb.toString();
    }

    @SuppressWarnings("resource")
    @Override
    public List<SmimeKeys> getKeys() throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<SmimeKeys> keys = new ArrayList<SmimeKeys>(1);
        try {
            stmt = con.prepareStatement(SmimeStorageSql.SELECT_ALL_PUBLIC_KEYS);
            rs = stmt.executeQuery();
            while (rs.next()) {
                java.sql.Date lastSeen = rs.getDate("lastSeen");
                //@formatter:off
                SmimeKeys key = new SmimeKeys(parseCertificate(rs.getString("publicKey")),
                    parseCertificates(rs.getString("chain")),
                    rs.getBoolean("userKey"),
                    rs.getInt("userid"),
                    rs.getInt("cid"),
                    rs.getString("email"),
                    lastSeen == null ? 0 : lastSeen.getTime());
                //@formatter:on
                keys.add(key);
            }
        } catch (SQLException | IOException | CertificateException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return keys;
    }

    @SuppressWarnings("resource")
    @Override
    public List<SmimeKeys> getKeysExpiring(int days) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<SmimeKeys> keys = new ArrayList<SmimeKeys>(1);
        try {
            stmt = con.prepareStatement(SmimeStorageSql.SELECT_LOCAL_PUBLIC_KEYS_EXPIRING);
            stmt.setInt(1, days);
            rs = stmt.executeQuery();
            while (rs.next()) {
                java.sql.Date lastSeen = rs.getDate("lastSeen");
                //@formatter:off
                SmimeKeys key = new SmimeKeys(parseCertificate(rs.getString("publicKey")),
                    parseCertificates(rs.getString("chain")),
                    rs.getBoolean("userKey"),
                    rs.getInt("userid"),
                    rs.getInt("cid"),
                    rs.getString("email"),
                    lastSeen == null ? 0 : lastSeen.getTime());
                //@formatter:on
                keys.add(key);
            }
        } catch (SQLException | IOException | CertificateException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return keys;
    }

    @SuppressWarnings("resource")
    @Override
    public List<SmimeKeys> getKeys(String email) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<SmimeKeys> keys = new ArrayList<SmimeKeys>(1);
        try {
            stmt = con.prepareStatement(SmimeStorageSql.SELECT_PUBLIC_KEY_EMAIL_ALT);
            stmt.setString(1, email);
            stmt.setString(2, email);
            rs = stmt.executeQuery();
            while (rs.next()) {
                java.sql.Date lastSeen = rs.getDate("lastSeen");
                //@formatter:off
                SmimeKeys key = new SmimeKeys(parseCertificate(rs.getString("publicKey")),
                    parseCertificates(rs.getString("chain")),
                    rs.getBoolean("userKey"),
                    rs.getInt("userid"),
                    rs.getInt("cid"),
                    rs.getString("email"),
                    lastSeen == null ? 0 : lastSeen.getTime());
                //@formatter:on
                keys.add(key);
            }
        } catch (SQLException | IOException | CertificateException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return keys;
    }

    @SuppressWarnings("resource")
    @Override
    public SmimeKeys getKey(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SmimeStorageSql.SELECT_PUBLIC_KEY_SERIAL);
            int i = 1;
            stmt.setString(i++, serial);
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            rs = stmt.executeQuery();
            if (rs.next()) {
                java.sql.Date lastSeen = rs.getDate("lastSeen");
                //@formatter:off
                return new SmimeKeys(parseCertificate(rs.getString("publicKey")),
                    parseCertificates(rs.getString("chain")),
                    rs.getBoolean("userKey"),
                    rs.getInt("userid"),
                    rs.getInt("cid"),
                    rs.getString("email"),
                    lastSeen == null ? 0 : lastSeen.getTime());
                //@formatter:on
            }
        } catch (SQLException | IOException | CertificateException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return null;
    }

    private void updateLastSeen(ResultSet rs, String serial, Date fromDate) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        @SuppressWarnings("resource") Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            int userId = rs.getInt("userid");
            int cid = rs.getInt("cid");
            stmt = con.prepareStatement(SmimeStorageSql.UPDATE_LAST_SEEN);
            int i = 1;
            stmt.setDate(i++, new java.sql.Date(fromDate.getTime()));
            stmt.setString(i++, serial);
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.execute();
        } catch (SQLException e) {
            LOG.error("Problem updating last seen date", e);
        } finally {
            if (stmt != null) {
                DBUtils.closeSQLStuff(stmt);
            }
            databaseService.backWritableForGuard(con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean keyExists(String serial, String email, Date fromDate) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnlyForGuard();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SmimeStorageSql.SELECT_PUBLIC_SERIAL_EMAIL);
            stmt.setString(1, serial.toString());
            stmt.setString(2, email);
            rs = stmt.executeQuery();
            if (rs.next()) {
                java.sql.Date date = rs.getDate("lastSeen");
                if (date == null) {
                    if (fromDate != null) {
                        updateLastSeen(rs, serial, fromDate);
                    }
                } else if (fromDate != null && fromDate.getTime() > date.getTime()) {
                    updateLastSeen(rs, serial, fromDate);
                }
                return true;
            }
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnlyForGuard(con);
        }
        return false;
    }

    /**
     * Write alternate emails to smime_alt table
     *
     * @param keys SmimeKeys containing emails
     * @param con Write database connection
     * @throws SQLException
     */
    private void writeAltEmails(SmimeKeys keys, Connection con) throws SQLException {
        List<String> emails = keys.getEmails();
        if (emails == null || emails.size() < 2) {
            return;
        }
        for (String e : emails) {
            PreparedStatement stmt = null;
            try {
                // If adding a local key, will override userid and cid
                stmt = con.prepareStatement(SmimeStorageSql.INSERT_ALT_EMAILS);
                stmt.setString(1, e);
                stmt.setString(2, keys.getSerial().toString());
                stmt.setInt(3, keys.getUserId());
                stmt.setInt(4, keys.getContextId());
                stmt.execute();
            } finally {
                if (stmt != null) {
                    DBUtils.closeSQLStuff(stmt);
                }
            }
        }
    }

    @SuppressWarnings("resource")
    @Override
    public void storePublicKeys(int userId, int cid, SmimeKeys keys, Date fromDate) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            Databases.startTransaction(con);
            // Write the alternate emails in the alt table
            writeAltEmails(keys, con);
            // If adding a local key, will override userid and cid
            stmt = con.prepareStatement(keys.isLocal() ? SmimeStorageSql.INSERT_SMIME_KEY_LOCAL : SmimeStorageSql.INSERT_SMIME_KEY);
            int i = 1;
            stmt.setString(i++, keys.getSerial().toString());
            stmt.setString(i++, keys.getEmail());
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, encodeCertificate(keys.getCertificate()));
            stmt.setString(i++, encodeCertificates(keys.getChain()));
            stmt.setDate(i++, new java.sql.Date(keys.getExpires().getTime()));
            stmt.setBoolean(i++, keys.getPrivateKey() != null);
            stmt.setDate(i++, new java.sql.Date(fromDate.getTime()));
            if (keys.isLocal()) {
                stmt.setInt(i++, userId);
                stmt.setInt(i++, cid);
                stmt.setBoolean(i++, true);
            }
            stmt.setDate(i++, new java.sql.Date(fromDate.getTime()));
            stmt.execute();
            con.commit();
        } catch (SQLException | CertificateEncodingException e) {
            Databases.rollback(con);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            if (stmt != null) {
                DBUtils.closeSQLStuff(stmt);
            }
            Databases.autocommit(con);
            databaseService.backWritableForGuard(con);
        }
    }

    /**
     * Delete alternate subject email aliases from smime_alt table for the serial
     * deleteAltById
     *
     * @param serial
     * @param con
     * @throws OXException
     */
    private void deleteAltById(String serial, int userId, int cid, Connection con) throws OXException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_ALT_SERIAL);
            int i = 1;
            stmt.setString(i++, serial);
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.execute();
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteKeyById(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            Databases.startTransaction(con);
            deleteAltById(serial, userId, cid, con);
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_PUBLIC_KEY_SERIAL);
            int i = 1;
            stmt.setString(i++, serial);
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            int deleted = stmt.executeUpdate();
            con.commit();
            return deleted > 0;
        } catch (SQLException e) {
            Databases.rollback(con);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            Databases.autocommit(con);
            databaseService.backWritableForGuard(con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteForUser(int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            Databases.startTransaction(con);
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_ALT_BY_USER);
            stmt.setInt(1, userId);
            stmt.setInt(2, cid);
            stmt.execute();
            DBUtils.closeSQLStuff(stmt);
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_PUBLIC_BY_USER);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            int deleted = stmt.executeUpdate();
            con.commit();
            return deleted > 0;
        } catch (SQLException e) {
            Databases.rollback(con);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            Databases.autocommit(con);
            databaseService.backWritableForGuard(con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteForContext(int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritableForGuard();
        PreparedStatement stmt = null;
        try {
            Databases.startTransaction(con);
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_ALT_BY_CONTEXT);
            stmt.setInt(1, cid);
            stmt.execute();
            DBUtils.closeSQLStuff(stmt);
            stmt = con.prepareStatement(SmimeStorageSql.DELETE_PUBLIC_BY_CONTEXT);
            int i = 1;
            stmt.setInt(i++, cid);
            int deleted = stmt.executeUpdate();
            con.commit();
            return deleted > 0;
        } catch (SQLException e) {
            Databases.rollback(con);
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            Databases.autocommit(con);
            databaseService.backWritableForGuard(con);
        }
    }
}
