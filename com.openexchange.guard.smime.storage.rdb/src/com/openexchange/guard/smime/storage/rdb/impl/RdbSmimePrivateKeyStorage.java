/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.impl;

import java.security.PrivateKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.commons.SmimePrivateKeys;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.keymanagement.services.KeyRecoveryService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.smime.SmimeCryptoKeyUtil;
import com.openexchange.guard.smime.exceptions.SmimeExceptionCodes;
import com.openexchange.guard.smime.storage.SmimePrivateKeyStorage;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RdbSmimePrivateKeyStorage} is a database specific implementation of {@link SmimePrivateKeyStorage}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class RdbSmimePrivateKeyStorage implements SmimePrivateKeyStorage {

    private final static String INSERT_PRIVATE_KEY = "INSERT INTO og_smime_private (`userId`, `cid`, `key`, `recovery`, `salt`, `email`, `recovery_email`, `masterKeyIndex`, `serial`, `current`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private final static String UPDATE_PRIVATE_KEY = "UPDATE og_smime_private SET `key` = ?, `recovery` = ?, `salt` = ?, `email` = ?, `recovery_email` = ?, `masterKeyIndex` = ?, `current` = ? WHERE userId = ? AND cid = ? AND serial = ?";

    private final static String CLEAR_CURRENT_FLAG = "UPDATE og_smime_private SET current = 0 WHERE userId = ? AND cid = ?";
    private final static String SELECT_BY_SERIAL = "SELECT * FROM og_smime_private WHERE userId = ? AND cid = ? AND serial = ?";
    private final static String EXISTS = "SELECT EXISTS (SELECT 1 FROM og_smime_private WHERE userId= ? AND cid= ? AND serial = ?);";
    private final static String DELETE_BY_SERIAL = "DELETE FROM og_smime_private WHERE userId = ? AND cid = ? AND serial = ?";
    private final static String DELETE_BY_USER = "DELETE FROM og_smime_private WHERE userId = ? AND cid = ?";
    private final static String DELETE_BY_CONTEXT = "DELETE FROM og_smime_private WHERE cid = ?";
    private final static String SELECT_BY_USER = "SELECT * FROM og_smime_private WHERE userId = ? AND cid = ?";
    private final static String MAKE_CURRENT = "UPDATE og_smime_private SET current = 1 WHERE userId = ? AND cid = ? AND serial = ?";

    private final ServiceLookup services;
    private final RdbSmimePublicKeyStorage publicKeyStorage;

    /**
     * Initializes a new {@link RdbSmimePrivateKeyStorage}.
     *
     * @param services The service lookup
     * @param publicKeyStorage the public key storage
     */
    public RdbSmimePrivateKeyStorage(ServiceLookup services, RdbSmimePublicKeyStorage publicKeyStorage) {
        this.services = Objects.requireNonNull(services, "services must not be null");
        this.publicKeyStorage = Objects.requireNonNull(publicKeyStorage, "publicKeyStorage must not be null");
    }

    /**
     * Create SmimePrivateKeys from the resultSet
     *
     * @param rs The {@link ResultSet} to create the {@link SmimePrivateKeys} instance from
     * @return The {@link SmimePrivateKeys} created from the given {@link ResultSet}
     * @throws SQLException
     */
    public static SmimePrivateKeys createKeyFromRS(ResultSet rs) throws SQLException {
        //@formatter:off
        return new SmimePrivateKeys(rs.getString("serial"),
            rs.getString("key"),
            rs.getString("salt"),
            rs.getString("recovery"),
            rs.getInt("masterKeyIndex"),
            rs.getString("email"),
            rs.getString("recovery_email"),
            rs.getBoolean("current"));
        //@formatter:on
    }

    /**
     * Internal method to clear the "current" flag for every known key
     *
     * @param con The {@linkl Connection} to use
     * @param userId The id of the user to clear the key for
     * @param cid The cid of the user to clear the key for
     * @throws SQLException
     */
    private void clearCurrent(Connection con, int userId, int cid) throws SQLException {
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(CLEAR_CURRENT_FLAG);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.execute();
        } finally {
            DBUtils.closeSQLStuff(stmt);
        }
    }

    /**
     * Checks whether or not the certificate with the given serial exists for a user
     *
     * @param serial The serial to check
     * @param userId The ID for the user
     * @param cid The context ID for the user
     * @return <code>True</code> if the key exists, <code>false</code> otherwise.
     * @throws Exception
     */
    @SuppressWarnings("resource")
    private boolean keyExists(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnly(cid);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(EXISTS);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, serial);
            rs = stmt.executeQuery();
            return rs.next() && rs.getBoolean(1);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnly(cid, con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public List<SmimePrivateKeys> getPrivateKeys(int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnly(cid);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        ArrayList<SmimePrivateKeys> keys = new ArrayList<SmimePrivateKeys>();
        try {
            stmt = con.prepareStatement(SELECT_BY_USER);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            rs = stmt.executeQuery();
            while (rs.next()) {
                // return smimekey
                keys.add(createKeyFromRS(rs));
            }
            return keys;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnly(cid, con);
        }
    }

    @Override
    public SmimePrivateKeys getCurrentPrivateKey(int userId, int cid) throws OXException {
        List<SmimePrivateKeys> keys = getPrivateKeys(userId, cid);
        if (keys.isEmpty()) {
            return null;
        }
        Optional<SmimePrivateKeys> current = keys.stream().filter(k -> k.isCurrent()).findFirst();
        return current.orElse(null);
    }

    @SuppressWarnings("resource")
    private void updateKeys(SmimeKeys keys, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        SmimePrivateKeys pkey = keys.getPrivateKey();

        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        boolean rollback = true;
        // Begin transaction
        try {
            DBUtils.startTransaction(con);
            if (pkey.isCurrent()) {
                clearCurrent(con, userId, cid);  // If this key is current, then mark others as not
            }
            stmt = con.prepareStatement(UPDATE_PRIVATE_KEY);
            int i = 1;
            stmt.setString(i++, pkey.getEncryptedKeyData());
            stmt.setString(i++, pkey.getRecovery());   /// recovery
            stmt.setString(i++, pkey.getSalt());   /// salt
            stmt.setString(i++, pkey.getEmail());
            stmt.setString(i++, pkey.getRecoveryEmail());
            stmt.setInt(i++, pkey.getMasterKeyIndex()); // master key index
            stmt.setBoolean(i++, pkey.isCurrent());
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, keys.getSerial().toString());
            stmt.execute();
            rollback = false;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            if (stmt != null) {
                DBUtils.closeSQLStuff(stmt);
            }
            if (rollback) {
                DBUtils.rollback(con);
            }
            DBUtils.autocommit(con);   // Restore autocommit
            databaseService.backWritableForGuard(con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public void storePrivateKey(SmimeKeys keys, int userId, int cid, boolean replaceIfPresent) throws OXException {
        if (keyExists(keys.getSerial().toString(), userId, cid)) {
            if (!replaceIfPresent) {
                throw SmimeExceptionCodes.DUPLICATE_PRIVATE_KEY.create();
            }
            // updatekey
            updateKeys(keys, userId, cid);
            return;
        }
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        SmimePrivateKeys pkey = keys.getPrivateKey();

        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        // Begin transaction
        boolean rollback = false;
        try {
            DBUtils.startTransaction(con);
            if (pkey.isCurrent()) {
                clearCurrent(con, userId, cid);  // If this key is current, then mark others as not
            }
            stmt = con.prepareStatement(INSERT_PRIVATE_KEY);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, pkey.getEncryptedKeyData());
            stmt.setString(i++, pkey.getRecovery());   /// recovery
            stmt.setString(i++, pkey.getSalt());   /// salt
            stmt.setString(i++, keys.getEmail());
            stmt.setString(i++, pkey.getRecoveryEmail());
            stmt.setInt(i++, pkey.getMasterKeyIndex()); // master key index
            stmt.setString(i++, keys.getSerial().toString());
            stmt.setBoolean(i++, pkey.isCurrent());
            stmt.execute();
            try {
                publicKeyStorage.storePublicKeys(userId, cid, keys, new Date());
                con.commit();
            } catch (OXException ex) {
                rollback = true;
                throw ex;
            }
        } catch (SQLException e) {
            rollback = true;
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            if (stmt != null) {
                DBUtils.closeSQLStuff(stmt);
            }
            if (rollback) {
                DBUtils.rollback(con);
            }
            DBUtils.autocommit(con);   // Restore autocommit
            databaseService.backWritableForGuard(con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public SmimePrivateKeys getKeyById(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getReadOnly(cid);
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_BY_SERIAL);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, serial);
            rs = stmt.executeQuery();
            if (rs.next()) {
                // return smimekey
                return createKeyFromRS(rs);
            }
            return null;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
            databaseService.backReadOnly(cid, con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteKeyById(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_BY_SERIAL);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, serial);
            stmt.execute();
            return stmt.getUpdateCount() > 0;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            databaseService.backWritable(cid, con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean makeCurrent(String serial, int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        // Begin transaction
        boolean rollback = false;
        try {
            DBUtils.startTransaction(con);
            clearCurrent(con, userId, cid);
            stmt = con.prepareStatement(MAKE_CURRENT);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            stmt.setString(i++, serial);
            stmt.execute();
            int count = stmt.getUpdateCount();
            if (count > 0) {
                con.commit();
                return true;
            }
            rollback = true;
            return false;
        } catch (SQLException e) {
            rollback = true;
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            if (rollback) {
                DBUtils.rollback(con);
            }
            DBUtils.autocommit(con);   // Restore autocommit
            databaseService.backWritableForGuard(con);
        }
    }

    @Override
    public boolean updateRecovery(SmimePrivateKeys key, String password, int userId, int cid) throws OXException {
        MasterKeyService keyService = services.getServiceSafe(MasterKeyService.class);
        int index = keyService.getIndexForUser(userId, cid);
        KeyRecoveryService keyRecoveryService = services.getServiceSafe(KeyRecoveryService.class);
        String salt = key.getSalt();
        String extra = key.getSerial();
        String email = key.getEmail();
        String recovery = null;
        if (keyRecoveryService.checkCreateRecovery(cid, userId)) {
            recovery = keyRecoveryService.createRecovery(index, CipherUtil.getSHA(password, salt), salt, extra, userId, cid, email);
            key.setRecovery(recovery);
            return true;
        }
        return false;
    }

    @Override
    public boolean changePasswordWithRecovery(SmimePrivateKeys key, String newPass, int userId, int cid) throws OXException {
        MasterKeyService keyService = services.getServiceSafe(MasterKeyService.class);
        int index = keyService.getIndexForUser(userId, cid);
        KeyRecoveryService keyRecoveryService = services.getServiceSafe(KeyRecoveryService.class);
        String salt = key.getSalt();
        String extra = key.getSerial();
        String recovery = key.getRecovery();
        if (recovery == null || recovery.isEmpty()) {
            return false;
        }
        String recoveryHash = keyRecoveryService.getRecoveryHash(recovery, index, salt, extra, userId, cid);
        final boolean hashPassword = false; /* do not hash the password for decrypting the key because we are using the recovery here */
        try {
            PrivateKey privKey = SmimeCryptoKeyUtil.decryptPrivateKey(key.getEncryptedKeyData(), recoveryHash, key.getSalt(), hashPassword);
            String encryptedPrivateKeyData = SmimeCryptoKeyUtil.encryptPrivateKey(privKey, newPass, key.getSalt());
            key.setEncryptedKeyData(encryptedPrivateKeyData);
            return true;
        }
        catch(OXException e) {
            if (e.similarTo(GuardAuthExceptionCodes.BAD_PASSWORD)) {
                throw SmimeExceptionCodes.SMIME_ERROR.create(e, "Unable to restore from recovery");
            }
            throw e;
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteForUser(int userId, int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_BY_USER);
            int i = 1;
            stmt.setInt(i++, userId);
            stmt.setInt(i++, cid);
            return stmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            databaseService.backWritable(cid, con);
        }
    }

    @SuppressWarnings("resource")
    @Override
    public boolean deleteForContext(int cid) throws OXException {
        GuardDatabaseService databaseService = services.getServiceSafe(GuardDatabaseService.class);
        Connection con = databaseService.getWritable(cid);
        PreparedStatement stmt = null;
        try {
            stmt = con.prepareStatement(DELETE_BY_CONTEXT);
            int i = 1;
            stmt.setInt(i++, cid);
            stmt.execute();
            return stmt.getUpdateCount() > 0;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            databaseService.backWritable(cid, con);
        }
    }
}
