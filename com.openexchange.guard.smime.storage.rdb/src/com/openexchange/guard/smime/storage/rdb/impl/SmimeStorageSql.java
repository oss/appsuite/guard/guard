/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.impl;


/**
 * {@link SmimeStorageSql}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
class SmimeStorageSql {

    public final static String INSERT_SMIME_KEY = "INSERT INTO smime_pub (serial, email, userid, cid, publicKey, chain, expires, userKey, lastSeen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE lastSeen = ?";
    public final static String INSERT_SMIME_KEY_LOCAL =
        "INSERT INTO smime_pub (serial, email, userid, cid, publicKey, chain, expires, userKey, lastSeen) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE userid = ?, cid = ?, userKey = userKey || ?, lastSeen = ?";

    public final static String SELECT_PUBLIC_SERIAL_EMAIL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE serial = ? AND email = ?";
    public final static String SELECT_ALL_PUBLIC_KEYS = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub;";
    public final static String SELECT_LOCAL_PUBLIC_KEYS_EXPIRING = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE `expires` >= date(NOW()) AND `expires` <= DATE_ADD(NOW(), INTERVAL ? Day) AND `userKey` = 1;";
    public final static String SELECT_PUBLIC_KEY_EMAIL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE email = ?";
    public final static String SELECT_PUBLIC_KEY_EMAIL_ALT = "SELECT * from smime_pub sp left outer join smime_alt sa on (sp.serial = sa.serial and sp.cid = sa.cid and sp.userid = sa.userid) where (sp.email = ? or sa.email = ?)";
    public final static String SELECT_PUBLIC_KEY_SERIAL = "SELECT `email`, `userid`, `cid`, `publicKey`, `chain`, `expires`, `userKey`, `lastSeen` from smime_pub WHERE serial = ? AND userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_KEY_SERIAL = "DELETE from smime_pub WHERE serial = ? AND userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_BY_USER = "DELETE from smime_pub WHERE userid = ? AND cid = ?";
    public final static String DELETE_PUBLIC_BY_CONTEXT = "DELETE from smime_pub WHERE cid = ?";
    public final static String UPDATE_LAST_SEEN = "UPDATE smime_pub SET `lastSeen` = ? WHERE serial = ? AND userid = ? AND cid = ?";

    public final static String INSERT_ALT_EMAILS = "INSERT INTO `smime_alt` (`email`, `serial`, `userid`, `cid`) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE `cid` = `cid`";
    public final static String DELETE_ALT_SERIAL = "DELETE FROM `smime_alt` WHERE `serial` = ? AND `userid` = ? AND `cid` = ?";
    public final static String DELETE_ALT_BY_CONTEXT = "DELETE from `smime_alt` WHERE `cid` = ?";
    public final static String DELETE_ALT_BY_USER = "DELETE from `smime_alt` WHERE `userid` = ? and `cid` = ?";
}
