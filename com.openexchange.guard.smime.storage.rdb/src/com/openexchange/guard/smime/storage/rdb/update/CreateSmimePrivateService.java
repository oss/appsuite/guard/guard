/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.storage.rdb.update;

import com.openexchange.database.AbstractCreateTableImpl;

/**
 * {@link CreateSmimePrivateService} Creates the SmimePrivateKey table
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CreateSmimePrivateService extends AbstractCreateTableImpl {

    private String createSmimePrivateTable() {
        return "CREATE TABLE `og_smime_private` (\n"
            + "  `id` int(11) NOT NULL AUTO_INCREMENT,\n"
            + "  `userId` int(11) DEFAULT NULL,\n"
            + "  `cid` int(11) DEFAULT NULL,\n"
            + "  `key` varchar(4096) DEFAULT NULL,\n"
            + "  `recovery` varchar(512) DEFAULT NULL,\n"
            + "  `salt` varchar(100) DEFAULT NULL,\n"
            + "  `email` varchar(100) DEFAULT NULL,\n"
            + "  `masterKeyIndex` int(11) DEFAULT NULL,\n"
            + "  `recovery_email` varchar(100) DEFAULT NULL,\n"
            + "  `serial` varchar(100) DEFAULT NULL,\n"
            + "  `current` bit(1) DEFAULT NULL,\n"
            + "  PRIMARY KEY (`id`),\n"
            + "  KEY `og_smime_private_userId_IDX` (`userId`,`cid`) USING BTREE\n"
            + ") ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;";
    }

    @Override
    public String[] requiredTables() {
        return new String[] {};
    }

    @Override
    public String[] tablesToCreate() {
        return new String[] { "og_smime_private" };
    }

    @Override
    protected String[] getCreateStatements() {
        return new String[] { createSmimePrivateTable() };
    }

}
