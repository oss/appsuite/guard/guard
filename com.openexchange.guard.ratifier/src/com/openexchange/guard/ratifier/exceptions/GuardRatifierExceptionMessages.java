
package com.openexchange.guard.ratifier.exceptions;

import com.openexchange.i18n.LocalizableStrings;

public class GuardRatifierExceptionMessages implements LocalizableStrings {

    /** Returned (for multiple actions) when an invalid email address has been provided */
    public static final String INVALID_EMAIL_ADDRESS_MSG = "The given Email address %1$s is invalid";
    public static final String INVALID_DATE_RANGE = "The date is outside expected range";

    private GuardRatifierExceptionMessages() {
        super();
    }
}
