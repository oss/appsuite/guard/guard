/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.ratifier;

import com.openexchange.exception.OXException;

/**
 * {@link GuardRatifierService}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public interface GuardRatifierService {

    /**
     * Validates the specified e-mail address
     *
     * @param email The e-mail to validate
     * @throws OXException in case of an validation error
     */
    void validate(String email) throws OXException;

    /**
     * Normalise the specified string. Primarily for PGP in-line.
     *
     * @param data The data to normalise
     * @return The normalised string
     */
    String normalise(String data) throws OXException;

    /**
     * Verifies the ipAddress string is a valid IP
     * @param ipAddress
     * @return ipAddress if valid, null if not
     */
    String verifyIPAddress (String ipAddress);

    /**
     * Validates that the ipAddress string is a valid remote IP address
     * @param ipAddress
     * @return ipAddress if valid, null if not or local IP address
     */
    String verifyRemoteIP(String ipAddress);

    /**
     * Verify that that a date is within a reasonable time frame.  Must be within the last 10 years,
     * and not more than 1 day in the future (to allow for clock async)
     * @throws OXException
     */
    void validateReasonablePastDate(Long ticks) throws OXException;

    /**
     * @param email
     * @return true if valid email address
     */
    boolean isValid(String email);
}
