/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.settings.internal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.settings.GlobalSettingsStorage;
import com.openexchange.guard.settings.osgi.Services;

/**
 * {@link GlobalSettingsStorageImpl} provides a DB implementation of the GlobalSettingsStorage
 */
public class GlobalSettingsStorageImpl implements GlobalSettingsStorage {

    @Override
    public String getByKey(String key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(GlobalSettingsSql.SELECT_STMT);
            stmt.setString(1, key);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return resultSet.getString(1);
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void update(String key, String value) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(GlobalSettingsSql.UPDATE_STMT);
            stmt.setString(1, value);
            stmt.setString(2, key);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }
}
