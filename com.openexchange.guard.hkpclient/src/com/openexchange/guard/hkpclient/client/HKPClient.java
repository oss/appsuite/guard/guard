
package com.openexchange.guard.hkpclient.client;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TimerTask;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.config.MessageConstraints;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.clients.utils.TimerUtils;
import com.openexchange.guard.common.streams.SizeLimitedInputStream;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.server.ServiceLookup;

/**
 *
 * {@link HKPClient} Basic HKP client for searching and downloading public keys from HKP Servers
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class HKPClient {

    private static final Logger LOG = LoggerFactory.getLogger(HKPClient.class);

    private final static String HKP = "hkp://";
    private final static String HKPS = "hkps://";
    private final static String HTTP = "http://";
    private final static String HTTPS = "https://";
    private final static int HTTPS_DEFAULT_PORT = 443;
    private final static String DEFAULT_HKP_PATH = "pks/lookup?";
    private final static String HKP_OP_SEARCH = "op=index&options=mr&search=";
    private final static String HKP_OP_GETKEY = "op=get&options=mr&search=0x";
    private final static String HKP_OP_GETEMAIL = "op=get&options=mr&search=";

    private final String clientToken;
    private final TLSMODE tlsMode;
    private final ServiceLookup services;

    /**
     *
     * {@link TLSMODE} specifies the TLS connection handling for the {@link HKPClient}
     *
     * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
     * @since 2.4.2
     */
    public enum TLSMODE {
        /**
         * Using only TLS to connect to HKP servers
         */
        TLS,
        /**
         * Using only plain HTTP to connect to HKP servers
         */
        NO_TLS,
        /**
         * Determine by port. Using TLS for connecting on port 443, HTTP for all other ports.
         */
        BY_PORT,
    }

    /**
     * Initializes a new {@link HKPClient} with {@link TLSMODE} set to <i>TLSMODE.BY_PORT<i> by default.
     *
     * @param clientToken A token which will be set in the "X-UI-INTERNAL-ACCOUNT-ID" header or null to omit this header
     */
    public HKPClient(String clientToken, ServiceLookup services) {
        this(clientToken, TLSMODE.BY_PORT, services);
    }

    /**
     * Initializes a new {@link HKPClient}.
     *
     * @param clientToken A token which will be set in the "X-UI-INTERNAL-ACCOUNT-ID" header or null to omit this header
     * @param tlsMode The TLS mode to use
     */
    public HKPClient(String clientToken, TLSMODE tlsMode, ServiceLookup services) {
        this.clientToken = clientToken;
        this.tlsMode = tlsMode;
        this.services = services;
    }

    private HttpClient getClient(int connectionTimeout) {
        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectTimeout(connectionTimeout)
            .setSocketTimeout(connectionTimeout)
            .setRedirectsEnabled(false)
            .build();
        return HttpClientBuilder.create()
            .setDefaultRequestConfig(requestConfig)
            .setDefaultConnectionConfig(
                ConnectionConfig.custom().setMessageConstraints(
                    MessageConstraints.custom()
                        .setMaxLineLength(10000)
                        .setMaxHeaderCount(200)
                        .build())
                    .build())
            .build();
    }

    /**
     * Checks the size of the response. Rejects if too large
     * checkSize
     *
     * @param response
     * @throws OXException If too large
     */
    private int checkSize(HttpResponse response) throws IOException {
        GuardConfigurationService config = services.getService(GuardConfigurationService.class);
        if (config == null) {
            return 0;
        }
        int maxKeySize = config.getIntProperty(GuardProperty.maxRemoteKeySize);
        if (maxKeySize < response.getEntity().getContentLength()) {
            throw new IOException("Remote key rejected due to size");
        }
        return maxKeySize;

    }

    /**
     * Internal method to perform HTTP GET requests
     *
     * @param url the URL to GET
     * @return response as string
     * @throws OXException
     */
    private String doRequest(String url, int timeout) throws OXException {
        if (timeout <= 0) {
            return null;  // Already timed out
        }
        if (url.startsWith(HKP)) {
            url = url.replace(HKP, HTTP);
        } else if (url.startsWith(HKPS)) {
            url = url.replace(HKPS, HTTPS);
        }
        HttpClient httpClient = getClient(timeout);
        HttpGet getRequest = new HttpGet(url);
        TimerTask timer = TimerUtils.limitUriRequest(getRequest, timeout);
        HttpResponse response = null;
        try {
            getRequest.addHeader("accept", "application/json");
            getRequest.setHeader("Connection", "close");
            if (clientToken != null) {
                getRequest.addHeader("X-UI-INTERNAL-ACCOUNT-ID", clientToken);
            }
            response = httpClient.execute(getRequest);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == HttpStatus.SC_OK) {
                int maxSize = checkSize(response);
                try (InputStream keyData = new SizeLimitedInputStream(response.getEntity().getContent(), maxSize)) {
                    return IOUtils.toString(keyData, StandardCharsets.UTF_8.name());
                }
            }
            if (statusCode == HttpStatus.SC_NOT_IMPLEMENTED) {
                throw HKPClientExceptionCodes.UNSUPPORTED.create();
            }
        } catch (IOException ste) {
            if (ste.getClass().equals(java.net.SocketTimeoutException.class)) {
                LOG.warn("Timeout waiting for remote key server " + url + " - time remaining in lookup queue was " + timeout + "ms");
            } else {
                LOG.warn("Unable to retrieve keys from server.", ste);
            }
        } finally {
            HttpClientUtils.closeQuietly(response);
            getRequest.releaseConnection();
            HttpClientUtils.closeQuietly(httpClient);
            if (timer != null) {
                timer.cancel();
            }
        }
        return null;
    }

    /**
     * Internal method to execute a HKP-index operation
     *
     * @param url the full URL to the HKP lookup path (i.e hkp://example.org:11371/custom/path/to/pks/lookup?). The lookup path can be omitted (i.e. hkp://example.org:11371/) , in this case the default HKP PATH is added)
     * @param email the email to get the public keys for
     * @return the sever's response as string
     * @throws Exception
     */
    private String index(String url, String email, int timeout) throws Exception {
        if (!url.contains("lookup?")) {
            if (!url.endsWith("/")) {
                url += "/";
            }
            url += DEFAULT_HKP_PATH;
        }
        String queryUrl = url + HKP_OP_SEARCH + email;
        try {
            String answer = doRequest(queryUrl, timeout);
            return (answer);
        } catch (OXException x) {
            if (HKPClientExceptionCodes.UNSUPPORTED.equals(x)) {
                return (get(url, email, timeout));
            }
        }
        return null;
    }

    /**
     * Internal method to execute a HKP-get operation
     *
     * @param url the full URL to the HKP lookup path (i.e hkp://example.org:11371/custom/path/to/pks/lookup?). The lookup path can be omitted (i.e. hkp://example.org:11371/) , in this case the default HKP PATH is added)
     * @param keyid The id of the key to get
     * @return The requested key data as String or null if no such key was found
     * @throws Exception
     */
    private String get(String url, String keyid, int timeout) throws Exception {
        if (!url.contains("lookup?")) {
            if (!url.endsWith("/")) {
                url += "/";
            }
            url += DEFAULT_HKP_PATH;
        }
        String params = HKP_OP_GETKEY + keyid;
        if (keyid.contains("@")) {
            params = HKP_OP_GETEMAIL + keyid;
        }
        return doRequest(url + params, timeout);
    }

    /**
     * Internal method for parsing a HKP-index response from a HKP server and extract the public-ids related to the given email
     *
     * @param indexResponse The response from from an HKP-index request
     * @param email The email to extract the public ids for
     * @return A list of public ids for the given email
     */
    private ArrayList<String> parseIndexResponse(String indexResponse, String email) {
        String[] lines = indexResponse.split("\n");
        ArrayList<String> pubids = new ArrayList<String>();
        String pub = "";
        for (String line : lines) {
            // Get count of keys
            /*
             * if (line.startsWith("info")) {
             * int i = line.lastIndexOf(":");
             * String count = line.substring(i + 1).trim();
             * }
             */
            if (line.startsWith("pub")) {
                String[] data = line.split(":");
                pub = data[1].trim();
                if (pub.length() > 16) {
                    pub = pub.substring(pub.length() - 16);
                }
            }
            if (line.startsWith("uid")) {
                if (email != null && line.toLowerCase().contains(email.toLowerCase())) {
                    if (!pubids.contains(pub)) {
                        pubids.add(pub);
                    }
                }
            }
        }
        return pubids;
    }

    /**
     * Internal method for parsing a HKP-get response from a HKP server
     *
     * @param getResponse the HKP-get response to parse
     * @return the parsed PGPPublicKeyRing
     * @throws Exception
     */
    private PGPPublicKeyRing parseGetResponse(String getResponse) throws Exception {
        if (getResponse == null) {
            return null;
        }
        if (getResponse.contains("PUBLIC KEY BLOCK")) {
            getResponse = getResponse.substring(getResponse.indexOf("-----BEGIN"));
            getResponse = getResponse.substring(0, getResponse.lastIndexOf("BLOCK-----") + 10);
            PGPPublicKeyRing ring = PGPPublicKeyRingFactory.create(getResponse);
            if (!ring.getPublicKey().hasRevocation()) {
                return ring;
            }
        }
        return null;
    }

    /**
     * Internal method determine the protocol to use
     *
     * @param port The port
     * @return The protocol to use
     */
    private String getProtocol(int port) {
        if (this.tlsMode == TLSMODE.TLS ||
            this.tlsMode == TLSMODE.BY_PORT && port == HTTPS_DEFAULT_PORT) {
            return HTTPS;
        }
        return HTTP;
    }

    /**
     * Internal method to construct the URL for accessing the HKP Server
     *
     * @param host The host of the HKP Server
     * @param port The port to use
     * @return The URL for accessing the HKP Server
     */
    private String getURL(String host, int port) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(getProtocol(port));
        urlBuilder.append(host);
        urlBuilder.append(":");
        urlBuilder.append(port);
        urlBuilder.append("/");
        urlBuilder.append(DEFAULT_HKP_PATH);
        return (urlBuilder.toString());
    }

    /**
     * Queries a HKP server for the given email
     *
     * @param url the full URL to the HKP lookup path (i.e hkp://example.org:11371/custom/path/to/pks/lookup?). The lookup path can be omitted (i.e. hkp://example.org:11371/) , in this case the default HKP PATH is added)
     * @param email the email to get the public keys for
     * @return A list of public key rings found for the given email
     * @throws Exception
     */
    public Collection<PGPPublicKeyRing> findKeys(String url, String email, int timeout) throws OXException {

        ArrayList<PGPPublicKeyRing> ret = new ArrayList<PGPPublicKeyRing>();
        try {
            String response = index(url, email, timeout);
            if (response != null && !response.isEmpty()) {
                ArrayList<String> ids = parseIndexResponse(response, email);
                if (ids.size() > 0) {
                    for (int j = 0; j < ids.size(); j++) {
                        PGPPublicKeyRing ring = parseGetResponse(get(url, ids.get(j), timeout));
                        if (ring != null) {
                            ret.add(ring);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw HKPClientExceptionCodes.UNEXPECTED_HKP_ERROR.create(e, e.getMessage());
        }
        return ret;
    }

    /**
     * Queries a HKP server for the given email
     *
     * @param host the host of the HKP sever
     * @param port the port of the HKP server
     * @param email the email to get the public keys for
     * @return A list of public key rings found for the given email
     * @throws Exception
     */
    public Collection<PGPPublicKeyRing> findKeys(String host, int port, String email, int timeout) throws OXException {

        try {
            return findKeys(getURL(host, port), email, timeout);
        } catch (Exception e) {
            throw HKPClientExceptionCodes.UNEXPECTED_HKP_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Queries an HKP server for given ID
     *
     * @param host the host of the HKP sever
     * @param port the port of the HKP server
     * @param id ID of the key
     * @return the PublicKeyRing
     * @throws OXException
     */
    public PGPPublicKeyRing getKeyById(String host, int port, String id, int timeout) throws OXException {
        try {
            return parseGetResponse(get(getURL(host, port), id, timeout));
        } catch (Exception e) {
            throw HKPClientExceptionCodes.UNEXPECTED_HKP_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Queries an HKP server for given ID
     *
     * @param host the host of the HKP sever
     * @param port the port of the HKP server
     * @param id ID of the key
     * @return the PublicKeyRing
     * @throws OXException
     */
    public PGPPublicKeyRing getKeyById(String url, String id, int timeout) throws OXException {
        try {
            return parseGetResponse(get(url, id, timeout));
        } catch (Exception e) {
            throw HKPClientExceptionCodes.UNEXPECTED_HKP_ERROR.create(e, e.getMessage());
        }
    }

}
