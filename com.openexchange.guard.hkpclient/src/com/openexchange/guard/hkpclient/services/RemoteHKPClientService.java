
package com.openexchange.guard.hkpclient.services;

import java.util.ArrayList;
import java.util.Collection;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.LongUtil;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.hkpclient.client.HKPClient;
import com.openexchange.guard.hkpclient.osgi.Services;
import com.openexchange.guard.hkpclient.util.TimeTracker;
import com.openexchange.server.ServiceLookup;

/**
 * {@link RemoteHKPClientService} - default implementation for querying public HKP servers
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class RemoteHKPClientService implements HKPClientService {

    private static final Logger logger = LoggerFactory.getLogger(RemoteHKPClientService.class);
    private final HKPClientService delegate;
    private final GuardConfigurationService guardConfigService;
    private final ServiceLookup services;

    public RemoteHKPClientService(ServiceLookup services) throws OXException {
        this(null, services);
    }

    public RemoteHKPClientService(HKPClientService delegate, ServiceLookup services) throws OXException {
        this.delegate = delegate;
        this.guardConfigService = Services.getService(GuardConfigurationService.class);
        this.services = services;
    }

    private Collection<String> getConfiguredRemoteServers(int userId, int cid) throws OXException {
        String urlStrings =
            guardConfigService.getProperty(GuardProperty.publicPGPDirectory, userId, cid) + "," +
                guardConfigService.getProperty(GuardProperty.trustedPGPDirectory, userId, cid);
        return getRemoteHKPServers(urlStrings);
    }

    private Collection<String> getuntrustedRemoteServers(int userId, int cid) throws OXException {
        return getRemoteHKPServers(guardConfigService.getProperty(GuardProperty.untrustedPGPDirectory, userId, cid));
    }

    /**
     * Internal method to read configured HKP servers from configuration
     *
     * @return A list of configured HKP servers
     * @throws OXException
     */
    private Collection<String> getRemoteHKPServers(String urlStrings) throws OXException {
        Collection<String> hkpServers = new ArrayList<String>();
        String[] urls = urlStrings.split(",");
        for (String url : urls) {
            url = url.trim();
            if (url.startsWith("htt") || url.startsWith("hkp")) {
                if (!url.endsWith("?")) {
                    if (!url.endsWith("/")) {
                        url += "/";
                    }
                }
                hkpServers.add(url);
            }
        }
        return hkpServers;
    }

    /**
     * Internal method to query a list HKP Servers
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param hkpServers a list of HKP servers to query
     * @return A list of found keys
     * @throws Exception
     */
    private ArrayList<PGPPublicKeyRing> findByEmail(String clientToken, Collection<String> hkpServers, String email, int timeout) throws Exception {
        ArrayList<PGPPublicKeyRing> ret = new ArrayList<PGPPublicKeyRing>();
        TimeTracker timer = new TimeTracker(timeout);
        for (String hkpServer : hkpServers) {
            Collection<PGPPublicKeyRing> result = new HKPClient(clientToken, services).findKeys(hkpServer, email, timer.getRemaining());
            if (!result.isEmpty()) {
                ret.addAll(ret.size(), result);
            }
        }
        return ret;
    }

    private PGPPublicKeyRing findById (String clientToken, Collection<String> hkpServers, String id, int timeout) throws Exception {
        TimeTracker timer = new TimeTracker(timeout);
        for (String hkpServer : hkpServers) {
            PGPPublicKeyRing result = new HKPClient(clientToken, services).getKeyById(hkpServer, id, timer.getRemaining());
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    private RemoteKeyResult findInternal(String clientToken, Long id, String email, int userId, int cid, int timeout) throws Exception {
        String search = LongUtil.longToHexString(id);
        TimeTracker timer = new TimeTracker(timeout);
        PGPPublicKeyRing ring = findById(clientToken, getConfiguredRemoteServers(userId, cid), search, timeout);
        if (ring != null) {
            logger.debug("Remote key found in trusted HKP server");
            return new RemoteKeyResult (ring, HKPKeySources.TRUSTED_HKP_REMOTE_SERVER);
        }
        else {
            ring = findById(clientToken, getuntrustedRemoteServers(userId, cid), search, timer.getRemaining());
            if (ring != null) {
                logger.debug("Remote keys found in public HKP server");
                return new RemoteKeyResult (ring, HKPKeySources.PUBLIC_HKP_REMOTE_SERVER);
            }
        }
        RemoteKeyResult result = null;
        if (delegate != null) {
            result = delegate.find(clientToken, email, id, userId, cid, timer.getRemaining());
        }
        return result;
    }

    /**
     * Lookup public keys for email address. Checks SRV records first, then configured public servers
     *
     * @param clientToken an identification token put into the X-UI-INTERNAL-ACCOUNT-ID header, or null not not set the header
     * @param email
     * @return
     * @throws Exception
     */
    private Collection<RemoteKeyResult> findAllInternal(String clientToken, String email, int userId, int cid, int timeout) throws Exception {

        //Searching configured HKP servers
        try {
            TimeTracker timer = new TimeTracker(timeout);
            // First check the trusted configured HKP servers
            Collection<PGPPublicKeyRing> ret = findByEmail(clientToken, getConfiguredRemoteServers(userId, cid), email, timeout);
            if (ret != null && ret.size() > 0) {
                logger.debug("Remote keys found in trusted HKP server");
                return RemoteKeyResult.createCollectionFrom(ret, HKPKeySources.TRUSTED_HKP_REMOTE_SERVER);
            } else {
                // If not found, then check the untrusted servers.
                ret = findByEmail(clientToken, getuntrustedRemoteServers(userId, cid), email, timer.getRemaining());
                if (ret != null && ret.size() > 0) {
                    logger.debug("Remote keys found in public HKP server");
                    return RemoteKeyResult.createCollectionFrom(ret, HKPKeySources.PUBLIC_HKP_REMOTE_SERVER);
                }
            }
        }
        catch (Exception e) {
            logger.error("Error querying remote HKP server", e);
        }
        if (delegate != null) {
            return delegate.find(clientToken, email, userId, cid, timeout);
        }
        return null;
    }

    @Override
    public Collection<RemoteKeyResult> find(String clientToken, String email, int userId, int cid, int timeout) throws OXException {
        try {
            return findAllInternal(clientToken, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public RemoteKeyResult find(String clientToken, String email, Long id, int userId, int cid, int timeout) throws OXException {
        try {
            return findInternal(clientToken, id, email, userId, cid, timeout);
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }

}
