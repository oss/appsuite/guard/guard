/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.ShareTargetPath;
import com.openexchange.share.core.groupware.AbstractTargetProxy;
import com.openexchange.share.groupware.TargetPermission;
import com.openexchange.share.groupware.TargetProxyType;

/**
 * {@link GuardMailTargetProxy}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuardMailTargetProxy extends AbstractTargetProxy {

    private final ShareTargetPath targetPath;
    private final ShareTarget target;

    public GuardMailTargetProxy() {
        this(new ShareTargetPath(FolderObject.MAIL, null, null), new ShareTarget(FolderObject.MAIL, null, null));
    }

    public GuardMailTargetProxy (ShareTargetPath targetPath) {
        this(targetPath, new ShareTarget(FolderObject.MAIL, targetPath.getFolder(), targetPath.getItem()));
    }

    public GuardMailTargetProxy (ShareTarget shareTarget) {
        this(new ShareTargetPath(FolderObject.MAIL, shareTarget.getFolder(), shareTarget.getItem()), shareTarget);
    }

    public GuardMailTargetProxy(ShareTargetPath targetPath, ShareTarget shareTarget) {
        this.targetPath = targetPath;
        this.target = shareTarget;
    }

    @Override
    public String getID() {
        return target.getItem();
    }

    @Override
    public String getFolderID() {
        return target.getFolder();
    }

    @Override
    public ShareTarget getTarget() {
        return target;
    }

    @Override
    public ShareTargetPath getTargetPath() {
        return targetPath;
    }


    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#getTitle()
     */
    @Override
    public String getTitle() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#getPermissions()
     */
    @Override
    public List<TargetPermission> getPermissions() {
        // TODO Auto-generated method stub
        return Collections.emptyList();
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#applyPermissions(java.util.List)
     */
    @Override
    public void applyPermissions(List<TargetPermission> permissions) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#removePermissions(java.util.List)
     */
    @Override
    public void removePermissions(List<TargetPermission> permissions) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#getProxyType()
     */
    @Override
    public TargetProxyType getProxyType() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#mayAdjust()
     */
    @Override
    public boolean mayAdjust() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.openexchange.share.groupware.TargetProxy#getTimestamp()
     */
    @Override
    public Date getTimestamp() {
        // TODO Auto-generated method stub
        return null;
    }

}
