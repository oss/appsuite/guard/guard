/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.impl;

import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import com.openexchange.config.ConfigurationService;
import com.openexchange.context.ContextService;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.container.FolderObject;
import com.openexchange.groupware.ldap.LdapExceptionCode;
import com.openexchange.groupware.notify.hostname.HostData;
import com.openexchange.groupware.notify.hostname.HostnameService;
import com.openexchange.groupware.userconfiguration.UserPermissionBits;
import com.openexchange.guard.guest.MailGuestService;
import com.openexchange.guard.guest.OxMailAccountCreatorService;
import com.openexchange.server.ServiceLookup;
import com.openexchange.serverconfig.ServerConfigService;
import com.openexchange.session.Session;
import com.openexchange.share.CreatedShares;
import com.openexchange.share.ShareInfo;
import com.openexchange.share.ShareService;
import com.openexchange.share.ShareTarget;
import com.openexchange.share.recipient.GuestRecipient;
import com.openexchange.share.recipient.InternalRecipient;
import com.openexchange.share.recipient.ShareRecipient;
import com.openexchange.user.User;
import com.openexchange.user.UserService;
import com.openexchange.userconf.UserPermissionService;


/**
 * {@link MailGuestServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class MailGuestServiceImpl implements MailGuestService {

    private final ServiceLookup services;
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(MailGuestServiceImpl.class);

    public MailGuestServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Private function to check if language exists in backend translations
     * @param language
     * @return
     */
    private boolean languageExists(String language, Session session) {
        if (language == null) {
            return false;
        }
        try {
            com.openexchange.serverconfig.ServerConfig config = services.getServiceSafe(ServerConfigService.class).getServerConfig((String) session.getParameter(Session.PARAM_HOST_NAME), session);
            List<SimpleEntry<String, String>> avail = config.getLanguages();
            for (SimpleEntry<String, String> lang : avail) {
                if (lang.getKey().equals(language)) {
                    return true;
                }
            }
        } catch (OXException e) {
            logger.error("Error parsing available languages for Guest email ", e);
        }
        return false;
    }

    public User searchUser(Session session, String email) throws OXException {
        UserService userService = services.getServiceSafe(UserService.class);
        ContextService contextService = services.getServiceSafe(ContextService.class);
        try {
            return userService.searchUser(email, contextService.getContext(session.getContextId()), true, true, false);
        }
        catch(OXException e) {
           //javadoc of searchUser claims to return null if no user was found but doesn't:
           if(e.similarTo(LdapExceptionCode.NO_USER_BY_MAIL)) {
              return null;
           }
           else {
               throw e;
           }
        }
    }

    /**
     * Check for webmail permission.  If missing, add for user
     * @param guestUser
     * @param contextId
     * @throws OXException
     */
    private void checkPermissions(User guestUser, int contextId) throws OXException {
        UserPermissionService userPermissionService = services.getServiceSafe(UserPermissionService.class);
        UserPermissionBits perm = userPermissionService.getUserPermissionBits(guestUser.getId(), contextId);
        if (!perm.hasWebMail()) {
            perm.setWebMail(true);
            userPermissionService.saveUserPermissionBits(perm);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.guest.MailGuestService#createGuestLink(java.lang.String, java.lang.String)
     */
    @Override
    public String createGuestLink(Session session, String email, String mailId, String language) throws OXException {
        List<ShareRecipient> recipients;
        User existingUser = searchUser(session, email);
        if (existingUser != null) {
            InternalRecipient internalRecip = new InternalRecipient();
            internalRecip.setEntity(existingUser.getId());
            recipients = Arrays.asList(internalRecip);
            checkPermissions(existingUser, session.getContextId());
        } else {
            GuestRecipient guestRecipient = new GuestRecipient();
            guestRecipient.setEmailAddress(email);
            if (!languageExists(language, session)) {
                language = null;  // Null language defaults to senders language
            }
            guestRecipient.setPreferredLanguage(language);
            recipients = Arrays.asList(guestRecipient);
        }
        ShareTarget shareTarget = new ShareTarget(FolderObject.MAIL, "default0/INBOX", mailId);
        CreatedShares shares = services.getServiceSafe(ShareService.class).addTarget(session, shareTarget, recipients);
        ShareInfo share = shares.getShare(recipients.get(0));
        services.getServiceSafe(OxMailAccountCreatorService.class).createAccount(share.getGuest(), session);  // Check mail config all set up
        HostData hostData = (HostData) session.getParameter(HostnameService.PARAM_HOST_DATA);
        if (hostData == null) {
            hostData = new GuestHostDataImpl(services.getServiceSafe(ConfigurationService.class).getProperty("com.openexchange.dispatcher.prefix", "/ajax/"));  // Basically empty Host Data, will be filled with com.openexchange.share.guestHostname
        }
        String url = share.getShareURL(hostData);
        return url;
    }


}
