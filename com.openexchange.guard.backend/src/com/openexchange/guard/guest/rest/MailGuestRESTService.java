/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.guest.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.rest.services.annotation.Role;
import com.openexchange.rest.services.annotation.RoleAllowed;

/**
 * {@link MailGuestRESTService} provides a REST interface for creating share links for OX Guard guests
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
@Path("/preliminary/guard/guest/v1")
@RoleAllowed(Role.BASIC_AUTHENTICATED)
public interface MailGuestRESTService {

    /**
     * Creates a new share link for an OX Guard guest email item
     *
     * @param body The JSON body
     * @return JSON The JSON response
     * @throws OXException
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/shareLink")
    public JSONObject create(JSONObject body) throws OXException;

    /**
     * Gets the email addresses associated with a user
     *
     * @param body The JSON body
     * @return JSON The JSON response
     * @throws OXException
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getEmails/{context}/{userid}")
    public JSONObject getEmails (@PathParam("userid") int userId, @PathParam("context") int context) throws OXException;

    /**
     * Gets the OX user id within the context that contains the email address
     * getId
     *
     * @param email
     * @param guestContext
     * @return
     * @throws OXException
     */
    @GET
    @Path("/getoxid/{context}/{email}")
    public JSONObject getId(@PathParam("email") String email, @PathParam("context") int guestContext) throws OXException;

}
