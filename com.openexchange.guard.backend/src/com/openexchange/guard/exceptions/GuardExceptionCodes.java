/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * {@link GuardExceptionCodes}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public enum GuardExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * Required Guard Authentication is missing
     */
    MISSING_AUTH("Missing authentication", GuardExceptionMessages.MISSING_AUTH_MSG, CATEGORY_PERMISSION_DENIED, 1),
    MISSING_ENDPOINT("Guard endpoint '%1$s' not configured", GuardExceptionMessages.MISSING_ENDPOINT_MSG, CATEGORY_CONFIGURATION, 2),
    BAD_AUTH("Bad authentication",GuardExceptionMessages.BAD_AUTH_MSG,  CATEGORY_PERMISSION_DENIED, 3),
    UNKNOWN_ERROR("Error: \"%1$s\"",GuardExceptionMessages.UNKNOWN_ERROR_MSG, CATEGORY_ERROR,4),
    LOCKOUT("Lockout", GuardExceptionMessages.LOCKOUT_MSG, CATEGORY_ERROR, 5),
    URI_PARSE_FAILED("Unable to parse IMAP server URI \"%1$s\".",GuardExceptionMessages.URI_PARSE_FAILED_MSG, CATEGORY_ERROR,6),
    ACTION_NOT_ALLOWED("Action not allowed. '%1$s'", GuardExceptionMessages.ACTION_NOT_ALLOWED_MSG, CATEGORY_ERROR, 7),
    ACTION_NOT_SUPPORTED("Action not allowed. '%1$s'", GuardExceptionMessages.ACTION_NOT_SUPPORTED_MSG, CATEGORY_ERROR, 8),
    BLOCKED_BY_PIN("Unable to perform action until pin is removed", GuardExceptionMessages.BLOCKED_BY_PIN, CATEGORY_ERROR, 9),
    MISSING_KEYS("No keys found", GuardExceptionMessages.MISSING_KEYS, CATEGORY_ERROR, 10),
    DRAFT_ENCRYPTION_ERROR("Error encrypting draft. Check your keys to make sure they are valid.", GuardExceptionMessages.DRAFT_ENCRYPTION_ERROR, CATEGORY_ERROR, 11),
    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    private GuardExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private GuardExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-MW";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }

}
