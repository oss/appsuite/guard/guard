/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.Collections;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFile;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.composition.AbstractDelegatingIDBasedFileAccess;
import com.openexchange.file.storage.composition.IDBasedFileAccess;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.session.Session;

/**
 * {@link AbstractGuardIDBasedFileAccess}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v3.0.0
 */
public class AbstractGuardIDBasedFileAccess extends AbstractDelegatingIDBasedFileAccess {

    protected final GuardApi guardApi;
    protected final Session session;
    protected final EncryptedFileRecognizer encryptedFileRecognizer;

    /**
     * Initializes a new {@link AbstractGuardIDBasedFileAccess}.
     *
     * @param fileAccess
     */
    protected AbstractGuardIDBasedFileAccess(IDBasedFileAccess fileAccess, GuardApi guardApi, Session session, EncryptedFileRecognizer encryptedFileRecognizer) {
        super(fileAccess);
        this.guardApi = guardApi;
        this.session = session;
        this.encryptedFileRecognizer = encryptedFileRecognizer;
    }

    /**
     * Checks if the file is an encrypted document
     *
     * @param file The file to check
     * @return True, if the file is encrypted, false otherwise
     */
    protected boolean isEncryptedGuardFile(File file) {
        return encryptedFileRecognizer.isEncrypted(file);
    }

    protected void makeCurrentFile(String file, String version) throws OXException {
        File versionedFile = new DefaultFile(fileAccess.getFileMetadata(file, null));
        versionedFile.setVersion(version);
        List<Field> versionField = Collections.singletonList(Field.VERSION);
        fileAccess.saveFileMetadata(versionedFile, FileStorageFileAccess.DISTANT_FUTURE, versionField);
    }

}
