/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.guard.api.GuardApi;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.sharing.SharingUtil;
import com.openexchange.session.Session;

/**
 * {@link GuardEncryptingInputStream} encrypts data using OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GuardEncryptingInputStream extends AbstractGuardInputStream {

    private final GuardApi guardApi;
    private final InputStream plainTextData;
    private final boolean armored;
    private final Collection<String> recipients;
    private final GuardAuthenticationToken signingAuthentication;
    private final Session session;
    private final File file;

    /**
     * Initializes a new {@link GuardEncryptingInputStream}.
     *
     * @param guardApi The Guard endpoint to use
     * @param plainTextData The data to encrypt
     * @param recipients A collection of recipients to encrypt the data for
     * @param armored True, to encrypt the data as ASCII-Armored, false otherwise
     * @param signingAuthentication The authentication token for signing the encrypted data, or null to not sign the encrypted data
     */
    public GuardEncryptingInputStream(File file, GuardApi guardApi, InputStream plainTextData, Collection<String> recipients, boolean armored, GuardAuthenticationToken signingAuthentication, Session session) {
        this.guardApi = Objects.requireNonNull(guardApi, "guardApi must not be null");
        this.plainTextData = plainTextData;
        this.recipients = Objects.requireNonNull(recipients, "recipients must not be null");
        this.armored = armored;
        this.signingAuthentication = signingAuthentication;
        this.session = session;
        this.file = file;
    }

    /**
     * Initializes a new {@link GuardEncryptingInputStream} for encrypting data without signing it.
     *
     * @param guardApi The Guard endpoint to use
     * @param plainTextData The data to encrypt
     * @param recipients A collection of recipients to encrypt the data for
     * @param armored True, to encrypt the data as ASCII-Armored, false otherwise
     */
    public GuardEncryptingInputStream(File file, GuardApi guardApi, InputStream plainTextData, Collection<String> recipients, boolean armored, Session session) {
        this(file, guardApi, plainTextData, recipients, armored, null, session);
    }

    /**
     * Initializes a new {@link GuardEncryptingInputStream} for encrypting data without signing it.
     *
     * The output will be binary (NOT ASCII-Armored).
     *
     * @param guardApi
     * @param plainTextData
     * @param recipients
     */
    public GuardEncryptingInputStream(File file, GuardApi guardApi, InputStream plainTextData, Collection<String> recipients, Session session) {
        this(file, guardApi, plainTextData, recipients, false, null, session);
    }

    /**
     * Internal helper method to put a collection to a JSON structure.
     *
     * @param json The json
     * @param key The key
     * @param collection The collection to add
     * @throws IllegalArgumentException due an internal JSON Exception
     */
    private void putCollectionToJsonSave(JSONObject json, String key, Collection<? extends Object> collection) {
        try {
            json.put(key, collection);
        } catch (JSONException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private boolean isGuest() {
        return session.containsParameter(Session.PARAM_GUEST) && Boolean.TRUE.equals(session.getParameter(Session.PARAM_GUEST));
    }
    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.AbstractGuardInputStream#getGuardStream()
     */
    @Override
    protected InputStream getGuardStream() throws OXException {

        JSONObject json = new JSONObject();
        json.putSafe("armored", armored);
        if (!isGuest()) {  // Guests aren't the primary for encrypting.  No keys associated with userid/cid for guests
            putCollectionToJsonSave(json, "recipients", recipients);
        }
        if (signingAuthentication != null) {
            JSONObject user = new JSONObject();
            user.putSafe("session", signingAuthentication.getGuardSessionId());
            user.putSafe("auth", signingAuthentication.getValue());
            user.putSafe("id", session.getUserId());
            user.putSafe("cid", session.getContextId());
            json.putSafe("user", user);
        }
        if (file != null && file.getObjectPermissions() != null) {
            try {
                json.putSafe("sharing", SharingUtil.getEmails(session, file.getObjectPermissions(), file.getCreatedBy()));
            } catch (JSONException e) {
                throw OXException.general("Error creating sharing JSON for Guard", e);
            }
        }
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));
        LinkedHashMap<String, InputStream> multipartParameters = new LinkedHashMap<>();
        multipartParameters.put("json", jsonStream);
        multipartParameters.put("data", plainTextData);

        return guardApi.processResources(GuardApis.mapFor("action", "encrypt", "respondWithJSON", "true" /* forcing guard to render errors in JSON */), multipartParameters, null, session);
    }
}
