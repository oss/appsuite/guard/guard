/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.io.IOException;
import java.io.InputStream;
import com.openexchange.exception.OXException;

/**
 * {@link AbstractGuardInputStream} represents an InputStream to an OX Guard API response.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public abstract class AbstractGuardInputStream extends InputStream {

    private InputStream guardStream = null;
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AbstractGuardInputStream.class);

    /**
     * Gets the {@link InputStream} to an OX Guard response.
     *
     * @return The{@link InputStream} to an OX Guard response.
     * @throws OXException
     */
    protected abstract InputStream getGuardStream() throws OXException;

    /**
     * Internal method to get the concrete stream to use.
     *
     * @return The InputStream to use
     * @throws OXException
     */
    private synchronized InputStream guardStreamAccess() throws OXException {
        if (guardStream == null) {
            guardStream = getGuardStream();
        }
        return guardStream;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#available()
     */
    @Override
    public int available() throws IOException {
        try {
            return guardStreamAccess().available();
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#close()
     */
    @Override
    public void close() throws IOException {
        try {
            guardStreamAccess().close();
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#markSupported()
     */
    @Override
    public boolean markSupported() {
        try {
            return guardStreamAccess().markSupported();
        } catch (OXException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#mark(int)
     */
    @Override
    public synchronized void mark(int readlimit) {
        try {
            guardStreamAccess().mark(readlimit);
        } catch (OXException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#reset()
     */
    @Override
    public synchronized void reset() throws IOException {
        try {
            guardStreamAccess().reset();
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#skip(long)
     */
    @Override
    public long skip(long n) throws IOException {
        try {
            return guardStreamAccess().skip(n);
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#read()
     */
    @Override
    public int read() throws IOException {
        try {
            return guardStreamAccess().read();
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#read(byte[])
     */
    @Override
    public int read(byte[] b) throws IOException {
        try {
            return guardStreamAccess().read(b);
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.InputStream#read(byte[], int, int)
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        try {
            return guardStreamAccess().read(b, off, len);
        } catch (OXException e) {
            throw new IOException(e.getMessage(), e);
        }
    }
}
