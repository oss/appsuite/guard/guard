/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.file.storage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;

/**
 * {@link MetadataFileRecognizer} recognizes a file as encrypted based on it's meta data.
 * <p>
 * A file is considered as encrypted, if it contains a meta data object {@link MetadataFileRecognizer#ENCRYPTED_FLAG}
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class MetadataFileRecognizer implements EncryptedFileRecognizer {

    private static final String ENCRYPTED_FLAG = "Encrypted";

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#isEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public boolean isEncrypted(File file) {
        if (file != null) {
            return file.getMeta().containsKey(ENCRYPTED_FLAG);
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markEncrypted(File file) {
        ArrayList<Field> ret = new ArrayList<Field>();
        if (file != null && !isEncrypted(file)) {
            file.getMeta().put(ENCRYPTED_FLAG, true);
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markEncrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markEncrypted(Document document) {
        return markEncrypted(document.getFile());
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.File)
     */
    @Override
    public Collection<Field> markDecrypted(File file) {
        if (file.getMeta().containsKey(ENCRYPTED_FLAG)) {
            file.getMeta().remove(ENCRYPTED_FLAG);
            return Arrays.asList(new Field[] { Field.META });
        }
        return new ArrayList<Field>();
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.file.storage.EncryptedFileRecognizer#markDecrypted(com.openexchange.file.storage.Document)
     */
    @Override
    public Collection<Field> markDecrypted(Document document) {
        return markDecrypted(document.getFile());
    }
}
