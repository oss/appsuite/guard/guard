/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.authentication;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.sessiond.SessiondService;
import com.openexchange.tools.servlet.AjaxExceptionCodes;

/**
 * {@link GuardAuthenticationFactory} creates OX Guard specific authentication tokens from given HTTP requests.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class GuardAuthenticationFactory implements CryptographicServiceAuthenticationFactory {

    public static final String VALUE_DELIMITER = ";";
    private static final String AUTH_TOKEN_PARAMETER_NAME = "cryptoAuth";
    private static final String GUARD_AUTH_COOKIE_NAME = "cryptoAuth";
    private static final String GUARD_SESSION_COOKIE_NAME = "open-xchange-secret";
    private static final String JSON_PARAMETER = "json_0";
    private static final String SECURITY_JSON = "security";
    private static final String AUTHENTICATION_JSON = "authentication";

    private final ServiceLookup services;

    /**
     * Initializes a new {@link GuardAuthenticationFactory}.
     *
     * @param sessiondService The {@link SessionService} to use
     */
    public GuardAuthenticationFactory(ServiceLookup services) {
        this.services = services;
    }

    /**
     * internal method to create a hash of the given value
     *
     * @param value The value to create a hash from
     * @return The hashed value as HEX formatted string
     * @throws OXException
     */
    private String hash(String value) throws OXException {
        if (value == null) {
            return null;
        }
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (final Exception e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
        final byte[] bytes = md.digest(value.getBytes(StandardCharsets.UTF_8));
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        String sessionId = sb.toString();
        sessionId = sessionId.substring(0, 40);
        return sessionId.toString();
    }

    /**
     * Internal method to obtain a certain cookie value from a list of cookies.
     *
     * @param cookies The list of cookies.
     * @param cookieName The name of the cookie to obtain the value from.
     * @return The value of the cookie with the given name, or null if no such cookie is present.
     * @throws OXException
     */
    private String getValueFromCookie(Cookie[] cookies, String cookieName) throws OXException {
        String result = null;
        if (cookies != null) {
            for (final Cookie cookie : cookies) {
                if (cookie.getName().startsWith(cookieName)) {
                    result = cookie.getValue();
                }
            }
        }
        return result;
    }

    /**
     * Pull session from request using URL parameter, then get the has of the session secret
     *
     * @param request
     * @return The GuardSession String
     * @throws OXException
     */
    private String getIdFromRequest(HttpServletRequest request) throws OXException {
        if (request == null) {
            return null;
        }
        final String sessionId = request.getParameter("session");
        if (sessionId == null) {
            throw AjaxExceptionCodes.MISSING_PARAMETER.create(request.getRequestURL() + " missing parameter session");
        }
        SessiondService sessiondService = services.getServiceSafe(SessiondService.class);
        final Session session = sessiondService.getSession(sessionId);
        if (session == null) {
            return null;
        }
        return hash(session.getSecret());
    }

    /**
     * Internal method to construct the authentication string used for accessing OX Guard.
     *
     * @param sessionValue The OX Guard Session value.
     * @param authenticationToken The OX Guard authentication token
     * @return The authentication string which can be used for accessing OX Guard.
     */
    private static String buildAuthentication(String sessionValue, String authenticationToken) {
        if (sessionValue != null && authenticationToken != null && !authenticationToken.equals("null")) {
            return sessionValue + VALUE_DELIMITER + authenticationToken;
        }
        return null;
    }

    @Override
    public String getTokenValueFromString(String string) {
        final GuardAuthenticationToken token = GuardAuthenticationToken.fromString(string);
        if (token == null) {
            return null;
        }
        try {
            return URLEncoder.encode(token.getValue(), "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            return null;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.file.storage.json.crypto.EncryptionServiceAuthenticationFactory#createAuthenticationFrom(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public String createAuthenticationFrom(HttpServletRequest request) throws OXException {

        if (request == null) {
            return null;
        }

        final String sessionValue = getIdFromRequest(request);
        String authToken = getValueFromCookie(request.getCookies(), GUARD_AUTH_COOKIE_NAME);
        if (authToken == null) {
            authToken = request.getParameter(AUTH_TOKEN_PARAMETER_NAME);
        }

        return buildAuthentication(sessionValue, authToken);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory#createAuthenticationFrom(javax.servlet.http.HttpServletRequest, com.openexchange.ajax.requesthandler.AJAXRequestData)
     */
    @Override
    public String createAuthenticationFrom(AJAXRequestData requestData) throws OXException {

        if (requestData == null) {
            return null;
        }

        //Fetching authentication from the request object if possible
        final HttpServletRequest request = requestData.optHttpServletRequest();
        if (request != null) {
            final String authFromRequest = createAuthenticationFrom(request);
            if (authFromRequest != null) {
                return authFromRequest;
            }
        }

        //Fetching authentication from JSON data if available
        try {
            if (requestData.getUploadEvent() != null) {
                final String formField = requestData.getUploadEvent().getFormField(JSON_PARAMETER);
                if (formField != null) {
                    final JSONObject jsonObject = new JSONObject(formField);
                    if (jsonObject.has(SECURITY_JSON)) {
                        return buildFromJSONObject(jsonObject, getIdFromRequest(request));

                    }
                    return null;
                }
            } else if (request != null && requestData.getData() instanceof JSONObject) {
                return buildFromJSONObject((JSONObject) requestData.getData(), getIdFromRequest(request));
            }
        } catch (final JSONException e) {
            throw AjaxExceptionCodes.JSON_ERROR.create(e, e.getMessage());
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.ajax.requesthandler.crypto.CryptographicServiceAuthenticationFactory#createAuthenticationFrom(com.openexchange.session.Session, java.lang.String)
     */
    @Override
    public String createAuthenticationFrom(Session session, String data, CryptoType.PROTOCOL type) throws OXException {
        if (session != null) {
            if (data != null) {
                final String secret = session.getSecret();
                if (secret == null) {
                    //If the passed in session does not contain a secret, we going to try to lookup the session by ID
                    final String sessionId = session.getSessionID();
                    if (sessionId != null) {
                        SessiondService sessiondService = services.getServiceSafe(SessiondService.class);
                        return createAuthenticationFrom(sessiondService.getSession(sessionId), data, type);
                    }
                } else {
                    return buildAuthentication(hash(secret), data);
                }
            } else {
                if (type == null) {
                    type = CryptoType.PROTOCOL.PGP;
                }
                //If no data given, we try to fetch the auth-token from the given session
                final String authToken = getAuthTokenFromSession(session, type);
                if (authToken != null) {
                    return authToken;
                }
            }
        }
        return null;
    }

    @Override
    public String getSessionValueFrom(Session session) throws OXException {
        if (session != null && session.getSecret() != null) {
            //The hashed session-secret represents the guard-session-id
            return hash(session.getSecret());
        }
        return null;
    }

    private String buildFromJSONObject(JSONObject jsonObject, String sessionValue) throws JSONException {
        if (jsonObject.has(SECURITY_JSON)) {
            String authToken = null;
            final JSONObject jsonSecurity = jsonObject.getJSONObject(SECURITY_JSON);
            if (jsonSecurity.has(AUTHENTICATION_JSON)) {
                authToken = jsonSecurity.getString(AUTHENTICATION_JSON);
            }

            return buildAuthentication(sessionValue, authToken);
        }
        return null;
    }

    @Override
    public String getAuthTokenFromSession(Session session, CryptoType.PROTOCOL type) throws OXException {
        final GuardAuthenticationToken token = new AuthenticationTokenHandler().getForSession(session, false, type.getValue());
        if (token == null) {
            return null;
        }
        return token.getValue();
    }

    @Override
    public String getAuthTokensFromSession(Session session) throws OXException {
        final List<GuardAuthenticationToken> tokens = new AuthenticationTokenHandler().getAllForSession(session);
        if (tokens == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        tokens.forEach((token) -> sb.append(token.getValue()).append(","));
        return sb.substring(0, sb.length() - 1); // Remove last ","
    }

    @Override
    public String createAuthenticationFrom(Session session, String data) throws OXException {
        // Default's to pgp
        return createAuthenticationFrom(session, data, null);
    }
}
