/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link SessionKeysAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.0
 */
public class SessionKeysAction extends AbstractGuardAction {

    private static final String JSON_FIELD_FOLDER = "folder";
    private static final String JSON_FIELD_ID = "id";
    private static final String JSON_MODULE = "module";
    private final SessionKeyExtractionHandler[] extractionHandlers;

    /**
     * Initializes a new {@link SessionKeysAction}.
     *
     * @param services
     */
    public SessionKeysAction(ServiceLookup services, SessionKeyExtractionHandler...extractionHandlers) {
        super(services);
        this.extractionHandlers = extractionHandlers;
    }

    private JSONObject asJsonResult(String[] sessions) throws JSONException {
        JSONObject result = new JSONObject();
        if (sessions != null) {
            result.put("encryptedSessionKeyPackets", new JSONArray(Arrays.asList(sessions)));
        }
        return result;
    }

    private SessionKeyExtractionHandler getExtractionHandlerFor(JSONObject json) throws JSONException {
        for (SessionKeyExtractionHandler sessionKeyExtractionHandler : extractionHandlers) {
            if(sessionKeyExtractionHandler.canHandle(json)) {
               return sessionKeyExtractionHandler;
            }
        }
        return null;
    }

    private AJAXRequestResult doPerformPost(AJAXRequestData requestData, Session session) throws OXException, JSONException {
        final JSONObject json = parseStreamToJson(requestData);
        final SessionKeyExtractionHandler extractionHandler = getExtractionHandlerFor(json);
        if(extractionHandler != null) {
            String[] sessions = extractionHandler.handle(json, session);
            requestData.setResponseHeader("content-type", "application/json; charset=UTF-8");
            return new AJAXRequestResult(asJsonResult(sessions), "json");
        }
        else {
            throw AjaxExceptionCodes.JSON_ERROR.create("No suitable extraction handler found.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.json.actions.AbstractGuardAction#doPerform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    protected AJAXRequestResult doPerform(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException {
        if (requestData.optHttpServletRequest().getMethod().equals("POST")) {
            return doPerformPost(requestData, session);
        } else {
            return new AJAXRequestResult(null, "json");
        }
    }
}
