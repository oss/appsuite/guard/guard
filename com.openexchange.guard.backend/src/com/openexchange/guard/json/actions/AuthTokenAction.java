/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.internal.authentication.GuardLoginHandler;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link AuthTokenAction} creates, attaches or detaches an {@link GuardAuthenticationToken} to or from a groupware session
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.2
 */
public class AuthTokenAction extends AbstractGuardAction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenAction.class);


    private static final String FIELD_MINUTES_VALID = "minutesValid";
    private static final String FIELD_GUARD_SESSION = "guardSession";
    private static final String FIELD_PASSWORD = "password";
    private static final String ENCRYPTED_PASSWORD = "e_password";
    private static final String PGP_SESSION_KEY = "pgp_session_key";
    private static final String FIELD_AUTH = "auth";
    private static final String TYPE = "type";

    private static final String[] KNOWN_TYPES = { "PGP", "SMIME" };  // Known and possibly supported Guard TYPES

    /**
     * Initializes a new {@link AuthTokenAction}.
     *
     * @param services
     */
    public AuthTokenAction(ServiceLookup services) {
        super(services);
    }

    private static String hash(String value) throws OXException {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (Exception e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
        byte[] bytes = md.digest(value.getBytes(StandardCharsets.UTF_8));
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++){
           sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        String sessionId = sb.toString();
        sessionId = sessionId.substring(0, 40);
        return sessionId.toString();
    }


    /**
     * Internal method to extract the guard session ID from the given {@link AJAXRequestData}.
     * If the client did not specify a session with the guardSession parameter the hash session secret is used as fallback.
     *
     * @param requestData Client request data
     * @return The session ID specified by the client, or the hash secret as fallback.
     * @throws OXException
     * @throws JSONException
     */
    private static String extractGuardSessionId(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException {
        JSONObject json = (JSONObject) requestData.getData();
        if (json != null && json.has(FIELD_GUARD_SESSION)) {
            return json.getString(FIELD_GUARD_SESSION);
        }
        return (hash(session.getSecret()));
    }

    private static String getType(AJAXRequestData request) throws OXException {
        if (null != request && request.containsParameter(TYPE)) {
            final String type = request.getParameter(TYPE);
            if (type.isEmpty()) {
                return "PGP";
            }
            for (String known : KNOWN_TYPES) {
                if (known.equals(type.toUpperCase())) {
                    return type;
                }
            }
            throw GuardExceptionCodes.ACTION_NOT_SUPPORTED.create(request.getParameter(TYPE));
        }
        return "PGP";  // default
    }

    /**
     * Internal method to extract the minutesValid value from the given request data
     *
     * @param requestData The request data
     * @return The parsed minutes, or <code>null</code> if the given requests data does not contain a minutesValid value.
     */
    private static Integer extractMinutesValid(AJAXRequestData requestData) {
        String minutesValid = requestData.getParameter(FIELD_MINUTES_VALID);
        if (minutesValid == null) {
            JSONObject json = (JSONObject) requestData.getData();
            if (json != null && json.has("minutesValid")) {
                try {
                    minutesValid = json.getString("minutesValid");
                } catch (JSONException e) {
                    LOGGER.debug("Unable to get minutes from JSON", e);
                    return null;
                }
            }
        }
        if(minutesValid != null) {
            try {
                return new Integer(minutesValid);
            }
            catch(@SuppressWarnings("unused") NumberFormatException nfe) {
                return null;
            }
        }
        return null;
    }

    /**
     * Creates an {@link GuardAuthenticationToken} from the given request
     *
     * @param requestData The request
     * @return The authentication token parsed from the given JSON
     * @throws JSONException
     * @throws OXException
     */
    private static AJAXRequestResult toAuthenticationToken(AJAXRequestData requestData, ServerSession session, CryptoType.PROTOCOL type) throws JSONException, OXException {
        JSONObject json = (JSONObject) requestData.requireData();

        if (!json.has(FIELD_AUTH)) {
            throw AjaxExceptionCodes.MISSING_FIELD.create(FIELD_AUTH);
        }
        String authToken = json.getString(FIELD_AUTH);

        if (authToken.contains("!")) {
            String typeString = authToken.substring(0, authToken.indexOf("!"));
            if (!typeString.contains("#")) {
                type = CryptoType.getTypeFromString(typeString);
                authToken = authToken.substring(authToken.indexOf("!") + 1);
            }
        }

        String guardSessionId = extractGuardSessionId(requestData, session);
        if (guardSessionId == null) {
            throw AjaxExceptionCodes.MISSING_FIELD.create(FIELD_GUARD_SESSION);
        }

        int minutesValid = -1;
        if (json.has(FIELD_MINUTES_VALID)) {
            minutesValid = json.getInt(FIELD_MINUTES_VALID);
        }
        GuardAuthenticationToken authenticationToken = new GuardAuthenticationToken(guardSessionId, authToken, minutesValid);
        return doPerformSet(authenticationToken, session, type);
    }

    /**
     * Creates a JSON response for a given authentication token
     *
     * @param token The token
     * @param attachedToSession <code>true</code> if the given token was attached to the session, <code>false</code> otherwise
     * @return the token as JSONObject
     * @throws JSONException
     */
    private static JSONArray asJsonResult(List<GuardAuthenticationToken> tokens, boolean attachedToSession) {
        JSONArray results = new JSONArray();
        tokens.forEach((token) -> {
            JSONObject result = new JSONObject();
            try {
                result.put("auth", token.getValue());
                result.put("createdOn", token.getCreatedOn().getTime());
                result.put("minutesValid", attachedToSession ? I(token.getMinutesValid()) : JSONObject.NULL);
                result.put("type", token.getType());
                results.put(result);
            } catch (JSONException e) {
                LOGGER.debug("Unable to generate JSON", e);
            }
        });
        return results;
    }

    /**
     * Internal method to perform an authentication against the OX Guard server on behalf of the user
     *
     * @param requestData The request data
     * @param session The user's session
     * @return An {@link GuardAuthenticationToken} if the authentication was successful, null otherwise
     * @throws OXException
     * @throws JSONException
     */
    private GuardAuthenticationToken doAuthentication(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException {

        JSONObject json = parseStreamToJson (requestData);

        if (json == null ||
            !(json.has(FIELD_PASSWORD) || json.has(ENCRYPTED_PASSWORD) || json.has(PGP_SESSION_KEY))) {
            throw AjaxExceptionCodes.MISSING_FIELD.create(FIELD_PASSWORD);
        }
        String password = json.has(FIELD_PASSWORD) ? json.getString(FIELD_PASSWORD) : null;

        String ePassword = json.has(ENCRYPTED_PASSWORD) ? json.getString(ENCRYPTED_PASSWORD) : null;

        String pgpSessionKey = json.has(PGP_SESSION_KEY) ? json.getString(PGP_SESSION_KEY) : null;

        if (password == null && ePassword == null && pgpSessionKey == null) {
            throw AjaxExceptionCodes.MISSING_FIELD.create(FIELD_PASSWORD);
        }

        String guardSessionId = extractGuardSessionId(requestData, session);
        if (guardSessionId == null) {
            throw AjaxExceptionCodes.MISSING_FIELD.create(FIELD_GUARD_SESSION);
        }
        HttpServletRequest request = requestData.optHttpServletRequest();
        final String userAgent = null == request ? null : request.getHeader("User-Agent");
        if(pgpSessionKey == null) {
            return new GuardLoginHandler().login(
                session,
                guardSessionId,
                password,
                ePassword,
                GuardApis.extractCookiesFrom(request, session),
                userAgent,
                getType(requestData));
        }
        return new GuardLoginHandler().loginWithSymmetricKey(
            session,
            guardSessionId,
            pgpSessionKey,
            GuardApis.extractCookiesFrom(request, session),
            userAgent);
    }

    /**
     * Gets if a Guard authentication token has been attached to a session
     *
     * @param requestData The request data
     * @param session The session
     * @return The result describing whether an authentication token has been attached to a session or not
     */
    private static AJAXRequestResult doPerformGet(AJAXRequestData requestData, ServerSession session) {
        List<GuardAuthenticationToken> tokensForSession = new AuthenticationTokenHandler().getAllForSession(session);
        //  Check for same session Secret?
        requestData.setResponseHeader("content-type", "application/json; charset=UTF-8");
        return new AJAXRequestResult(asJsonResult(tokensForSession, true), "json");
    }

    /**
     * Attache/detaches a Guard authentication token to/from a given session
     *
     * @param authenticationToken The token to attach to the given session or null or an empty token in order to detach the token from the given session.
     * @param session The session The session
     * @return an empty result
     * @throws OXException
     * @throws JSONException
     */
    private static AJAXRequestResult doPerformSet(GuardAuthenticationToken authenticationToken, ServerSession session, CryptoType.PROTOCOL type) throws OXException {
        if (authenticationToken.getValue() == null || authenticationToken.getValue().isEmpty()) {
            //Detach existing token
            new AuthenticationTokenHandler().destroyForSession(session, false, type.getValue());
            return new AJAXRequestResult(null, "json");
        }
        //Attach a new token
        new AuthenticationTokenHandler().setForSession(session, authenticationToken, type.getValue());
        return new AJAXRequestResult(asJsonResult(Collections.singletonList(authenticationToken), true), "json");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.json.actions.AbstractGuardAction#doPerform(com.openexchange.ajax.requesthandler.AJAXRequestData, com.openexchange.tools.session.ServerSession)
     */
    @Override
    protected AJAXRequestResult doPerform(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException {
        CryptoType.PROTOCOL type = CryptoType.getTypeFromString(requestData.getParameter("type"));
        HttpServletRequest request = requestData.optHttpServletRequest();
        if (null != request && request.getMethod().equals("GET")) {
            //GET -> Query existing token
            return doPerformGet(requestData, session);
        }
        else if(null != request && request.getMethod().equals("POST")) {
            //POST -> Perform direct authentication against Guard and attach returned token if valid
            GuardAuthenticationToken token = doAuthentication(requestData, session);
            if(token != null) {
                //Authentication successful: Attach token to session as controlled by the client
                Integer minutesValid = extractMinutesValid(requestData);
                token.setMinutesValid(minutesValid != null ? i(minutesValid) : -1);
                if(minutesValid != null) {
                    return doPerformSet(token, session, type);
                }
                return new AJAXRequestResult(asJsonResult(Collections.singletonList(token), false), "json");
            }
            //Authentication failed
            throw GuardExceptionCodes.BAD_AUTH.create();
        }
        else {
            //PUT -> Attach/Detach existing authentication token sent by client
            return toAuthenticationToken(requestData, session, type);
        }
    }
}
