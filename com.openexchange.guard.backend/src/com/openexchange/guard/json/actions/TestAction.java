/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.json.actions;

import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.ajax.container.FileHolder;
import com.openexchange.ajax.requesthandler.AJAXRequestData;
import com.openexchange.ajax.requesthandler.AJAXRequestResult;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.Cookie;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.api.Header;
import com.openexchange.java.Strings;
import com.openexchange.server.ServiceLookup;
import com.openexchange.tools.servlet.AjaxExceptionCodes;
import com.openexchange.tools.session.ServerSession;

/**
 * {@link TestAction}
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.0
 */
public class TestAction extends AbstractGuardAction {

    /**
     * Initializes a new {@link TestAction}.
     *
     * @param services The OSGi service look-up
     */
    public TestAction(ServiceLookup services) {
        super(services);
    }

    @Override
    protected AJAXRequestResult doPerform(AJAXRequestData requestData, ServerSession session) throws OXException, JSONException {
        String method = requestData.requireParameter("method");

        Map<String, String> parameters = null;
        {
            String tmp = requestData.getParameter("parameters");
            if (null != tmp) {
                String[] sa = Strings.splitByComma(tmp);
                parameters = GuardApis.mapFor(sa);
            }
        }

        List<Cookie> cookies = null;
        {
            String tmp = requestData.getParameter("cookies");
            if (null != tmp) {
                String[] sa = Strings.splitByComma(tmp);
                Map<String, String> map = GuardApis.mapFor(sa);
                cookies = new LinkedList<Cookie>();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    cookies.add(new Cookie(entry.getKey(), entry.getValue()));
                }
            } else {
                cookies = GuardApis.extractCookiesFrom(requestData.optHttpServletRequest(), session);
            }
        }

        List<Header> headers = null;
        {
            String tmp = requestData.getParameter("headers");
            if (null != tmp) {
                String[] sa = Strings.splitByComma(tmp);
                Map<String, String> map = GuardApis.mapFor(sa);
                headers = new LinkedList<Header>();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    headers.add(new Header(entry.getKey(), entry.getValue()));
                }
            } else {
                headers = Collections.singletonList(new Header("User-Agent", requestData.getUserAgent()));
            }
        }

        AJAXRequestResult result;
        if ("doGet".equals(method)) {
            JSONObject jResult = getGuardApi(GUARDADMIN_ENDPOINT).doCallSessionSensitiveGet(parameters, JSONObject.class, session, cookies, headers);
            result = new AJAXRequestResult(jResult, "json");
        } else if ("doPut".equals(method)) {
            JSONObject jResult = getGuardApi(GUARDADMIN_ENDPOINT).doCallSessionSensitivePut(parameters, (JSONObject) requestData.requireData(), JSONObject.class, session, cookies, headers);
            result = new AJAXRequestResult(jResult, "json");
        } else if ("requestResource".equals(method)) {
            InputStream in = getGuardApi(GUARDADMIN_ENDPOINT).requestSessionSensitiveResource(parameters, session, cookies, headers);
            result = new AJAXRequestResult(new FileHolder(in, -1, null, null), "file");
        } else {
            throw AjaxExceptionCodes.INVALID_PARAMETER_VALUE.create("method", method);
        }

        return result;
    }

}
