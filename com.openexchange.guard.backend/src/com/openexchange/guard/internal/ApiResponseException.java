/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import org.apache.http.client.HttpResponseException;
import org.json.JSONObject;

/**
 * {@link ApiResponseException}
 * Used to extend HttpResponse to contain associated JSON
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v3.0.0
 */
public class ApiResponseException extends HttpResponseException {


    private static final long serialVersionUID = -5549947661932915216L;
    private JSONObject json;

    /**
     * Initializes a new {@link ApiResponseException}.
     *
     * @param code The exception code
     * @param reason The exception reason
     * @param json The JSON
     */

    public ApiResponseException(int code, String reason, JSONObject json) {
        super(code, reason);
        this.json = json;
    }

    /**
     * Return the json containing the exception
     *
     * @return The JSON
     */
    public JSONObject getJson() {
        return this.json;
    }

}
