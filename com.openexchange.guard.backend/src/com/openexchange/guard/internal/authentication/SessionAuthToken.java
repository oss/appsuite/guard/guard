/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal.authentication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.openexchange.crypto.CryptoType;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.java.Strings;

/**
 * {@link SessionAuthToken}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SessionAuthToken {

    /** The delimiter used to separate tokens*/
    public static final char TYPE_DELIMITER = '!';

    Map<String, String> authTokens;

    /**
     * Initializes a new {@link SessionAuthToken}.
     *
     * @param sessionVar A string containing tokens
     */
    public SessionAuthToken(Object sessionVar) {
        authTokens = new HashMap<String, String>();
        if (sessionVar == null) {
            return;
        }
        if (!(sessionVar instanceof String)) {
            return;
        }
        String[] tokens = ((String) sessionVar).split(",");
        for (String token : tokens) {
            if (!token.isEmpty()) {
                int split = token.indexOf(TYPE_DELIMITER);
                if (split > 0) {
                    authTokens.put(token.substring(0, split).toUpperCase(), token.substring(split + 1));
                }
            }
        }
    }

    /**
     * Get the auth token for the given type
     * 
     * @param type The type
     * @return The token or <code>null</code> if not set
     */
    public String getAuthToken(String type) {
        if (Strings.isNotEmpty(type)) {
            return authTokens.get(type.toUpperCase());
        }
        return null;
    }

    /**
     * Get a copy of all tokens
     *
     * @return All tokens
     */
    public List<String> getAllTokens() {
        ArrayList<String> tokens = new ArrayList<String>();
        authTokens.forEach((key, value) -> {
            tokens.add(value);
        });
        return tokens;
    }

    /**
     * Removes a token for the given type
     *
     * @param type The type. If the type is <code>null</code> or equals to {@link PROTOCOL#NONE} all tokens are removed
     * @return The value associated with the type or <code>null</code>
     */
    public String removeAuthToken(String type) {
        if (Strings.isEmpty(type) || CryptoType.PROTOCOL.NONE.getValue().equalsIgnoreCase(type)) {
            authTokens = new HashMap<String, String>();
            return null;
        }
        return authTokens.remove(type.toUpperCase());
    }

    /**
     * Replace the token of type with new value.
     *
     * @param token Token to replace
     * @param type Type of the token.
     */
    public void replaceAuthToken(String token, String type) {
        String cryptoType;
        if (Strings.isEmpty(type)) {
            cryptoType = CryptoType.PROTOCOL.NONE.getValue();
        } else {
            cryptoType = type.toUpperCase();
        }
        if (token == null) {
            authTokens.remove(cryptoType);
            return;
        }
        if (authTokens.containsKey(cryptoType)) {
            authTokens.replace(cryptoType, token);
        } else {
            authTokens.put(cryptoType, token);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        authTokens.forEach((key, value) -> {
            sb.append(key);
            sb.append(TYPE_DELIMITER);
            sb.append(value);
            sb.append(",");
        });
        return sb.toString();
    }
}
