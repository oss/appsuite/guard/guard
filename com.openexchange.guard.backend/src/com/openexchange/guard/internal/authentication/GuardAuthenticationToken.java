/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal.authentication;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.openexchange.crypto.CryptoType;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.guard.json.authentication.GuardAuthenticationFactory;
import com.openexchange.java.Strings;

/**
 * {@link GuardAuthenticationToken} represents a token used for authenticating a user against OX Guard.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class GuardAuthenticationToken {

    private final String tokenValue;
    private final String guardSessionId;
    private final Date createdOn;
    private int minutesValid;
    private volatile boolean used;
    private boolean multitoken = false;
    private Map<String, GuardAuthenticationToken> multitokens = null;

    /**
     * Initializes a new {@link GuardAuthenticationToken}.
     *
     * @param guardSessionId A secret random value which represents a guard session.
     * @param tokenValue The value of the authentication token.
     */
    public GuardAuthenticationToken(String guardSessionId, String tokenValue) {
        this.guardSessionId = guardSessionId;
        this.tokenValue = tokenValue;
        this.minutesValid = 0;
        this.createdOn = new Date();
        this.used = false;
    }

    /**
     * Initializes a new {@link GuardAuthenticationToken}.
     *
     * @param guardSessionId A secret random value which represents a Guard session.
     * @param tokenValue The value of the authentication token.
     * @paran minutesValid The amount of time (in minutes) the authentication token should be valid.
     */
    public GuardAuthenticationToken(String guardSessionId, String tokenValue, int minutesValid) {
        this.guardSessionId = guardSessionId;
        this.tokenValue = tokenValue;
        this.minutesValid = minutesValid;
        this.createdOn = new Date();
        this.used = false;
    }

    /**
     * Initializes a new {@link GuardAuthenticationToken}.
     *
     * @param guardSessionId A secret random value which represents a Guard session.
     * @param tokenValue The value of the authentication token.
     * @param createdOn The date the token was created
     * @paran minutesValid The amount of time (in minutes) the authentication token should be valid.
     */
    public GuardAuthenticationToken(String guardSessionId, String tokenValue, Date createdOn, int minutesValid, boolean used) {
        this.guardSessionId = guardSessionId;
        this.tokenValue = tokenValue;
        this.minutesValid = minutesValid;
        this.createdOn = createdOn;
        this.used = used;
    }

    /**
     * Initializes a new {@link GuardAuthenticationToken}.
     * 
     * @param multitoken Multiple tokens as one string, delimited by {@link GuardAuthenticationFactory#VALUE_DELIMITER}
     */
    public GuardAuthenticationToken(Map<String, GuardAuthenticationToken> multitoken) {
        this.guardSessionId = null;
        this.tokenValue = null;
        this.minutesValid = 0;
        this.createdOn = new Date();
        this.used = false;
        this.multitokens = multitoken;
        this.multitoken = true;
    }

    /**
     * Loads multiple tokens from string. String is formatted as sessionId;token1,token2,token3
     * loadMultiToken
     *
     * @param string
     * @return
     */
    private static GuardAuthenticationToken loadMultiToken(String string) {
        HashMap<String, GuardAuthenticationToken> tokens = new HashMap<String, GuardAuthenticationToken>();
        if (Strings.isNotEmpty(string) && string.contains(GuardAuthenticationFactory.VALUE_DELIMITER)) {
            int div = string.indexOf(GuardAuthenticationFactory.VALUE_DELIMITER);
            String sessionId = string.substring(0, div);
            String tokenlist = string.substring(div + 1);
            String[] tokenStrings = tokenlist.split(",");
            for (String tString : tokenStrings) {
                GuardAuthenticationToken t = fromString(sessionId + GuardAuthenticationFactory.VALUE_DELIMITER + tString);
                tokens.put(t.getType().getValue(), t);
            }
        }
        return new GuardAuthenticationToken(tokens);
    }

    /**
     * Factory method which creates a {@link GuardAuthenticationToken} from the given string representation.
     *
     * @param string The string representation to create the {@link GuardAuthenticationToken} from.
     * @return The {@link GuardAuthenticationToken} from the given string representation.
     */
    public static GuardAuthenticationToken fromString(String string) {
        if (string == null) {
            return null;
        }
        // Comma separated list of multiple
        if (string.contains(",")) {
            return loadMultiToken(string);
        }
        final String delimiter = GuardAuthenticationFactory.VALUE_DELIMITER;
        if (string.contains(delimiter)) {
            String[] parts = string.split(delimiter);
            if (parts.length == 2) {
                return new GuardAuthenticationToken(parts[0], parts[1]);
            } else if (parts.length == 3) {
                return new GuardAuthenticationToken(parts[0], parts[1], Integer.parseInt(parts[2]));
            } else if (parts.length == 5) {
                return new GuardAuthenticationToken(parts[0], parts[1], new Date(Long.parseLong(parts[2])), Integer.parseInt(parts[3]), Boolean.parseBoolean(parts[4]));
            }
        }
        return null;
    }

    @Override
    public String toString() {
        //if (this.isUsed()) return null;
        StringBuilder sb = new StringBuilder();
        sb.append(guardSessionId);
        sb.append(GuardAuthenticationFactory.VALUE_DELIMITER);
        sb.append(tokenValue);
        sb.append(GuardAuthenticationFactory.VALUE_DELIMITER);
        sb.append(createdOn.getTime());
        sb.append(GuardAuthenticationFactory.VALUE_DELIMITER);
        sb.append(minutesValid);
        sb.append(GuardAuthenticationFactory.VALUE_DELIMITER);
        sb.append(used);
        return sb.toString();
    }

    /**
     * @return A secret random value which represents a Guard session
     */
    public String getGuardSessionId() {
        return this.guardSessionId;
    }

    /**
     * @return The token value
     */
    public String getValue() {
        return this.tokenValue;
    }

    /**
     * @return The amount of minutes the authentication token should be valid.
     */
    public int getMinutesValid() {
        return this.minutesValid;
    }

    /**
     * Sets the minutes for which the token should be valid
     *
     * @param minutesValid The minutes
     */
    public void setMinutesValid(int minutesValid) {
        this.minutesValid = minutesValid;
    }

    /**
     * @return The creation time stamp of the token.
     */
    public Date getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @return True, if the token has been used at least once for authentication, false otherwise.
     */
    public boolean isUsed() {
        return this.used;
    }

    /**
     * Marks the token as used
     */
    public void setUsed() {
        this.used = true;
    }

    /**
     * Returns the type (SMIME, PGP, etc)
     * 
     * @return The crypto type
     */
    public CryptoType.PROTOCOL getType() {
        if (this.tokenValue.indexOf(SessionAuthToken.TYPE_DELIMITER) > 0) {
            return CryptoType.getTypeFromString(this.tokenValue.substring(0, this.tokenValue.indexOf(SessionAuthToken.TYPE_DELIMITER)));
        }
        // Default to PGP
        return CryptoType.PROTOCOL.PGP;
    }

    /**
     * Get the token for the specific crypto type
     *
     * @param type The type
     * @return The token
     */
    public GuardAuthenticationToken getToken(CryptoType.PROTOCOL type) {
        if (this.multitoken) {
            return this.multitokens.get(null == type ? PROTOCOL.NONE : type.getValue());
        }
        return this;
    }
}
