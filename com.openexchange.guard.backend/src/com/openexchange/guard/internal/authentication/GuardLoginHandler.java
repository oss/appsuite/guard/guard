/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal.authentication;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.Cookie;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.api.Header;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.ApiDynamicExceptionCodes;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.osgi.Services;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link GuardLoginHandler} performs a login request against the OX Guard service on behalf of a given user.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class GuardLoginHandler extends AbstractGuardAccess {

    private final static String AUTH_TOKEN = "auth";

    /**
     * Internal method to check if we got a valid auth-token from the Guard server
     *
     * @param authToken The authToken to check for validity
     * @return true if the authToken seems to be valid, false otherwise.
     */
    private boolean isAuthTokenValid(String authToken) {
        if (authToken != null && authToken.length() > 20) {
            return true;
        }
        return false;
    }

    /**
     * Check if Guard reporting lockout
     * @param authToken
     * @return
     */
    private boolean isLockout(String authToken) {
        return (authToken != null && authToken.equals("Lockout"));
    }

    private boolean isNoKey(String authToken) {
        return (authToken != null && authToken.equals("No Key"));
    }

    private boolean isPin(JSONObject resp) {
        try {
            if (resp.has("pin") && resp.getBoolean("pin")) {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private GuardAuthenticationToken loginInternal(JSONObject json, String guardSessionToken, Session session, List<Cookie> cookies, String userAgent, String type) throws OXException {
        GuardApiImpl guardApi = requireGuardApi(LOGIN_ENDPOINT);
        try {
            //Perform Guard login on behalf of the user
            InputStream responseStream = guardApi.doCallSessionSensitivePost( //@formatter:off
                GuardApis.mapFor("action", "login", "session", session.getSessionID(), "type", type),
                json,
                InputStream.class,
                session,
                cookies,
                Arrays.asList(new Header[] {
                    new Header("User-Agent", userAgent),
                    new Header("X-Forwarded-For", session.getLocalIp())
                }));//@formatter:on

            //Parse the response
            JSONObject response = new JSONObject(new InputStreamReader(responseStream, StandardCharsets.UTF_8));
            if (response.has(AUTH_TOKEN)) {
                final String authToken = response.getString(AUTH_TOKEN);
                if (isAuthTokenValid(authToken)) {
                    return new GuardAuthenticationToken(guardSessionToken, authToken);
                }
                if (isLockout(authToken)) {
                    throw GuardExceptionCodes.LOCKOUT.create();
                }
                if (isNoKey(authToken)) {
                    throw GuardExceptionCodes.MISSING_KEYS.create();
                }
                if (authToken != null && authToken.startsWith("Error")) {
                    throw GuardExceptionCodes.UNKNOWN_ERROR.create(authToken);
                }
                //invalid login
                return null;
            }
            if (isPin(response)) {
                throw GuardExceptionCodes.BLOCKED_BY_PIN.create();
            }
            throw new ApiDynamicExceptionCodes(response, null).create();
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    public GuardAuthenticationToken loginWithSymmetricKey(Session session, String guardSessionToken, String key, List<Cookie> cookies, String userAgent) throws OXException {
        try {
            JSONObject json = new JSONObject();
            if (key != null) {
                json.put("pgp_session_key", key);
            }
            return loginInternal(json, guardSessionToken, session, cookies, userAgent, "pgp");
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Performs a login against the OX Guard server on behalf of the given session's user.
     *
     * @param session The session associated to the user which will be logged in into OX Guard
     * @param guardSessionToken The session value which will be used
     * @param password The OX Guard password
     * @param cookies user's client cookies
     * @param userAgent The client's user agent
     * @return An {@link GuardAuthenticationToken} if the login was successful, null otherwise
     * @throws OXException
     */
    public GuardAuthenticationToken login(Session session, String guardSessionToken, String password, String ePassword, List<Cookie> cookies, String userAgent, String type) throws OXException {
        try {
            JSONObject json = new JSONObject();
            if (ePassword != null) {
                json.put("e_encr_password", ePassword);
            }
            if (password != null) {
                json.put("encr_password", password);
            }
            UserService userService = Services.getService(UserService.class);
            User user = userService.getUser(session.getUserId(), session.getContextId());
            json.put("email", user.getMail());
            json.put("guest", user.getCreatedBy() > 0);
            json.put("oxUserId", session.getUserId());
            json.put("oxCid", session.getContextId());
            return loginInternal(json, guardSessionToken, session, cookies, userAgent, type);
        } catch (JSONException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }
}
