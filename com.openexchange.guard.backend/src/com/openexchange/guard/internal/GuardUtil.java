/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.guard.internal;

import com.openexchange.exception.OXException;
import com.openexchange.guard.mail.service.dataObjects.GuardSecuritySettings;
import com.openexchange.mail.dataobjects.SecuritySettings;

/**
 * General Utility Class
 */
public class GuardUtil {

    /**
     * Cast the SecuritySettings object to GuardSecuritySettings
     *
     * @param obj SecuritySettings to be casted
     * @return GuardSecuritySetting
     * @throws OXException If SecuritySettings is not of type GuardSecuritySetting
     */
    public static GuardSecuritySettings getGuardSecurity(SecuritySettings obj) throws OXException {
        if (obj == null)
            return null;
        if (obj instanceof GuardSecuritySettings) {
            return (GuardSecuritySettings) obj;
        }
        throw OXException.general("Unable to cast security object to Guard security object");
    }
}
