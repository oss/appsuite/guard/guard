/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.sharing;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.File;
import com.openexchange.file.storage.File.Field;
import com.openexchange.file.storage.FileStorageFileAccess;
import com.openexchange.file.storage.FileStorageIgnorableVersionFileAccess;
import com.openexchange.file.storage.FileStorageObjectPermission;
import com.openexchange.file.storage.composition.crypto.CryptoAwareSharingService;
import com.openexchange.groupware.ComparedPermissions;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;


/**
 * {@link CryptoAwareSharingServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class CryptoAwareSharingServiceImpl extends AbstractGuardAccess implements CryptoAwareSharingService {

    /**
     * Check if file is encrypted
     * @param file
     * @return
     */
    @Override
    public boolean isEncrypted (File file) {
        return file.getMeta().containsKey("Encrypted") && file.getMeta().get("Encrypted").equals(true);
    }


    /* (non-Javadoc)
     * @see com.openexchange.file.storage.composition.CryptoAwareSharingService#updateSharing(com.openexchange.session.Session, com.openexchange.file.storage.File, com.openexchange.groupware.ComparedPermissions)
     */
    @Override
    public void updateSharing(Session session, File file, FileStorageFileAccess fileAccess, ComparedPermissions permissions, List<FileStorageObjectPermission> updatedPermissions) throws OXException {
        // If no changes in users, then return
        if (!permissions.hasAddedGuests() && !permissions.hasAddedUsers() && !permissions.hasNewGuests() && !permissions.hasRemovedGuests() && !permissions.hasRemovedUsers()) {
            return;
        }
        if (permissions.hasAddedGroups()) {
            throw GuardExceptionCodes.ACTION_NOT_ALLOWED.create("Group sharing");
        }

        AuthenticationTokenHandler authHandler = new AuthenticationTokenHandler();
        GuardAuthenticationToken authToken = authHandler.requireForSession(session, CryptoType.PROTOCOL.PGP.getValue());
        InputStream fileData = fileAccess.getDocument(file.getFolderId(), file.getId(), FileStorageFileAccess.CURRENT_VERSION);
        GuardApiImpl guardApi = requireGuardApi(PGPCORE_ENDPOINT);
        GuardSharingModificationStream modifiedStream =
            (updatedPermissions != null) ?
                new GuardSharingModificationStream(guardApi, fileData, updatedPermissions, authToken, session) :
                new GuardSharingModificationStream(guardApi, fileData, permissions, authToken, session);
        try (InputStream input = modifiedStream.getGuardStream()) {
            ((FileStorageIgnorableVersionFileAccess) fileAccess).saveDocument(file,
                input,
                file.getSequenceNumber(),
                new ArrayList<Field>(),
                true);
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create("Problem saving file with new share recipients");
        }


    }

}
