/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.sharing;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.DefaultFileStorageObjectPermission;
import com.openexchange.file.storage.FileStorageObjectPermission;
import com.openexchange.guard.osgi.Services;
import com.openexchange.session.Session;
import com.openexchange.user.User;
import com.openexchange.user.UserService;

/**
 * {@link SharingUtil}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class SharingUtil {

    /**
     * Get user IDs of shares within the same context
     * @param list
     * @return
     * @throws JSONException
     */
    public static JSONArray getIDs (List<FileStorageObjectPermission> list) throws JSONException {
        JSONArray items = new JSONArray();
        if (!list.isEmpty()) {
            for (FileStorageObjectPermission pem : list) {
                JSONObject user = new JSONObject();
                user.put("id", pem.getEntity());
                items.put(user);
            }
        }
        return items;
    }

    public static JSONArray getIDs(Collection list) throws JSONException {
        Iterator items = list.iterator();
        JSONArray users = new JSONArray();
        while (items.hasNext()) {
            DefaultFileStorageObjectPermission share = (DefaultFileStorageObjectPermission) items.next();
            JSONObject user = new JSONObject();
            user.put("id", share.getEntity());
            users.put(user);
        }
        return users;
    }

    private static void addUser(JSONArray items, User shareUser) throws JSONException {
        final String mail = shareUser.getMail();
        if (mail != null && !mail.isEmpty()) {
            JSONObject user = new JSONObject();
            user.put("email", mail);
            items.put(user);
        }
    }

    public static JSONArray getEmails(Session session, List<FileStorageObjectPermission> list, int createdBy) throws OXException, JSONException {
        JSONArray items = new JSONArray();
        if (!list.isEmpty()) {
            UserService userService = Services.getService(UserService.class);
            for (FileStorageObjectPermission pem : list) {
                User shareUser = userService.getUser(pem.getEntity(), session.getContextId());
                addUser(items, shareUser);
            }
            if (session.getUserId() != createdBy) {  // Bug GUARD-203, if shared file, original owner keys not added
                User owner = userService.getUser(createdBy, session.getContextId());
                addUser(items, owner);
            }
        }
        return items;
    }


}
