/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.osgi;

import com.openexchange.capabilities.CapabilityService;
import com.openexchange.guard.guest.MailGuestService;
import com.openexchange.guard.guest.impl.GuardGuestFolderHandlerModuleExt;
import com.openexchange.guard.guest.impl.GuardGuestUserService;
import com.openexchange.guard.guest.impl.GuardMailStorageHandler;
import com.openexchange.guard.mail.service.impl.EncryptedMailServiceImpl;
import com.openexchange.guard.mail.service.impl.GuardAwareMailAccessFactory;
import com.openexchange.guard.mail.service.impl.SMIMEMailRecognizer;
import com.openexchange.guard.mail.service.impl.guests.GuardGuestMailProvider;
import com.openexchange.mail.MailProviderRegistry;
import com.openexchange.mail.api.crypto.CryptographicAwareMailAccessFactory;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizer;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizerService;
import com.openexchange.mail.service.EncryptedMailService;
import com.openexchange.mailaccount.MailAccountStorageService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.share.core.ModuleHandler;
import com.openexchange.share.groupware.spi.FolderHandlerModuleExtension;

/**
 * {@link MailActivator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class MailActivator extends HousekeepingActivator {

    //private static final String GUARD_GUEST_PROTOCOL = null;

    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { MailGuestService.class, CapabilityService.class, MailAccountStorageService.class };  // Require MailGuestService to verify GuardActivator loaded
    }

    @Override
    protected void startBundle() throws Exception {

        //For testing purpose:
        GuardGuestMailProvider guardGuestMailProvider = new GuardGuestMailProvider();
        MailProviderRegistry.registerMailProvider(guardGuestMailProvider.getProtocol().getName(), guardGuestMailProvider);

        registerService(EncryptedMailService.class, new EncryptedMailServiceImpl());

        registerService(ModuleHandler.class,
            new GuardMailStorageHandler(
                getService(MailAccountStorageService.class),
                new GuardGuestUserService()));

        registerService(CryptographicAwareMailAccessFactory.class, new GuardAwareMailAccessFactory(this));


        registerService(FolderHandlerModuleExtension.class, new GuardGuestFolderHandlerModuleExt());

        trackService(CryptoMailRecognizerService.class);

        openTrackers();

        registerService(CryptoMailRecognizer.class, new SMIMEMailRecognizer());

    }

}
