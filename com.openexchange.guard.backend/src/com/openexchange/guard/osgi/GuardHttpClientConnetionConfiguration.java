/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.osgi;

import java.util.Optional;
import com.openexchange.config.ConfigurationService;
import com.openexchange.guard.config.GuardBackendProp;
import com.openexchange.rest.client.httpclient.DefaultHttpClientConfigProvider;
import com.openexchange.rest.client.httpclient.HttpBasicConfig;
import com.openexchange.version.VersionService;

/**
 * {@link GuardHttpClientConnetionConfiguration}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.4
 */
public class GuardHttpClientConnetionConfiguration extends DefaultHttpClientConfigProvider {

    public static String HTTP_CLIENT_ID = "guard";

    private final ConfigurationService configurationService;

    /**
     * Initializes a new {@link GuardHttpClientConnetionConfiguration}.
     *
     * @param versionService The {@link VersionService}
     * @param configurationService The {@link ConfigurationService}
     */
    public GuardHttpClientConnetionConfiguration(VersionService versionService, ConfigurationService configurationService) {
        super(HTTP_CLIENT_ID, "OX Guard Http Client v", Optional.ofNullable(versionService));
        this.configurationService = configurationService;
    }

    @Override
    public HttpBasicConfig configureHttpBasicConfig(HttpBasicConfig config) {
        int timeout = configurationService.getIntProperty(GuardBackendProp.endPointTimeout.getFQPropertyName(), (int) GuardBackendProp.endPointTimeout.getDefaultValue());
        ///@formatter:off
        return config.setMaxTotalConnections(100)
            .setMaxTotalConnections(100)
            .setMaxConnectionsPerRoute(100)
            .setConnectTimeout(5000)
            .setSocketReadTimeout(timeout);
        ///@formatter:on
    }
}
