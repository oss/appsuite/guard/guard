/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.guard.mail.service.dataObjects;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.mail.dataobjects.SecuritySettings;

/**
 * Implements SecuritySettings and contains the required Guard settings.
 */
public class GuardSecuritySettings implements SecuritySettings {

    /**
     * Creates a new builder for an instance of <code>GuardSecuritySettings</code>
     *
     * @return The new builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * Creates a new builder for an instance of <code>GuardSecuritySettings</code> pre-filled with the arguments from given <code>Security</code> instance.
     *
     * @return The new builder
     * @throws IllegalArgumentException If given <code>GuardSecuritySettings</code> instance is <code>null</code>
     */
    public static Builder builder(GuardSecuritySettings other) {
        if (other == null) {
            throw new IllegalArgumentException("Security object must not be null");
        }
        return new Builder(other);
    }

    /** The builder for an instance of <code>Security</code> */
    public static class Builder {

        private boolean encrypt;
        private boolean pgpInline;
        private boolean sign;
        private boolean addAutocrypt;
        private boolean encryptDraft;
        private String language;
        private String message;
        private String pin;
        private String msgRef;
        private String authToken;
        private String type;

        /**
         * Initializes a new {@link Builder}.
         */
        Builder() {
            super();
        }

        /**
         * Initializes a new {@link Builder}.
         */
        Builder(GuardSecuritySettings other) {
            this();
            encrypt = other.isEncrypt();
            pgpInline = other.isPgpInline();
            sign = other.isSign();
            language = other.getLanguage();
            message = other.getMessage();
            pin = other.getPin();
            msgRef = other.getMsgRef();
            authToken = other.getAuthToken();
            type = other.getType();
            addAutocrypt = other.isAddAutocrypt();
            encryptDraft = other.isEncryptDraft();
        }

        public Builder withEncrypt(boolean encrypt) {
            this.encrypt = encrypt;
            return this;
        }

        public Builder withPgpInline(boolean pgpInline) {
            this.pgpInline = pgpInline;
            return this;
        }

        public Builder withSign(boolean sign) {
            this.sign = sign;
            return this;
        }

        public Builder withAddAutocrypt(boolean addAutocrypt) {
            this.addAutocrypt = addAutocrypt;
            return this;
        }

        public Builder withEncryptDraft(boolean encryptDraft) {
            this.encryptDraft = encryptDraft;
            return this;
        }

        public Builder withLanguage(String language) {
            this.language = language;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withPin(String pin) {
            this.pin = pin;
            return this;
        }

        public Builder withMsgRef(String msgRef) {
            this.msgRef = msgRef;
            return this;
        }

        public Builder withAuthToken(String authToken) {
            // AuthToken should be null rather than empty
            if (authToken != null && authToken.isEmpty()) {
                authToken = null;
            }
            this.authToken = authToken;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public GuardSecuritySettings build() {
            return new GuardSecuritySettings(encrypt, pgpInline, sign, addAutocrypt, encryptDraft, language, message, pin, msgRef, authToken, type);
        }
    }

    // ----------------------------------------------------------------------------------------------------------------------------

    /** The constant for disabled security */
    public static final GuardSecuritySettings DISABLED = new GuardSecuritySettings(false, false, false, false, false, null, null, null, null, null, null);

    private final boolean encrypt;
    private final boolean pgpInline;
    private final boolean sign;
    private final boolean addAutocrypt;
    private final boolean encryptDraft;
    private final String language;
    private final String message;
    private final String pin;
    private final String msgRef;
    private String authToken;
    private final String type;

    /**
     * Initializes a new {@link GuardSecuritySettings }.
     *
     * @param encrypt <code>true</code> to encrypt; otherwise <code>false</code>
     * @param pgpInline <code>true</code> for PGP inline; otherwise <code>false</code>
     * @param sign <code>true</code> to sign; otherwise <code>false</code>
     * @param language The language identifier
     * @param message An arbitrary message
     * @param msgRef Message reference ID for guest emails
     * @param pin The PIN code
     * @param type The optional crypto type
     */
    GuardSecuritySettings(boolean encrypt, boolean pgpInline, boolean sign, boolean addAutocrypt, boolean encryptDraft, String language, String message, String pin, String msgRef, String authToken, String type) {
        super();
        this.encrypt = encrypt;
        this.pgpInline = pgpInline;
        this.sign = sign;
        this.addAutocrypt = addAutocrypt;
        this.encryptDraft = encryptDraft;
        this.language = language;
        this.message = message;
        this.pin = pin;
        this.msgRef = msgRef;
        this.authToken = authToken;
        this.type = type;
    }

    /**
     * Signals <code>true</code> if all security options are disabled.
     *
     * @return <code>true</code> if all security options are disabled; otherwise <code>false</code>
     */
    public boolean isDisabled() {
        return false == encrypt && false == pgpInline && false == sign && null == language && null == message && null == pin && false == addAutocrypt && false == encryptDraft;
    }

    @Override
    public boolean anythingSet() {
        return !isDisabled();
    }

    /**
     * Gets the language
     *
     * @return The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Gets the message
     *
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the PIN code
     *
     * @return The PIN code
     */
    public String getPin() {
        return pin;
    }

    /**
     * Gets the encrypt flag
     *
     * @return <code>true</code> to encrypt; otherwise <code>false</code>
     */
    @Override
    public boolean isEncrypt() {
        return encrypt;
    }

    /**
     * Gets the flag for PGP inline
     *
     * @return <code>true</code> for PGP inline; otherwise <code>false</code>
     */
    public boolean isPgpInline() {
        return pgpInline;
    }

    /**
     * Gets the flag whether to sign to message
     *
     * @return <code>true</code> to sign; otherwise <code>false</code>
     */
    public boolean isSign() {
        return sign;
    }

    /**
     * Simple check if authentication likely required. Signing will require auth
     * Encryption will require auth if sending to guest (session parameters required)
     * TODO encrypt is only required auth if sending to guest.
     */
    @Override
    public boolean isAuthRequired() {
        return sign || encrypt;
    }

    /**
     * Gets the msg reference String
     *
     * @return
     */
    public String getMsgRef() {
        return msgRef;
    }

    /**
     * Gets the auth token.
     *
     * @return The auth token
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Gets the type of crypto
     *
     * @return
     */
    @Override
    public String getType() {
        return type;
    }

    /**
     * Autocrypt is to be added
     *
     * @return
     */
    public boolean isAddAutocrypt() {
        return addAutocrypt;
    }

    /**
     * Drafts are to be encrypted
     *
     * @return
     */
    public boolean isEncryptDraft() {
        return encryptDraft;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (encrypt ? 1231 : 1237);
        result = prime * result + (pgpInline ? 1231 : 1237);
        result = prime * result + (sign ? 1231 : 1237);
        result = prime * result + (addAutocrypt ? 1231 : 1237);
        result = prime * result + (encryptDraft ? 1231 : 1237);
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((msgRef == null) ? 0 : msgRef.hashCode());
        result = prime * result + ((pin == null) ? 0 : pin.hashCode());
        result = prime * result + ((authToken == null) ? 0 : authToken.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GuardSecuritySettings)) {
            return false;
        }
        GuardSecuritySettings other = (GuardSecuritySettings) obj;
        if (encrypt != other.encrypt) {
            return false;
        }
        if (pgpInline != other.pgpInline) {
            return false;
        }
        if (sign != other.sign) {
            return false;
        }
        if (addAutocrypt != other.addAutocrypt) {
            return false;
        }
        if (encryptDraft != other.encryptDraft) {
            return false;
        }
        if (language == null) {
            if (other.language != null) {
                return false;
            }
        } else if (!language.equals(other.language)) {
            return false;
        }
        if (message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!message.equals(other.message)) {
            return false;
        }
        if (msgRef == null) {
            if (other.msgRef != null) {
                return false;
            }
        } else if (!msgRef.equals(other.msgRef)) {
            return false;
        }
        if (pin == null) {
            if (other.pin != null) {
                return false;
            }
        } else if (!pin.equals(other.pin)) {
            return false;
        }
        if (authToken == null) {
            if (other.authToken != null) {
                return false;
            }
        } else if (!authToken.equals(other.authToken)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!type.equals(other.type)) {
            return false;
        }

        return true;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jSecurity = new JSONObject(9);
        jSecurity.put("encrypt", this.isEncrypt());
        jSecurity.put("pgpInline", this.isPgpInline());
        jSecurity.put("addAutocrypt", this.isAddAutocrypt());
        jSecurity.put("encryptDraft", this.isEncryptDraft());
        jSecurity.put("sign", this.isSign());
        jSecurity.putSafe("language", this.getLanguage());
        jSecurity.putSafe("message", this.getMessage());
        jSecurity.putSafe("pin", this.getPin());
        jSecurity.putSafe("msgRef", this.getMsgRef());
        jSecurity.putSafe("type", this.getType());
        jSecurity.putSafe("authToken", this.getAuthentication());
        return jSecurity;
    }

    @Override
    public String toString() {
        StringBuilder builder2 = new StringBuilder();
        builder2
        .append("[encrypt=")
        .append(encrypt)
        .append(", pgpInline=")
        .append(pgpInline)
        .append(", sign=")
        .append(sign)
        .append(", addAutocrypt=")
        .append(addAutocrypt)
        .append(", encryptDraft=")
        .append(encryptDraft)
        .append(", ");
        if (language != null) {
            builder2.append("language=").append(language).append(", ");
        }
        if (message != null) {
            builder2.append("message=").append(message).append(", ");
        }
        if (pin != null) {
            builder2.append("pin=").append(pin).append(", ");
        }
        if (msgRef != null) {
            builder2.append("msgRef=").append(msgRef).append(", ");
        }
        if (authToken != null) {
            builder2.append("authToken=***").append(", ");
        }
        if (type != null) {
            builder2.append("type=").append(type);
        }

        builder2.append(']');
        return builder2.toString();
    }

    @Override
    public void updateAuthentication(String authToken) {
        this.authToken = authToken;

    }

    @Override
    public String getAuthentication() {
        return this.authToken;
    }

    @Override
    public GuardSecuritySettings duplicate() {
        return new Builder(this).build();
    }
}
