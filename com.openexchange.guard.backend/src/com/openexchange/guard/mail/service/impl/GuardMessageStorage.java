/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.AuthenticationTokenHandler;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.java.Strings;
import com.openexchange.mail.IndexRange;
import com.openexchange.mail.MailExceptionCode;
import com.openexchange.mail.MailField;
import com.openexchange.mail.MailFields;
import com.openexchange.mail.MailSortField;
import com.openexchange.mail.OrderDirection;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageDelegator;
import com.openexchange.mail.api.IMailMessageStorageExt;
import com.openexchange.mail.api.IMailMessageStorageMimeSupport;
import com.openexchange.mail.api.IMailMessageStorageThreadReferences;
import com.openexchange.mail.api.ISimplifiedThreadStructure;
import com.openexchange.mail.api.ISimplifiedThreadStructureEnhanced;
import com.openexchange.mail.api.MailMessageStorage;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.mail.dataobjects.SecurityInfo;
import com.openexchange.mail.dataobjects.compose.ComposedMailMessage;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizerService;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.mail.parser.MailMessageParser;
import com.openexchange.mail.parser.handlers.ImageMessageHandler;
import com.openexchange.mail.parser.handlers.MailPartHandler;
import com.openexchange.mail.search.SearchTerm;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link GuardMessageStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorage extends MailMessageStorage implements IMailMessageStorageDelegator {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(GuardMessageStorage.class);

    protected final IMailMessageStorage delegate;
    protected final Session session;
    protected final ServiceLookup services;
    private final GuardAuthenticationToken authToken;
    private final CryptoType.PROTOCOL type;

    /**
     * Initializes a new {@link GuardMessageStorage}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     */
    public GuardMessageStorage(Session session, IMailMessageStorage delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        this.session = session;
        this.delegate = delegate;
        this.services = services;
        this.authToken = authToken;
        this.type = type;
    }

    @Override
    public <T> T supports(Class<T> iface) throws OXException {
        T instance = super.supports(iface);
        if (null == instance) {
            return null;
        }

        if (IMailMessageStorageExt.class == iface) {
            return (T) new GuardMessageStorageExt(session, (IMailMessageStorageExt) instance, services, authToken, type);
        }

        if (IMailMessageStorageMimeSupport.class == iface) {
            return (T) new GuardMessageStorageMimeSupport(session, (IMailMessageStorageMimeSupport) instance, services, authToken, type);
        }

        if (IMailMessageStorageThreadReferences.class == iface) {
            return (T) new GuardMessageStorageThreadReferences(session, (IMailMessageStorageThreadReferences) instance, services, authToken, type);
        }

        if (ISimplifiedThreadStructureEnhanced.class == iface) {
            return (T) new GuardMessageStorageThreadStructureEnhanced(session, (ISimplifiedThreadStructureEnhanced) instance, services, authToken, type);
        }

        if (ISimplifiedThreadStructure.class == iface) {
            return (T) new GuardMessageStorageThreadStructure(session, (ISimplifiedThreadStructure) instance, services, authToken, type);
        }

        return instance;
    }

    protected GuardAuthenticationToken getAuthToken(Session session, CryptoType.PROTOCOL type, boolean markUsed) throws OXException {
        return this.authToken != null ?
            this.authToken.getToken(type) :
            new AuthenticationTokenHandler().getForSession(session, markUsed, type.getValue());
    }

    /**
     * Checks whether the given message is a PGP message
     *
     * @param message
     * @return True, if the message is a PGP message, false otherwise
     * @throws OXException
     */
    private boolean isCryptoMessage(MailMessage message) throws OXException {
        CryptoMailRecognizerService cryptoMailRecognizer = services.getServiceSafe(CryptoMailRecognizerService.class);
        return cryptoMailRecognizer.isCryptoMessage(message);
    }

    /**
     * Checks if plain-text message is signed
     *
     * @param message
     * @return
     * @throws OXException
     */
    private boolean isSignedMessage(MailMessage message) throws OXException {
        CryptoMailRecognizerService cryptoMailRecognizer = services.getServiceSafe(CryptoMailRecognizerService.class);
        return cryptoMailRecognizer.isSignedMessage(message);
    }

    private CryptoType.PROTOCOL getTypeCrypto(MailMessage message) throws OXException {
        CryptoMailRecognizerService cryptoMailRecognizer = services.getServiceSafe(CryptoMailRecognizerService.class);
        return cryptoMailRecognizer.getTypeCrypto(message);
    }

    /**
     * Internal method to process a MimeMessage using OX Guard
     *
     * @param authToken The authToken, might be null for messages which does not require an authentication (for example verifying signed messages)
     * @param message The message
     * @return The processed (i.e decrypted and/or verified) message; otherwise given message
     * @throws OXException
     */
    protected MailMessage processMessage(MailMessage message) throws OXException {
        MailMessage processedMessage = optProcessMessage(message);
        return null == processedMessage ? message : processedMessage;
    }

    /**
     * Internal method to process a MimeMessage using OX Guard
     *
     * @param mailMessage The processed message or <code>null</code>
     * @return The processed (i.e decrypted and/or verified) message or <code>null</code>
     * @throws OXException
     */
    protected MailMessage optProcessMessage(MailMessage mailMessage) throws OXException {
        SecurityInfo info = mailMessage.getSecurityInfo();
        CryptoType.PROTOCOL type = info == null ? getTypeCrypto(mailMessage) : info.getType();
        if (type == null) {
            LOG.error("Unknown crypto type");
            return mailMessage;
        }
        GuardAuthenticationToken authToken = getAuthToken(session, type, false);  // Grab token non-destructively

        final GuardMimeDecrypter decrypter = new GuardMimeDecrypter();

        MailMessage mail = mailMessage;
        mail.removeHeader("X-Guard-Signature-Result");  // Remove previous header if there

        // Copy flags
        int flags = mail.containsFlags() ? mail.getFlags() : -1;
        int label = mail.containsColorLabel() ? mail.getColorLabel() : -1;
        String[] userFlags = mail.containsUserFlags() ? mail.getUserFlags() : null;
        int threadLevel = mail.containsThreadLevel() ? mail.getThreadLevel() : -1;
        Boolean hasAttachment = mail.containsHasAttachment() ? Boolean.valueOf(mail.isHasAttachment()) : null;

        boolean changed = false;
        if (isCryptoMessage(mail)) {
            if (authToken == null) {
                LOG.error("Missing / Bad authentication");
                throw GuardExceptionCodes.MISSING_AUTH.create();
            }

            mail = decrypter.decrypt(session, authToken, mail, getTypeCrypto(mail));
            changed = true;
        }
        if (isSignedMessage(mail)) {
            GuardMimeVerifier verifier = new GuardMimeVerifier();
            mail = verifier.verify(session, mail, type);
            changed = true;
        }
        if (false == changed) {
            // Not processed
            return null;
        }

        getAuthToken(session, type, true);  // Mark token as used now.

        // Restore flags to decoded email
        if (flags >= 0) {
            mail.setFlags(flags);
        }
        if (label >= 0) {
            mail.setColorLabel(label);
        }
        if (null != userFlags) {
            mail.addUserFlags(userFlags);
        }
        if (threadLevel >= 0) {
            mail.setThreadLevel(threadLevel);
        }
        if (null != hasAttachment) {
            mail.setHasAttachment(hasAttachment.booleanValue());
        }

        return mail;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorage#updateMessageFlags(java.lang.String, java.lang.String[], int, java.lang.String[], boolean)
     */
    @Override
    public void updateMessageFlags(String folder, String[] mailIds, int flags, String[] userFlags, boolean set) throws OXException {
        delegate.updateMessageFlags(folder, mailIds, flags, userFlags, set);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailMessageStorage#appendMessages(java.lang.String, com.openexchange.mail.dataobjects.MailMessage[])
     */
    @Override
    public String[] appendMessages(String destFolder, MailMessage[] msgs) throws OXException {
        return delegate.appendMessages(destFolder, msgs);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailMessageStorage#copyMessages(java.lang.String, java.lang.String, java.lang.String[], boolean)
     */
    @Override
    public String[] copyMessages(String sourceFolder, String destFolder, String[] mailIds, boolean fast) throws OXException {
        return delegate.copyMessages(sourceFolder, destFolder, mailIds, fast);
    }

    @Override
    public void deleteMessages(String folder, String[] mailIds, boolean hardDelete) throws OXException {
        delegate.deleteMessages(folder, mailIds, hardDelete);
    }

    @Override
    public MailMessage[] getAllMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, MailField[] fields) throws OXException {
        return delegate.getAllMessages(folder, indexRange, sortField, order, fields);
    }

    @Override
    public MailMessage[] getMessages(String folder, String[] mailIds, MailField[] fields) throws OXException {
        MailMessage[] messages = delegate.getMessages(folder, mailIds, fields);
        if (null == messages || messages.length <= 0) {
            return messages;
        }

        // Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {
            for (int i = messages.length; i-- > 0;) {
                messages[i] = processMessage(messages[i]);
            }
        }

        return messages;
    }

    @Override
    public MailMessage[] getThreadSortedMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields) throws OXException {
        MailMessage[] messages = super.getThreadSortedMessages(folder, indexRange, sortField, order, searchTerm, fields);
        if (null == messages || messages.length <= 0) {
            return messages;
        }

        //Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {

            for (int i = messages.length; i-- > 0;) {
                messages[i] = processMessage(messages[i]);
            }
        }

        return messages;
    }

    @Override
    public MailMessage getMessage(String folder, String mailId, boolean markSeen) throws OXException {
        MailMessage message = delegate.getMessage(folder, mailId, markSeen);
        if (null == message) {
            return message;
        }

        return processMessage(message);
    }

    @Override
    public MailPart getAttachment(String folder, String mailId, String sequenceId) throws OXException {
        // Get message from delegate storage
        MailMessage message = delegate.getMessage(folder, mailId, false);
        if (null == message) {
            throw MailExceptionCode.MAIL_NOT_FOUND.create(mailId, folder);
        }

        // Check whether message is supposed to be processed
        MailMessage processedMessage = optProcessMessage(message);
        if (null == processedMessage) {
            // Plain...
            return delegate.getAttachment(folder, mailId, sequenceId);
        }

        // Grab attachment from processed message
        message = processedMessage;
        MailPartHandler handler = new MailPartHandler(sequenceId);
        new MailMessageParser().parseMailMessage(message, handler);
        MailPart ret = handler.getMailPart();
        if (ret == null) {
            throw MailExceptionCode.ATTACHMENT_NOT_FOUND.create(sequenceId, mailId, folder);
        }
        return ret;
    }

    @Override
    public MailPart getImageAttachment(String folder, String mailId, String contentId) throws OXException {
        if (Strings.isEmpty(contentId)) {
            return null;
        }

        // Get message from delegate storage
        MailMessage message = delegate.getMessage(folder, mailId, false);
        if (null == message) {
            throw MailExceptionCode.MAIL_NOT_FOUND.create(mailId, folder);
        }

        // Check whether message is supposed to be processed
        MailMessage processedMessage = optProcessMessage(message);
        if (null == processedMessage) {
            // Plain...
            return delegate.getImageAttachment(folder, mailId, contentId);
        }

        // Grab image from processed message
        message = processedMessage;
        ImageMessageHandler handler = new ImageMessageHandler(contentId);
        new MailMessageParser().parseMailMessage(message, handler);
        MailPart ret = handler.getImagePart();
        if (ret == null) {
            throw MailExceptionCode.IMAGE_ATTACHMENT_NOT_FOUND.create(contentId, mailId, folder);
        }
        return ret;
    }

    @Override
    public MailMessage[] getUnreadMessages(String folder, MailSortField sortField, OrderDirection order, MailField[] fields, int limit) throws OXException {
        return delegate.getUnreadMessages(folder, sortField, order, fields, limit);
    }

    @Override
    public String[] moveMessages(String sourceFolder, String destFolder, String[] mailIds, boolean fast) throws OXException {
        return delegate.moveMessages(sourceFolder, destFolder, mailIds, fast);
    }

    @Override
    public void releaseResources() throws OXException {
        delegate.releaseResources();
    }

    @Override
    public MailMessage saveDraft(String draftFullname, ComposedMailMessage draftMail) throws OXException {
        //TODO
        return delegate.saveDraft(draftFullname, draftMail);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.MailMessageStorage#searchMessages(java.lang.String, com.openexchange.mail.IndexRange, com.openexchange.mail.MailSortField, com.openexchange.mail.OrderDirection, com.openexchange.mail.search.SearchTerm, com.openexchange.mail.MailField[])
     */
    @Override
    public MailMessage[] searchMessages(String folder, IndexRange indexRange, MailSortField sortField, OrderDirection order, SearchTerm<?> searchTerm, MailField[] fields) throws OXException {
        MailMessage[] messages = delegate.searchMessages(folder, indexRange, sortField, order, searchTerm, fields);

        if (null == messages || messages.length <= 0) {
            return messages;
        }

        //Only decrypt if body should be fetched
        MailFields mailFields = new MailFields(fields);
        if (mailFields.contains(MailField.BODY) || mailFields.contains(MailField.FULL)) {

            for (int i = messages.length; i-- > 0;) {
                messages[i] = processMessage(messages[i]);
            }
        }

        return messages;
    }

    @Override
    public void updateMessageColorLabel(String folder, String[] mailIds, int colorLabel) throws OXException {
        delegate.updateMessageColorLabel(folder, mailIds, colorLabel);
    }

    @Override
    public void updateMessageUserFlags(String folder, String[] mailIds, String[] flags, boolean set) throws OXException {
        delegate.updateMessageUserFlags(folder, mailIds, flags, set);
    }

    @Override
    public void updateMessageFlags(String folder, String[] mailIds, int flags, boolean set) throws OXException {
        delegate.updateMessageFlags(folder, mailIds, flags, set);
    }

    @Override
    public MailMessage[] getNewAndModifiedMessages(String folder, MailField[] fields) throws OXException {
        return delegate.getNewAndModifiedMessages(folder, fields);
    }

    @Override
    public MailMessage[] getDeletedMessages(String folder, MailField[] fields) throws OXException {
        return delegate.getDeletedMessages(folder, fields);
    }

    @Override
    public int getUnreadCount(String folder, SearchTerm<?> searchTerm) throws OXException {
        return delegate.getUnreadCount(folder, searchTerm);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailMessageStorageDelegator#getDelegateMessageStorage()
     */
    @Override
    public IMailMessageStorage getDelegateMessageStorage() throws OXException {
        return delegate;
    }
}
