/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.GuardResult;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.mail.service.dataObjects.CryptoSecurityResult;
import com.openexchange.guard.mail.service.dataObjects.GuardSecuritySettings;
import com.openexchange.mail.MailField;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.session.Session;

/**
 * {@link GuardMimeVerifier}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.8.4
 */
class GuardMimeVerifier extends AbstractGuardAccess {

    private static final Address[] NO_RECIPIENTS = new Address[] {};
    private static final GuardAuthenticationToken NULL_AUTHENTICATION_TOKEN = null;

    /**
     * Verify signature of Mail Message
     *
     * @param session The session used for verifying
     * @param message The {@link MailMessage} to verify
     * @return The type of the message's signature
     * @throws OXException
     */
    public MailMessage verify(Session session, MailMessage message, CryptoType.PROTOCOL type) throws OXException {

        Message originalMimeMessage = MimeMessageConverter.convertMailMessage(message, true);
        Message verifiedMimeMessage = verify(session, originalMimeMessage, type);

        final MailField[] ALL_FIELDS = {MailField.FULL};
        MailMessage ret = MimeMessageConverter.convertMessage((MimeMessage)verifiedMimeMessage ,
            message.getMailId(),
            message.getFolder(),
            com.openexchange.mail.MailPath.SEPERATOR,
            ALL_FIELDS);
        CryptoSecurityResult security;
        if (message.hasSecurityResult()) {  // If already decoded, just add signature results
            security = (CryptoSecurityResult) message.getSecurityResult();
            security.addHeaderSignatureResults(ret.getHeader("X-Guard-Signature-Result"));
        } else {
            security = new CryptoSecurityResult(ret, type);
        }
        ret.setSecurityResult(security);
        return ret;

    }


    private MimeMessage verify(Session session, Message message, CryptoType.PROTOCOL type) throws OXException {

        GuardApiImpl guardApi = requireGuardApi(CRYPTO_ENDPOINT);
        // Add decrypt flag?
        GuardSecuritySettings settings = GuardSecuritySettings.builder().build();

        GuardResult result = guardApi.processMimeMessage(message, NO_RECIPIENTS, settings,GuardApis.mapFor(
            "action", "verify",
            "user", Integer.toString(session.getUserId()),
            "context", Integer.toString(session.getContextId()), "type", type.toString()),
            session,
            NULL_AUTHENTICATION_TOKEN);
        return result.getMimeMessage();
    }
}
