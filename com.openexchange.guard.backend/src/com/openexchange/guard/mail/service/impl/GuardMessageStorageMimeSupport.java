/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.mail.api.IMailMessageStorage;
import com.openexchange.mail.api.IMailMessageStorageMimeSupport;
import com.openexchange.mail.compose.HeaderUtility;
import com.openexchange.mail.dataobjects.SecurityInfo;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizerService;
import com.openexchange.mail.mime.crypto.PGPMailRecognizer;
import com.openexchange.server.ServiceLookup;
import com.openexchange.session.Session;

/**
 * {@link GuardMessageStorageMimeSupport} - The Guard-aware message storage delegating to {@link IMailMessageStorageMimeSupport}.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v7.8.4
 */
public class GuardMessageStorageMimeSupport extends GuardMessageStorage implements IMailMessageStorageMimeSupport {

    private final IMailMessageStorageMimeSupport messageStorageMimeSupport;
    private final CryptoType.PROTOCOL type;

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(GuardMessageStorageMimeSupport.class);
    }

    /**
     * Initializes a new {@link GuardMessageStorageMimeSupport}.
     *
     * @param session A {@link Session} a session object
     * @param delegate The underlying {@link IMailMessageStorage} to wrap with guard.
     * @param pgpMailRecognizer A {@link PGPMailRecognizer} object recognizes PGP messages.
     * @param authToken
     */
    public GuardMessageStorageMimeSupport(Session session, IMailMessageStorageMimeSupport delegate, ServiceLookup services, GuardAuthenticationToken authToken, CryptoType.PROTOCOL type) {
        super(session, delegate, services, authToken, type);
        this.messageStorageMimeSupport = delegate;
        this.type = type;
    }

    @Override
    public boolean isMimeSupported() throws OXException {
        return messageStorageMimeSupport.isMimeSupported();
    }

    @Override
    public String[] appendMimeMessages(String destFolder, Message[] msgs) throws OXException {
        return messageStorageMimeSupport.appendMimeMessages(destFolder, msgs);
    }

    @Override
    public Message getMimeMessage(String fullName, String id, boolean markSeen) throws OXException {
        Message mimeMessage = messageStorageMimeSupport.getMimeMessage(fullName, id, markSeen);
        if (null == mimeMessage) {
            return null;
        }

        return processMimeMessage(mimeMessage);
    }

    /**
     * Get the security Info from the headers
     *
     * @param message
     * @return SecurityInfo if any from message, null if none
     */
    private SecurityInfo getSecurityInfo(Message message) {
        String[] securityHeaders;
        try {
            securityHeaders = message.getHeader("X-Security");
        } catch (MessagingException e) {
            return null;
        }
        if (securityHeaders != null && securityHeaders.length > 0) {
            String header = securityHeaders[0];
            SecurityInfo info = new SecurityInfo(header.contains("encrypt=true"),
                header.contains("sign=true"),
                header.contains("type=smime") ? CryptoType.PROTOCOL.SMIME : CryptoType.PROTOCOL.PGP);
            return info;
        }
        return null;
    }

    /**
     * Checks for encoded security header. If present, returns stored auth token
     *
     * @param message
     * @return AuthToken string if any, null if not found
     */
    private String getStoredAuthToken(Message message) {
        try {
            String[] header = message.getHeader(HeaderUtility.HEADER_X_OX_SECURITY);
            if (header.length > 0) {
                String securityStr = HeaderUtility.decodeHeaderValue(header[0]);
                if (securityStr != null) {
                    JSONObject security = new JSONObject(securityStr);
                    return security.getString("authToken");
                }
            }
        } catch (MessagingException | JSONException e) {
            LoggerHolder.LOG.error("Error checking message for security header", e);
        }
        return null;
    }

    /**
     * Check if there has been a change in type crypto of a message. I.E. user changes from S/Mime
     * to PGP while composing. The stored auth token will be different. The old token should be used
     * to decrypt the message, where it will be re-encrypted with the new crypto and new token stored.
     *
     * @param message Message to check
     * @param token Token currently sent with the action
     * @return Current token if not changed. Old stored token if present and different.
     */
    private GuardAuthenticationToken checkChangedType(Message message, GuardAuthenticationToken token) {
        final String savedToken = getStoredAuthToken(message);
        if (savedToken == null) {
            return token;
        }
        if (token == null)
            return null;
        if (!savedToken.startsWith(token.getType().getValue())) {
            return new GuardAuthenticationToken(token.getGuardSessionId(), savedToken);
        }
        return token;
    }

    /**
     * Internal method to process a MIME message using OX Guard
     *
     * @param authToken The authToken
     * @param message The MIME message
     * @return The processed (i.e decrypted) message
     * @throws OXException
     */
    private Message processMimeMessage(Message message) throws OXException {
        CryptoMailRecognizerService cryptoMail = services.getOptionalService(CryptoMailRecognizerService.class);
        // Check if encrypted email.  Drafts, for example, may or may not be encrypted depending on the crypto type
        if (cryptoMail != null) {
            if (cryptoMail.isEncryptedMessage(message)) {
                // Check to see if already has a security header containing an authentication

                // Get the authentication sent with the message
                GuardAuthenticationToken guardAuthToken = getAuthToken(session, type, false);
                // If we have changed type, we may be encrypting with a different auth, and this needs
                // to be decrypted with the old auth saved in the header.  Check that they are the same
                guardAuthToken = checkChangedType(message, guardAuthToken);

                if (guardAuthToken == null) {
                    throw GuardExceptionCodes.MISSING_AUTH.create();
                }
                return new GuardMimeDecrypter().decrypt(session, guardAuthToken, message, getSecurityInfo(message));
            }
        }
        return message;

    }
}
