/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.util.Objects;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.internet.MimeMessage;
import com.openexchange.crypto.CryptoType;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.GuardResult;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.guard.mail.service.dataObjects.CryptoSecurityResult;
import com.openexchange.mail.MailField;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.SecurityInfo;
import com.openexchange.mail.mime.converters.MimeMessageConverter;
import com.openexchange.session.Session;

/**
 * {@link GuardMimeDecrypter}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
class GuardMimeDecrypter extends AbstractGuardAccess {

    private static final Address[] NO_RECIPIENTS = new Address[] {};

    public MailMessage decrypt(Session session, GuardAuthenticationToken authToken, MailMessage message, CryptoType.PROTOCOL type) throws OXException {
        session = Objects.requireNonNull(session, "session must not be null");
        authToken = Objects.requireNonNull(authToken, "authToken must not be null");
        message = Objects.requireNonNull(message, "message must not be null");

        Message originalMimeMessage = MimeMessageConverter.convertMailMessage(message, true);

        Message decryptedMimeMessage = decrypt(session, authToken, originalMimeMessage, message.getSecurityInfo());

        final MailField[] ALL_FIELDS = { MailField.FULL };
        MailMessage ret = MimeMessageConverter.convertMessage((MimeMessage) decryptedMimeMessage,
            message.getMailId(),
            message.getFolder(),
            com.openexchange.mail.MailPath.SEPERATOR,
            ALL_FIELDS);
        CryptoSecurityResult security = new CryptoSecurityResult(ret, type);
        security.setDecoded();
        ret.setSecurityResult(security);
        return ret;

    }

    public MimeMessage decrypt(Session session, GuardAuthenticationToken authToken, Message message, SecurityInfo security) throws OXException {
        session = Objects.requireNonNull(session, "session must not be null");
        authToken = Objects.requireNonNull(authToken, "authToken must not be null");
        message = Objects.requireNonNull(message, "message must not be null");
        GuardApiImpl guardApi = requireGuardApi(CRYPTO_ENDPOINT);

        GuardResult result = guardApi.processMimeMessage(
            message,
            NO_RECIPIENTS,
            null,
            GuardApis.mapFor("action", "decrypt", "user", Integer.toString(session.getUserId()),
                "context", Integer.toString(session.getContextId()),
                "type", security == null ? "pgp" : security.getType().getValue()),
            session,
            authToken);

        return result.getMimeMessage();
    }
}
