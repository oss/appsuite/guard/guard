/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailUpdater} updates a guest mail item in OX Guard
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailUpdater extends AbstractGuardAccess {

    /**
     * Internal method to create an update object
     *
     * @param mailIds The IDs of the mails to update
     * @param flags The flag(s) to enable or disable, or null if no flags to update
     * @param set True to enable the given flags, false to disable
     * @param colorLabel The color label to update, or null if no color labels to update
     * @return The update JSONObject
     */
    private JSONObject createUpdate(String[] mailIds, Integer flags, Boolean set, Integer colorLabel) {
       JSONObject json = new JSONObject()
           .putSafe("mailIds", new JSONArray(Arrays.asList(mailIds)));

       if(flags != null) {
           json.putSafe("mailFlags", flags);
           json.putSafe("mailFlagsMode", set ? "SET" : "UNSET");
       }

       if(colorLabel != null) {
           json.putSafe("colorLabel", colorLabel);
       }

       return json;
    }

    /**
     * Internal method to update a guest message in OX Guard
     *
     * @param session The session
     * @param authToken The authentication token to use
     * @param mailIds The IDs of the mails to update
     * @param flags The flag(s) to enable or disable, or null if no flags to update
     * @param set True to enable the given flags, false to disable
     * @param colorLabel The color label to update, or null if no color labels to update
     * @throws OXException
     */
    private void updateMessageInternal(Session session, GuardAuthenticationToken authToken, String[] mailIds, Integer flags, Boolean set, Integer colorLabel) throws OXException {

       final GuardApiImpl guardGuestApi = requireGuardApi(GUEST_ENDPOINT);
       final JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
       final JSONObject json = new JSONObject()
           .putSafe("auth", authJson)
           .putSafe("update", createUpdate(mailIds, flags, set, colorLabel));
       ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        try(InputStream result = guardGuestApi.processResource(GuardApis.mapFor("action", "updatemessages"),
            jsonStream,
            null,
            "json",
            session)){
            //no-op
        }
        catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Updates the message flags of the given mails
     *
     * @param session The session
     * @param authToken The authentication token to use
     * @param mailIds The IDs of the mails to update
     * @param flags The flag(s) to enable or disable
     * @param set True to enable the given flags, false to disable
     * @throws OXException
     */
    public void updateMessageFlags(Session session, GuardAuthenticationToken authToken, String[] mailIds, int flags, boolean set) throws OXException {
        final Integer colorLabel = null; //do not update color labels
        updateMessageInternal(session, authToken, mailIds, flags, set, colorLabel);
    }

    /**
     * Updates the color flags for the given mails
     *
     * @param session The session
     * @param authToken The authentication token to use
     * @param mailIds The IDs of the mails to update
     * @param colorLabel The new color label to set
     * @throws OXException
     */
    public void updateColorLabel(Session session, GuardAuthenticationToken authToken, String[] mailIds, int colorLabel) throws OXException {
        final Integer messageFlags = null; //do not update message flags
        final Boolean messageFlagsSetMode = null; //do not update message flags
        updateMessageInternal(session, authToken, mailIds, messageFlags, messageFlagsSetMode, colorLabel);
    }
}
