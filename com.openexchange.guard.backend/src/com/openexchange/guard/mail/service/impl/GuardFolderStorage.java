/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import com.openexchange.exception.OXException;
import com.openexchange.mail.Quota;
import com.openexchange.mail.Quota.Type;
import com.openexchange.mail.api.IMailFolderStorage;
import com.openexchange.mail.api.IMailFolderStorageDelegator;
import com.openexchange.mail.dataobjects.MailFolder;
import com.openexchange.mail.dataobjects.MailFolderDescription;

/**
 * {@link GuardFolderStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class GuardFolderStorage implements IMailFolderStorageDelegator {

    private final IMailFolderStorage delegate;

    /**
     * Initializes a new {@link GuardFolderStorage}.
     *
     * @param delegate The folder storage to delegate to
     */
    public GuardFolderStorage(IMailFolderStorage delegate) {
        this.delegate = delegate;
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#exists(java.lang.String)
     */
    @Override
    public boolean exists(String fullName) throws OXException {
        return delegate.exists(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getFolder(java.lang.String)
     */
    @Override
    public MailFolder getFolder(String fullName) throws OXException {
        return delegate.getFolder(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSubfolders(java.lang.String, boolean)
     */
    @Override
    public MailFolder[] getSubfolders(String parentFullName, boolean all) throws OXException {
        return delegate.getSubfolders(parentFullName, all);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getRootFolder()
     */
    @Override
    public MailFolder getRootFolder() throws OXException {
        return delegate.getRootFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getDefaultFolderPrefix()
     */
    @Override
    public String getDefaultFolderPrefix() throws OXException {
        return delegate.getDefaultFolderPrefix();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#checkDefaultFolders()
     */
    @Override
    public void checkDefaultFolders() throws OXException {
        delegate.checkDefaultFolders();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#createFolder(com.openexchange.mail.dataobjects.MailFolderDescription)
     */
    @Override
    public String createFolder(MailFolderDescription toCreate) throws OXException {
        return delegate.createFolder(toCreate);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#updateFolder(java.lang.String, com.openexchange.mail.dataobjects.MailFolderDescription)
     */
    @Override
    public String updateFolder(String fullName, MailFolderDescription toUpdate) throws OXException {
        return delegate.updateFolder(fullName, toUpdate);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#moveFolder(java.lang.String, java.lang.String)
     */
    @Override
    public String moveFolder(String fullName, String newFullName) throws OXException {
        return delegate.moveFolder(fullName, newFullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#renameFolder(java.lang.String, java.lang.String)
     */
    @Override
    public String renameFolder(String fullName, String newName) throws OXException {
        return delegate.renameFolder(fullName, newName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#deleteFolder(java.lang.String)
     */
    @Override
    public String deleteFolder(String fullName) throws OXException {
        return delegate.deleteFolder(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#deleteFolder(java.lang.String, boolean)
     */
    @Override
    public String deleteFolder(String fullName, boolean hardDelete) throws OXException {
        return delegate.deleteFolder(fullName, hardDelete);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#clearFolder(java.lang.String)
     */
    @Override
    public void clearFolder(String fullName) throws OXException {
        delegate.clearFolder(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#clearFolder(java.lang.String, boolean)
     */
    @Override
    public void clearFolder(String fullName, boolean hardDelete) throws OXException {
        delegate.clearFolder(fullName, hardDelete);

    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getPath2DefaultFolder(java.lang.String)
     */
    @Override
    public MailFolder[] getPath2DefaultFolder(String fullName) throws OXException {
        return delegate.getPath2DefaultFolder(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getStorageQuota(java.lang.String)
     */
    @Override
    public Quota getStorageQuota(String fullName) throws OXException {
        return delegate.getMessageQuota(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getMessageQuota(java.lang.String)
     */
    @Override
    public Quota getMessageQuota(String fullName) throws OXException {
        return delegate.getMessageQuota(fullName);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getQuotas(java.lang.String, com.openexchange.mail.Quota.Type[])
     */
    @Override
    public Quota[] getQuotas(String fullName, Type[] types) throws OXException {
        return delegate.getQuotas(fullName, types);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getConfirmedHamFolder()
     */
    @Override
    public String getConfirmedHamFolder() throws OXException {
        return delegate.getConfirmedHamFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getConfirmedSpamFolder()
     */
    @Override
    public String getConfirmedSpamFolder() throws OXException {
        return delegate.getConfirmedSpamFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getDraftsFolder()
     */
    @Override
    public String getDraftsFolder() throws OXException {
        return delegate.getDraftsFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSpamFolder()
     */
    @Override
    public String getSpamFolder() throws OXException {
        return delegate.getSpamFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getSentFolder()
     */
    @Override
    public String getSentFolder() throws OXException {
        return delegate.getSentFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#getTrashFolder()
     */
    @Override
    public String getTrashFolder() throws OXException {
        return delegate.getTrashFolder();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorage#releaseResources()
     */
    @Override
    public void releaseResources() throws OXException {
        delegate.releaseResources();
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailStorage#supports(java.lang.Class)
     */
    @Override
    public <T> T supports(Class<T> iface) throws OXException {
        return delegate.supports(iface);
    }

    /* (non-Javadoc)
     * @see com.openexchange.mail.api.IMailFolderStorageDelegator#getDelegateFolderStorage()
     */
    @Override
    public IMailFolderStorage getDelegateFolderStorage() throws OXException {
        return delegate;
    }
}
