/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl.guests;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.api.GuardApis;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.internal.AbstractGuardAccess;
import com.openexchange.guard.internal.GuardApiImpl;
import com.openexchange.guard.internal.authentication.GuardAuthenticationToken;
import com.openexchange.session.Session;

/**
 * {@link GuardGuestMailDeleter} deletes OX Guard guest E-Mails
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuardGuestMailDeleter extends AbstractGuardAccess {

    /**
     * Internal method to create a delete object
     *
     * @param mailIds The IDs of the guest E-Mails to delete
     * @return The created delete object ready to include in the request
     */
    private JSONObject createDelete(String[] mailIds, String folderId) {
        JSONObject jsonObject = new JSONObject();
        if(mailIds != null) {
            jsonObject.putSafe("mailIds", new JSONArray(Arrays.asList(mailIds)));
        }
        if(folderId != null) {
            jsonObject.putSafe("folderId", folderId);
        }
        return jsonObject;
    }

    private void deleteMessagesInternal(Session session, GuardAuthenticationToken authToken, String[] mailIds, String folderId) throws OXException {
        final GuardApiImpl guardGuestApi = requireGuardApi(GUEST_ENDPOINT);
        final JSONObject authJson = GuardGuestAuthHelper.toAuthJSON(authToken, session);
        final JSONObject json = new JSONObject()
            .putSafe("auth", authJson)
            .putSafe("delete", createDelete(mailIds, folderId));
        ByteArrayInputStream jsonStream = new ByteArrayInputStream(json.toString().getBytes(StandardCharsets.UTF_8));

        try (InputStream result = guardGuestApi.processResource(GuardApis.mapFor("action", "deletemessages"),
            jsonStream,
            null,
            "json",
            session)) {
            //no-op
        } catch (IOException e) {
            throw GuardExceptionCodes.UNKNOWN_ERROR.create(e, e.getMessage());
        }

    }

    /**
     * Deletes a set of mail items
     *
     * @param session The session
     * @param authToken The Guard authentication token
     * @param mailIds The IDs of the mail itesm to delete
     * @throws OXException
     */
    public void deleteMessages(Session session, GuardAuthenticationToken authToken, String[] mailIds) throws OXException {
        final String folderId = null; //Not specifying the folderID, because we want to delete a bunch of mails by ID
        deleteMessagesInternal(session, authToken, mailIds, folderId);
    }

    /**
     * Deletes all mail items in a given folder
     *
     * @param session The sesion
     * @param authToken The Guard authentication token
     * @param folderId The ID of the folder to delete all items from
     * @throws OXException
     */
    public void deleteAllMessages(Session session, GuardAuthenticationToken authToken, String folderId) throws OXException {
        final String[] mailIds = null; //Not specifying the mail IDs, because we want to delete all mails from a specific folder
        deleteMessagesInternal(session, authToken, mailIds, folderId);
    }
}
