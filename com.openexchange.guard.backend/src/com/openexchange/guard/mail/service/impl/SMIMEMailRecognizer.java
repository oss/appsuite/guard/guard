/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import javax.mail.Message;
import javax.mail.MessagingException;
import com.openexchange.crypto.CryptoType;
import com.openexchange.crypto.CryptoType.PROTOCOL;
import com.openexchange.exception.OXException;
import com.openexchange.java.Strings;
import com.openexchange.mail.dataobjects.MailMessage;
import com.openexchange.mail.dataobjects.MailPart;
import com.openexchange.mail.mime.ContentType;
import com.openexchange.mail.mime.crypto.CryptoMailRecognizer;

/**
 * {@link SMIMEMailRecognizer}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.6
 */
public class SMIMEMailRecognizer implements CryptoMailRecognizer {

    /**
     * Check if contentType refers to encrypted SMIME content
     *
     * @param contentType The type
     * @return <code>true</code> if SMIME, <code>false</code> otherwise
     */
    private boolean isEncryptedContent(String contentType) {
        if (Strings.isNotEmpty(contentType) && contentType.contains("application/pkcs7-mime") && contentType.replaceAll(" ", "").contains("smime-type=enveloped-data")) {
            return true;
        }
        return false;
    }

    /**
     * Return string representation of content Type with all lowercase and spaces removed
     *
     * @param type ContentType of mail object
     * @return <code>""</code> if type is null, otherwise string
     */
    private String getContentType(ContentType type) {
        if (type == null)
            return "";
        return type.toString().toLowerCase().replaceAll(" ", "").toLowerCase();
    }

    @Override
    public boolean isCryptoMessage(MailMessage message) throws OXException {
        if (message.getEnclosedCount() == MailPart.NO_ENCLOSED_PARTS) {
            return isEncryptedContent(getContentType(message.getContentType()));
        }
        for (int i = 0; i < message.getEnclosedCount(); i++) {
            if (isEncryptedContent(getContentType(message.getEnclosedMailPart(i).getContentType()))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isSignedMessage(MailMessage message) throws OXException {
        for (int i = 0; i < message.getEnclosedCount(); i++) {
            String contentType = getContentType(message.getEnclosedMailPart(i).getContentType());
            if (contentType.contains("pkcs7-signature") || contentType.contains("smime-type=signed-data")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public CryptoType.PROTOCOL getType() {
        return PROTOCOL.SMIME;
    }

    @Override
    public boolean isEncryptedMessage(Message msg) throws OXException {
        try {
            return msg.getContentType() != null && isEncryptedContent(msg.getContentType());
        } catch (MessagingException e) {
            return false;
        }
    }

}
