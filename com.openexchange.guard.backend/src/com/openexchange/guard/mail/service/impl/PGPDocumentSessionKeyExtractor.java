/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mail.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import com.openexchange.exception.OXException;
import com.openexchange.file.storage.Document;
import com.openexchange.file.storage.FileStorageExceptionCodes;
import com.openexchange.guard.file.storage.PGPSessionKeyExtractor;
import com.openexchange.session.Session;

/**
 * {@link PGPDocumentSessionKeyExtractor} uses an OX Guard endpoint to extract "Public-Key Encrypted Session Key Packets" from a given PGP file object.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPDocumentSessionKeyExtractor {

    public String[] getEncryptedSession(Document document, Session session) throws OXException {
        document = Objects.requireNonNull(document, "document must not be null");
        try (InputStream data = document.getData()) {
            return new PGPSessionKeyExtractor().getEncryptedSession(data, session);
        } catch (IOException e) {
            throw FileStorageExceptionCodes.IO_ERROR.create(e, e.getMessage());
        }
    }
}
