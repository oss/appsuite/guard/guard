/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.guard.mail.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardExceptionCodes;
import com.openexchange.guard.mail.service.dataObjects.GuardSecuritySettings;
import com.openexchange.mail.service.SecurityParsingService;

/**
 * Implements parsing service for GuardSecuritySettings
 */
public class SecurityParsingServiceImpl implements SecurityParsingService {
    
    // List of supported JSON parameters in the Security Object
    private static final String[] ALLOWED = new String[] { "encrypt",
        "pgpInline",
        "addAutocrypt",
        "encryptDraft",
        "sign",
        "authToken",
        "authentication",
        "language",
        "message",
        "pin",
        "type",
        "msgRef" };
    private static final ArrayList<String> SUPPORTED = new ArrayList<String>(Arrays.asList(ALLOWED));

    /**
     * Iterates through JSON and confirms all items supported by this parser
     *
     * @param json JSONObject containing security json
     * @throws OXException Throws exception if parameter found not supported
     */
    private void checkSupported(JSONObject json) throws OXException {
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            final String key = keys.next();
            if (!SUPPORTED.contains(key)) {
                throw GuardExceptionCodes.ACTION_NOT_SUPPORTED.create(key);
            }
        }
    }

    @Override
    public GuardSecuritySettings parseJson(JSONObject json) throws OXException {
        if (json == null)
            return null;
        checkSupported(json);
        return GuardSecuritySettings.builder()
            .withEncrypt(json.optBoolean("encrypt", false))
            .withPgpInline(json.optBoolean("pgpInline", false))
            .withAddAutocrypt(json.optBoolean("addAutocrypt", false))
            .withEncryptDraft(json.optBoolean("encryptDraft", false))
            .withSign(json.optBoolean("sign", false))
            .withAuthToken(json.optString("authToken", null))
            .withLanguage(json.optString("language", null))
            .withMessage(json.optString("message", null))
            .withPin(json.optString("pin", null))
            .withType(json.optString("type", null))
            .withMsgRef(json.optString("msgRef", null))
            .build();
    }
}
