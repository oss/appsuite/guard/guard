/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.pgpcore.services.GuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;

/**
 * {@link UserIDGuardKeyRingRetrievalStrategy} is a strategy for retrieving a Guard key ring for a given user id.
 * <p>
 * It retrieves key rings for recipients specified in the form &lt;contextid&gt;&lt;delimiter&gt;&lt;userid&gt; where &lt;delimiter&gt; by default is "/"
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class UserIDGuardKeyRingRetrievalStrategy implements GuardKeyRingRetrievalStrategy {

    private static final String DEFAULT_DELIMITER = "/";
    private final GuardKeyService guardKeyService;
    private final String delimiter;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(UserIDGuardKeyRingRetrievalStrategy.class);

    /**
     * Initializes a new {@link UserIDGuardKeyRingRetrievalStrategy}.
     *
     * @param guardKeyService The {@link GuardKeyService} to use for key retrieval
     */
    public UserIDGuardKeyRingRetrievalStrategy(GuardKeyService guardKeyService) {
        this(guardKeyService, DEFAULT_DELIMITER);
    }

    /**
     * Initializes a new {@link UserIDGuardKeyRingRetrievalStrategy}.
     *
     * @param guardKeyService The {@link GuardKeyService} to use for key retrieval
     * @param delimiter A delimiter used for parsing context- and user-ID from the recipient value.
     */
    public UserIDGuardKeyRingRetrievalStrategy(GuardKeyService guardKeyService, String delimiter) {
        this.guardKeyService = guardKeyService;
        this.delimiter = delimiter;
    }

    /**
     * Internal method to parse context-ID and user-ID from the given recipient
     *
     * @param recipient The recipient
     * @return An array of length two containing the parsed context-ID at index 0 and the parsed user-ID at index 1
     */
    private String[] parseRecipient(String recipient) {
        return recipient.split(delimiter);
    }

    /**
     * Internal method to get the user-ID from the given recipient value
     *
     * @param recipient The recipient to get the user-ID for
     * @return The user-ID for the given recipient
     * @throws OXException if a parsing error occurred
     */
    private int getUserIdFor(String recipient) throws OXException {
        try {
            return Integer.parseInt(parseRecipient(recipient)[1]);
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.RECIPIENT_PARSING_ERROR.create(recipient);
        }
    }

    /**
     * Internal method to get the context-ID from the given recipient value
     *
     * @param recipient The recipient to get the context-ID for
     * @return The context-ID for the given recipient
     * @throws OXException if a parsing error occurred
     */
    private int getContextIdFor(String recipient) throws OXException {
        try {
            return Integer.parseInt(parseRecipient(recipient)[0]);
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.RECIPIENT_PARSING_ERROR.create(recipient);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.commons.RecipientKeyRetrievalStrategy#getPublicKeyForRecipient(java.lang.String)
     */
    @Override
    public GuardKeys getKeyRingForRecipient(String recipient) throws OXException {
        int userId = 0;
        int contextId = 0;

        try {
            if (recipient.contains(delimiter)) {
                userId = getUserIdFor(recipient);
                contextId = getContextIdFor(recipient);
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }

        return guardKeyService.getKeys(userId, contextId);
    }
}
