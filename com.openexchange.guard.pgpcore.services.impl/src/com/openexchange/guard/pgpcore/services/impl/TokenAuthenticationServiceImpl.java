/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.util.Iterator;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.crypto.AuthCryptoType;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.pgpcore.services.GuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.user.OXGuardUser;
import com.openexchange.guard.user.OXUser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.keys.common.PGPSymmetricKey;
import com.openexchange.server.ServiceLookup;

/**
 * {@link TokenAuthenticationServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class TokenAuthenticationServiceImpl implements TokenAuthenticationService {

    private ServiceLookup services;
    private final GuardKeyRingRetrievalStrategy guardKeyRingRetrievalStrategy;
    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationServiceImpl.class);

    /**
     * Initializes a new {@link TokenAuthenticationServiceImpl}.
     *
     * @param guardKeyService
     */
    public TokenAuthenticationServiceImpl(GuardKeyRingRetrievalStrategy guardKeyRingRetrievalStrategy, ServiceLookup services) {
        this.guardKeyRingRetrievalStrategy = guardKeyRingRetrievalStrategy;
        this.services = services;
    }

    /**
     * Decodes a private key from a PGPSecretKey package
     *
     * @param secretKey The package to decode the private key from
     * @param password The password required for extracting the key
     * @return The extracted private key
     * @throws Exception
     */
    private PGPPrivateKey decodePrivateKey(PGPSecretKey secretKey, char[] password) throws Exception {
        PBESecretKeyDecryptor extractor = new BcPBESecretKeyDecryptorBuilder(new BcPGPDigestCalculatorProvider()).build(password);
        return secretKey.extractPrivateKey(extractor);
    }

    /**
     * Verifies the password for a given GuardKeys object
     *
     * @param userKeys The key object
     * @param password The password to verify
     * @return True if the password is correct for the given key object, false otherwise
     */
    private boolean verifyPassword(GuardKeys userKeys, char[] password) {
        //This will verify the password against the "current" key
        //TODO: verify against other keys as well
        Iterator<PGPSecretKey> secretKeyIterator = userKeys.getPGPSecretKeyRing().getSecretKeys();
        while (secretKeyIterator.hasNext()) {
            PGPSecretKey secretKey = secretKeyIterator.next();
            try {
                char[] hashedPassword = CipherUtil.getSHA(new String(password), userKeys.getSalt()).toCharArray();
                PGPPrivateKey decodePrivateKey = decodePrivateKey(secretKey, hashedPassword);
                if (decodePrivateKey != null) {
                    return true;
                }
            } catch (Exception e) {
                logger.error("Error while decoding private key for user {}: {}", userKeys.getUserid(), e.getMessage());
            }
        }
        return false;
    }

    /**
     * Creates a new JSONObject which can be used to create the auth-token
     *
     * @param userIdentity The user's identity
     * @param password The user's password
     * @return A JSON object containing userIdentity and password
     * @throws JSONException
     */
    private JSONObject createPlainToken(String userIdentity, char[] password) throws JSONException {
        JSONObject plainToken = new JSONObject();
        //user identity is more generic allowing to be an email-address, a context-id / user-id combination or something else
        plainToken.put("UserIdentity", userIdentity);
        plainToken.put("Password", new String(password));
        return plainToken;
    }

    /**
     * Creates a new UserIdentity from the given JSON
     *
     * @param json The json to create the UserIdentity from
     * @return The UserIdentity created from the given json, or null if the json does not provide all required information
     * @throws JSONException
     * @throws OXException
     */
    private UserIdentity createUserIdentity(JSONObject json) throws JSONException, OXException {
        UserIdentity ret = null;
        if (json.has("UserIdentity") && json.has("Password")) {
            ret = new UserIdentity(json.getString("UserIdentity"), json.getString("Password"));
        }
        else if(json.has("email") && json.has("encr_password")) {
            ret = new UserIdentity(json.getString("email"), json.getString("encr_password"));
        }
        else if(json.has("email") && json.has("pgp_session_key")) {
            ret = new UserIdentity(json.getString("email"))
                .setPGPSession(PGPSymmetricKey.fromBase64(json.getString("pgp_session_key")));
        }

        if(ret != null && json.has("user_id") && json.has("cid")) {
            final int userId = json.getInt("user_id");
            final int cid = json.getInt("cid");
            ret.setOXGuardUser(new OXGuardUser(userId, cid));
            if (json.has("oxUserId") && json.has("oxCid")) {
                ret.setOXUser(new OXUser(ret.getIdentity(), json.getInt("oxUserId"), json.getInt("oxCid"), null, false, null));
            } else {
                if (cid < 0) {  // Guest user, try lookup
                    ret.setOXUser(services.getServiceSafe(OXUserService.class).getUser(cid, userId));
                } else
                    ret.setOXUser(new OXUser(ret.getIdentity(), userId, cid, null, false, null));
            }
        }
        return ret;
    }

    /**
     * Encrypts the JSON object with the given key
     *
     * @param tokenKey The key to use for encryption
     * @param plainAuthenticationToken The JSON object to encrypt
     * @return The encrypted data
     * @throws OXException
     */
    private String encryptAuthenticationToken(String tokenKey, JSONObject plainAuthenticationToken, String type) throws OXException {
        return AuthCryptoType.getType(type).getValue() + getCipherService().encrypt(plainAuthenticationToken.toString(), tokenKey);
    }

    private String getAuthTokenValue(String string) {
        if (string.contains("!")) {
            if (string.contains("#") && string.indexOf("#") < string.indexOf("!"))
                return string;  // Missing type parameter, old UI
            return string.substring(string.indexOf("!") + 1);
        }
        return string;
    }

    /**
     * Decrypts the given data into a JSON object
     *
     * @param tokenKey The key to use for decryption
     * @param authToken The decrypted data
     * @return The decrypted JSON object, or null if decryption failed
     * @throws OXException
     */
    private JSONObject decryptAuthenticationToken(String tokenKey, String authToken) throws OXException {
        String jsonData = getCipherService().decrypt(getAuthTokenValue(authToken), tokenKey);
        if (jsonData != null) {
            try {
                return new JSONObject(jsonData);
            } catch (JSONException e) {
                logger.error("Error while parsing JSON object: " + e.getMessage());
            }
        }
        logger.error("Unable to decrypt authentication token");
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.TokenAuthenticationService#authenticate(java.lang.String, java.lang.String)
     */
    @Override
    public String createAuthenticationToken(String sessionIdentifier, String userIdentity, char[] password, String type) throws OXException {

        //Get user's keyring
        GuardSessionService guardSessionService = services.getServiceSafe(GuardSessionService.class);
        GuardKeys userKeys = guardKeyRingRetrievalStrategy.getKeyRingForRecipient(userIdentity);
        if (userKeys != null) {
            //Verify password
            boolean accessGranted = verifyPassword(userKeys, password);
            if (accessGranted) {

                //Check for existing token keys for the given identifier
                String tokenKey = guardSessionService.getToken(sessionIdentifier);
                if (tokenKey == null) {
                    //Create a new tokenKey, if we do not already have one
                    tokenKey = guardSessionService.newToken(sessionIdentifier, userKeys.getUserid(), userKeys.getContextid());
                }

                try {
                    //Create and encrypt the authentication token
                    return encryptAuthenticationToken(tokenKey, createPlainToken(userIdentity, password), type);
                } catch (JSONException e) {
                    throw PGPCoreServicesExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
                }
            }
            else {
                logger.error("Unable to verify password for user {}", userIdentity);
            }
        }
        else {
            logger.error("Unable to create authentication token: No PGP key found for user {}", userIdentity);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.TokenAuthenticationService#deleteAuthenticationToken(java.lang.String)
     */
    @Override
    public void deleteAuthenticationToken(String sessionIdentifier) throws OXException {
        services.getServiceSafe(GuardSessionService.class).deleteToken(sessionIdentifier);
    }

    /**
     * Internal method to get an appropriated cipher service
     *
     * @return a cipher service
     * @throws OXException
     */
    private GuardCipherService getCipherService() throws OXException {
        GuardCipherFactoryService cipherFactory = services.getServiceSafe(GuardCipherFactoryService.class);
        return cipherFactory.getCipherService(GuardCipherAlgorithm.AES_GCM);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.TokenAuthenticationService#authenticate(java.lang.String, java.lang.String)
     */
    @Override
    public UserIdentity decryptUserIdentity(String sessionIdentifier, String authToken) throws OXException {
        String tokenKey = services.getServiceSafe(GuardSessionService.class).getToken(sessionIdentifier);
        if (tokenKey != null) {
            try {
                JSONObject jsonUserIdentity = decryptAuthenticationToken(tokenKey, authToken);
                if (jsonUserIdentity != null) {
                    return createUserIdentity(jsonUserIdentity);
                }
            } catch (JSONException e) {
                logger.error("Error while parsing user identity JSON object: " + e.getMessage());
            }
        }
        else {
            logger.error("Unknown session identifier while decrypting user identity");
        }
        return null;
    }
}
