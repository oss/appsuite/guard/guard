
package com.openexchange.guard.pgpcore.services.impl.osgi;

import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.hkpclient.services.HKPClientService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.pgpcore.services.GuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.PGPKeySigningService;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.pgpcore.services.PGPPacketService;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.pgpcore.services.impl.CompositeGuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.impl.DBKeyRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.impl.EmailGuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.impl.PGPCryptoServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.PGPKeySigningServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.PGPMimeCryptoServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.PGPMimeSigningServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.PGPPacketServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.PGPSigningServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.TokenAuthenticationServiceImpl;
import com.openexchange.guard.pgpcore.services.impl.UserIDGuardKeyRingRetrievalStrategy;
import com.openexchange.guard.session.GuardSessionService;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;

public class PGPCoreServicesImplActivator extends HousekeepingActivator {

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardCipherFactoryService.class, GuardKeyService.class,
            GuardSessionService.class, OXUserService.class, OGPGPKeysStorage.class,
            AutoCryptStorageService.class, GuardConfigurationService.class, HKPClientService.class, CryptoManager.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(PGPCoreServicesImplActivator.class).info("Starting bundle: {}", context.getBundle().getSymbolicName());

        //Key retrieval strategies
        GuardKeyRingRetrievalStrategy guardKeyRingRetrievalStrategy = new CompositeGuardKeyRingRetrievalStrategy(new EmailGuardKeyRingRetrievalStrategy(getService(GuardKeyService.class)), new UserIDGuardKeyRingRetrievalStrategy(getService(GuardKeyService.class)));
        PGPKeyRetrievalStrategy pgpKeyRetrievalStrategy = new DBKeyRetrievalStrategy(getService(GuardKeyService.class));

        //Register the services
        PGPCryptoServiceImpl pgpCryptoService = new PGPCryptoServiceImpl(guardKeyRingRetrievalStrategy,
                                                                         getService(GuardKeyService.class),
                                                                         getService(OGPGPKeysStorage.class),
                                                                         getService(AutoCryptStorageService.class));
        registerService(TokenAuthenticationService.class,
            new TokenAuthenticationServiceImpl(guardKeyRingRetrievalStrategy, this));
        registerService(PGPCryptoService.class, pgpCryptoService);
        registerService(PGPMimeCryptoService.class, new PGPMimeCryptoServiceImpl(pgpCryptoService));
        // Register Crypto Service
        registerService(MimeSignatureVerificationService.class, new PGPMimeSigningServiceImpl(
            getService(GuardKeyService.class),
            getService(OGPGPKeysStorage.class),
            getService(AutoCryptStorageService.class),
            getService(GuardConfigurationService.class),
            getService(HKPClientService.class)));
        registerService(PGPSigningService.class, new PGPSigningServiceImpl(guardKeyRingRetrievalStrategy, pgpKeyRetrievalStrategy));
        registerService(PGPPacketService.class, new PGPPacketServiceImpl());
        registerService(PGPKeySigningService.class, new PGPKeySigningServiceImpl(
            getService(GuardKeyService.class),
            getService(OGPGPKeysStorage.class),
            getService(AutoCryptStorageService.class)));
        trackService(GuardCipherFactoryService.class);
        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        org.slf4j.LoggerFactory.getLogger(PGPCoreServicesImplActivator.class).info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(TokenAuthenticationService.class);
        unregisterService(PGPCryptoService.class);
        unregisterService(PGPSigningService.class);
        unregisterService(PGPPacketService.class);
        unregisterService(PGPKeySigningService.class);
        super.stopBundle();
    }
}
