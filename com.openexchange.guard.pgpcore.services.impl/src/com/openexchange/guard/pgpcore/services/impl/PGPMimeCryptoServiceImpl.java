/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.pgpcore.services.PGPMimeDecryptionResult;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPDecryptionResult;

/**
 * {@link PGPMimeCryptoServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPMimeCryptoServiceImpl implements PGPMimeCryptoService {

    private final PGPCryptoService pgpCryptoService;

    /**
     * Initializes a new {@link PGPMimeCryptoServiceImpl}.
     *
     * @param pgpCryptoService The underlaying {@link PGPCryptoService} to use for actual encryption/decryption
     */
    public PGPMimeCryptoServiceImpl(PGPCryptoService pgpCryptoService) {
        this.pgpCryptoService = Objects.requireNonNull(pgpCryptoService, "pgpCryptoService must not be null");
    }

    /**
     * Internal method to return the encrypted body of a given message.
     *
     * @param multipartMessage The message to get the encrypted body from.
     * @return The body part containing the encrypted message.
     * @throws MessagingException
     */
    private BodyPart getPGPContentBody(Multipart multipartMessage) throws MessagingException {
        for (int i = 0; i < multipartMessage.getCount(); i++) {
            BodyPart part = multipartMessage.getBodyPart(i);
            if (part.getContentType().contains("application/octet-stream")) {
                return part;
            }
        }
        return null;
    }

    /**
     * Internal method to create a MimeMessage from the given OutputStream
     *
     * @param mimeOutputStream The output stream
     * @return A mime message
     * @throws MessagingException
     */
    private MimeMessage createMimeMessage(ByteArrayOutputStream mimeOutputStream) throws MessagingException {
        return new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(mimeOutputStream.toByteArray()));
    }

    /**
     * Internal method to create a result object
     *
     * @param mimeOutputStream The decrypted PGP/MIME data
     * @param pgpResult verification results
     * @return The created result object
     * @throws MessagingException
     */
    private PGPMimeDecryptionResult createResult(ByteArrayOutputStream mimeOutputStream, PGPDecryptionResult pgpResult) throws MessagingException {
        return new PGPMimeDecryptionResult(createMimeMessage(mimeOutputStream), pgpResult.getSignatureVerificationResults(), pgpResult.getMDCVerificationResult());
    }

    /**
     * Internal method to create an InputStream to the PGP content of the given MimeMessage
     *
     * @param mimeMessage The MimeMessage
     * @return The InputStream to the PGP content of the given MimeMessage
     * @throws OXException
     */
    private InputStream getPGPInputStreamFrom(MimeMessage mimeMessage) throws OXException {
        try {
            Object content = mimeMessage.getContent();
            if (content instanceof MimeMultipart) {
                BodyPart pgpContentBody = getPGPContentBody((Multipart) content);

                if (pgpContentBody != null) {
                    return pgpContentBody.getInputStream();
                } else {
                    throw PGPCoreServicesExceptionCodes.NOT_A_MULTIPART_MSG_ERROR.create();
                }
            } else {
                throw PGPCoreServicesExceptionCodes.NOT_A_MULTIPART_MSG_ERROR.create();
            }
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPMimeCryptoService#decrypt(com.openexchange.guard.pgpcore.services.MimeMessage, com.openexchange.guard.user.UserIdentity)
     */ @Override
    public PGPMimeDecryptionResult decrypt(MimeMessage mimeMessage, UserIdentity user) throws OXException {

        mimeMessage = Objects.requireNonNull(mimeMessage, "mimeMessage must not be null");
        user = Objects.requireNonNull(user, "user must not be null");

        try (InputStream pgpContentStream = getPGPInputStreamFrom(mimeMessage);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
            //Decrypt
            return createResult(outputStream, pgpCryptoService.decrypt(pgpContentStream, outputStream, user));
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPMimeCryptoService#decrypt(javax.mail.internet.MimeMessage, com.openexchange.guard.keymanagement.commons.GuardKeys, java.lang.String)
     */
    @Override
    public PGPMimeDecryptionResult decrypt(MimeMessage mimeMessage, GuardKeys key, String password) throws OXException {
        mimeMessage = Objects.requireNonNull(mimeMessage, "mimeMessage must not be null");
        key = Objects.requireNonNull(key, "key must not be null");
        password = Objects.requireNonNull(password, "password must not be null");

        try (InputStream pgpContentStream = getPGPInputStreamFrom(mimeMessage);
             ByteArrayOutputStream outputStream = new ByteArrayOutputStream();) {
            //Decrypt
            return createResult(outputStream, pgpCryptoService.decrypt(pgpContentStream, outputStream, key, password));
        } catch (IOException e) {
            throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }
}
