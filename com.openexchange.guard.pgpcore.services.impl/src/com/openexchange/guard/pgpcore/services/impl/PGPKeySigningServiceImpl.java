/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPSignature;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.database.AutoCryptStorageService;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.storage.OGPGPKeysStorage;
import com.openexchange.guard.pgpcore.services.PGPKeySigningService;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.core.PGPKeySignatureVerifier;
import com.openexchange.pgp.core.PGPSignatureVerificationResult;

/**
 * {@link PGPKeySigningServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class PGPKeySigningServiceImpl implements PGPKeySigningService {

    private final GuardKeyService guardKeyService;
    private final OGPGPKeysStorage ogPGPKeysStorage;
    private final AutoCryptStorageService autocryptStorageService;

    /**
     * Initializes a new {@link PGPKeySigningServiceImpl}.
     *
     * @param guardKeyService
     * @param ogPGPKeysStorage
     * @param autocryptStorageService
     */
    public PGPKeySigningServiceImpl(GuardKeyService guardKeyService, OGPGPKeysStorage ogPGPKeysStorage,
        AutoCryptStorageService autocryptStorageService) {
        this.guardKeyService = guardKeyService;
        this.ogPGPKeysStorage = ogPGPKeysStorage;
        this.autocryptStorageService = autocryptStorageService;
    }

    private PGPKeyRetrievalStrategy getPGPKeyRetrievalStrategyFor(UserIdentity verifier) throws OXException {
        return new DefaultPGPKeyRetrievalStrategy(guardKeyService,
            ogPGPKeysStorage,
            autocryptStorageService,
            verifier.getOXUser().getId(),
            verifier.getOXUser().getContextId());
    }

    /**
     * Internal method to verify a signature
     *
     * @param signature The signature to verify.
     * @param keyRetrievalStrategy The strategy to retrieval the public key for signature verification
     * @param publicKey The public key containing the given signature
     * @return The verification result
     * @throws Exception
     */
    private PGPSignatureVerificationResult verifySignature(PGPSignature signature, PGPKeyRetrievalStrategy keyRetrievalStrategy, PGPPublicKey publicKey) throws Exception {
        return new PGPKeySignatureVerifier(keyRetrievalStrategy).verifySignatures(signature, publicKey);
    }

    /**
     * Internal method to verify all signatures contained in the given public key.
     *
     * @param keyRetrievalStrategy The strategy to retrieval the public key for signature verification
     * @param publicKey The public key containing the signatures to verify.
     * @return The verification result
     * @throws Exception
     */
    private Collection<PGPSignatureVerificationResult> verifySignaturesFor(PGPKeyRetrievalStrategy keyRetrievalStrategy, PGPPublicKey publicKey) throws Exception {
        ArrayList<PGPSignatureVerificationResult> ret = new ArrayList<>();
        @SuppressWarnings("unchecked") Iterator<PGPSignature> signatures = publicKey.getSignatures();
        while (signatures.hasNext()) {
            ret.add(verifySignature(signatures.next(), keyRetrievalStrategy, publicKey));
        }
        return ret;
    }

    /**
     * Internal method to verify all signatures within the given public keys
     *
     * @param keyRetrievalStrategy The strategy to retrieval the public key for signature verification
     * @param publicKeys The public keys containing the signatures to verify.
     * @return The verification results
     * @throws Exception
     */
    private Collection<PGPSignatureVerificationResult> verifySignaturesFor(PGPKeyRetrievalStrategy keyRetrievalStrategy, Iterator<PGPPublicKey> publicKeys) throws Exception {
        ArrayList<PGPSignatureVerificationResult> ret = new ArrayList<>();
        while (publicKeys.hasNext()) {
            ret.addAll(verifySignaturesFor(keyRetrievalStrategy, publicKeys.next()));
        }
        return ret;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPKeySigningService#verifyKey(com.openexchange.guard.user.UserIdentity, com.openexchange.guard.keymanagement.commons.GuardKeys)
     */
    @Override
    public Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, GuardKeys keys) throws OXException {
        keys = Objects.requireNonNull(keys, "keys must not be null");
        return verifyKey(verifier, keys.getPGPPublicKeyRing());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPKeySigningService#verifyKey(com.openexchange.guard.user.UserIdentity, org.bouncycastle.openpgp.PGPPublicKeyRing)
     */
    @Override
    public Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, PGPPublicKeyRing keyRing) throws OXException {
        keyRing = Objects.requireNonNull(keyRing, "keyRing must not be null");
        try {
            return verifySignaturesFor(getPGPKeyRetrievalStrategyFor(verifier), keyRing.getPublicKeys());
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.SIGNATURE_VERIFICATION_ERROR.create(e);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPKeySigningService#verifyKey(com.openexchange.guard.user.UserIdentity, org.bouncycastle.openpgp.PGPPublicKey)
     */
    @Override
    public Collection<PGPSignatureVerificationResult> verifyKey(UserIdentity verifier, PGPPublicKey key) throws OXException {
        key = Objects.requireNonNull(key, "key must not be null");
        try {
            return verifySignaturesFor(getPGPKeyRetrievalStrategyFor(verifier), key);
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.SIGNATURE_VERIFICATION_ERROR.create(e.getLocalizedMessage());
        }
    }
}
