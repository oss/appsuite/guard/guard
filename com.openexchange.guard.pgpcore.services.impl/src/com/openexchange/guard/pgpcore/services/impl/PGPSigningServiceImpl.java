/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.pgpcore.services.impl;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.bouncycastle.openpgp.PGPSecretKey;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.pgpcore.services.GuardKeyRingRetrievalStrategy;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.guard.pgpcore.services.exceptions.PGPCoreServicesExceptionCodes;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.PGPKeyRetrievalStrategy;
import com.openexchange.pgp.core.PGPSignatureCreator;
import com.openexchange.pgp.core.PGPSignatureVerifier;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link PGPSigningServiceImpl}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class PGPSigningServiceImpl implements PGPSigningService {

    private final PGPKeyRetrievalStrategy keyRetrievalStrategy;
    private final GuardKeyRingRetrievalStrategy recipientKeyRingRetrievalStrategy;

    /**
     * Initializes a new {@link PGPSigningServiceImpl}.
     *
     * @param guardKeyService
     */
    public PGPSigningServiceImpl(GuardKeyRingRetrievalStrategy recipientKeyRingRetrievalStrategy, PGPKeyRetrievalStrategy keyRetrievalStrategy) {
        this.recipientKeyRingRetrievalStrategy = recipientKeyRingRetrievalStrategy;
        this.keyRetrievalStrategy = keyRetrievalStrategy;
    }

    /**
     * Fetches the signing key for the given identity
     *
     * @param identity The identity to get the signing key for
     * @return The signing key for the given identity
     * @throws OXException If the key was not found
     */
    GuardKeys getSignerGuardKey(String identity) throws OXException {
        GuardKeys guardKeys  = recipientKeyRingRetrievalStrategy.getKeyRingForRecipient(identity);
        if (guardKeys != null) {
            return guardKeys;
        }
        else {
            throw PGPCoreServicesExceptionCodes.SIGNING_KEY_NOT_FOUND.create(identity);
        }
    }

    /**
     * Extracts the PGP signing key from the given Guard key
     *
     * @param guardKey The key to get the singing key for
     * @return The signing key for the given Guard key
     * @throws OXException If no signing key was found
     */
    PGPSecretKey getSignerKey(GuardKeys guardKey) throws OXException {
        PGPSecretKey signingKey = PGPKeysUtil.getSigningKey(guardKey.getPGPSecretKeyRing());
        if (signingKey != null) {
            return signingKey;
        }
        else {
            throw PGPCoreServicesExceptionCodes.SIGNING_KEY_NOT_FOUND.create(guardKey.getKeyid());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPSigningService#sign(java.io.InputStream, java.io.OutputStream, boolean, org.bouncycastle.openpgp.PGPSecretKey, char[])
     */
    @Override
    public void sign(InputStream input, OutputStream output, boolean armored, UserIdentity signer) throws OXException {
        GuardKeys signerGuardKey = getSignerGuardKey(signer.getIdentity());
        sign(input, output, armored, signerGuardKey, signer.getPassword());
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.pgpcore.services.PGPSigningService#sign(java.io.InputStream, java.io.OutputStream, boolean, com.openexchange.guard.keymanagement.commons.GuardKeys, char[])
     */
    @Override
    public void sign(InputStream input, OutputStream output, boolean armored, GuardKeys signerKey, char[] password) throws OXException {
        try {
            PGPSecretKey signingKey = getSignerKey(signerKey);
            char[] hashedPassword = CipherUtil.getSHA(new String(password), signerKey.getSalt()).toCharArray();
            new PGPSignatureCreator().createSignature(input, output, armored, signingKey, hashedPassword);
        } catch (OXException e) {
            throw e;
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.SIGNING_ERROR.create(e, e.getMessage());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.pgpcore.services.PGPSigningService#verify(java.io.InputStream, java.io.InputStream)
     */
    @Override
    public List<SignatureVerificationResult> verify(InputStream signedData, InputStream signatureData) throws OXException {
        try {
            return new PGPSignatureVerifier(keyRetrievalStrategy).verifySignatures(signedData, signatureData);
        } catch (Exception e) {
            throw PGPCoreServicesExceptionCodes.SIGNATURE_VERIFICATION_ERROR.create(e, e.getMessage());
        }
    }
}
