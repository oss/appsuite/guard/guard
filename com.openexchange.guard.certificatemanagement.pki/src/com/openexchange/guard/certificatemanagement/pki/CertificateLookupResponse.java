/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki;

import java.security.cert.X509Certificate;

/**
 * The response of a certificate lookup. Contains the certificate and
 * the source where the certificate was found.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateLookupResponse {

    private final X509Certificate certificate;
    private final String sourceName;

    /**
     * Initializes a new {@link CertificateLookupResponse}.
     *
     * @param certificate The X509Certificate
     * @param sourceName Name of the service that found the certificate
     */
    public CertificateLookupResponse(X509Certificate certificate, String sourceName) {
        super();
        this.certificate = certificate;
        this.sourceName = sourceName;
    }

    /**
     * Gets the certificate
     *
     * @return The certificate
     */
    public X509Certificate getCertificate() {
        return certificate;
    }

    /**
     * Gets the sourceName
     *
     * @return The sourceName
     */
    public String getSourceName() {
        return sourceName;
    }

}
