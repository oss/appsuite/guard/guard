/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.certificatemanagement.pki.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class PKIExceptionMessages implements LocalizableStrings {

    // Error with the configuration or setup of the public key infrastructure server
    public static final String SETUP_ERROR = "Error with setup or configuration of public key server";

    // Error occurred when communicating with the public key infrastructure server
    public static final String API_ERROR = "Error communicating with the public key server";

    // There was an error processing the response
    public static final String RESPONSE_ERROR = "Error processing response";

    // There was a problem when trying to process the returned certificate
    public static final String ERROR_CERTIFICATE = "There was an error processing the certificate";

    // There was a problem with the request for a new signed certificate.
    public static final String ERROR_CSR = "There was an error with the certificate signing request";

    // The service isn't available for the user
    public static final String SERVICE_NOT_AVAILABLE = "The required service %1$s is not available for this user";

    // User requested new certificate.  This was rejected by the certificate service.  Contact support for help.
    public static final String CERTIFICATE_REQUEST_REJECTED = "The request for a new certificate was rejected. Contact support for a resolution.";

    /**
     * Prevent instantiation.
     */
    private PKIExceptionMessages() {}

}
