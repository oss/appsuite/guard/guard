/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public enum PKIExceptionCodes implements DisplayableOXExceptionCode {

    /**
     * Error with setting up or how the EJBCA server is configured
     */
    SETUP_ERROR("Error with setup or configuration of PKI server: %1$s", PKIExceptionMessages.SETUP_ERROR, CATEGORY_ERROR, 1),
    API_ERROR("Problem communicating with the PKI Server: %1$s", PKIExceptionMessages.API_ERROR, CATEGORY_ERROR, 2),
    RESPONSE_ERROR("Problem processing response from PKI Server: %1$s", PKIExceptionMessages.RESPONSE_ERROR, CATEGORY_ERROR, 3),
    ERROR_CERTIFICATE("There was a problem processing the certificate: %1$s", PKIExceptionMessages.ERROR_CERTIFICATE, CATEGORY_ERROR, 4),
    ERROR_CSR("There was a problem with the csr request: %1$s", PKIExceptionMessages.ERROR_CSR, CATEGORY_ERROR, 5),
    SERVICE_NOT_AVAILABLE("The required service %1$s is not available for this user", PKIExceptionMessages.SERVICE_NOT_AVAILABLE, CATEGORY_ERROR, 6),
    CERTIFICATE_REQUEST_REJECTED("The request for a new certificate was rejected.", PKIExceptionMessages.CERTIFICATE_REQUEST_REJECTED, CATEGORY_ERROR, 7),
    ;

    private String message;
    private String displayMessage;
    private Category category;
    private int number;

    private PKIExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private PKIExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "GRD-CRYPTO";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }

}
