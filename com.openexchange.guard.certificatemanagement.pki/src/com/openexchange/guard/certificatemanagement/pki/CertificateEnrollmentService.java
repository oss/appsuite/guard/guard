/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.pki;

import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus;

/**
 * {@link CertificateEnrollmentService} Creates new certificate for users signed by PKI
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public interface CertificateEnrollmentService {

    /**
     * Gets any custom messaging for creating new certificates. Defaults to null
     * Could, for example, specify that the certificates are only valid within the company/context.
     *
     * @param userId
     * @param contextId
     * @return Custom message before user creates certificates. Should be translated
     * @throws OXException
     */
    public String getCustomCreateMessage(int userId, int contextId) throws OXException;

    /**
     * Begins the certificate enrollment for a user. This will attempts to move through all of the enrollment steps
     *
     * @param userId
     * @param contextId
     * @param password Password to use for key
     * @return
     * @throws OXException
     */
    public CertificateEnrollmentStatus createKeys(int userId, int contextId, char[] password) throws OXException;

    /**
     * Begin a new certificate enrollment for a user
     *
     * @param userId
     * @param contextId
     * @param password Password to use for key
     * @return new SmimeKeys if certificate obtained for the user. null if not complete. Error if unable
     */
    public CertificateEnrollmentStatus beginNewCertificateEnrollment(int userId, int contextId, char[] password) throws OXException;

    /**
     * If certificate enrollment wasn't completed (pending approval, etc), calling this procedure will attempt the next step
     * if able. Would be initiated after user received notice of approval, etc.
     *
     * @param userId
     * @param contextId
     * @param currentStatus The status from the BeginNewCertificateEnrollment
     * @return new SmimeKeys if certificate obtained for the user. null if not complete. Error if unable
     */
    public CertificateEnrollmentStatus continueCertificateEnrollment(int userId, int contextId, CertificateEnrollmentStatus currentStatus) throws OXException;

}
