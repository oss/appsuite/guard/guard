/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.pki;

import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PKIServiceHandler} Handles the PKI calls for a certain PKI service. May be multiple.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public abstract class PKIServiceHandler {

    public PKIServiceHandler(ServiceLookup services, CertificateLookupService certificateLookupService, CertificateEnrollmentService certificateEnrollmentService) {
        this.certificateLookupService = certificateLookupService;
        this.certificateEnrollmentService = certificateEnrollmentService;
        this.services = services;
    }

    protected CertificateLookupService certificateLookupService;
    protected CertificateEnrollmentService certificateEnrollmentService;
    protected ServiceLookup services;

    /**
     * Internal method to obtain the Guard configuration service
     *
     * @return
     */
    protected GuardConfigurationService getConfigurationService() {
        return services.getService(GuardConfigurationService.class);
    }

    /**
     * Returns true if this PKI service is assigned to this user for management of their certificates
     *
     * @param userId User ID of the user
     * @param cid Context Id of the user
     * @return True if this service should be used to handle this users certificates
     */
    public abstract boolean handlesThisUser(int userId, int cid);

    /**
     * Returns true if this PKI service is available for certificate lookup.
     *
     * @param userId User ID of the user
     * @param cid Context Id of the user
     * @return True if the service is available for lookup
     */
    public abstract boolean enabledForLookup(int userId, int cid);

    /**
     * Gets the certificate enrollment service for this PKI service
     *
     * @return
     */
    //public CertificateEnrollmentService getCertificateEnrollmentService() {
    //     return this.certificateEnrollmentService;
    //}

    /**
     * Gets the certificate lookup service for this PKI service
     *
     * @return
     */
    public CertificateLookupService getCertificateLookupService() {
        return this.certificateLookupService;
    }

    /**
     * Gets the certificate enrollment service for this PKI Service
     *
     * @return
     */
    public CertificateEnrollmentService getCertificateEnrollmentService() {
        return this.certificateEnrollmentService;
    }

}
