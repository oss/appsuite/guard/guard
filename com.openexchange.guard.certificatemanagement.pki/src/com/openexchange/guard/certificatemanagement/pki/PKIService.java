/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.pki;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;

/**
 * {@link PKIService} Used to retrieve and manage user certificates
 * Accesses registry of registered PKI implementations, calling implementations if any registered
 * for the user
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class PKIService {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(PKIService.class);
    }

    private List<PKIServiceHandler> services;

    public PKIService() {
        services = Collections.synchronizedList(new ArrayList<PKIServiceHandler>());
    }

    /**
     * Gets a certificate for given email address. Result may be specific to the user, as some users
     * may have access to certin PKI services, while others not. In addition, users may trust some
     * Certificate Authorities, while others do not.
     *
     * @param email Email address to check
     * @param userId UserId of user doing check
     * @param cid ContextId of the user
     * @return Certificate if any
     * @throws OXException
     */
    public CertificateLookupResponse getCertificate(String email, int userId, int cid) throws OXException {
        List<PKIServiceHandler> services = getLookupServices(userId, cid);
        if (services == null || services.isEmpty()) {
            return null;
        }
        CertificateLookupResponse cert;
        for (PKIServiceHandler handler : services) {
            try {
                cert = handler.getCertificateLookupService().getCertificate(email, userId, cid);
                if (cert != null) {
                    return cert;
                }
            } catch (OXException x) {
                // Show error then continue search if any other handlers exist
                LoggerHolder.LOGGER.error("Error checking PKI Service for certificate", x);
            }
        }
        return null;
    }

    /**
     * Returns true if the user has a certificate enrollment service and it is enabled
     *
     * @param userId
     * @param cid
     * @return True if the user has the capability to create keys
     */
    public boolean canCreateKeys(int userId, int cid) {
        PKIServiceHandler service = getManagingService(userId, cid);
        if (service == null || service.getCertificateEnrollmentService() == null)
            return false;
        return true;
    }

    /**
     * Gets any custom message to display to the user before creating keys. May specify
     * where the keys will be trusted (due to Certificate Authority specifics), key requirements, etc
     *
     * @param userId
     * @param cid
     * @return Custom string if any
     * @throws OXException
     */
    public String getCustomCreateMessage(int userId, int cid) throws OXException {
        PKIServiceHandler service = getManagingService(userId, cid);
        if (service == null || service.getCertificateEnrollmentService() == null)
            return null;
        return service.getCertificateEnrollmentService().getCustomCreateMessage(userId, cid);
    }

    /**
     * Create a new certificate for the suer
     *
     * @param userId
     * @param cid
     * @param password Password to use to secure the key
     * @return New certificate and/or status
     * @throws OXException
     */
    public CertificateEnrollmentStatus createKeys(int userId, int cid, char[] password) throws OXException {
        PKIServiceHandler service = getManagingService(userId, cid);
        if (service == null || service.getCertificateEnrollmentService() == null)
            throw PKIExceptionCodes.SERVICE_NOT_AVAILABLE.create("Certificate Enrollment Service");
        return service.getCertificateEnrollmentService().createKeys(userId, cid, password);
    }

    // Service Registry

    /**
     * Add an available service
     *
     * @param service
     */
    public void addService(PKIServiceHandler service) {
        this.services.add(service);
    }

    /**
     * Remove a service
     *
     * @param service
     */
    public void removeService(PKIServiceHandler service) {
        this.services.remove(service);
    }

    /**
     * Get the PKI Service that manages this users certificates
     *
     * @param userId
     * @param cid
     * @return PKI service managing this users certificates, null if none available
     */
    private PKIServiceHandler getManagingService(int userId, int cid) {
        PKIServiceHandler result = null;
        for (PKIServiceHandler service : services) {
            if (service.handlesThisUser(userId, cid)) {
                result = service;
                break;
            }
        }
        return result;
    }

    /**
     * Gets an array of available PKI Services that can be used for lookup of certificates
     *
     * @param userId
     * @param cid
     * @return A list of PKI Services that can be used for certificate lookup
     */
    private List<PKIServiceHandler> getLookupServices(int userId, int cid) {
        if (services.size() == 0)
            return Collections.emptyList();
        ArrayList<PKIServiceHandler> result = new ArrayList<PKIServiceHandler>(services.size());
        for (PKIServiceHandler service : services) {
            if (service.enabledForLookup(userId, cid)) {
                result.add(service);
            }
        }
        return result;
    }

    /**
     * Get JsonObject containing PKI capabilities and settings to return with login data
     *
     * @param userId
     * @param cid
     * @return JsonObject containing the PKI capabilities and settings
     * @throws OXException
     */
    public JsonObject getPkiLoginJson(int userId, int cid) throws OXException {
        JsonObject pki = new JsonObject();
        pki.addProperty("canCreate", canCreateKeys(userId, cid));
        final String message = getCustomCreateMessage(userId, cid);
        if (message != null && !message.isEmpty()) {
            pki.addProperty("message", message);
        }
        return pki;
    }

}
