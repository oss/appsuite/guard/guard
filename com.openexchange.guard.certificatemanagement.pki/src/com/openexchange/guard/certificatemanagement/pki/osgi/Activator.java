/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.osgi;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.certificatemanagement.RecipCertificateLookupStrategy;
import com.openexchange.guard.certificatemanagement.pki.PKIService;
import com.openexchange.guard.certificatemanagement.pki.PKIServiceHandler;
import com.openexchange.guard.certificatemanagement.pki.impl.PKIRecipLookupStrategy;
import com.openexchange.osgi.HousekeepingActivator;

/**
 *
 * {@link Activator}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class Activator extends HousekeepingActivator {

    @Override
    protected Class<?>[] getNeededServices() {
        return null;
    }

    @Override
    protected void startBundle() throws Exception {
        final BundleContext context = super.context;
        Logger logger = LoggerFactory.getLogger(Activator.class);
        logger.info("Starting PKI bundle");
        final PKIService pkiService = new PKIService();
        registerService(PKIService.class, pkiService);

        final ServiceTrackerCustomizer<PKIServiceHandler, PKIServiceHandler> pkiTracker = new ServiceTrackerCustomizer<PKIServiceHandler, PKIServiceHandler>() {

            @Override
            public PKIServiceHandler addingService(ServiceReference<PKIServiceHandler> reference) {
                PKIServiceHandler service = context.getService(reference);
                pkiService.addService(service);
                return service;
            }

            @Override
            public void modifiedService(ServiceReference<PKIServiceHandler> reference, PKIServiceHandler service) {}

            @Override
            public void removedService(ServiceReference<PKIServiceHandler> reference, PKIServiceHandler service) {
                PKIServiceHandler serviceToRemove = context.getService(reference);
                pkiService.removeService(serviceToRemove);
            }
        };

        track(PKIServiceHandler.class, pkiTracker);
        registerService(RecipCertificateLookupStrategy.class, new PKIRecipLookupStrategy(this));
        openTrackers();
    }

}
