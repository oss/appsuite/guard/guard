/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.pki.exceptions.PKIExceptionCodes;

public class CertificateUtil {

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(CertificateUtil.class);
    }

    /**
     * Certificate factory expects PEM boundaries. Method adds boundary around base64 decoded data
     *
     * @param data
     * @parm decode If data is base64 encoded
     * @return Wrapped decoded data
     */
    public static String toPem(String data, boolean decode) {
        StringBuilder sb = new StringBuilder();
        sb.append("-----BEGIN CERTIFICATE-----\r\n");
        sb.append((decode ? new String(Base64.decode(data)) : data));
        sb.append("\r\n-----END CERTIFICATE-----");
        return sb.toString();
    }

    /**
     * Get a certificate from data
     *
     * @param data DER data
     * @parm decode If data is base64 encoded
     * @return X509Certificate
     * @throws OXException
     */
    public static X509Certificate getCertificate(String data, boolean decode) throws OXException {
        try (InputStream inputStream = new ByteArrayInputStream(toPem(data, decode).getBytes())) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
            return (X509Certificate) cf.generateCertificate(inputStream);
        } catch (CertificateException | IOException | NoSuchProviderException e) {
            LoggerHolder.LOGGER.error("Error checking X509 Certificate", e);
            throw PKIExceptionCodes.ERROR_CERTIFICATE.create(e.getMessage());
        }
    }

}
