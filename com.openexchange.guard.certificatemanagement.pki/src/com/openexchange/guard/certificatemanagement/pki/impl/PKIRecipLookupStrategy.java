/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.pki.impl;

import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.RecipCertificateLookupStrategy;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.pki.CertificateLookupResponse;
import com.openexchange.guard.certificatemanagement.pki.PKIService;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.server.ServiceLookup;

/**
 * Implements lookup of recipient certificates using PKI service
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class PKIRecipLookupStrategy implements RecipCertificateLookupStrategy {

    private ServiceLookup services;

    public PKIRecipLookupStrategy(ServiceLookup services) {
        super();
        this.services = services;
    }

    @Override
    public RecipCertificate lookup(int userId, int contextId, String email, int timeout) throws OXException {
        if (timeout < 0)
            return null;
        PKIService pkiService = services.getServiceSafe(PKIService.class);
        CertificateLookupResponse cert = pkiService.getCertificate(email, userId, contextId);
        if (cert != null) {
            SmimeKeys key = new SmimeKeys(cert.getCertificate(), null, false, 0, 0, email, 0L);
            return new RecipCertificate(key, new KeySource(cert.getSourceName(), 5));
        }
        return null;
    }

}
