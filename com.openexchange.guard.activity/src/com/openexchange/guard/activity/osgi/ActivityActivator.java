/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 */
package com.openexchange.guard.activity.osgi;

import com.openexchange.guard.activity.ActivityTrackingService;
import com.openexchange.guard.activity.impl.ActivityTrackingServiceImpl;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.osgi.HousekeepingActivator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActivityActivator extends HousekeepingActivator {

    /**
     * Initialises a new {@link ActivityActivator}.
     */
    public ActivityActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardDatabaseService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(ActivityActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);
        registerService (ActivityTrackingService.class, new ActivityTrackingServiceImpl(getService(GuardDatabaseService.class)));



    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(ActivityActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        unregisterService(ActivityTrackingService.class);
        Services.setServiceLookup(null);
        super.stopBundle();

    }
}

