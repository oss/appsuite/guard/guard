/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.antiabuse.bad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.osgi.Services;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link GuardAntiAbuseServiceImpl} is a REST DB implementation for a BadStorage
 */
public class GuardBadStorageImpl implements GuardBadStorage {

    @Override
    public Bad getBad(String id) throws OXException {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("parameter id is null or empty");
        }

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        try {
            stmt = connection.prepareStatement(BadSql.SELECT_BY_ID_STMT);
            stmt.setString(1, id);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new Bad(id, resultSet.getTimestamp("last"), resultSet.getTimestamp("cur"), resultSet.getInt("count"));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void addBad(String id) throws OXException {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("parameter id is null or empty");
        }

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(BadSql.DELETE_BY_TIME_STMT);
            GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
            stmt.setInt(1, configService.getIntProperty(GuardProperty.badMinuteLock));
            stmt.executeUpdate();

            DBUtils.closeSQLStuff(stmt);

            stmt = connection.prepareStatement(BadSql.ADD_STMT);
            stmt.setString(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void removeBad(String id) throws OXException {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("parameter id is null or empty");
        }

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(BadSql.DELETE_BY_ID_STMT);
            stmt.setString(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.database.storage.bad.BadStorage#isBad(java.lang.String, int)
     */
    @Override
    public boolean isBad(String id, int threshold) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        Bad badEntry = getBad(id);
        if (badEntry != null) {
            long differential = configService.getIntProperty(GuardProperty.badMinuteLock) * 60L * 1000;
            if (badEntry.getLast().getTime() > (badEntry.getCurrentTime().getTime() - differential)) {
                if (badEntry.getCount() > threshold) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.database.storage.bad.BadStorage#isBad(java.lang.String)
     */
    @Override
    public boolean isBad(String id) throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        return isBad(id,configService.getIntProperty(GuardProperty.badMinuteLock));
    }
}
