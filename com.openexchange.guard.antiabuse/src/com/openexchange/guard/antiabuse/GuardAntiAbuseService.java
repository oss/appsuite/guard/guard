package com.openexchange.guard.antiabuse;

import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.ReportParameters;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.bad.Bad;

public interface GuardAntiAbuseService {

    /**
     * Gets the bad login attempts for a given id/session
     *
     * @param id the session to the get the attempts for
     * @return the bad login attempts for the given id
     * @throws OXException due an error
     */
    public Bad getBad(String id) throws OXException;

    /**
     * Adds a bad login attempt for a given id/session
     *
     * @param id the session to add the bad login attempt for
     * @throws OXException due an error
     */
    public void addBad(String id) throws OXException;

    /**
     * Removes all bad login attempts for a given session
     *
     * @param id the session to remove the bad login attempts for
     * @throws OXException due an error
     */
    public void removeBad(String id) throws OXException;

    /**
     * Check if id is listed as bad.
     *
     * @param id Could be IP, itemID, or session
     * @param threshold Number of bad tries within time period acceptable
     * @return true, if the ID has been listed as bad, false otherwise
     * @throws OXException
     */
    boolean isBad(String id, int threshold) throws OXException;

    /**
     * Check if id is listed as bad while using the default threshold from configuration
     * @param id id could be IP, itemID or session
     * @return true, if the ID has been listed as bad, false otherwise
     * @throws OXExcpetion
     */
    boolean isBad(String id) throws OXException;

    /**
     * Check if login should be allowed
     * @param userid
     * @param cid
     * @param password
     * @param ip
     * @return
     * @throws OXException
     */
    boolean allowLogin (AllowParameters allowParam) throws OXException;

    /**
     * Check if the login should be blocked
     * @param userid
     * @param cid
     * @param password
     * @param ip
     * @return
     * @throws OXException
     */
    boolean blockLogin (AllowParameters allowParam) throws OXException;


    /**
     * Report login success or fail.  Inverse of allowLogin
     * @param ReportParameters
     * @return
     * @throws OXException
     */

    void report (ReportParameters reportParams) throws OXException;

}
