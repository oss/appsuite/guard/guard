package com.openexchange.guard.antiabuse.internal;

import com.openexchange.antiabuse.AllowParameters;
import com.openexchange.antiabuse.ReportParameters;
import com.openexchange.antiabuse.ReportValue;
import com.openexchange.exception.OXException;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.antiabuse.bad.Bad;
import com.openexchange.guard.antiabuse.bad.GuardBadStorage;
import com.openexchange.guard.antiabuse.bad.GuardBadStorageImpl;
import com.openexchange.guard.antiabuse.weakforced.WeakforcedImpl;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;

public class GuardAntiAbuseImpl implements GuardAntiAbuseService {

    GuardBadStorage bad;
    WeakforcedImpl wf;
    GuardConfigurationService config;

    public GuardAntiAbuseImpl (GuardConfigurationService config) {
        bad = new GuardBadStorageImpl();
        wf = new WeakforcedImpl();
        this.config = config;
    }

    @Override
    public Bad getBad(String id) throws OXException {
        return bad.getBad(id);
    }

    @Override
    public void addBad(String id) throws OXException {
        bad.addBad(id);
    }

    @Override
    public void removeBad(String id) throws OXException {
        bad.removeBad(id);
    }

    @Override
    public boolean isBad(String id, int threshold) throws OXException {
        return bad.isBad(id, threshold);
    }

    @Override
    public boolean isBad(String id) throws OXException {
        return bad.isBad(id);
    }

    @Override
    public boolean allowLogin(AllowParameters allowParam) throws OXException {
        if (bad.isBad(allowParam.getLogin(), config.getIntProperty(GuardProperty.badPasswordCount))) return false;
        return wf.isAllowed(allowParam);
    }

    @Override
    public boolean blockLogin(AllowParameters allowParam) throws OXException {
        return (!allowLogin(allowParam));
    }

    @Override
    public void report (ReportParameters reportParams) throws OXException {
        if (reportParams.getReportValue() == ReportValue.SUCCESS) {
            removeBad(reportParams.getLogin());
        } else {
            addBad (reportParams.getLogin());
        }
        wf.report(reportParams);
    }




}
