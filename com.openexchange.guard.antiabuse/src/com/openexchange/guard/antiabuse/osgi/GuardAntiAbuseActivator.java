package com.openexchange.guard.antiabuse.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.antiabuse.AntiAbuseService;
import com.openexchange.guard.antiabuse.GuardAntiAbuseService;
import com.openexchange.guard.antiabuse.internal.GuardAntiAbuseImpl;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.osgi.HousekeepingActivator;

public class GuardAntiAbuseActivator extends HousekeepingActivator {

	 /**
     * Initialises a new {@link GuardAntiAbuseServiceActivator}.
     */
    public GuardAntiAbuseActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardDatabaseService.class, GuardConfigurationService.class };
    }

    @Override
    protected Class<?>[] getOptionalServices() {
    	return new Class<?>[] { AntiAbuseService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardAntiAbuseActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);

        GuardConfigurationService config = Services.getService(GuardConfigurationService.class);
        registerService(GuardAntiAbuseService.class, new GuardAntiAbuseImpl(config));
        logger.info("GuardAntiAbuse registered.");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardAntiAbuseActivator.class);

        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(null);
        //unregisterService(GuardAntiAbuseService.class);
        logger.info("GuardAntiAbuse unregistered.");

        super.stopBundle();
    }
}
