# OpenPGP Web Key Service Configuration

The Web Key service provides a method for external users to query for public PGP Keys.  The details of the specification can be found https://tools.ietf.org/id/draft-koch-openpgp-webkey-service-01.html

OX Guard currently does not allow keys to be submitted by external users.  Rather, the Web Key implmentation is only for retrieval of public keys for current OX Guard users.

## Configuration File (`proxy-http.conf`)

### Additional entries required

~~~bash
ProxyPass /.well-known/openpgpkey/hu balancer://oxcluster/hu
~~~

## First Service Start

When the service is first started, it will check to see if the email hashes exist in the database.  If not, it will spawn a seperate thread to create the hashes needed for the Web Key Service lookup.  This should happen in the background, and should not interfere with normal functioning.


