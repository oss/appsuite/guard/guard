/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.caching.codec;

import java.util.Date;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.guard.guest.metadata.GuardGuestEmailMetadata;
import com.openexchange.java.Strings;

/**
 * {@link GuardGuestEmailMetadataCodec}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class GuardGuestEmailMetadataCodec extends AbstractJSONObjectCacheValueCodec<GuardGuestEmailMetadata> {

    private static final UUID CODEC_ID = UUID.fromString("57ce3f20-bea5-41f1-a6e2-411e0cdb06bf");

    private static final String ID = "i";
    private static final String VERSION = "v";
    private static final String FOLDER_ID = "fo";
    private static final String CONTENT_TYPE = "ct";
    private static final String FROM = "fr";
    private static final String TO = "t";
    private static final String CC = "cc";
    private static final String BCC = "bcc";
    private static final String SUBJECT = "su";
    private static final String SIZE = "si";
    private static final String SENT_DATE = "sd";
    private static final String RECEIVED_DATE = "rd";
    private static final String MESSAGE_FLAGS = "fl";
    private static final String COLOR_LABEL = "cl";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(GuardGuestEmailMetadata value) throws Exception {
        JSONObject obj = new JSONObject(14);
        obj.put(ID, value.getId());
        obj.put(VERSION, value.getGuardGuestItemVersion());
        obj.put(FOLDER_ID, value.getFolderId());
        obj.put(CONTENT_TYPE, value.getContentType());
        String[] from = value.getFrom();
        if (null != from && 0 < from.length) {
            obj.put(FROM, Strings.toCommaSeparatedList(from));
        }
        String[] to = value.getTo();
        if (null != to && 0 < to.length) {
            obj.put(TO, Strings.toCommaSeparatedList(to));
        }
        String[] cc = value.getCc();
        if (null != cc && 0 < cc.length) {
            obj.put(CC, Strings.toCommaSeparatedList(cc));
        }
        String[] bcc = value.getBcc();
        if (null != bcc && 0 < bcc.length) {
            obj.put(BCC, Strings.toCommaSeparatedList(bcc));
        }
        obj.put(SUBJECT, value.getSubject());
        obj.put(SIZE, value.getSize());
        obj.put(SENT_DATE, value.getSentDate().getTime());
        obj.put(RECEIVED_DATE, value.getReceivedDate().getTime());
        obj.put(MESSAGE_FLAGS, value.getMessageFlags());
        String colorLabel = value.getColorLabel();
        if (null != colorLabel) {
            obj.put(COLOR_LABEL, colorLabel);
        }
        return obj;
    }

    @Override
    protected GuardGuestEmailMetadata parseJson(JSONObject jObject) throws Exception {
        String id = jObject.getString(ID);
        String version = jObject.getString(VERSION);
        GuardGuestEmailMetadata result = new GuardGuestEmailMetadata(id, version);
        String folderId = jObject.getString(FOLDER_ID);
        result.setFolderId(folderId);
        String contentType = jObject.getString(CONTENT_TYPE);
        result.setContentType(contentType);
        if (jObject.has(FROM)) {
            String[] from = Strings.splitByComma(jObject.getString(FROM));
            result.setFrom(from);
        }
        if (jObject.has(TO)) {
            String[] to = Strings.splitByComma(jObject.getString(TO));
            result.setTo(to);
        }
        if (jObject.has(CC)) {
            String[] cc = Strings.splitByComma(jObject.getString(CC));
            result.setCc(cc);
        }
        if (jObject.has(BCC)) {
            String[] bcc = Strings.splitByComma(jObject.getString(BCC));
            result.setBcc(bcc);
        }
        String subject = jObject.getString(SUBJECT);
        result.setSubject(subject);
        long size = jObject.getLong(SIZE);
        result.setSize(size);
        Date sentDate = new Date(jObject.getLong(SENT_DATE));
        result.setSentDate(sentDate);
        Date receivedDate = new Date(jObject.getLong(RECEIVED_DATE));
        result.setReceivedDate(receivedDate);
        int messageFlags = jObject.getInt(MESSAGE_FLAGS);
        result.setMessageFlags(messageFlags);
        if (jObject.has(COLOR_LABEL)) {
            String colorLabel = jObject.getString(COLOR_LABEL);
            result.setColorLabel(colorLabel);
        }
        return result;
    }

}
