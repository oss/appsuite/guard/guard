/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.caching.codec;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.UUID;
import org.json.JSONObject;
import com.openexchange.cache.v2.codec.json.AbstractJSONObjectCacheValueCodec;
import com.openexchange.guard.caching.CertificateCacheItem;
import com.openexchange.guard.certificatemanagement.pki.impl.CertificateUtil;


/**
 * {@link X509CertificateCodec}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public class X509CertificateCodec extends AbstractJSONObjectCacheValueCodec<CertificateCacheItem> {

    private static final UUID CODEC_ID = UUID.fromString("d57b94b1-4763-4f09-bc92-24d36f3c3c0b");

    private static final String PEM = "p";
    private static final String JSON = "j";

    @Override
    public UUID getCodecId() {
        return CODEC_ID;
    }

    @Override
    protected JSONObject writeJson(CertificateCacheItem value) throws Exception {
        X509Certificate cert = value.getCertificate();
        if (cert == null) {
            return new JSONObject();
        }
        String pem = cert2Pem(cert);
        JSONObject json = new JSONObject(2);
        json.put(PEM, pem);
        json.put(JSON, value.getJson());
        return json;
    }

    @Override
    protected CertificateCacheItem parseJson(JSONObject jObject) throws Exception {
        String pem = (jObject == null || jObject.isEmpty()) ? null : jObject.getString(PEM);
        if (pem == null)
            return new CertificateCacheItem(null, new JSONObject());
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        X509Certificate cert = (X509Certificate) factory.generateCertificate(
            new ByteArrayInputStream(CertificateUtil.toPem(pem, false).getBytes(StandardCharsets.UTF_8))
        );
        return new CertificateCacheItem(cert, jObject.getJSONObject(JSON));
    }

    /**
     * Simple internal method to encode certificate into base64
     * 
     * @param cert Certificate to encode
     * @return Base64 encoded certificate (DER format)
     * @throws Exception
     */
    private String cert2Pem(X509Certificate cert) throws Exception {
        byte[] encodedCert = cert.getEncoded();
        return Base64.getEncoder().encodeToString(encodedCert);
    }
}
