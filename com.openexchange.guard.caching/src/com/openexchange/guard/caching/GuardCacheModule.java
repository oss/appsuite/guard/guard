/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.caching;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.filter.DefaultVersionName;
import com.openexchange.cache.v2.filter.ModuleName;
import com.openexchange.cache.v2.filter.VersionName;

/**
 * {@link GuardCacheModule}
 *
 * @author <a href="mailto:jan.bauerdick@open-xchange.com">Jan Bauerdick</a>
 */
public enum GuardCacheModule implements ModuleName {

    GUEST_FILE_ITEM("GuestFileItem"),

    GUEST_META_DATA("GuestMetaData"),

    GUEST_LOOKUP("GuestLookup"),

    LDAP_CERT_CACHE("LdapCertCache"),

    GUEST_2_OX_LOOKUP("Guest2OXLookup"),
    
    EJBCA("ejbcaResponse"),
    ;

    private final static String PREFIX = "og_";

    private final String name;
    private final VersionName versionName;
    private final long expirationSeconds;

    private GuardCacheModule(String name) {
        this.name = PREFIX + name;
        this.expirationSeconds = CacheOptions.getDefaultExpirationSeconds();
        this.versionName = VersionName.DEFAULT_VERSION_NAME;
    }

    private GuardCacheModule(String name, long expirationSeconds, int versionNumber) {
        this.name = PREFIX + name;
        this.expirationSeconds = expirationSeconds;
        this.versionName = DefaultVersionName.versionNameFor(versionNumber);
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Gets the expiration seconds.
     *
     * @return The expiration seconds
     */
    public long getExpirationSeconds() {
        return expirationSeconds;
    }

    /**
     * Gets the version name associated with this core module.
     *
     * @return The version name
     */
    public VersionName getVersionName() {
        return versionName;
    }

 // -------------------------------------------------------------------------------------------------------------------------------------

    private static final Map<String, GuardCacheModule> VALUES = Arrays.stream(GuardCacheModule.values()).collect(Collectors.toMap(GuardCacheModule::getName, e -> e));

    /**
     * Returns the {@link GuardCacheModule} for the specified name
     *
     * @param name the name to resolve
     * @return The {@link GuardCacheModule} or <code>null</code> if none exists
     */
    public static GuardCacheModule moduleNameFor(String name) {
        return VALUES.get(PREFIX + name);
    }

    /**
     * Returns all {@link GuardCacheModule} values mapped by their name
     *
     * @return The map of {@link GuardCacheModule}
     */
    public static Map<String, GuardCacheModule> getGuardCacheModules() {
        return VALUES;
    }
}
