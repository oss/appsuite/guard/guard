/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.caching.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.openexchange.cache.v2.Cache;
import com.openexchange.cache.v2.CacheOptions;
import com.openexchange.cache.v2.CacheService;
import com.openexchange.cache.v2.invalidation.InvalidationCacheService;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.CertificateCacheItem;
import com.openexchange.guard.caching.FileCacheItem;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.caching.GuardCacheModule;
import com.openexchange.guard.caching.codec.GuardEmailCodec;
import com.openexchange.guard.caching.codec.GuardGuestEmailMetadataCodec;
import com.openexchange.guard.caching.codec.GuestFileItemCodec;
import com.openexchange.guard.caching.codec.X509CertificateCodec;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.metadata.GuardGuestEmailMetadata;

/**
 * {@link GenericCacheFactoryImpl} default implementation of a factory for creating {@link GenericCache} instances.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GenericCacheFactoryImpl implements GenericCacheFactory {

    private static final Map<String, CacheOptions<?>> cacheOptions = new ConcurrentHashMap<String, CacheOptions<?>>();
    static {
        cacheOptions.put(GuardCacheModule.GUEST_META_DATA.getName(), CacheOptions.<GuardGuestEmailMetadata> builder()
                         .withModuleName(GuardCacheModule.GUEST_META_DATA)
                         .withCodecAndVersion(new GuardGuestEmailMetadataCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
                         cacheOptions.put(GuardCacheModule.GUEST_LOOKUP.getName(), CacheOptions.<Email> builder()
                         .withModuleName(GuardCacheModule.GUEST_LOOKUP)
                         .withCodecAndVersion(new GuardEmailCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
                         cacheOptions.put(GuardCacheModule.GUEST_2_OX_LOOKUP.getName(), CacheOptions.<Email> builder()
                         .withModuleName(GuardCacheModule.GUEST_2_OX_LOOKUP)
                         .withCodecAndVersion(new GuardEmailCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
                         cacheOptions.put(GuardCacheModule.GUEST_FILE_ITEM.getName(), CacheOptions.<FileCacheItem> builder()
                         .withModuleName(GuardCacheModule.GUEST_FILE_ITEM)
                         .withCodecAndVersion(new GuestFileItemCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
                         cacheOptions.put(GuardCacheModule.LDAP_CERT_CACHE.getName(), CacheOptions.<CertificateCacheItem> builder()
                             .withModuleName(GuardCacheModule.LDAP_CERT_CACHE)
                         .withCodecAndVersion(new X509CertificateCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
                         cacheOptions.put(GuardCacheModule.EJBCA.getName(), CacheOptions.<CertificateCacheItem> builder()
                             .withModuleName(GuardCacheModule.EJBCA)
                         .withCodecAndVersion(new X509CertificateCodec())
                         .withExpirationSeconds(3600)
                         .build()
                         );
    }

    private final InvalidationCacheService cacheService;

    /**
     * Initializes a new {@link GenericCacheFactoryImpl}.
     *
     * @param cacheService The underlying {@link CacheService} to use by the created caches
     */
    public GenericCacheFactoryImpl(InvalidationCacheService cacheService) {
        this.cacheService = cacheService;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.caching.GenericCacheFactory#createCache(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public GenericCache<?> createCache(GuardCacheModule module) throws OXException {
        CacheOptions<?> opt = cacheOptions.get(module.getName());
        if (opt == null) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create("Cache not found");
        }
        Cache<?> cache = cacheService.getCache(opt);
        return new GenericCacheImpl(cache, module.getName());
    }
}
