/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.caching;

import java.io.Serializable;
import java.security.cert.X509Certificate;
import org.json.JSONObject;

/**
 * Certificate Cache class that contains an X509Certificate as well as
 * associating json data
 */
public class CertificateCacheItem implements Serializable {

    /**
     * The CertificateCache.java.
     */
    private static final long serialVersionUID = 4874427929591142612L;

    X509Certificate certificate;
    JSONObject json;

    public CertificateCacheItem(X509Certificate certificate, JSONObject json) {
        super();
        this.certificate = certificate;
        this.json = json;
    }

    /**
     * Gets the certificate
     *
     * @return The certificate
     */
    public X509Certificate getCertificate() {
        return certificate;
    }

    /**
     * Sets the certificate
     *
     * @param certificate The certificate to set
     */
    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    /**
     * Gets the json
     *
     * @return The json
     */
    public JSONObject getJson() {
        return json;
    }

    /**
     * Sets the json
     *
     * @param json The json to set
     */
    public void setJson(JSONObject json) {
        this.json = json;
    }


}
