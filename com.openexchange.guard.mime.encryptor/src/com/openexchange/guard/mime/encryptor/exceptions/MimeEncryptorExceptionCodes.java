/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.exceptions;

import com.openexchange.exception.Category;
import com.openexchange.exception.DisplayableOXExceptionCode;
import com.openexchange.exception.OXException;
import com.openexchange.exception.OXExceptionFactory;
import com.openexchange.exception.OXExceptionStrings;

/**
 * {@link MimeEncryptorExceptionCodes}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public enum MimeEncryptorExceptionCodes implements DisplayableOXExceptionCode {

    SEND_EMAIL_ERROR("Unable to send email to '%1$s'", MimeEncryptorExceptionExceptionMessages.SEND_EMAIL_ERROR, CATEGORY_ERROR, 1),
    SEND_NOT_AUTHORIZED("User is not authorized to send Guard emails", MimeEncryptorExceptionExceptionMessages.SEND_NOT_AUTHORIZED, CATEGORY_ERROR, 2),
    SEND_UNABLE_FIND_KEYS("Unable to find keys for all recipients", MimeEncryptorExceptionExceptionMessages.SEND_UNABLE_FIND_KEYS, CATEGORY_ERROR, 3),
    NO_RECIPIENTS("Unable to encrypt, no recipients", MimeEncryptorExceptionExceptionMessages.NO_RECIPIENTS, CATEGORY_ERROR, 4),
    UNKONWN_BODY_TYPE("Unable to encrypt due to unknown body type", MimeEncryptorExceptionExceptionMessages.UNKONWN_BODY_TYPE, CATEGORY_ERROR, 5),
    PASSWORD_MISSING("Password is missing or authentication bad", MimeEncryptorExceptionExceptionMessages.PASSWORD_BAD, CATEGORY_ERROR, 6),
    PROBLEM_DECODING("Problem decoding encrypted attachment", MimeEncryptorExceptionExceptionMessages.PROBLEM_DECODING_ENCRYPTED_ATTACHMENT, CATEGORY_ERROR, 7),
    PROBLEM_SAVING_GUEST_DATA("Problem saving Guest data", CATEGORY_ERROR, 8),
    RECIPIENTS_NOT_IN_ORIGINAL_CHAIN("One or more recipients was not in the original email chain, '%1$s", MimeEncryptorExceptionExceptionMessages.RECIPIENT_NOT_IN_CHAIN , CATEGORY_ERROR, 9),
    MISSING_MDC("Unable to decrypt this message.  It is missing Modification Detection Code, and the system is unable to verify the integrity of the message.", MimeEncryptorExceptionExceptionMessages.MISSING_MDC, CATEGORY_ERROR, 10);
    ;

    private final String message;
    private final String displayMessage;
    private final Category category;
    private final int number;

    private MimeEncryptorExceptionCodes(String message, Category category, int number) {
        this(message, null, category, number);
    }

    private MimeEncryptorExceptionCodes(String message, String displayMessage, Category category, int number) {
        this.message = message;
        this.displayMessage = displayMessage != null ? displayMessage : OXExceptionStrings.MESSAGE;
        this.category = category;
        this.number = number;
    }

    @Override
    public String getPrefix() {
        return "ENCRYPT";
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getDisplayMessage() {
        return displayMessage;
    }

    @Override
    public Category getCategory() {
        return category;
    }

    @Override
    public boolean equals(OXException e) {
        return OXExceptionFactory.getInstance().equals(this, e);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @return The newly created {@link OXException} instance
     */
    public OXException create() {
        return OXExceptionFactory.getInstance().create(this, new Object[0]);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Object... args) {
        return OXExceptionFactory.getInstance().create(this, (Throwable) null, args);
    }

    /**
     * Creates a new {@link OXException} instance pre-filled with this code's attributes.
     *
     * @param cause The optional initial cause
     * @param args The message arguments in case of printf-style message
     * @return The newly created {@link OXException} instance
     */
    public OXException create(Throwable cause, Object... args) {
        return OXExceptionFactory.getInstance().create(this, cause, args);
    }
}
