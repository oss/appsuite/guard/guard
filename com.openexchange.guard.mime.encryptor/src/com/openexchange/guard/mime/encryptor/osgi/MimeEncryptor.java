package com.openexchange.guard.mime.encryptor.osgi;

import org.slf4j.Logger;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.encryption.EncryptedItemsStorage;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.mime.encryptor.PGPMimeEncryptionService;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.pgpcore.services.PGPSigningService;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.storage.Storage;
import com.openexchange.guard.translation.GuardTranslationService;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.html.HtmlService;
import com.openexchange.osgi.HousekeepingActivator;
import com.openexchange.pgp.mail.PGPMimeService;

public class MimeEncryptor extends HousekeepingActivator {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardConfigurationService.class, GuardTranslationService.class, MailCreatorService.class,
            GuardNotificationService.class, MasterKeyService.class, GuardCipherFactoryService.class, GuardKeyService.class,
            OXUserService.class, EncryptedItemsStorage.class, RecipKeyService.class, PGPCryptoService.class, PGPSigningService.class, Storage.class,
            AccountCreationService.class, PGPMimeService.class, PGPMimeCryptoService.class, HelpFileService.class,
            GuardGuestEMailMetadataService.class, EmailStorage.class, GuardGuestService.class, SecondFactorService.class,
            HtmlService.class, AutoCryptService.class, CryptoManager.class };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void startBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(MimeEncryptor.class);
        logger.info("Starting bundle: {}", context.getBundle().getSymbolicName());

        Services.setServiceLookup(this);

        MimeEncryptionService pgpMimeEncr = new PGPMimeEncryptionService(this);
        registerService(MimeEncryptionService.class, pgpMimeEncr);
        logger.info("MimeEncryptor registered.");

        openTrackers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void stopBundle() throws Exception {
        Logger logger = org.slf4j.LoggerFactory.getLogger(MimeEncryptor.class);
        logger.info("Stopping bundle: {}", this.context.getBundle().getSymbolicName());
        unregisterService(MimeEncryptor.class);
        logger.info("MimeEncryptor unregistered.");

        Services.setServiceLookup(null);

        super.stopBundle();
    }

}
