/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;

/**
 * {@link Encryptor}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public abstract class Encryptor {

    protected GuardParsedMimeMessage msg;
    protected ArrayList<RecipKey> recips;
    protected int userid;
    protected int cid;
    protected String password;
    protected final PGPCryptoService pgpCryptoService;

    public Encryptor (PGPCryptoService pgpCryptoService, GuardParsedMimeMessage msg, ArrayList<RecipKey> recips, int userid, int cid, String password) {
        this.pgpCryptoService = Objects.requireNonNull(pgpCryptoService, "pgpCryptoService must not be null");
        this.msg = msg;
        this.recips = new ArrayList<RecipKey>(recips);
        this.userid = userid;
        this.cid = cid;
        this.password = password;
    }

    /**
     * Encrypt Mime message and return as UTF-8 encoded string
     * @return
     * @throws OXException
     */
    public String doEncryptToString() throws OXException {
        ByteArrayOutputStream wrapped_out = new ByteArrayOutputStream();
        try {
            MimeMessage msg = doEncrypt();
            if (msg != null) {
                doEncrypt().writeTo(wrapped_out);
                byte[] wrapped_bytes = wrapped_out.toByteArray();
                return (new String (wrapped_bytes, StandardCharsets.UTF_8));
            }
            return "";
        } catch (IOException | MessagingException e) {
            throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(e.getMessage());
        }
    }

    /**
     * Perform the encryption
     *
     * @return The encrypted message
     * @throws OXException
     */
    public abstract MimeMessage doEncrypt () throws OXException;
}
