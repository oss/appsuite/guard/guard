/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.impl;

import java.io.IOException;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.html.HtmlService;

/**
 * {@link HTMLStripper}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class HTMLStripper {

    /**
     * Convert html to plaintext string
     * @param html
     * @return plaintext with html markup removed
     */
    private String stripHTML (String html) {
        HtmlService htmlService = Services.getService(HtmlService.class);
        return htmlService.html2text(html, false);
    }

    /**
     * Breaks all HTML by converting tags to displayable UI code.
     * Assumed input is plaintext, which shouldn't have html to begin with.
     * Therefore, any conversion will break existing html
     * @param html
     * @return Text with HTML converted for display only
     */
    public String convertTextHTML (String html) {
        html = html.replaceAll("<", "&lt");
        html =html.replaceAll(">", "&gt");
        return html;
    }

    /**
     * Loop through multipart sections of an email and convert any html found to plaintext
     * If text/plain section found, the text/html will be removed.
     * @param mp
     * @throws MessagingException
     * @throws IOException
     */
    private void parseMultiparts(Multipart mp) throws MessagingException, IOException {
        boolean hasPlainText = false;
        if (mp.getContentType().contains("multipar")) {
            final int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                final BodyPart bp = mp.getBodyPart(i);
                final Object content = bp.getContent();
                if (content instanceof String) {  // Check if this string is a text attachment, or just the message body
                    if (bp.getDisposition() != null && bp.getDisposition().contains("attachment")) {
                            // ignore for now
                    } else {
                        if (hasPlainText) {  // If we already found a plaintext section, use it and remove the html bodypart
                            bp.setText("removed");  // Emtpy it out
                            bp.setDisposition("none");  // Remove display
                            bp.setHeader("Content-Type", "none");
                        } else {
                            if (bp.getContentType() != null && bp.getContentType().contains("text/plain")) {
                                hasPlainText = true;
                            }
                            bp.setText(convertTextHTML((String) content));
                        }
                    }

                } else if (content instanceof Multipart || content instanceof MimeMultipart) {
                    parseMultiparts((Multipart) content);
                }
            }
        }
    }

    /**
     * Removes any HTML from Mime Message
     * @param message
     * @return
     * @throws MessagingException
     * @throws IOException
     */
    public MimeMessage stripAllHTML (MimeMessage message) throws MessagingException, IOException {
        Object content = message.getContent();
        if (content instanceof String) {
            String contentType = message.getHeader("Content-Type", ",");
            if (contentType != null && contentType.contains("text/plain")) {
                message.setText(convertTextHTML((String) content));
            } else {
                message.setText(stripHTML((String) content));
            }
        } else {
            if (content instanceof Multipart || content instanceof MimeMultipart) {
                parseMultiparts((Multipart) content);
            }
        }
        String messageId = message.getMessageID();
        message.saveChanges();
        message.setHeader("Message-ID", messageId);
        return message;

    }

    /**
     * Converts HTML to plaintext from string
     * @param content
     * @return String with HTML removed
     */
    public String stripAllHTML (String content) {
        return stripHTML(content);
    }

}
