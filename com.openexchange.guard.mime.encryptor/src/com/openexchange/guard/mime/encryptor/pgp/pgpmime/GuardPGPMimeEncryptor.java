/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.pgpmime;

import static com.openexchange.java.Autoboxing.I;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPSecretKey;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.CipherUtil;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.commons.util.RecipKeyListUtils;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mime.encryptor.Encryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;
import com.openexchange.pgp.mail.PGPMimeService;


/**
 * Perform Mime encryption.
 * {@link GuardPGPMimeEncryptor}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class GuardPGPMimeEncryptor extends Encryptor {

    private final HelpFileService helpFileService;
    private final int oxUserId, oxCid;

    public GuardPGPMimeEncryptor (PGPCryptoService pgpCryptoService,
                                  HelpFileService helpFileService,
                                  GuardParsedMimeMessage msg,
                                  ArrayList<RecipKey> recips,
                                  int userid,
                                  int cid,
        int oxUserId,
        int oxCid,
                                  String password) {
        super (pgpCryptoService, msg, recips, userid, cid, password);
        this.oxUserId = oxUserId;
        this.oxCid = oxCid;
        this.helpFileService = Objects.requireNonNull(helpFileService, "helpFileService must not be null");
    }

    /**
     * Internal method to fetch the sender's PGP key
     *
     * @return The sender's key
     * @throws OXException
     */
    private GuardKeys getKey() throws OXException {
        GuardKeyService guardKeyService = Services.getService(GuardKeyService.class);
        return msg.senderIsGuest() ?
                guardKeyService.getKeys(msg.getSenderEmail()) :
                guardKeyService.getKeys(userid, cid);
    }

    @Override
    public MimeMessage doEncrypt () throws OXException {
        try {
            PGPEncryptedAttachmentManager pgpAttach = new PGPEncryptedAttachmentManager(msg, password);
            if (pgpAttach.hasEncryptedAttachment()) {
                msg.setMessage(pgpAttach.getDecoded());
            }

            GuardKeys key = getKey();
            if(key == null) {
                throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(I(userid), I(cid));
            }
            RecipKey senderKey = new RecipKey(key);
            if(senderKey.getEncryptionKey() == null) {
                //Cannot find sender's key or use it was revoked or is expired
                throw GuardCoreExceptionCodes.NO_USABLE_KEY_FOUND.create(key.getEmail());
            }
            recips.add(senderKey);  // Add the sender here so it can be read in sent folder

            //Get additional help files to attach to the message
            final List<BodyPart> helpFiles = helpFileService.attachHelpFile(oxUserId, oxCid, recips) ? helpFileService.getHelpFiles(oxUserId, oxCid, RecipKeyListUtils.getLanguagesFor(recips)) :
                null;


            PGPMimeService pgpMimeService = Services.getService(PGPMimeService.class);
            MimeMessage ret = null;
            if (msg.getPIN() != null) {  // If pin added, add header within encrypted email for sender to check
                msg.getMessage().setHeader(GuardMimeHeaders.X_GUARD_PIN, msg.getPIN());
            }
            if (!msg.isDraft()) {
                msg.getMessage().setRecipients(RecipientType.BCC, (String) null);  // remove BCC from Mime before encrypting
            }

            if (msg.isSign() && !msg.isDraft()) {  // not signing Drafts
                char[] hashedPassword = CipherUtil.getSHA(password, key.getSalt()).toCharArray();
                ret = pgpMimeService.encryptSigned(msg.getMessage(), getSignerKey(key), hashedPassword, toPGPPublicKeys(recips), helpFiles);
            } else {
                ret = pgpMimeService.encrypt(msg.getMessage(), toPGPPublicKeys(recips), helpFiles);
            }

            return ret;
        } catch (Exception e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }

    }

    /**
     * Extracts the PGP signing key from the given Guard key
     *
     * @param guardKey The key to get the singing key for
     * @return The signing key for the given Guard key
     * @throws OXException If no signing key was found
     */
    PGPSecretKey getSignerKey(GuardKeys guardKey) {
        PGPSecretKey signingKey = PGPKeysUtil.getSigningKey(guardKey.getPGPSecretKeyRing());
        if (signingKey != null) {
            return signingKey;
        }
        //TODO: Throw Exception?
        return null;
    }

    private List<PGPPublicKey> toPGPPublicKeys(List<RecipKey> recipients){
        //Getting recipient keys suitable for encryption
        List<PGPPublicKey> pgpRecipientKeysList = new ArrayList<PGPPublicKey>();
        for (RecipKey recipientKey : recipients) {
            PGPPublicKey encryptionKey = recipientKey.getEncryptionKey();
            if(encryptionKey != null) {
                pgpRecipientKeysList.add(encryptionKey);
            }
        }
        return pgpRecipientKeysList;
    }


    @Override
    public String doEncryptToString() throws OXException {
        ByteArrayOutputStream wrapped_out = new ByteArrayOutputStream();
        try {
            doEncrypt().writeTo(wrapped_out);
            byte[] wrapped_bytes = wrapped_out.toByteArray();
            return (new String (wrapped_bytes, StandardCharsets.UTF_8));
        } catch (IOException | MessagingException e) {
            throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(e.getMessage());
        }
    }
}
