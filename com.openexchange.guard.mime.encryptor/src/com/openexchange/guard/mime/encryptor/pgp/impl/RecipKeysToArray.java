/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.impl;

import java.util.ArrayList;
import org.bouncycastle.openpgp.PGPPublicKey;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.commons.RecipKey;

/**
 * {@link RecipKeysToArray}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class RecipKeysToArray {
    /**
     * Generate PGPPublicKey Array from recipients
     * @param recips
     * @return
     * @throws OXException
     */
    public static PGPPublicKey[] convert(ArrayList<RecipKey> recips) {

        ArrayList<PGPPublicKey> ret = new ArrayList<>();
        for (int i = 0; i < recips.size(); i++) {
            ret.add(recips.get(i).getEncryptionKey());
        }
        return ret.toArray(new PGPPublicKey[ret.size()]);
    }

}
