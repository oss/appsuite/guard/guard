/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.autocrypt.AutoCryptService;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.guest.metadata.GuardGuestEmailMetadata;
import com.openexchange.guard.guest.metadata.storage.GuardGuestEMailMetadataService;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.mime.encryptor.Decryptor;
import com.openexchange.guard.mime.encryptor.Encryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.encryptor.pgp.guest.PGPGuestEncryptor;
import com.openexchange.guard.mime.encryptor.pgp.inline.PGPInlineDecryptor;
import com.openexchange.guard.mime.encryptor.pgp.inline.PGPInlineEncryptor;
import com.openexchange.guard.mime.encryptor.pgp.pgpmime.GuardPGPMimeEncryptor;
import com.openexchange.guard.mime.encryptor.pgp.pgpmime.PGPMimeDecryptor;
import com.openexchange.guard.mime.helpfiles.HelpFileService;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.mime.services.MimeParser;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;
import com.openexchange.guard.user.GuardCapabilities;
import com.openexchange.guard.user.GuardCapabilities.Permissions;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.mail.PGPMimeService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PGPProcessMime}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class PGPProcessMime {

    private static final Logger LOG = LoggerFactory.getLogger(PGPProcessMime.class);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link PGPProcessMime}.
     *
     * @param pgpCryptoService The {@link PGPCryptoService} to use for mime encryption
     */
    public PGPProcessMime(ServiceLookup services) {
        this.services = services;
    }

    private MailCreatorService getMailCreatorService() throws OXException {
        return services.getServiceSafe(MailCreatorService.class);
    }

    private GuardGuestService getGuardGuestService() throws OXException {
        return services.getServiceSafe(GuardGuestService.class);
    }

    private GuardNotificationService getGuardNotificationService() throws OXException {
        return services.getServiceSafe(GuardNotificationService.class);
    }

    private PGPMimeService getPGPMimeService() throws OXException {
        return services.getServiceSafe(PGPMimeService.class);
    }

    private HelpFileService getHelpFileService() throws OXException {
        return services.getServiceSafe(HelpFileService.class);
    }

    private AutoCryptService getAutoCryptService() {
        return services.getService(AutoCryptService.class);
    }

    private EmailStorage getEmailStorage() throws OXException {
        return services.getServiceSafe(EmailStorage.class);
    }

    private GuardGuestEMailMetadataService getGuardGuestEMailMetadataService() throws OXException {
        return services.getServiceSafe(GuardGuestEMailMetadataService.class);
    }

    private GuardConfigurationService getGuardConfigurationService() throws OXException {
        return services.getServiceSafe(GuardConfigurationService.class);
    }

    private PGPCryptoService getPGPCryptoService() throws OXException {
        return services.getServiceSafe(PGPCryptoService.class);
    }

    /**
     * Encrypts a {@link GuardParsedMimeMessage} for the given user id
     *
     * @param msg The {@link GuardParsedMimeMessage} to encrypt
     * @param userId The ID of the user who wants to encrypt the message
     * @param contextId The context ID of the user who wants to encrypt the message
     * @param password The password required for decryptiion
     * @return The decrypted MimeMessage as raw String
     * @throws OXException
     */
    public String doEncrypt(GuardParsedMimeMessage msg, int userId, int contextId, String password) throws OXException {

        final int originalUserId = userId;
        final int originalContextId = contextId;

        //Assures appropriated Guard permissions for user
        // If doesn't have guard-mail, possible secure reply.  Verify all users have keys
        // No new users allowed.
        // TODO enhance this
        if (!checkMailPermission(userId, contextId) && !msg.isDraft()) {
            try {
                // Check recipients to verify no new keys created)
                RecipientParser testRecipParse = new RecipientParser(msg, userId, contextId, false);
                ArrayList<RecipKey> checkRecipts = testRecipParse.getAllRecipients();
                checkRecipts.removeIf(r -> r.isNewKey());
                // Verify found keys same length as number recipients
                if (checkRecipts.size() != msg.getRecipients().length()) {
                    LOG.error("Attempt to send email from guard only capable account, with new recipients");
                    throw MimeEncryptorExceptionCodes.SEND_NOT_AUTHORIZED.create();
                }
            } catch (AddressException | JSONException e) {
                LOG.error("Error checking for added recipients for guard capability only send", e);
                throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(e);
            }
        }

        try {
            String origMessageID = msg.getMessage().getMessageID();
            RecipientParser recipParse = new RecipientParser(msg, userId, contextId, msg.isDraft());
            ArrayList<RecipKey> recips = recipParse.getPgpRecipients();
            ArrayList<RecipKey> guests = recipParse.getGuestRecipients();
            ArrayList<RecipKey> allRecipts = recipParse.getAllRecipients();
            // Handle Guests
            if (guests.size() > 0 && !msg.isDraft()) {  // Won't create guests for draft messages
                PGPGuestEncryptor guestEncryptor = new PGPGuestEncryptor(getPGPCryptoService(),
                    getPGPMimeService(),
                    getGuardGuestService(),
                    getHelpFileService(),
                    msg,
                    guests,
                    userId,
                    contextId,
                    password);
                if (!guestEncryptor.doEncryptAndSend()) {
                    throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create("Unable to send Guest emails");
                }
            }
            // Verify sender is Guest guest user, and change userid/cid to guard user
            if (msg.senderIsGuest()) {
                Email emailResult = getEmailStorage().getByEmail(msg.getFromAddress().getAddress());
                if (emailResult == null) {
                    throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create();
                }
                userId = emailResult.getUserId();  // Need to change the UserId/Cid from the OX Guest to Guard userid/cid
                contextId = emailResult.getContextId();
                if (!msg.isDraft()) {
                    checkGuestSendOK(userId, contextId, msg.getGuardMessagRef(), recips);
                }
            }

            Encryptor encryptor = null;
            if (recipParse.isInline() || msg.isInline()) {
                encryptor = new PGPInlineEncryptor(getPGPCryptoService(), msg, recips, userId, contextId, password);
            } else {
                AutoCryptService autocryptService = getAutoCryptService();

                if (!msg.isDraft() && autocryptService != null && msg.addAutocrypt()) {  // For emails being sent, attach all gossip headers
                    final InternetAddress addr = msg.getFromAddress();
                    String autocryptHeader = autocryptService.getOutgoingHeaderString(userId, contextId, originalUserId, originalContextId, addr == null ? null : addr.getAddress());
                    if (autocryptHeader != null && !autocryptHeader.isEmpty()) {
                        msg.getMessage().addHeader("Autocrypt", autocryptHeader);
                    }
                    msg.setMessage(autocryptService.addGossipHeaders(msg.getMessage(), userId, contextId, allRecipts));
                }
                if (recips.size() > 0) {
                    encryptor = new GuardPGPMimeEncryptor(getPGPCryptoService(), getHelpFileService(), msg, recips, userId, contextId, originalUserId, originalContextId, password);
                } else {
                    if (guests.size() > 0) {  // No recips, but we had guests, we need to send this for the sent folder
                        encryptor = new GuardPGPMimeEncryptor(getPGPCryptoService(), getHelpFileService(), msg, guests, userId, contextId, originalUserId, originalContextId, password);
                    }
                }
            }
            if (encryptor != null) {
                //encrypt the message
                final MimeMessage encryptedResult = encryptor.doEncrypt();

                if (msg.senderIsGuest() && !msg.isDraft()) {
                    //Sending on behalf of a guest
                    send(encryptedResult, msg.getSenderEmail(), userId, contextId, originalUserId, originalContextId, recips, msg.getSenderIP());

                    //Remove the all recipients handled by Guard
                    return convertToString(removeRecipients(encryptedResult, allRecipts, origMessageID));
                }

                //Just remove the guests handled by Guard
                return convertToString(removeRecipients(encryptedResult, guests, origMessageID));
            }
            throw MimeEncryptorExceptionCodes.NO_RECIPIENTS.create();
        } catch (JSONException | MessagingException | IOException e) {
            LOG.error("Error encrypting email ", e);
            throw MimeEncryptorExceptionCodes.SEND_EMAIL_ERROR.create(e.getMessage());
        }
    }

    private void send(Message message, String senderEmail, int guardUserId, int guardCid, int userId, int senderCid, ArrayList<RecipKey> recipients, String remoteIp) throws OXException {
        String fromAddress = getFromEmailAddress(recipients, senderEmail);
        for (RecipKey recipient : recipients) {
            getGuardNotificationService().sendMessage(message, recipient.getEmail(), fromAddress, guardUserId, guardCid, userId, senderCid, recipient.getLang(), remoteIp);
        }
    }

    private String getFromEmailAddress(List<RecipKey> recipients, String fromaddres) throws OXException {
        int cid = 0;
        int id = 0;
        for (int i = 0; i < recipients.size(); i++) {// Find at least one member with positive cid, and use that configured address
            if (recipients.get(i).getCid() > 0) {
                cid = recipients.get(i).getCid();
                id = recipients.get(i).getUserid();
                break;
            }
        }
        return getMailCreatorService().getFromAddress(fromaddres, fromaddres, id, cid).get(1);
    }

    /**
     * Internal method to make sure Guest not replying to new recipients. Maintaining email chain
     *
     * @param userid Guard Guest userid
     * @param contextId Guard guest contextId
     * @param msgRef Original message ref
     * @param recipients List of recipients
     * @throws OXException
     */
    private void checkGuestSendOK(int userid, int contextId, String msgRef, ArrayList<RecipKey> recipients) throws OXException {
        if (msgRef != null) {
            GuardGuestEmailMetadata meta = getGuardGuestEMailMetadataService().get(contextId, userid, msgRef);
            if (meta == null) {
                throw MimeEncryptorExceptionCodes.RECIPIENTS_NOT_IN_ORIGINAL_CHAIN.create("Unable to retrieve original meta data");
            }
            String[] metaTos = ArrayUtils.addAll(
                meta.getTo(),
                ArrayUtils.addAll(meta.getCc(), meta.getFrom()));

            for (RecipKey recip : recipients) {
                String email = recip.getEmail();
                boolean found = false;
                for (String inChain : metaTos) {
                    if (inChain.toLowerCase().contains(email.toLowerCase())) {
                        found = true;
                    }
                }
                if (!found) {
                    throw MimeEncryptorExceptionCodes.RECIPIENTS_NOT_IN_ORIGINAL_CHAIN.create(email);
                }
            }
        } else {
            throw MimeEncryptorExceptionCodes.RECIPIENTS_NOT_IN_ORIGINAL_CHAIN.create("");
        }
    }

    /**
     * @param userId
     * @param contextId
     * @throws OXException
     */
    private void requirePermission(int userId, int contextId, boolean sending) throws OXException {
        // Check user permission
        GuardCapabilities guardUserCapabilities = Services.getService(OXUserService.class).getGuardCapabilieties(contextId, userId);
        if (sending && !guardUserCapabilities.hasPermission(Permissions.MAIL)) {
            // TODO
            // Handle replies to encrypted emails when the user doesn't have Guard enabled
            // Guest replies when moved to core?
            throw MimeEncryptorExceptionCodes.SEND_NOT_AUTHORIZED.create();
        }
        if (!guardUserCapabilities.hasMinimumPermission()) {
            throw MimeEncryptorExceptionCodes.SEND_NOT_AUTHORIZED.create();
        }
    }

    /**
     * Checks to see if has Guard-Mail capability. If so, returns true. Of Guard only, returns false
     * If no Guard permissions, throws Not_Authorized
     *
     * @param userId
     * @param contextId
     * @return
     * @throws OXException
     */
    private boolean checkMailPermission(int userId, int contextId) throws OXException {
        GuardCapabilities guardUserCapabilities = Services.getService(OXUserService.class).getGuardCapabilieties(contextId, userId);
        if (guardUserCapabilities.hasPermission(Permissions.MAIL)) {
            return true;
        }
        if (guardUserCapabilities.hasMinimumPermission()) {
            return false;
        }
        throw MimeEncryptorExceptionCodes.SEND_NOT_AUTHORIZED.create();
    }

    /**
     * Decrypts a {@link MimeMessage}
     *
     * @param message The {@link MimeMessage} to encrypt
     * @param userIdentity the {@link UserIdentity} to decrypt the message for
     * @return The encrypted {@link MimeMessage}
     * @throws OXException
     */
    public MimeMessage doDecrypt(MimeMessage message, UserIdentity userIdentity) throws OXException {

        //Assures appropriated Guard permissions for user
        requirePermission(userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId(), false);

        try {
            MimeParser mimeParser = new MimeParser(message);
            Decryptor decryptor = null;

            if (mimeParser.isPGPMime()) {
                decryptor = new PGPMimeDecryptor(services, userIdentity);
            } else if (mimeParser.isPGPInline()) {
                decryptor = new PGPInlineDecryptor(getPGPCryptoService(), getGuardConfigurationService(), userIdentity);
            }

            if (decryptor != null) {
                return decryptor.doDecrypt(message);
            }
            return message;
        } catch (Exception e) {
            if (e instanceof OXException) {
                throw (OXException) e;
            }
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    private String convertToString(MimeMessage message) throws IOException, MessagingException {
        ByteArrayOutputStream wrapped_out = new ByteArrayOutputStream();
        message.writeTo(wrapped_out);
        byte[] wrapped_bytes = wrapped_out.toByteArray();
        return new String(wrapped_bytes, StandardCharsets.UTF_8);
    }

    /**
     * Add header to the email to remove the Guests from the list of recipients that the middleware will send to
     *
     * @param message
     * @param recipients
     * @return
     * @throws MessagingException
     * @throws IOException
     */
    private MimeMessage removeRecipients(MimeMessage message, ArrayList<RecipKey> recipients, String origMessageID) throws MessagingException {
        if (message == null) {
            return null;
        }
        for (RecipKey key : recipients) {
            message.addHeader("Remove-recip", key.getEmail());
        }
        message.removeHeader(GuardMimeHeaders.X_GUARD_PIN);  // Remove record of pin from non-encrypted email
        message.saveChanges();
        if (origMessageID != null) {  // Save changes and enryption may change messageID.  Restore original here
            message.setHeader("Message-ID", origMessageID);
        }
        return message;
    }
}
