/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.impl;

import java.util.ArrayList;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.IDNUtil;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.AccountCreationService;
import com.openexchange.guard.keymanagement.services.RecipKeyService;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.secondfactor.SecondFactorService;

/**
 * {@link RecipientParser}
 * Parse the recipients from a the incoming message, find keys, and sort Guests vs pgp
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class RecipientParser {

    private static Logger LOG = LoggerFactory.getLogger(RecipientParser.class);
    private ArrayList <RecipKey> pgpRecips;
    private ArrayList <RecipKey> guestRecips;
    private ArrayList <RecipKey> allRecips;

    /**
     * Evaluate if Guest recipient wants direct email.
     *
     * @param recip The recipient to check
     * @return True, if the recipient wants to receive direct PGP mail, false to receive via guest-reader.
     */
    private static boolean isDirectGuestRecipient(RecipKey recip) {
        try {
            if (recip.getCid() > 0) {
                return (true);  // Members get direct
            }
            if (recip.isGuest() == false) {
                return (true);  // Have uploaded key, not guest
            }
            if (recip.getSettings() == null) {
                return (false);
            }
            if (!recip.getSettings().has("direct")) {
                return (false);
            }
            return recip.getSettings().get("direct").getAsBoolean();
        } catch (Exception e) {
            LOG.error("Trouble checking is direct", e);
            return false;
        }
    }

    public RecipientParser (GuardParsedMimeMessage msg, int userId, int cid, boolean draft) throws JSONException, AddressException, OXException {
        pgpRecips = new ArrayList<RecipKey>();
        guestRecips = new ArrayList<RecipKey>();
        allRecips = new ArrayList<RecipKey>();

        JSONArray recips = draft ?  // If draft, we only care about the sender
            new JSONArray().put(new JSONObject().put("Email", msg.getFromAddress().getAddress())) : msg.getRecipients();
        for (int i = 0; i < recips.length(); i++) {
            JSONObject recip = (JSONObject) recips.get(i);
            InternetAddress emailAddress = new InternetAddress(IDNUtil.aceEmail(recip.getString("Email")));
            String email = IDNUtil.decodeEmail(emailAddress.getAddress());
          //Lookup recipient key
          RecipKey key = Services.getService(RecipKeyService.class).getRecipKey(userId, cid, email);
            if (key != null &&  key.isNewKey() && !draft) {
                if (key.isGuest()) {
                    key.setLang(msg.getGuestLanguage());
                    key.setSenderCid(cid);
                    key.setSenderUserId(userId);
                    // Check if this is an existing guest that just doesn't have any valid keys
                    // If so, we need to reuse the userId
                    EmailStorage emailStorage = Services.getService(EmailStorage.class);
                    if (emailStorage != null) {
                        Email emailLookup = emailStorage.getByEmail(email);
                        if (emailLookup != null) {
                            key.setCid(emailLookup.getContextId());
                            key.setUserid(emailLookup.getUserId());
                        }
                    }
                }
                key = Services.getService(AccountCreationService.class).createUserFor(key);
                if (key.isGuest()) {
                    String pin = msg.getPIN();
                    if (pin != null && !pin.isEmpty()) {
                        Services.getService(SecondFactorService.class).addSecondFactor(userId, cid, key.getUserid(), key.getCid(), pin);
                    }
                }
            }

            if (key != null) {
                if (key.getPubkey() == null) {
                    // Fail to create key
                    if (!draft) {
                        if (key.isPgp() == false) {
                            throw MimeEncryptorExceptionCodes.SEND_UNABLE_FIND_KEYS.create();
                        }
                    }
                }
                //Check if we have a valid key (Not revoked; Not expired)
                if(key.getEncryptionKey() == null) {
                    if (draft) {
                        continue;
                    }
                    throw MimeEncryptorExceptionCodes.SEND_UNABLE_FIND_KEYS.create();
                }
                key.setEmail(email);
                if (key.isGuest()) {
                    key.setType(msg.getRecipType(email));  // Guests need recipient type
                }
                try {
                    key.setName(recip.has("Name") ? recip.getString("Name") : recip.getString("Email"));
                } catch (Exception ex) {
                    key.setName("");
                    LOG.error("No key name set for " + email, ex);
                }
                if (key.isGuest() && !isDirectGuestRecipient(key)) {
                    guestRecips.add(key);
                } else {
                    pgpRecips.add(key);
                }
                allRecips.add(key);
            } else {
                throw MimeEncryptorExceptionCodes.SEND_UNABLE_FIND_KEYS.create();
            }
        }
        return;
    }


    public ArrayList <RecipKey> getPgpRecipients () {
        return pgpRecips;
    }

    public ArrayList <RecipKey> getGuestRecipients () {
        return guestRecips;
    }

    public ArrayList <RecipKey> getAllRecipients() {
        return allRecips;
    }

    public boolean isInline () {
        for (RecipKey key : pgpRecips) {
            if (key.isInline()) {
                return true;
            }
        }
        return false;
    }

}
