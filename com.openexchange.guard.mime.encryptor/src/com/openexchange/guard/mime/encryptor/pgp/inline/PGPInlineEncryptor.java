/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.inline;

import static com.openexchange.java.Autoboxing.I;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.bouncycastle.openpgp.PGPException;
import org.json.JSONException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.commons.RecipKey;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mime.encryptor.Encryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.osgi.Services;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.pgpcore.services.PGPCryptoService;

/**
 * {@link PGPInlineEncryptor}
 * Encrypt a Mime Message as PGP Inline
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.8.0
 */
public class PGPInlineEncryptor extends Encryptor {

    /*
     * A list of body part header names which should be preserved when encrypting body parts.
     * Common mail headers are preserved anyway see this::copyHeaders
     */
    private static List<String> preservedBodyPartHeaders = Arrays.asList(
        "X-Part-Id" /* OX Mail-compose */
    );

    public PGPInlineEncryptor(PGPCryptoService pgpCryptoService, GuardParsedMimeMessage msg, ArrayList<RecipKey> recips, int userid, int cid, String password) {
        super(pgpCryptoService, msg, recips, userid, cid, password);
    }

    /**
     * A class for returning encrypted parts of a new mime message
     * {@link PartToAdd}
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v2.8.0
     */
    private class PartToAdd {

        private final Object data;
        private final String contentType;
        private final String filename;
        private final String contentID;

        public PartToAdd(Object data, String contentType, String filename, String[] contentId) {
            this.filename = filename;
            this.data = data;
            this.contentType = contentType;
            this.contentID = (contentId != null && contentId.length > 0) ? contentId[0] : null;
        }

        public PartToAdd(Object data, String contentType) {
            this.data = data;
            this.contentType = contentType;
            this.filename = null;
            this.contentID = null;
        }

        public boolean hasFileName() {
            return (this.filename != null);
        }

        public String getFileName() {
            return this.filename;
        }

        public String getContentType() {
            return this.contentType;
        }

        public Object getData() {
            return this.data;
        }

        public String getContentId() {
            return this.contentID;
        }
    }

    /**
     * Perform the encrypt action
     *
     * @return
     * @throws Exception
     */
    @Override
    public MimeMessage doEncrypt() throws OXException {
        MimeMessage newMime = new MimeMessage(Session.getDefaultInstance(new Properties()));
        PartToAdd content;
        try {
            content = getEncrContent(msg.getMessage().getContent(), msg.getMessage().getContentType());
            if (content.getData() instanceof String) {
                newMime.setContent(content.getData(), content.getContentType());
            } else {
                newMime.setContent((Multipart) content.getData());
            }
            return copyHeaders(newMime);
        } catch (IOException | PGPException | JSONException | MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Recursively parse message for body parts and encrypt individually
     *
     * @param mimePart
     * @param type
     * @return
     * @throws OXException
     * @throws IOException
     * @throws PGPException
     * @throws JSONException
     * @throws MessagingException
     */
    private PartToAdd getEncrContent(Object mimePart, String type) throws OXException, IOException, PGPException, JSONException, MessagingException {
        // If text or HTML, just encrypt and return this part
        if (type == null || type.contains("text")) {
            if (mimePart instanceof String) {
                return new PartToAdd(doEncrypt((String) mimePart), type);
            }
            if (mimePart instanceof BodyPart) {
                final Object bodyContent = ((BodyPart) mimePart).getContent();
                if (bodyContent instanceof String) {
                    return new PartToAdd(doEncrypt((String) ((BodyPart) mimePart).getContent()), type);
                } else if (bodyContent instanceof InputStream) {
                    return new PartToAdd(doEncrypt((InputStream) ((BodyPart) mimePart).getContent()), type);
                }
            }
        }

        Object mimeContent = mimePart;
        // Check if this is a bodypart, and if so, get the content
        if (mimePart instanceof BodyPart) {
            mimeContent = ((BodyPart) mimePart).getContent();
        }

        // If multipart, get individual sections and encrypt
        if (mimeContent instanceof Multipart) {
            Multipart mp = (Multipart) mimeContent;
            // Need to make the new multipart the same type as
            Multipart newMp = new MimeMultipart(getMultipartType(mp.getContentType()));
            for (int i = 0; i < mp.getCount(); i++) {
                BodyPart newPart = new MimeBodyPart();
                BodyPart partToDecrypt = mp.getBodyPart(i);

                //Preserve body part headers
                Enumeration<Header> headers = partToDecrypt.getAllHeaders();
                ArrayList<Header> preservedHeaders = new ArrayList<>();
                while(headers.hasMoreElements()) {
                    Header header = headers.nextElement();
                    if(preservedBodyPartHeaders.contains(header.getName())) {
                        preservedHeaders.add(header);
                    }
                }

                PartToAdd content = getEncrContent(partToDecrypt, partToDecrypt.getContentType());
                if (content.getData() instanceof String) {
                    newPart.setContent(content.getData(), content.getContentType());
                    if (content.hasFileName()) {
                        newPart.setFileName(content.getFileName());
                        newPart.setHeader("Content-Transfer-Encoding", "7bit");
                        if (content.getContentId() != null) {
                            newPart.setHeader("Content-ID", content.getContentId());
                        }
                    }
                } else if (content.getData() instanceof Multipart) {
                    newPart.setContent((Multipart) content.getData());
                } else {
                    newPart.setContent(content.getData(), content.getContentType());
                }

                //Preserve body part headers
                for(Header preservedHeader : preservedHeaders) {
                   newPart.setHeader(preservedHeader.getName(), preservedHeader.getValue());
                }
                newMp.addBodyPart(newPart);
            }
            return new PartToAdd(newMp, newMp.getContentType());
        }

        // If not multipart or text, then file/image.

        if (mimeContent instanceof BodyPart) { // ? Ever fired?
            BodyPart part = (BodyPart) mimeContent;
            if (part.getFileName() != null) {
                part.setFileName(part.getFileName() + ".pgp");
            }
            part.setContent(doEncrypt((String) part.getContent()), part.getContentType());
            return new PartToAdd(part, part.getContentType());
        }

        if (mimeContent instanceof InputStream) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            doEncrypt((InputStream) mimeContent, out);
            out.close();
            return new PartToAdd(new String(out.toByteArray(), StandardCharsets.UTF_8), "application/octet-stream", ((BodyPart) mimePart).getFileName() + ".pgp", ((BodyPart) mimePart).getHeader("Content-ID"));
        }
        throw MimeEncryptorExceptionCodes.UNKONWN_BODY_TYPE.create();
    }

    /**
     * Encrypt an input stream
     *
     * @param in
     * @param encrypted
     * @throws IOException
     * @throws PGPException
     * @throws OXException
     * @throws JSONException
     */
    private void doEncrypt(InputStream in, OutputStream encrypted) throws OXException, JSONException {
        GuardKeyService gk = Services.getService(GuardKeyService.class);
        GuardKeys key = gk.getKeys(userid, cid);
        if (key != null) {
            RecipKey senderKey = new RecipKey(key);
            if (senderKey.getEncryptionKey() != null) {
                recips.add(senderKey);  // Add the sender here so it can be read in sent folder
                if (msg.isSign() && !msg.isDraft()) {
                    pgpCryptoService.encryptSigned(in, encrypted, true /* armored */, key, password, recips.toArray(new RecipKey[recips.size()]));
                } else {
                    pgpCryptoService.encrypt(in, encrypted, true /* armored */, recips.toArray(new RecipKey[recips.size()]));
                }
            }
            else {
                throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(I(userid), I(cid));
            }
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND.create();
        }
    }

    /**
     * Encrypt a string
     *
     * @param toEncrypt The data to encrypt
     * @return The encrypted Data
     * @throws OXException
     * @throws IOException
     * @throws PGPException
     * @throws JSONException
     */
    private String doEncrypt(String toEncrypt) throws OXException, IOException, JSONException {
        InputStream in = new ByteArrayInputStream(toEncrypt.getBytes(StandardCharsets.UTF_8));
        ByteArrayOutputStream encrypted = new ByteArrayOutputStream();
        doEncrypt(in, encrypted);
        in.close();
        encrypted.close();
        return (new String(encrypted.toByteArray(), StandardCharsets.UTF_8));
    }

    /**
     * Encrypts an InputStream
     *
     * @param in The InputStream to encrypt
     * @return An InputStream to read the encrypted data from
     * @throws IOException
     * @throws PGPException
     * @throws OXException
     * @throws JSONException
     */
    private String doEncrypt(InputStream in) throws OXException, JSONException {
        ByteArrayOutputStream encrypted = new ByteArrayOutputStream();
        doEncrypt(in, encrypted);
        return new String(encrypted.toByteArray(), StandardCharsets.UTF_8);
    }

    /**
     * Copy the headers from the orignal mime to a new message
     * Do not copy content-type
     *
     * @param newMessage
     * @return
     * @throws MessagingException
     */
    private MimeMessage copyHeaders(MimeMessage newMessage) throws MessagingException {
        Enumeration<Header> headers = msg.getMessage().getAllHeaders();
        while (headers.hasMoreElements()) {
            Header h = headers.nextElement();
            if (!h.getName().contains("Content-Type")) {
                newMessage.addHeader(h.getName(), h.getValue());
            }
        }
        newMessage.saveChanges();
        return newMessage;
    }

    /**
     * Get the type of multipart from a content-type string
     *
     * @param type
     * @return
     */
    private String getMultipartType(String type) {
        String mpType = type.indexOf("/") > 0 ? type.substring(type.indexOf("/") + 1) : type;
        if (mpType.indexOf(";") > 0) {
            mpType = mpType.substring(0, mpType.indexOf(";"));
        }
        return mpType;
    }
}
