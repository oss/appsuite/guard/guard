/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.encryptor.pgp.pgpmime;

import java.io.IOException;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.util.GuardMimeHeaders;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.mime.encryptor.Decryptor;
import com.openexchange.guard.mime.encryptor.exceptions.MimeEncryptorExceptionCodes;
import com.openexchange.guard.mime.encryptor.pgp.impl.HTMLStripper;
import com.openexchange.guard.pgpcore.services.FromHeaderVerifier;
import com.openexchange.guard.pgpcore.services.PGPMimeCryptoService;
import com.openexchange.guard.pgpcore.services.PGPMimeDecryptionResult;
import com.openexchange.guard.pgpcore.services.SignatureVerificationResultUtil;
import com.openexchange.guard.user.UserIdentity;
import com.openexchange.pgp.core.SignatureVerificationResult;
import com.openexchange.server.ServiceLookup;


/**
 * {@link PGPMimeDecryptor}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.8.0
 */
public class PGPMimeDecryptor extends Decryptor {

    private final ServiceLookup services;
    private final UserIdentity         userIdentity;

    /**
     * Initializes a new {@link PGPMimeDecryptor}.
     *
     * @param pgpMimeCryptoService The {@link PGPMimeCryptoService} to use for decrypting the content.
     * @param identity The identity of the user to decrypt the content for.
     */
    public PGPMimeDecryptor(ServiceLookup services, UserIdentity identity) {
        this.services = services;
        this.userIdentity = identity;
    }

    /**
     * Check for missing required headers in the encyrpted body. If missing, add from original plaintext
     *
     * @param orig
     * @param decrypted
     */
    private void checkMissingHeaders(MimeMessage orig, MimeMessage decrypted) {
        checkMissingHeader(orig, decrypted, "Date");
        checkMissingHeader(orig, decrypted, "Subject");
        checkMissingHeader(orig, decrypted, "From");
        checkMissingHeader(orig, decrypted, "To");
        checkMissingHeader(orig, decrypted, "CC");
        checkMissingHeader(orig, decrypted, "Return-Path");
    }

    /**
     * Check for missing headers in decrypted email. If not present, then add from original
     *
     * @param orig
     * @param decrypted
     */
    private void checkMissingHeader(MimeMessage orig, MimeMessage decrypted, String header) {
        try {
            if (decrypted.getHeader(header) == null) {
                if (orig.getHeader(header) != null && orig.getHeader(header).length > 0) {
                    String[] headers = orig.getHeader(header);
                    for (String data : headers) {
                        decrypted.addHeader(header, data);
                    }
                }
            }
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.mime.encryptor.Decryptor#doDecrypt(javax.mail.internet.MimeMessage)
     */
    @Override
    public MimeMessage doDecrypt(MimeMessage message) throws OXException {
        try {
            final PGPMimeDecryptionResult decryptionResult = services.getServiceSafe(PGPMimeCryptoService.class).decrypt(message, userIdentity);
            final MimeMessage decodedMsg = decryptionResult.getMimeMessage();
            final List<SignatureVerificationResult> decrypt = decryptionResult.getSignatureVerificationResults();

            checkMissingHeaders(message, decodedMsg);
            if (!decryptionResult.getMDCVerifiacionResult().isPresent()) {
                if (services.getServiceSafe(GuardConfigurationService.class).getBooleanProperty(GuardProperty.failForMissingMDC, userIdentity.getOXUser().getId(), userIdentity.getOXUser().getContextId())) {
                    throw MimeEncryptorExceptionCodes.MISSING_MDC.create();
                }
                try {
                    decodedMsg.addHeader(GuardMimeHeaders.X_GUARD_FAILED_INTEGRITY, "TRUE");
                    new HTMLStripper().stripAllHTML(decodedMsg);
                } catch (IOException e) {
                    throw GuardCoreExceptionCodes.IO_ERROR.create(e, e.getMessage());
                }
            }
            decodedMsg.removeHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT);
            //We need to compare the FROM header of the Message with the actual User ID of the signature's issuer in order ensure that the signature was really created by the sender of the email.

            FromHeaderVerifier.verify(message, decrypt);
            String[] signatures = SignatureVerificationResultUtil.toHeaders(decrypt);

            for (String sig : signatures) {
                decodedMsg.addHeader(GuardMimeHeaders.X_GUARD_SIGNATURE_RESULT, sig);
            }
            if (message.getHeader(GuardMimeHeaders.X_GUARD_SECURITY) != null) {
                decodedMsg.addHeader(GuardMimeHeaders.X_GUARD_SECURITY, message.getHeader(GuardMimeHeaders.X_GUARD_SECURITY)[0]);
            }
            return decodedMsg;
        } catch (MessagingException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, e.getMessage());
        }
    }

}
