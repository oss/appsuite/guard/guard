/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.keycache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.storage.KeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;

/**
 * {@link KeyCacheStorageImpl} provides a REST DB implementation of the KeyCacheStorage
 */
public class KeyCacheStorageImpl implements KeyCacheStorage {

    /**
     * {@link PreGeneratedKey} represents a pre generated key stored in the Database
     */
    public class PreGeneratedKey {

        private final int id;
        private final String data;

        public PreGeneratedKey(int id, String data) {
            super();
            this.id = id;
            this.data = data;
        }

        public int getId() {
            return id;
        }

        public String getData() {
            return data;
        }
    }

    /**
     * Internal method to get the next key from the DB
     *
     * @return the next key or null if no more keys are available
     */
    protected PreGeneratedKey getNextKey() throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyCacheSql.NEXT_STMT);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new PreGeneratedKey(resultSet.getInt(1), resultSet.getString(2));
            }
            return null;
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    /**
     * Deletes a pre generated key from the database
     *
     * @param key the key to delete
     * @throws OXException due an error
     */
    protected void deleteKey(PreGeneratedKey key) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(KeyCacheSql.DELETE_STMT);
            stmt.setInt(1, key.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void insert(String keyData) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(KeyCacheSql.INSERT_STMT);
            stmt.setString(1, keyData);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public String popNext() throws OXException {
        PreGeneratedKey key = getNextKey();
        if (key != null) {
            String data = key.getData();
            deleteKey(key);
            if (Strings.isEmpty(data)) {
                data = popNext();
            }
            return data;
        }
        return null;
    }

    @Override
    public int getCount() throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(KeyCacheSql.COUNT_STMT);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void wipe() throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(KeyCacheSql.WIPE);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }

    }
}
