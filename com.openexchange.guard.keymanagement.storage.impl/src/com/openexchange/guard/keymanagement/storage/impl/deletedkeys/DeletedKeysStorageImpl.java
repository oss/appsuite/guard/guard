/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.deletedkeys;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.keymanagement.commons.DeletedKey;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;

/**
 * {@link DeletedKeysStorageImpl} provides a REST DB implementation of the DeletedKeysStorage
 */
public class DeletedKeysStorageImpl implements DeletedKeysStorage {

    @Override
    public void insert(DeletedKey... deletedKeys) throws OXException {

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            stmt = connection.prepareStatement(DeletedKeysSql.INSERT_KEY_STMT);

            for (DeletedKey key : deletedKeys) {
                stmt.setInt(1, key.getUserId());
                stmt.setInt(2, key.getCid());
                stmt.setString(3, key.getEmail());
                stmt.setString(4, key.getPGPSecret());
                stmt.setString(5, key.getSalt());
                stmt.setString(6, key.getRecovery());
                stmt.setInt(7, key.getVersion());
                stmt.addBatch();
            }
            stmt.executeBatch();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void setExposedForEmail(String email, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String exposedDateString = sdf.format(new Date());

            stmt = connection.prepareStatement(DeletedKeysSql.UPDATE_KEY_SET_EXPOSED_STMT);
            stmt.setString(1, exposedDateString);
            stmt.setString(2, email);
            stmt.setInt(3, cid);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public DeletedKey getFirstForEmail(String email, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        try {
            stmt = connection.prepareStatement(DeletedKeysSql.SELECT_KEY_BY_EMAIL_CID_STMT);
            stmt.setString(1, email);
            stmt.setInt(2, cid);
            resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                return new DeletedKey(resultSet.getInt("id"), resultSet.getInt("cid"), resultSet.getString("PGPSecret"), resultSet.getString("recovery"), resultSet.getString("salt"), resultSet.getInt("version"), resultSet.getString("email"), resultSet.getBoolean("exposed"));
            }
            return null;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public int setAllUnexposed(Date before) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String beforeDate = sdf.format(before);

            stmt = connection.prepareStatement(DeletedKeysSql.UPDATE_KEY_SET_UNEXPOSED_BY_DATE_STMT);
            stmt.setString(1, beforeDate);
            int updated = stmt.executeUpdate();

            return updated;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public ArrayList<DeletedKey> getAllExposedForEmail(String email, int id, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;

        ArrayList<DeletedKey> keys = new ArrayList<DeletedKey>();
        try {
            stmt = connection.prepareStatement(DeletedKeysSql.SELECT_KEY_EXPOSED_BY_EMAIL_CID_STMT);
            stmt.setInt(1, cid);
            stmt.setString(2, email);
            stmt.setInt(3, id);
            resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                keys.add(new DeletedKey(resultSet.getInt("id"), resultSet.getInt("cid"), resultSet.getString("PGPSecret"), resultSet.getString("recovery"), resultSet.getString("salt"), resultSet.getInt("version"), resultSet.getString("email"), resultSet.getBoolean("exposed")));
            }
            return keys;
        } catch (SQLException e) {
            throw KeysExceptionCodes.DB_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }
}
