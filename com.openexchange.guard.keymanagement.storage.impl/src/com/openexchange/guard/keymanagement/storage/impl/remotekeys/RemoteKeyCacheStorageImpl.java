/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.storage.impl.remotekeys;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.openpgp.PGPPublicKey;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.keymanagement.commons.CachedKey;
import com.openexchange.guard.keymanagement.commons.exceptions.KeysExceptionCodes;
import com.openexchange.guard.keymanagement.commons.export.KeyExportUtil;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.guard.keymanagement.commons.trust.KeySourceFactory;
import com.openexchange.guard.keymanagement.storage.RemoteKeyCacheStorage;
import com.openexchange.guard.keymanagement.storage.impl.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;

public class RemoteKeyCacheStorageImpl implements RemoteKeyCacheStorage {

    private static final String RING_COLUMN = "ring";
    private static final String KEYSOURCE_COLUMN = "keysource";
    private final KeySourceFactory keySourceFactory;

    /**
     * Initializes a new {@link RemoteKeyCacheStorageImpl}.
     *
     * @param keySourceFactory The {@link KeySourceFactory} service
     */
    public RemoteKeyCacheStorageImpl(KeySourceFactory keySourceFactory) {
        this.keySourceFactory = Objects.requireNonNull(keySourceFactory, "keySourceFactory must not be null");
    }

    private KeySource toKeySource(ResultSet resultSet) throws SQLException, OXException {
        final String keySourceName = resultSet.getString(KEYSOURCE_COLUMN);
        return keySourceFactory.create(keySourceName);
    }

    private CachedKey toCachedKey(ResultSet resultSet) throws IOException, SQLException, OXException {
        return new CachedKey(
            PGPPublicKeyRingFactory.create(resultSet.getString(RING_COLUMN)),
            toKeySource(resultSet));
    }

    @Override
    public int deleteOld(int days) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        PreparedStatement stmt = null;
        Connection connection = guardDatabaseService.getWritableForGuard();
        try {
            stmt = connection.prepareStatement(RemoteKeyCacheSql.DELETE_REMOTE_KEYS_STMT);
            stmt.setInt(1, days);
            stmt.executeUpdate();
            DBUtils.closeSQLStuff(stmt);

            stmt = connection.prepareStatement(RemoteKeyCacheSql.DELETE_REMOTE_KEY_CACHE_STMT);
            stmt.setInt(1, days);
            final int affectedRows = stmt.executeUpdate();
            DBUtils.closeSQLStuff(stmt);
            return affectedRows;
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }
    }

    @Override
    public void insert(CachedKey cachedKey) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getWritableForGuard();

        PreparedStatement stmt = null;
        try {
            connection.setAutoCommit(false);
            stmt = connection.prepareStatement(RemoteKeyCacheSql.INSERT_RING_DATA_STMT);

            String keyRingAsc = KeyExportUtil.export(cachedKey.getPublicKeyRing());
            stmt.setLong(1, cachedKey.getPublicKeyRing().getPublicKey().getKeyID());
            stmt.setString(2, keyRingAsc);
            stmt.setString(3, keyRingAsc);
            stmt.executeUpdate();

            //Storing each identity related to the key ring
            Iterator<String> ids = cachedKey.getPublicKeyRing().getPublicKey().getUserIDs();
            Long refid = cachedKey.getPublicKeyRing().getPublicKey().getKeyID();
            while (ids.hasNext()) {
                String email = ids.next().trim();
                if (email.contains("@")){  // make sure has email address (Bug 45195)
                	if (email.contains("<")) {
                        email = email.substring(email.indexOf("<") + 1);
                        if (email.endsWith(">")) {
                            email = email.substring(0, email.length() - 1);
                        }
                    }
                    GuardRatifierService validatorService = Services.getService(GuardRatifierService.class);
                    validatorService.validate(email);

                    Iterator<PGPPublicKey> keys = cachedKey.getPublicKeyRing().getPublicKeys();
                    while (keys.hasNext()) {
                        PGPPublicKey key = keys.next();
                        if (!key.hasRevocation()) {

                            PreparedStatement stmt2 = null;
                            try {
                                stmt2 = connection.prepareStatement(RemoteKeyCacheSql.INSERT_RING_ID_STMT);

                                stmt2.setLong(1, key.getKeyID());
                                stmt2.setString(2, email);
                                stmt2.setLong(3, refid);
                                stmt2.setString(4, cachedKey.getKeySource().getName());
                                stmt2.setLong(5, refid);
                                stmt2.executeUpdate();
                            } finally {
                                DBUtils.closeSQLStuff(stmt2);
                            }
                        }
                    }
                }

            }
            connection.commit();
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.autocommit(connection);
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);

        }
    }

    @Override
    public List<CachedKey> getByEmail(String email) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(RemoteKeyCacheSql.SELECT_BY_EMAIL_STMT);
            stmt.setString(1, email);

            resultSet = stmt.executeQuery();
            ArrayList<CachedKey> ret = new ArrayList<CachedKey>();
            while (resultSet.next()) {
                ret.add(toCachedKey(resultSet));
            }
            return ret;
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public CachedKey getByID(Long id) throws OXException {
    	GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        Connection connection = guardDatabaseService.getReadOnlyForGuard();

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connection.prepareStatement(RemoteKeyCacheSql.SELECT_BY_ID_STMT);
            stmt.setLong(1, id);
            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
            	return toCachedKey(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } catch (IOException e) {
            throw KeysExceptionCodes.IO_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnlyForGuard(connection);
        }
    }

    @Override
    public void wipe() throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        PreparedStatement stmt = null;
        Connection connection = guardDatabaseService.getWritableForGuard();
        try {
            stmt = connection.prepareStatement(RemoteKeyCacheSql.WIPE_KEY_CACHE);
            stmt.executeUpdate();
            DBUtils.closeSQLStuff(stmt);

            stmt = connection.prepareStatement(RemoteKeyCacheSql.WIPE_REMTOE_KEYS);
            stmt.executeUpdate();
            DBUtils.closeSQLStuff(stmt);
        } catch (SQLException e) {
            throw KeysExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritableForGuard(connection);
        }

    }
}
