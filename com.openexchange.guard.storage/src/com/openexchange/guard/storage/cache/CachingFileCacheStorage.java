/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.storage.cache;

import java.io.Serializable;
import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.caching.FileCacheItem;
import com.openexchange.guard.caching.GenericCache;
import com.openexchange.guard.caching.GenericCacheFactory;
import com.openexchange.guard.caching.GuardCacheModule;

/**
 * {@link CachingFileCacheStorage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class CachingFileCacheStorage implements FileCacheStorage {

    private final FileCacheStorage delegate;
    private final GenericCache<FileCacheItem> cache;

    /**
     * Initializes a new {@link CachingFileCacheStorage}.
     *
     * @param cacheFactory The factory for creating the cache to use
     * @param delegate The delegate to enhance with caching
     * @throws OXException
     */
    public CachingFileCacheStorage(GenericCacheFactory cacheFactory, FileCacheStorage delegate) throws OXException {
        this.cache = cacheFactory.createCache(GuardCacheModule.GUEST_FILE_ITEM);
        this.delegate = delegate;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#getById(java.lang.String)
     */
    @Override
    public FileCacheItem getById(String itemId) throws OXException {
        Serializable key = cache.createKey(itemId);
        FileCacheItem item = this.cache.get(key);
        if(item == null) {
            item = delegate.getById(itemId);
            if(item != null) {
                cache.put(key, item);
            }
        }
        return item;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#getByIdForUser(java.lang.String, int)
     */
    @Override
    public FileCacheItem getByIdForUser(String itemId, int userId) throws OXException {
        Serializable key = cache.createKey(itemId);
        FileCacheItem item = this.cache.get(key);
        if(item == null) {
            item = delegate.getByIdForUser(itemId, userId);
            if(item != null) {
                cache.put(key, item);
            }
        }
        return item;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#getForUser(int, int, int)
     */
    @Override
    public List<FileCacheItem> getForUser(int userId, int cid, int offset, int maxCount) throws OXException {
        return delegate.getForUser(userId, cid, offset, maxCount);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#insert(java.lang.String, int, java.lang.String)
     */
    @Override
    public void insert(String itemId, int userId, String path) throws OXException {
        delegate.insert(itemId, userId, path);
        cache.remove(cache.createKey(itemId));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#updateLastDate(java.lang.String)
     */
    @Override
    public void updateLastDate(String itemId) throws OXException {
        delegate.updateLastDate(itemId);
        cache.remove(cache.createKey(itemId));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#delete(java.lang.String)
     */
    @Override
    public void delete(String itemId) throws OXException {
        delegate.delete(itemId);
        cache.remove(cache.createKey(itemId));
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#findOld(int)
     */
    @Override
    public List<FileCacheItem> findOld(int days) throws OXException {
        return delegate.findOld(days);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.storage.cache.FileCacheStorage#findAllForUser(int)
     */
    @Override
    public List<FileCacheItem> findAllForUser(int userId, int cid) throws OXException {
        return delegate.findAllForUser(userId, cid);
    }
}
