/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.secondfactor.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.secondfactor.exceptions.PinExceptionCodes;
import com.openexchange.guard.secondfactor.osgi.Services;
import com.openexchange.server.ServiceLookup;

/**
 * {@link PinImplementation}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class PinImplementation implements SecondFactorService {

    ServiceLookup services;

    public PinImplementation(ServiceLookup services) {
        this.services = services;
    }

    /**
     * Check if Guard configuration for pinEnabled is true
     * isEnabled
     *
     * @param userId
     * @param cid
     * @return
     */
    private boolean isEnabled(int userId, int cid) {
        GuardConfigurationService config = services.getService(GuardConfigurationService.class);
        return config.getBooleanProperty(GuardProperty.pinEnabled, userId, cid);
    }

    private String getPin (int userid, int cid) throws OXException {
        // only Guests have pin currently
        if (cid >= 0) return null;

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userid, cid, 0);
        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(PinImplSql.GET_PIN);
            stmt.setInt(1, userid);
            stmt.setInt(2, cid);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#hasSecondFactor(int, int)
     */
    @Override
    public boolean hasSecondFactor(int userid, int cid, int guardUserId, int guardCid) throws OXException {
        if (!isEnabled(userid, cid)) {
            return false;
        }
        return (getPin(guardUserId, guardCid) != null);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#addSecondFactor(int, int, java.lang.String)
     */
    @Override
    public boolean addSecondFactor(int userid, int cid, int guardUserId, int guardCid, String token) throws OXException {
        if (!isEnabled(userid, cid)) {
            return false;
        }
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(guardUserId, guardCid, 0);
        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(PinImplSql.SET_PIN);
            stmt.setInt(1, guardUserId);
            stmt.setInt(2, guardCid);
            stmt.setString(3, token);

            return stmt.execute();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#verifySecondFactor(int, int, java.lang.String)
     */
    @Override
    public void verifySecondFactor(int userid, int cid, int guardUserId, int guardCid, String token) throws OXException {
        if (!isEnabled(userid, cid)) {
            return;
        }
        if (!token.equals(getPin(guardUserId, guardCid))) {
            throw PinExceptionCodes.BAD_PIN.create();

        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#removeSecondFactor(int, int)
     */
    @Override
    public void removeSecondFactor(int userid, int cid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(userid, cid, 0);
        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(PinImplSql.DELETE_PIN);
            stmt.setInt(1, userid);
            stmt.setInt(2, cid);
            stmt.execute();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e,e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.secondfactor.SecondFactorService#removeSecondFactorIfSingleUse(int, int)
     */
    @Override
    public void removeSecondFactorIfSingleUse(int userid, int cid) throws OXException {
        // pin is currently always single use
        removeSecondFactor(userid, cid);

    }

}
