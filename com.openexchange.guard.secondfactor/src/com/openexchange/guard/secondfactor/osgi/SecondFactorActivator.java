package com.openexchange.guard.secondfactor.osgi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.secondfactor.impl.PinImplementation;
import com.openexchange.osgi.HousekeepingActivator;

public class SecondFactorActivator extends HousekeepingActivator {

    /**
     * Initialises a new {@link GuardRatifierActivator}.
     */
    public SecondFactorActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardDatabaseService.class, GuardConfigurationService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        Services.setServiceLookup(this);
        final Logger logger = LoggerFactory.getLogger(SecondFactorService.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());

        logger.info("Registering Second Factor Service");
        registerService(SecondFactorService.class, new PinImplementation(this));

    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(SecondFactorService.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        logger.info("Unregistering Second Factor Service");
        unregisterService(SecondFactorService.class);

        super.stopBundle();
    }
}
