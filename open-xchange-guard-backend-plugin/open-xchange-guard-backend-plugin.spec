
Name:          open-xchange-guard-backend-plugin
BuildArch:     noarch
BuildRequires: ant
BuildRequires: open-xchange-admin >= 7.10.0
BuildRequires: open-xchange-rest >= 7.10.0
BuildRequires: java-1.8.0-openjdk-devel
Version:       @OXVERSION@
%define        ox_release 4
Release:       %{ox_release}_<CI_CNT>.<B_CNT>
Group:         Applications/Productivity
License:       GPL-2.0
BuildRoot:     %{_tmppath}/%{name}-%{version}-build
URL:           http://www.open-xchange.com/
Source:        %{name}_%{version}.orig.tar.bz2
Summary:       OX Guard backend component
Autoreqprov:   no
Requires:      open-xchange-admin >= 7.10.0
Requires:      open-xchange-rest >= 7.10.0
Provides:      open-xchange-guard-backend = 7.6.2-8
Obsoletes:     open-xchange-guard-backend < 7.6.2-8

%description
This package adds the bundles to the backend needed to operate the OX
Guard product.

Authors:
--------
    Open-Xchange

%prep

%setup -q

%build

%install
export NO_BRP_CHECK_BYTECODE_VERSION=true
ant -lib build/lib -Dbasedir=build -DdestDir=%{buildroot} -DpackageName=%{name} -f build/build.xml clean build

%post
. /opt/open-xchange/lib/oxfunctions.sh

# SoftwareChange_Request-2429
ox_add_property com.openexchange.guard.endpoint "" /opt/open-xchange/etc/guard-api.properties

# SoftwareChange_Request-2884
ox_move_config_file /opt/open-xchange/etc /opt/open-xchange/etc guard.properties guard-api.properties

# SCR-171
conffile=/opt/open-xchange/etc/guard-api.properties
ox_add_property com.openexchange.guard.endpointTimeout 15000 ${conffile}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%dir /opt/open-xchange/bundles/
/opt/open-xchange/bundles/*
%dir /opt/open-xchange/osgi/bundle.d/
/opt/open-xchange/osgi/bundle.d/*
%dir /opt/open-xchange/etc/
%config(noreplace) /opt/open-xchange/etc/guard-api.properties

%changelog
* Thu Aug 11 2022 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.7 release
* Thu Aug 11 2022 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.7 release
* Mon Jul 04 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-07-11 (6145)
* Thu Jun 02 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-05-30 (6138)
* Wed Mar 16 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-03-21 (6116)
* Mon Feb 21 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-02-22 (6107)
* Mon Feb 14 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-02-15 (6099)
* Mon Jan 17 2022 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2022-01-24
* Mon Nov 29 2021 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.6 release
* Fri Oct 22 2021 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.6 release
* Wed Jun 09 2021 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.6 release
* Wed Apr 21 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-04-27 (5984)
* Tue Mar 23 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-03-29 (5979)
* Thu Feb 11 2021 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2021-02-22 (5954)
* Mon Feb 01 2021 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.10.5 release
* Fri Jan 15 2021 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.5 release
* Thu Dec 17 2020 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.5 release
* Mon Nov 30 2020 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.5 release
* Tue Oct 06 2020 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.5 release
* Tue Jul 28 2020 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.4 release
* Tue Jun 30 2020 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.4 release
* Wed May 20 2020 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.4 release
* Mon Feb 03 2020 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.4
* Thu Nov 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate of 2.10.3 release
* Thu Nov 21 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 2.10.3 release
* Thu Oct 17 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.3 release
* Wed Jun 19 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.3 release
* Fri May 10 2019 Marcus Klein <marcus.klein@open-xchange.com>
First candidate of 2.10.2 release
* Wed May 01 2019 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.2 release
* Thu Mar 28 2019 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.2 release
* Mon Mar 11 2019 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.2
* Fri Nov 23 2018 Marcus Klein <marcus.klein@open-xchange.com>
RC 1 for 2.10.1 release
* Mon Nov 05 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 2.10.1 release
* Fri Oct 12 2018 Marcus Klein <marcus.klein@open-xchange.com>
First preview for 2.10.1 release
* Mon Sep 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.1
* Mon Jun 25 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.10.0 release
* Mon Jun 11 2018 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.10.0 release
* Fri May 18 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 2.10.0 release
* Thu Apr 19 2018 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 2.10.0 release
* Tue Apr 03 2018 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 2.10.0 release
* Tue Feb 20 2018 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.10.0 release
* Fri Feb 02 2018 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.10.0 release
* Wed Jan 10 2018 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.10.0 release
* Tue May 16 2017 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.8.0 release
* Thu May 04 2017 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.8.0 release
* Mon Apr 03 2017 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.8.0 release
* Fri Dec 02 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.8.0 release
* Fri Nov 25 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second release candidate for 2.6.0 release
* Thu Nov 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First release candidate for 2.6.0 release
* Tue Nov 15 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third preview of 2.6.0 release
* Sat Oct 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview of 2.6.0 release
* Fri Oct 14 2016 Marcus Klein <marcus.klein@open-xchange.com>
First preview of 2.6.0 release
* Wed Oct 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.6.0 release
* Tue Jul 12 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.2 release
* Wed Jul 06 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.2 release
* Wed Jun 29 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second preview for 2.4.2 release
* Thu Jun 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.2 release
* Thu Jun 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.4.2
* Wed Mar 30 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.0 release
* Thu Mar 24 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.0 release
* Wed Mar 16 2016 Marcus Klein <marcus.klein@open-xchange.com>
Fifth preview of 2.4.0 release
* Fri Mar 04 2016 Marcus Klein <marcus.klein@open-xchange.com>
Fourth preview of 2.4.0 release
* Sat Feb 20 2016 Marcus Klein <marcus.klein@open-xchange.com>
Third candidate for 2.4.0 release
* Fri Feb 05 2016 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.4.0 release
* Fri Feb 05 2016 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.4.0 release
* Sun Dec 06 2015 Greg <greg@ubuntu>
Second candidate for 2.2.1 release
* Mon Nov 09 2015 Greg <greg@ubuntu>
First candidate for 2.2.1 release
* Mon Nov 09 2015 Greg <greg@ubuntu>
First release 2.2.1
* Fri Oct 23 2015 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.4.0 release
* Mon Oct 05 2015 Marcus Klein <marcus.klein@open-xchange.com>
Sixth candadate release for 2.2.0
* Sun Sep 27 2015 Marcus Klein <marcus.klein@open-xchange.com>
Fifth candidate release for 2.2.0
* Mon Sep 21 2015 Marcus Klein <marcus.klein@open-xchange.com>
Fourth candidate release 2.2.0
* Mon Sep 14 2015 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2015-09-24 (2756)
* Mon Sep 14 2015 Marcus Klein <marcus.klein@open-xchange.com>
Third candidate release 2.2.0
* Mon Aug 24 2015 Marcus Klein <marcus.klein@open-xchange.com>
Second candidate for 2.2.0 release
* Mon Aug 10 2015 Marcus Klein <marcus.klein@open-xchange.com>
First candidate for 2.2.0 release
* Mon Aug 10 2015 Marcus Klein <marcus.klein@open-xchange.com>
prepare for 2.2.0 release
* Wed Jul 29 2015 Marcus Klein <marcus.klein@open-xchange.com>
Build for patch 2015-07-27 (2626)
* Wed Jul 22 2015 Marcus Klein <marcus.klein@open-xchange.com>
move package for the Guard backend component to the Guard repository
