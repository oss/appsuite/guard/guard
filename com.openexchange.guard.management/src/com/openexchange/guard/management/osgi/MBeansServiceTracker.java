/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.osgi;

import javax.management.ObjectName;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.GuardMaintenanceService;
import com.openexchange.guard.management.maintenance.GuardMaintenanceMBean;
import com.openexchange.guard.management.maintenance.internal.GuardMaintenanceMBeanImpl;
import com.openexchange.guard.management.statistics.GuardServerStatisticsMBean;
import com.openexchange.guard.management.statistics.internal.GuardServerStatisticsMBeanImpl;
import com.openexchange.management.ManagementService;

/**
 * {@link MBeansServiceTracker}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class MBeansServiceTracker implements ServiceTrackerCustomizer<ManagementService, ManagementService> {

    private BundleContext context;

    private ObjectName statisticsObjectName;

    private ObjectName maintenanceObjectName;

    private GuardMaintenanceService guardMaintenanceService;

    /**
     * Initialises a new {@link MBeansServiceTracker}.
     */
    public MBeansServiceTracker(BundleContext context, GuardMaintenanceService guardMaintenanceService) {
        super();
        this.context = context;
        this.guardMaintenanceService = guardMaintenanceService;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
     */
    @Override
    public ManagementService addingService(ServiceReference<ManagementService> reference) {
        Logger logger = LoggerFactory.getLogger(MBeansServiceTracker.class);
        ManagementService managementService = context.getService(reference);
        try {
            statisticsObjectName = new ObjectName(GuardServerStatisticsMBean.DOMAIN, GuardServerStatisticsMBean.KEY, GuardServerStatisticsMBean.VALUE);
            managementService.registerMBean(statisticsObjectName, new GuardServerStatisticsMBeanImpl());
            logger.info("GuardServerStatisticsMBean successfully registered");

            maintenanceObjectName = new ObjectName(GuardMaintenanceMBean.DOMAIN, GuardMaintenanceMBean.KEY, GuardMaintenanceMBean.VALUE);
            managementService.registerMBean(maintenanceObjectName, new GuardMaintenanceMBeanImpl(this.guardMaintenanceService));
            logger.info("GuardMaintenanceMBean successfully registered");

            return managementService;
        } catch (Exception e) {
            logger.error("Could not register GuardServerStatisticsMBean", e);
        }

        context.ungetService(reference);
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference, java.lang.Object)
     */
    @Override
    public void modifiedService(ServiceReference<ManagementService> reference, ManagementService service) {
        // no-op

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference, java.lang.Object)
     */
    @Override
    public void removedService(ServiceReference<ManagementService> reference, ManagementService service) {
        Logger logger = LoggerFactory.getLogger(MBeansServiceTracker.class);
        if (service != null) {
            if (statisticsObjectName != null) {
                try {
                    service.unregisterMBean(statisticsObjectName);
                    logger.info("GuardServerStatisticsMBean successfully unregistered");
                } catch (OXException e) {
                    logger.warn("Could not unregister GuardServerStatisticsMBean", e);
                }
            }
            if (maintenanceObjectName != null) {
                try {
                    service.unregisterMBean(maintenanceObjectName);
                    logger.info("GuardMaintenanceMBean successfully unregistered");
                } catch (OXException e) {
                    logger.warn("Could not unregister GuardMaintenanceMBean", e);
                }
            }
        }
    }
}
