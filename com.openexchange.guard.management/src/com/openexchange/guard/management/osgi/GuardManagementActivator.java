/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.management.osgi;

import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.GuardMaintenanceService;
import com.openexchange.guard.oxapi.pooling.HttpConnectionPoolService;
import com.openexchange.management.ManagementService;
import com.openexchange.osgi.HousekeepingActivator;

/**
 * {@link GuardManagementActivator}
 *
 * @author <a href="mailto:ioannis.chouklis@open-xchange.com">Ioannis Chouklis</a>
 */
public class GuardManagementActivator extends HousekeepingActivator {

    private ServiceTracker<ManagementService, ManagementService> mbeansTracker;

    /**
     * Initialises a new {@link GuardManagementActivator}.
     */
    public GuardManagementActivator() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#getNeededServices()
     */
    @Override
    protected Class<?>[] getNeededServices() {
        return new Class<?>[] { GuardMaintenanceService.class, HttpConnectionPoolService.class };
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.DeferredActivator#startBundle()
     */
    @Override
    protected void startBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardManagementActivator.class);
        logger.info("Starting bundle {}", context.getBundle().getSymbolicName());
        Services.setServiceLookup(this);

        registerMBeans();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.osgi.HousekeepingActivator#stopBundle()
     */
    @Override
    protected void stopBundle() throws Exception {
        final Logger logger = LoggerFactory.getLogger(GuardManagementActivator.class);
        logger.info("Stopping bundle {}", context.getBundle().getSymbolicName());

        // Unregistering mbean
        if (mbeansTracker != null) {
            mbeansTracker.close();
            mbeansTracker = null;
        }

        Services.setServiceLookup(null);
        super.stopBundle();
    }

    /**
     * Register the Guard MBeans
     */
    private void registerMBeans() {
        ServiceTrackerCustomizer<ManagementService, ManagementService> customizer = new MBeansServiceTracker(context, getService(GuardMaintenanceService.class));
        mbeansTracker = new ServiceTracker<ManagementService, ManagementService>(context, ManagementService.class, customizer);
        mbeansTracker.open();
    }
}
