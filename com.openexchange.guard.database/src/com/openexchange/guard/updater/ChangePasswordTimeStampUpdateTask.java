/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.updater;

import java.sql.Connection;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;
import com.openexchange.guard.database.update.GuardCreateTableTask;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.tools.update.Column;
import com.openexchange.tools.update.Tools;

/**
 * {@link ChangePasswordTimeStampUpdateTask}
 *
 * Updates the lastUp column to store time information (not only date)
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.2
 */
public class ChangePasswordTimeStampUpdateTask extends UpdateTaskAdapter {

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#perform(com.openexchange.groupware.update.PerformParameters)
     */
    @Override
    public void perform(PerformParameters params) throws OXException {
        final Logger log = LoggerFactory.getLogger(ChangePasswordTimeStampUpdateTask.class);
        log.info("Performing update task {}", ChangePasswordTimeStampUpdateTask.class.getSimpleName());
        Connection connection = params.getConnection();
        boolean rollback = false;
        try {
            connection.setAutoCommit(false);
            rollback = true;

            Column column = new Column("lastMod", "datetime");
            Tools.checkAndModifyColumns(connection, "og_KeyTable", column);

            connection.commit();
            rollback = false;
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        }
        finally {
            if(rollback) {
               DBUtils.rollback(connection);
            }
            DBUtils.autocommit(connection);
        }
        log.info("{} successfully performed.", ChangePasswordTimeStampUpdateTask.class.getSimpleName());
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#getDependencies()
     */
    @Override
    public String[] getDependencies() {
        return new String[] { GuardCreateTableTask.class.getName() };
    }

}
