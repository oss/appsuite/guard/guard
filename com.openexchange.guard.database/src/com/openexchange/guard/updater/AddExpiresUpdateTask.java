/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.updater;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.PerformParameters;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.groupware.update.UpdateTaskAdapter;
import com.openexchange.guard.database.update.GuardCreateTableTask;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.pgp.keys.parsing.PGPPublicKeyRingFactory;
import com.openexchange.tools.update.Column;
import com.openexchange.tools.update.Tools;

/**
 * {@link AddExpiresUpdateTask}
 *
 * Updates the og_KeyTable to add the expires column and triggers population task
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v8.22
 */
public class AddExpiresUpdateTask extends UpdateTaskAdapter {

    private static String SELECT_KEYS = "SELECT id, cid, keyid, PGPPublic FROM og_KeyTable";
    private static String UPDATE = "UPDATE og_KeyTable SET expires = ? WHERE id = ? AND cid = ? AND keyid = ?";

    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOG = org.slf4j.LoggerFactory.getLogger(AddExpiresUpdateTask.class);
    }

    /**
     * Simple class to hold info necessary for update task
     * {@link Datablock}
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v8.22
     */
    private class Datablock {

        private int userid;
        private int cid;
        private long keyId;
        private Date expires;

        public Datablock(int userid, int cid, long keyId, Date expires) {
            super();
            this.userid = userid;
            this.cid = cid;
            this.keyId = keyId;
            this.expires = expires;
        }

        /**
         * Gets the userid
         *
         * @return The userid
         */
        public int getUserid() {
            return userid;
        }

        /**
         * Gets the cid
         *
         * @return The cid
         */
        public int getCid() {
            return cid;
        }

        /**
         * Gets the expires
         *
         * @return The expires
         */
        public Date getExpires() {
            return expires;
        }

        /**
         * Gets the keyId
         *
         * @return The keyId
         */
        public long getKeyId() {
            return keyId;
        }

    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#perform(com.openexchange.groupware.update.PerformParameters)
     */
    @Override
    public void perform(PerformParameters params) throws OXException {
        final Logger log = LoggerFactory.getLogger(AddExpiresUpdateTask.class);
        log.info("Performing update task {}", AddExpiresUpdateTask.class.getSimpleName());
        Connection connection = params.getConnection();
        boolean rollback = false;
        try {
            connection.setAutoCommit(false);
            rollback = true;

            Column column = new Column("expires", "date DEFAULT NULL");
            Tools.checkAndAddColumns(connection, "og_KeyTable", column);

            connection.commit();
            rollback = false;
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        }
        finally {
            if(rollback) {
                DBUtils.rollback(connection);
            }
            DBUtils.autocommit(connection);
        }
        doUpdateExpiresColumn(connection);
        log.info("{} successfully performed.", AddExpiresUpdateTask.class.getSimpleName());
    }

    /* (non-Javadoc)
     * @see com.openexchange.groupware.update.UpdateTaskV2#getDependencies()
     */
    @Override
    public String[] getDependencies() {
        return new String[] { GuardCreateTableTask.class.getName() };
    }

    /**
     * Method to populate the data in the newly added expires column
     *
     * @param con Active database connection
     * @throws OXException
     */
    private void doUpdateExpiresColumn(Connection con) throws OXException {
        List<Datablock> items = new ArrayList<Datablock>();

        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(SELECT_KEYS);
            rs = stmt.executeQuery();
            // Loop through existing keys and get expiration dates.  Store for later update
            while (rs.next()) {
                try {
                    int userId = rs.getInt("id");
                    int cid = rs.getInt("cid");
                    long keyId = rs.getLong("keyid");
                    String pubKey = rs.getString("PGPPublic");
                    PGPPublicKeyRing keyRing = PGPPublicKeyRingFactory.create(pubKey);
                    if (keyRing.getPublicKey().getValidSeconds() != 0) {  // 0 indicated no expiration date set
                        long expiration = keyRing.getPublicKey().getCreationTime().getTime() + (keyRing.getPublicKey().getValidSeconds() * 1000);
                        items.add(new Datablock(userId, cid, keyId, new Date(expiration)));
                    }
                } catch (IOException e) {
                    LoggerHolder.LOG.error("Error loading keys for updating expiration date", e);
                }
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(rs, stmt);
        }
        // Loop through items and update database entries
        if (!items.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                Datablock data = items.get(i);
                PreparedStatement updateStmt = null;
                try {
                    updateStmt = con.prepareStatement(UPDATE);
                    updateStmt.setDate(1, data.getExpires());
                    updateStmt.setInt(2, data.getUserid());
                    updateStmt.setInt(3, data.getCid());
                    updateStmt.setLong(4, data.getKeyId());
                    updateStmt.execute();
                } catch (SQLException ex) {
                    // Failed to update this one, but try others.  Non fatal
                    LoggerHolder.LOG.error("Problem updating expiration date", ex);
                } finally {
                    DBUtils.closeSQLStuff(updateStmt);
                }
            }
        }
    }

}
