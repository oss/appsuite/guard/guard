/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.database.custom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.commons.lang3.Validate;
import com.openexchange.guard.database.utils.DBUtils;
import liquibase.database.Database;
import liquibase.database.DatabaseConnection;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomPreconditionErrorException;
import liquibase.exception.CustomPreconditionFailedException;
import liquibase.precondition.CustomPrecondition;

/**
 * Verifies the size of the database column by ignoring the type of the column!
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since 2.4.0
 */
public class InitGlobalSettingsPrecondition implements CustomPrecondition {

    /**
     * {@inheritDoc}
     */
    @Override
    public void check(final Database database) throws CustomPreconditionErrorException, CustomPreconditionFailedException {
        PreparedStatement prepareStatement = null;
        ResultSet resultSet = null;
        try {
            Validate.notNull(database, "Database provided by Liquibase might not be null!");

            DatabaseConnection databaseConnection = database.getConnection();
            Validate.notNull(databaseConnection, "DatabaseConnection might not be null!");

            JdbcConnection connection = null;
            if (databaseConnection instanceof JdbcConnection) {
                connection = (JdbcConnection) databaseConnection;
            } else {
                throw new CustomPreconditionErrorException("Cannot get underlying connection because database connection is not from type JdbcConnection. Type is: " + databaseConnection.getClass().getName());
            }

            Connection underlyingConnection = connection.getUnderlyingConnection();

            prepareStatement = underlyingConnection.prepareStatement("SELECT COUNT(*) AS rowcount FROM global_settings WHERE `key` LIKE 'version';");
            resultSet = prepareStatement.executeQuery();
            if (resultSet.next()) {
                int size = resultSet.getInt("rowcount");
                if (size > 0) {
                    throw new CustomPreconditionFailedException("Version entry already exists. Nothing to do.");
                }
            }
        } catch (CustomPreconditionErrorException e) {
            throw e;
        } catch (CustomPreconditionFailedException e) {
            throw e;
        } catch (Exception e) {
            throw new CustomPreconditionErrorException("Unexpected error", e);
        } finally {
            DBUtils.closeSQLStuff(resultSet, prepareStatement);
        }
    }
}
