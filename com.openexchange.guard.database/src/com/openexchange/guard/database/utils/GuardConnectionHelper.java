
package com.openexchange.guard.database.utils;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.GuardShardingService;
import com.openexchange.guard.database.exception.GuardDatabaseExceptionCodes;
import com.openexchange.guard.database.internal.Services;
import com.openexchange.server.ServiceExceptionCode;

/**
 * {@link GuardConnectionHelper} Helper class for handling Guard connections to all databases
 *
 * Because OX Guard uses three different connections, transaction safety is not guaranteed over all connections
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardConnectionHelper implements AutoCloseable {

    private final Connection writableUserConnection;
    private final Connection writableGuardConnection;
    private final Connection writableGuestConnection;
    private final int userContextId;
    private boolean commited;
    private int shard;

    /**
     * Initializes a new {@link GuardConnectionHelper}.
     * 
     * @param userContextId The context id of the user
     * @param guestContextId The context id of the guest
     * @param guestUserId the user id of the guest
     * @throws OXException
     */
    public GuardConnectionHelper(int userContextId, int guestContextId, int guestUserId) throws OXException {
        this.userContextId = userContextId;

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        if (guardDatabaseService == null) {
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(GuardDatabaseService.class);
        }
        GuardShardingService shardingService = Services.getService(GuardShardingService.class);
        if (shardingService == null) {
            throw ServiceExceptionCode.SERVICE_UNAVAILABLE.create(GuardShardingService.class);
        }

        try {
            writableUserConnection = guardDatabaseService.getWritable(userContextId);
            writableGuardConnection = guardDatabaseService.getWritableForGuard();
            shard = shardingService.getShard(guestUserId, guestContextId);
            writableGuestConnection = guardDatabaseService.getWritableForShard(shard);
        } catch (Exception e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Starts the transaction for all connections
     * 
     * @throws OXException
     */
    public void start() throws OXException {
        try {
            DBUtils.startTransaction(writableUserConnection);
            DBUtils.startTransaction(writableGuardConnection);
            DBUtils.startTransaction(writableGuestConnection);
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Commits the transactions for all connections
     * 
     * @throws OXException
     */
    public void commit() throws OXException {
        try {
            writableUserConnection.commit();
            writableGuardConnection.commit();
            writableGuestConnection.commit();
            commited = true;
        } catch (SQLException e) {
            throw GuardDatabaseExceptionCodes.DB_ERROR.create(e, e.getMessage());
        }
    }

    /**
     * Cleans up all connections and, if commit() has not been called, performs a rollback
     * 
     * @throws OXException
     */
    public void finish() throws OXException {
        if (!commited) {
            DBUtils.rollback(writableUserConnection);
            DBUtils.rollback(writableGuardConnection);
            DBUtils.rollback(writableGuestConnection);
        }
        DBUtils.autocommit(writableUserConnection);
        DBUtils.autocommit(writableGuardConnection);
        DBUtils.autocommit(writableGuestConnection);

        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);
        guardDatabaseService.backWritable(userContextId, writableUserConnection);
        guardDatabaseService.backWritableForGuard(writableGuardConnection);
        guardDatabaseService.backWritableForShard(shard, writableGuestConnection);
    }

    /**
     * @return The connection to the OX user database
     */
    public Connection getUserConnection() {
        return writableUserConnection;
    }

    /**
     * @return The connection to the guard main database
     */
    public Connection getGuardConncetion() {
        return writableGuardConnection;
    }

    /**
     * The connection to the guard sharding database
     * 
     * @return
     */
    public Connection getGuestConnection() {
        return writableGuestConnection;
    }

    @Override
    public void close() throws Exception {
        finish();
    }
}
