/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.database.update.shardUpdate.updates;

import java.sql.Connection;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.groupware.update.UpdateExceptionCodes;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.update.shardUpdate.ShardUpdateTask;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.tools.update.Column;
import com.openexchange.tools.update.Tools;

/**
 * * {@link ExpiresUpdateTask}
 * Update task for Guard Guest shards
 * Add expires column. This update task does not populate the expiration
 * date for guest accounts.
 * 
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v8.22
 */
public class ExpiresUpdateTask extends ShardUpdateTask {

    GuardDatabaseService dbService;

    public ExpiresUpdateTask(GuardDatabaseService dbService) {
        this.name = "Add Expires";
        this.dbService = dbService;
    }

    @Override
    protected boolean doUpdate(int shardId) throws OXException {
        boolean rollback = false;
        Connection connection = dbService.getWritableForShard(shardId);
        try {
            connection.setAutoCommit(false);
            rollback = true;

            Column column = new Column("expires", "date DEFAULT NULL");
            Tools.checkAndAddColumns(connection, "og_KeyTable", column);

            connection.commit();
            rollback = false;
            return true;
        } catch (SQLException e) {
            throw UpdateExceptionCodes.SQL_PROBLEM.create(e, e.getMessage());
        } finally {
            if (rollback) {
                DBUtils.rollback(connection);
            }
            DBUtils.autocommit(connection);
            dbService.backReadOnlyForShard(shardId, connection);
        }
    }

}
