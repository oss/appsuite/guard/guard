/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.util.Date;
import org.bouncycastle.asn1.cmp.PKIBody;
import org.bouncycastle.asn1.crmf.CertReqMessages;
import org.bouncycastle.asn1.crmf.SubsequentMessage;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.cert.cmp.CMPException;
import org.bouncycastle.cert.cmp.ProtectedPKIMessage;
import org.bouncycastle.cert.cmp.ProtectedPKIMessageBuilder;
import org.bouncycastle.cert.crmf.CRMFException;
import org.bouncycastle.cert.crmf.CertificateRequestMessage;
import org.bouncycastle.cert.crmf.PKMACBuilder;
import org.bouncycastle.cert.crmf.jcajce.JcaCertificateRequestMessageBuilder;
import org.bouncycastle.cert.crmf.jcajce.JcePKMACValuesCalculator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import com.openexchange.exception.OXException;

/**
 * {@link CRMFSigningRequest} Used to generate a certificate CRMF signing request
 * for a new S/Mime certificate
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CRMFSigningRequest {


    /**
     * 
     * {@link CRMFSigningRequestBuilder}
     * Builder for {@link CRMFSigningRequest}. Requires minimum of KeyPair and email address
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v4.0.0
     */
    public static class CRMFSigningRequestBuilder {

        private String common_name;
        private String organization;
        private String email;
        private KeyPair keyPair;
        private boolean withEncryption;
        private boolean raVerified;


        /**
         * Sets the raVerified
         *
         * @param raVerified The raVerified to set
         */
        public CRMFSigningRequestBuilder setRaVerified(boolean raVerified) {
            this.raVerified = raVerified;
            return this;
        }

        /**
         * Sets the withEncryption
         *
         * @param withEncryption The withEncryption to set
         */
        public CRMFSigningRequestBuilder setWithEncryption(boolean withEncryption) {
            this.withEncryption = withEncryption;
            return this;
        }

        /**
         * Sets the common_name
         *
         * @param common_name The common_name to set
         */
        public CRMFSigningRequestBuilder setCommon_name(String common_name) {
            this.common_name = common_name;
            return this;
        }

        /**
         * Sets the organization. Optional
         *
         * @param organization The organization to set
         */
        public CRMFSigningRequestBuilder setOrganization(String organization) {
            this.organization = organization;
            return this;
        }


        /**
         * Sets the email
         *
         * @param email The email to set
         */
        public CRMFSigningRequestBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        
        /**
         * Sets the keypair used for the csr
         * setKeyPair
         *
         * @param keypair
         */
        public CRMFSigningRequestBuilder setKeyPair(KeyPair keypair) {
            this.keyPair = keypair;
            return this;
        }

        /**
         * Builds the {@link CRMFSigningRequest}.
         * Requires minimum of KeyPair and email address
         *
         * @return The {@link CRMFSigningRequest}.
         * @throws OXException
         */
        public CRMFSigningRequest build() throws OXException {
            // Check minimum parameters set
            if (keyPair == null) {
                throw OXException.mandatoryField("KeyPair missing");
            }
            if (common_name == null && email == null) {
                throw OXException.mandatoryField("email missing");
            }
            if (common_name == null) {  // email can be used for common_name
                common_name = email;
            }

            // Build csr
            X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE).addRDN(BCStyle.CN, common_name);
            if (organization != null) {
                builder.addRDN(BCStyle.ORGANIZATION_IDENTIFIER, organization);
            }

            final X500Name subject = builder.build();

            try {

                BigInteger certReqID = BigInteger.valueOf(System.currentTimeMillis());
                JcaCertificateRequestMessageBuilder certReqBuild = new JcaCertificateRequestMessageBuilder(certReqID);

                certReqBuild
                    .setPublicKey(keyPair.getPublic())
                    .setSubject(subject)
                    .addExtension(org.bouncycastle.asn1.x509.Extension.subjectAlternativeName, false,
                        new GeneralNames(
                            new GeneralName(
                                GeneralName.rfc822Name,
                                email)).getEncoded());
                if (withEncryption) {
                    certReqBuild.setProofOfPossessionSubsequentMessage(SubsequentMessage.encrCert);
                }
                if (raVerified) {
                    certReqBuild.setProofOfPossessionRaVerified();
                }

                CertificateRequestMessage request = certReqBuild.build();
                return new CRMFSigningRequest(request, certReqID);
            } catch (IOException | CRMFException e) {
                throw OXException.general("Unable to generate CSR", e);
            }

        }
    }

    private final CertificateRequestMessage request;
    private BigInteger id;

    /**
     * Initializes a new {@link CRMFSigningRequest}.
     * 
     * @param request
     */
    public CRMFSigningRequest(CertificateRequestMessage request, BigInteger id) {
        super();
        this.request = request;
        this.id = id;
    }

    public CertificateRequestMessage getRequest() {
        return request;
    }

    /**
     * Gets the id sent with the request
     * getId
     *
     * @return
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Gets a PKI Protected message containing the request
     * getPKIProtected
     *
     * @param nonce A noonce to use in the request
     * @param sender X500Name of the sender
     * @param recipient X500Name of the recipient
     * @param password Password to use
     * @return
     * @throws OXException
     */
    public ProtectedPKIMessage getPKIProtected(byte[] nonce, X500Name sender, X500Name recipient, String password) throws OXException {
        try {
            return new ProtectedPKIMessageBuilder(new GeneralName(sender), new GeneralName(recipient))
                .setMessageTime(new Date())
                .setSenderNonce(nonce)
                .setBody(new PKIBody(PKIBody.TYPE_INIT_REQ, new CertReqMessages(request.toASN1Structure())))
                .build(new PKMACBuilder(new JcePKMACValuesCalculator().setProvider(BouncyCastleProvider.PROVIDER_NAME)).build(password.toCharArray()));
        } catch (CMPException | CRMFException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw OXException.general("Problem createing PKIProctected CRMF", e);
        }
    }

}
