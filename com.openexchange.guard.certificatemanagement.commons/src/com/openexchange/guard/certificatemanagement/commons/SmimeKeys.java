/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import static com.openexchange.guard.certificatemanagement.commons.SmimeKeyUsageUtil.supportsKeyUsage;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;

/**
 * {@link SmimeKeys} - Represents a S/MIME {@link X509Certificate} including it's public key.
 * <p>
 * In addition, an optional private key in the form of {@link SmimePrivateKeys} can be present.
 * </p>
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class SmimeKeys {

    private final X509Certificate publicCert;
    private final List<X509Certificate> certs;
    private final int userId;
    private final int contextId;
    private final String email;

    private SmimePrivateKeys pKey;
    private boolean isLocal;
    private final long lastUpdate;

    /**
     * Initializes a new {@link SmimeKeys}.
     *
     * @param publicCert The certificate
     * @param certs The additional certificates
     * @param pKey The private key
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param email The primary email address associated with the key
     * @param lastUpdate the last update timestamp (milliseconds since January 1, 1970, 00:00:00 GMT), or <code>0</code> if the last update timestamp is unknown
     */
    public SmimeKeys(X509Certificate publicCert, List<X509Certificate> certs, SmimePrivateKeys pKey, int userId, int contextId, String email, long lastUpdate) {
        this.publicCert = Objects.requireNonNull(publicCert, "publicCert must not be null");
        this.certs = certs == null ? new ArrayList<X509Certificate>() : certs;
        this.pKey = pKey;
        this.isLocal = (pKey != null);
        this.userId = userId;
        this.contextId = contextId;
        this.email = email;
        this.lastUpdate = lastUpdate;
    }

    /**
     *
     * Initializes a new {@link SmimeKeys}.
     *
     * @param publicCert The certificate
     * @param certs The additional certificates
     * @param isLocal <code>True</code> if this certificate belongs to a local user, <code>False</code> otherwise
     * @param userId The user identifier
     * @param contextId The context identifier
     * @param email The primary email address associated with the key
     * @param lastUpdate the last update timestamp (milliseconds since January 1, 1970, 00:00:00 GMT), or <code>0</code> if the last update timestamp is unknown
     */
    public SmimeKeys(X509Certificate publicCert, List<X509Certificate> certs, boolean isLocal, int userId, int contextId, String email, long lastUpdate) {
        this.publicCert = Objects.requireNonNull(publicCert, "publicCert must not be null");
        this.pKey = null;
        this.certs = certs;
        this.isLocal = isLocal;
        this.userId = userId;
        this.contextId = contextId;
        this.email = email;
        this.lastUpdate = lastUpdate;
    }

    /**
     * Return the serial number of the certificate
     *
     * @return The unique serial number
     */
    public BigInteger getSerial() {
        return this.publicCert.getSerialNumber();
    }

    /**
     * Returns the main email associated with this certificate
     *
     * @return The email address associated with this certificate
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Return the emailes address associated with the owner of the public certificate
     *
     * @return a List of email addresses associated with the certificate
     */
    public List<String> getEmails() {
        return SmimeUtil.getEmails(publicCert);
    }

    /**
     * Return the private key
     *
     * @return the related {@link SmimePrivateKey}, or <code>null</code> if not present
     */
    public SmimePrivateKeys getPrivateKey() {
        return this.pKey;
    }

    /**
     * Returns whether or not a {@link SmimePrivateKey} instance is present .
     *
     * @return <code>True</code> if a {@link SmimePrivateKey} is present, <code>false</code> otherwise.
     */
    public boolean hasPrivateKey() {
        return this.pKey != null;
    }

    /**
     * Sets the {@link SmimePrivateKeys}
     *
     * @param pKey The private key used to protect the certificate
     */
    public void setPrivateKey(SmimePrivateKeys pKey) {
        this.pKey = pKey;
        this.isLocal = pKey != null;
    }

    /**
     * Return the user certificate
     *
     * @return The {@link X509Certificate}
     */
    public X509Certificate getCertificate() {
        return this.publicCert;
    }

    /**
     * A list of {@link X509Certificate}s representing the certificate chain
     *
     * @return The certificate chain's {@link X509Certificate}s
     */
    public List<X509Certificate> getChain() {
        return this.certs;
    }

    /**
     * Gets the certificate's date of expiration
     *
     * @return The date of expiration
     */
    public Date getExpires() {
        return publicCert.getNotAfter();
    }

    /**
     * Gets if the certificate is expired or not
     *
     * @return <code>True</code> if the certificate is expired, <code>False</code>otherwise
     */
    public boolean isExpired() {
        return getExpires().before(new Date());
    }

    /**
     * Returns the "keyEncipherment" usage defined in RFC 5280 and states if the certificate's public
     * key can be used to encrypt a symmetric content key.
     *
     * @return <code> True</code>if this certificate supports content encryption, <code>false</code> otherwise.
     */
    public boolean isForEncryption() {
        return supportsKeyUsage(publicCert, SmimeKeyUsageUtil.KeyUsage.keyEncipherment, true);
    }

    /**
     * Returns the "digitalSignature" usage defined in RFC 5280 and states if the certificate supports digital signatures
     *
     * @return <code>True</code> if this certificate supports digital signatures <code>False</code> otherwise.
     */
    public boolean isForSigning() {
        return supportsKeyUsage(publicCert, SmimeKeyUsageUtil.KeyUsage.digitalSignature, true);
    }

    /**
     * Gets the name of certificate's issuer
     *
     * @return The issuer name of the certificate
     */
    public String getCertifier() {
        final X500Name name = new X500Name(this.publicCert.getIssuerX500Principal().getName());
        final RDN cn = name.getRDNs(BCStyle.CN)[0];
        return IETFUtils.valueToString(cn.getFirst().getValue());
    }

    /**
     * Gets the current flag of the internal {@link SmimePrivateKeys} instance
     *
     * @return The current flag of the internal {@link SmimePrivateKeys} instance, or <code>false</code> if no private key is present.
     */
    public boolean isCurrent() {
        return pKey != null ? pKey.isCurrent() : false;
    }

    /**
     * Returns if this certificate is a "local" certificate, i.e belongs to a local ox user
     *
     * @return <code>True</code> if the certificate is a local certificate <code>False</code> otherwise
     */
    public boolean isLocal() {
        return isLocal;
    }

    /**
     * Gets the user id
     *
     * @return The ID of the user
     */
    public int getUserId() {
        return userId;
    }

    /**
     * gets the context id
     *
     * @return The ID of the context
     */
    public int getContextId() {
        return contextId;
    }

    /**
     * Gets the timestamp (milliseconds since January 1, 1970, 00:00:00 GMT) of the last update
     *
     * @return The last update timestamp
     */
    public long getLastUpdate() {
        return lastUpdate;
    }
}
