/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link SmimeUtil} class contains useful functions that are shared.
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimeUtil {
    
    /** Simple class to delay initialization until needed */
    private static class LoggerHolder {

        static final Logger LOGGER = LoggerFactory.getLogger(SmimeUtil.class);
    }

    private static boolean isValidEmail(String email) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
            return true;
        } catch (AddressException e) {
            return false;
        }
    }

    /**
     * Confirm and email address is found in a list of emails
     *
     * @param email Email address to compare
     * @param emails List of possible emails
     * @return True if found.
     */
    public static boolean containsEmail(String email, List<String> emails) {
        if (email == null || emails == null || emails.isEmpty())
            return false;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            for (String e : emails) {
                InternetAddress compareAddr = new InternetAddress(e);
                if (compareAddr.getAddress().equals(emailAddr.getAddress()))
                    return true;
            }
        } catch (AddressException e) {
            LoggerHolder.LOGGER.error("Error decrypting email address, ignoring", e);
        }

        return false;
    }


    /**
     * Get list of email addresses in the certificate
     *
     * @param cert
     * @return List<String> of email addresses
     */
    public static List<String> getEmails(X509Certificate cert) {
        Collection<List<?>> altNames;
        List<String> emails = new ArrayList<String>(1);
        try {
            altNames = cert.getSubjectAlternativeNames();
            if (altNames != null) {
                for(List<?> altName : altNames) {
                    Integer type = (Integer) altName.get(0);
                    if (type.intValue() == 1) { // RFC
                        String possibleEmail = (String) (altName.get(1));
                        if (isValidEmail(possibleEmail)) {
                            emails.add(possibleEmail);
                        }
                    }
                }
            }
            if (!emails.isEmpty()) {
                return emails;
            }
        } catch (CertificateParsingException e) {
            LoggerHolder.LOGGER.error("Error parsing certificate", e);
            // Continue to see if we can get from CN
        }
        X500Principal pn = cert.getSubjectX500Principal();
        X500Name x500name = new X500Name(pn.getName());
        // Check for email address in CN
        RDN[] cns = x500name.getRDNs(BCStyle.CN);
        if (cns != null && cns.length > 0) {
            for (RDN cn : cns) {
                String possibleEmail = IETFUtils.valueToString(cn.getFirst().getValue());
                if (isValidEmail(possibleEmail)) {
                    emails.add(possibleEmail);
                    return emails;
                }
            }
        }
        // Not found, check in EmailAddress
        RDN[] emailsRDN = x500name.getRDNs(BCStyle.EmailAddress);
        if (emailsRDN != null && emailsRDN.length > 0) {
            for (RDN email : emailsRDN) {
                String possibleEmail = IETFUtils.valueToString(email.getFirst().getValue());
                if (isValidEmail(possibleEmail)) {
                    emails.add(possibleEmail);
                    return emails;
                }
            }
        }
        return emails;
    }
}
