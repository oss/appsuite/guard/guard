/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.commons;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.services.KeyCreationService;
import com.openexchange.server.ServiceLookup;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class KeyPairGenerator {

    private static SecureRandom secureRandom;

    private String type;
    private String signingAlg;
    private ServiceLookup services;

    public KeyPairGenerator(String type, ServiceLookup services) throws OXException {
        this.type = type;
        if (!init(type)) {
            throw OXException.general("Unknown type");
        }
        this.services = services;
    }

    private boolean init(String type) {
        switch (type) {
            case "EdDSA":
                signingAlg = "ED25519";
                break;
            case "ECDSA":
                signingAlg = "SHA256withECDSA";
                break;
            case "RSA":
                signingAlg = "SHA256WithRSAEncryption";
                break;
            default:
                return false;
        }
        return true;
    }

    public KeyPair generateKeyPair() throws OXException {
        java.security.KeyPairGenerator gen = null;
        try {
            switch (type) {
                case "EdDSA":
                    gen = java.security.KeyPairGenerator.getInstance("Ed25519");
                    break;
                case "ECDSA":
                    gen = java.security.KeyPairGenerator.getInstance("ECDSA");
                    if (secureRandom == null) {
                        secureRandom = new SecureRandom();  // We only need to init once, not thread critical
                    }
                    gen.initialize(new ECGenParameterSpec("secp256r1"), secureRandom);
                    break;
                case "RSA":
                    KeyCreationService keyCreationService = services.getService(KeyCreationService.class);
                    if (keyCreationService == null) {
                        gen = java.security.KeyPairGenerator.getInstance("RSA");
                        GuardConfigurationService configService = services.getService(GuardConfigurationService.class);
                        int keysize = configService == null ? 4096 : configService.getIntProperty(GuardProperty.rsaKeyLength);
                        gen.initialize(keysize);
                    } else {
                        return keyCreationService.getKeyPair();
                    }
                    break;
            }
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            // Unlikely to happen unless system unable to handle key sizes
            throw OXException.general("Problem creating keys", e);
        }

        KeyPair keyPair = gen.generateKeyPair();
        return keyPair;
    }

    public String getSignignAlg() {
        return this.signingAlg;
    }
}
