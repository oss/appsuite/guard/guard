/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.commons;

import java.security.cert.X509Certificate;

/**
 * {@link SmimeKeyUsageUtil} - contains utility methods to handle X509 "key usage" extension described in RFC 5280
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class SmimeKeyUsageUtil {

    /**
     * {@link KEY_USAGE} Enumerates various usages of a S/MIME certificate according to RFC 5280 section 4.2.1.3
     * 
     * @see <a href=https://datatracker.ietf.org/doc/html/rfc5280#section-4.2.1.3>rfc5280#section-4.2.1.3</a>
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v2.10.7
     */
    public enum KeyUsage {

        digitalSignature(0),
        nonRepudiation(1),
        keyEncipherment(2),
        dataEncipherment(3),
        keyAgreement(4),
        keyCertSign(5),
        cRLSign(6),
        encipherOnly(7),
        decipherOnly(8);

        private int value;

        private KeyUsage(int val) {
            this.value = val;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * Checks if a {@link X509Certificate} supports the given {@link KeyUsage}
     *
     * @param certificate The certificate to get the key usage for
     * @param usage The usage to check
     * @return The value defined for the given usage if the certificate contains the key usage extension,
     *         the given dafaultValue otherwise.
     */
    public static boolean supportsKeyUsage(X509Certificate certificate, KeyUsage usage, boolean defaultValue) {
        final boolean[] keyUsage = certificate.getKeyUsage();
        if (keyUsage == null) {
            //No key usage extension specified within the given certificate
            return defaultValue;
        }

        if (keyUsage.length < usage.getValue()) {
            //usage is not defined within the certificate's key usage extension
            return false;
        }

        return keyUsage[usage.getValue()];
    }
}
