
/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import java.math.BigInteger;

/**
 * {@link DatabaseSmimePrivateKeys}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class SmimePrivateKeys {

    private final BigInteger serial;
    private final int masterKeyIndex;
    private final String salt;
    private final String email;

    private String recovery;
    private String encrPrivateKeyData;
    private String recoveryEmail;
    private boolean current;

    /**
     * Initializes a new {@link SmimePrivateKeys}.
     *
     * @param serial The serial number of the certificate related to this private key
     * @param encrPrivate The encrypted private key data
     * @param salt The salt related to this private key
     * @param recovery The recovery related to this private key
     * @param masterKeyIndex The master key index for the recovery
     * @param email The email related to the key's certificate
     * @param recoveryEmail The recovery email of the certificate's owner
     * @param current
     */
    public SmimePrivateKeys(//@formatter:off
                            String serial, 
                            String encrPrivate, 
                            String salt, 
                            String recovery, 
                            int masterKeyIndex, 
                            String email, 
                            String recoveryEmail, 
                            boolean current) {
                            //@formatter:on

        this.serial = new BigInteger(serial);
        this.masterKeyIndex = masterKeyIndex;
        this.recovery = recovery;
        this.salt = salt;
        this.email = email;
        this.recoveryEmail = recoveryEmail;
        this.current = current;
        this.encrPrivateKeyData = encrPrivate;
    }

    /**
     * Gets the encrypted, string encoded, private key data
     *
     * @return The encrypted, String encoded, private key data
     */
    public String getEncryptedKeyData() {
        return encrPrivateKeyData;
    }

    /**
     * Sets the, string encoded, private key data
     *
     * @param encrPrivate The key data
     */
    public void setEncryptedKeyData(String encrPrivate) {
        this.encrPrivateKeyData = encrPrivate;
    }

    /**
     * Get the recovery string
     *
     * @return The recovery string
     */
    public String getRecovery() {
        return recovery;
    }

    /**
     * Gets the salt associated with the private key
     *
     * @return The private key's salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * The master key index required for the recovery
     *
     * @return The master key index used
     */
    public int getMasterKeyIndex() {
        return masterKeyIndex;
    }

    /**
     * Gets the unique serial for the private key's certificate
     *
     * @return The serial
     */
    public String getSerial() {
        return serial.toString();
    }

    /**
     * Gets the "current" state of the key
     *
     * @return The current state
     */
    public boolean isCurrent() {
        return current;
    }

    /**
     * Sets the "current" state of the key
     *
     * @param current The current state of the key
     */
    public void setCurrent(boolean current) {
        this.current = current;
    }

    /**
     * The owner's email address
     *
     * @return The email address of the owner
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns whether or not a recovery is available
     *
     * @return <code>True</code>, if recovery is available, <code>False</code> otherwise
     */
    public boolean hasRecovery() {
        return recovery != null;
    }

    /**
     * Sets the recovery for the key
     *
     * @param recovery The recovery to set, or <code>null</code> to reset the whole recovery
     */
    public void setRecovery(String recovery) {
        this.recovery = recovery;
    }

    /**
     * Clears the recovery for the key
     * <p>
     * This is just a convenience method for calling {@link SmimePrivateKeys#setRecovery(String)} with <code>null</code> as parameter.
     * </p>
     */
    public void clearRecovery() {
        setRecovery(null);
    }

    /**
     * Get the recovery email associated with the key's certificate
     *
     * @return The recovery email of the certificate owner
     */
    public String getRecoveryEmail() {
        return recoveryEmail;
    }

    /**
     * Sets the recovery email of the certificate owner
     *
     * @param email The email to set
     */
    public void setRecoveryEmail(String email) {
        this.recoveryEmail = email;
    }
}
