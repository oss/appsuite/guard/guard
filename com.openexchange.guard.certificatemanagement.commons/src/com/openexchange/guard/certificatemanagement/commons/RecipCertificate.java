/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.commons;

import java.util.Objects;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;

/**
 * {@link RecipCertificate} - A S/MIME certificate used to encrypt content for a recipient
 * 
 * <p>
 * This class represents a S/MIME certificate used to encrypt content for a recipient.
 * It wraps a {@link SmimeKeys} which represents the certificate together with a {@link KeySource}.
 * The key source indicates the source/location from which the certificate was loaded.
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class RecipCertificate {

    private final SmimeKeys smimeKeys;
    private final KeySource source;

    /**
     * Initializes a new {@link RecipCertificate}.
     *
     * @param smimeKeys The {@link SmimeKeys}
     * @param source The source of the {@link SmimeKeys}
     */
    public RecipCertificate(SmimeKeys smimeKeys, KeySource source) {
        this.smimeKeys = Objects.requireNonNull(smimeKeys, "smimeKeys must not be null");
        this.source = Objects.requireNonNull(source, "source must not be null");
    }

    /**
     * Gets the {@link SmimeKeys}
     *
     * @return The {@link SmimeKeys} (i.e the certificate)
     */
    public SmimeKeys getSmimeKeys() {
        return smimeKeys;
    }

    /**
     * Gets the source of the keys/certificate
     *
     * @return The source of the certificates / keys
     */
    public KeySource getSource() {
        return source;
    }
}
