/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

/**
 * {@link CertificateEnrollmentStatus}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateEnrollmentStatus {

    public enum Status {
        Done,
        Continue,
        Rejected,
        Pending
    }

    private Status status;  // Stage in a given step
    private int stage;  // Step in the certificate enrollment process.  Implementation specific
    private SmimeKeys smimeKeys;
    private String message;

    public CertificateEnrollmentStatus(Status status, int stage) {
        this.message = null;
        this.stage = stage;
        this.status = status;
        this.smimeKeys = null;
    }

    public CertificateEnrollmentStatus(Status status, int stage, String message) {
        this.message = message;
        this.stage = stage;
        this.status = status;
        this.smimeKeys = null;
    }

    public CertificateEnrollmentStatus(Status status, int stage, SmimeKeys keys) {
        this.stage = stage;
        this.status = status;
        this.smimeKeys = keys;
        this.message = null;
    }

    /**
     * Gets the smimeKeys
     *
     * @return The smimeKeys
     */
    public SmimeKeys getSmimeKeys() {
        return smimeKeys;
    }

    /**
     * Sets the smimeKeys
     *
     * @param smimeKeys The smimeKeys to set
     */
    public void setSmimeKeys(SmimeKeys smimeKeys) {
        this.smimeKeys = smimeKeys;
    }

    /**
     * Gets the current status
     *
     * @return
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Set the current status
     *
     * @param status
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Get the current stage
     *
     * @return
     */
    public int getStage() {
        return stage;
    }

    /**
     * Sets the current stage of the process
     *
     * @param stage
     */
    public void setStage(int stage) {
        this.stage = stage;
    }

    /**
     * Returns message. Could be reason for delay or reason for failure
     *
     * @return String message, reason certificate isn't available
     */
    public String getMessage() {
        return this.message;
    }
}
