/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.security.KeyPair;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.ExtensionsGenerator;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import com.openexchange.exception.OXException;

/**
 * {@link CertificateSigningRequest} Used to generate a certificate signing request
 * for a new S/Mime certificate
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateSigningRequest {

    /**
     *
     * {@link CertificateSigningRequestBuilder}
     * Builder for {@link CertificateSigningRequest}. Requires minimum of KeyPair and email address
     *
     * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
     * @since v4.0.0
     */
    public static class CertificateSigningRequestBuilder {

        private String common_name;
        private String organization;
        private String email;
        private KeyPairGenerator keyPairGenerator;

        /**
         * Sets the keyPairGenerator
         *
         * @param keyPairGenerator The keyPairGenerator to set
         */
        public CertificateSigningRequestBuilder setKeyPairGenerator(KeyPairGenerator keyPairGenerator) {
            this.keyPairGenerator = keyPairGenerator;
            return this;
        }

        /**
         * Sets the common_name
         *
         * @param common_name The common_name to set
         */
        public CertificateSigningRequestBuilder setCommonName(String common_name) {
            this.common_name = common_name;
            return this;
        }

        /**
         * Sets the organization. Optional
         *
         * @param organization The organization to set
         */
        public CertificateSigningRequestBuilder setOrganization(String organization) {
            this.organization = organization;
            return this;
        }

        /**
         * Sets the email
         *
         * @param email The email to set
         */
        public CertificateSigningRequestBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        /**
         * Builds the {@link CertificateSigningRequest}.
         * Requires minimum of KeyPair and email address
         *
         * @return The {@link CertificateSigningRequest}.
         * @throws OXException
         */
        public CertificateSigningRequest build() throws OXException {
            // Check minimum parameters set
            if (common_name == null && email == null) {
                throw OXException.mandatoryField("email missing");
            }
            if (common_name == null) {  // email can be used for common_name
                common_name = email;
            }

            KeyPair keys = keyPairGenerator.generateKeyPair();

            // Build csr
            X500NameBuilder builder = new X500NameBuilder(BCStyle.INSTANCE).addRDN(BCStyle.CN, common_name);
            if (organization != null) {
                builder.addRDN(BCStyle.ORGANIZATION_IDENTIFIER, organization);
            }

            final X500Name subject = builder.build();

            try {
                // Add email as subjectAlternativeName
                ExtensionsGenerator extGen = new ExtensionsGenerator();

                extGen.addExtension(org.bouncycastle.asn1.x509.Extension.subjectAlternativeName, false,
                    new GeneralNames(
                        new GeneralName(
                            GeneralName.rfc822Name,
                            email)));

                Extensions extensions = extGen.generate();

                // Build the request
                PKCS10CertificationRequestBuilder requestBuilder = new JcaPKCS10CertificationRequestBuilder(
                    subject,
                    keys.getPublic());
                requestBuilder.addAttribute(
                    org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers.pkcs_9_at_extensionRequest, extensions);

                ContentSigner signer = new JcaContentSignerBuilder(keyPairGenerator.getSignignAlg())
                    .setProvider("BC")
                    .build(keys.getPrivate());

                return new CertificateSigningRequest(requestBuilder.build(signer), keys);
            } catch (IOException | OperatorCreationException e) {
                throw OXException.general("Unable to generate CSR", e);
            }

        }
    }

    private final PKCS10CertificationRequest request;
    private KeyPair keyPair;

    /**
     * Initializes a new {@link CertificateSigningRequest}.
     *
     * @param request
     */
    public CertificateSigningRequest(PKCS10CertificationRequest request, KeyPair keys) {
        super();
        this.request = request;
        this.keyPair = keys;
    }

    /**
     * Write the CSR to PEM String
     *
     * @return PEM encoded string
     * @throws OXException
     */
    public String writePemString() throws OXException {
        StringWriter stringWriter = new StringWriter();
        JcaPEMWriter pem = new JcaPEMWriter(stringWriter);
        try {
            pem.writeObject(request);
            pem.close();
            return stringWriter.toString();
        } catch (IOException e) {
            throw OXException.general("Error writing to PEM", e);
        }
    }

    /**
     * Write PEM encoded CSR to outputStream
     *
     * @param out OutputStream to use
     * @throws OXException
     */
    public void writePemToOutputStream(OutputStream out) throws OXException {
        OutputStreamWriter outWriter = new OutputStreamWriter(out);
        JcaPEMWriter pem = new JcaPEMWriter(outWriter);
        try {
            pem.writeObject(request);
            pem.close();
            outWriter.close();
        } catch (IOException e) {
            throw OXException.general("Error writing to PEM", e);
        }
    }

    /**
     * Returns the private and public keys used to generate this CSR
     *
     * @return
     */
    public KeyPair getKeyPair() {
        return this.keyPair;
    }
}
