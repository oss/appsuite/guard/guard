/*
 *
 *    OPEN-XCHANGE legal information
 *
 *    All intellectual property rights in the Software are protected by
 *    international copyright laws.
 *
 *
 *    In some countries OX, OX Open-Xchange, open xchange and OXtender
 *    as well as the corresponding Logos OX Open-Xchange and OX are registered
 *    trademarks of the Open-Xchange GmbH. group of companies.
 *    The use of the Logos is not covered by the GNU General Public License.
 *    Instead, you are allowed to use these Logos according to the terms and
 *    conditions of the Creative Commons License, Version 2.5, Attribution,
 *    Non-commercial, ShareAlike, and the interpretation of the term
 *    Non-commercial applicable to the aforementioned license is published
 *    on the web site http://www.open-xchange.com/EN/legal/index.html.
 *
 *    Please make sure that third-party modules and libraries are used
 *    according to their respective licenses.
 *
 *    Any modifications to this package must retain all copyright notices
 *    of the original copyright holder(s) for the original code used.
 *
 *    After any such modifications, the original and derivative code shall remain
 *    under the copyright of the copyright holder(s) and/or original author(s)per
 *    the Attribution and Assignment Agreement that can be located at
 *    http://www.open-xchange.com/EN/developer/. The contributing author shall be
 *    given Attribution for the derivative code and a license granting use.
 *
 *     Copyright (C) 2016-2020 Open-Xchange GmbH
 *     Mail: info@open-xchange.com
 *
 *
 *     This program is free software; you can redistribute it and/or modify it
 *     under the terms of the GNU General Public License, Version 2 as published
 *     by the Free Software Foundation.
 *
 *     This program is distributed in the hope that it will be useful, but
 *     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *     or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *     for more details.
 *
 *     You should have received a copy of the GNU General Public License along
 *     with this program; if not, write to the Free Software Foundation, Inc., 59
 *     Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

package com.openexchange.guard.certificatemanagement.commons;

import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.ByteArrayOutputStream;
import java.security.KeyPair;
import java.security.Security;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.crmf.CertReqMessages;
import org.bouncycastle.asn1.crmf.CertReqMsg;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.cmp.ProtectedPKIMessage;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.Test;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CRMFSigningRequest.CRMFSigningRequestBuilder;
import com.openexchange.server.ServiceLookup;

/**
 * {@link Csr_tests}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class Csr_tests {

    private class MockServiceLookup implements ServiceLookup {

        @Override
        public <S> S getService(Class<? extends S> clazz) {
            return null;
        }

        @Override
        public <S> S getOptionalService(Class<? extends S> clazz) {

            return null;
        }

    }

    @Test
    public void testPem() {
        Security.addProvider(new BouncyCastleProvider());
        CertificateSigningRequest request;
        try {
            ServiceLookup services = new MockServiceLookup();
            // Try with EDSA
            request = new CertificateSigningRequest.CertificateSigningRequestBuilder()
                .setKeyPairGenerator(new KeyPairGenerator("EdDSA", services))
                .setEmail("test14@encr.us")
                .build();
            String str = request.writePemString();
            System.out.println(str);
            assertTrue(str.contains("BEGIN CERTIFICATE") && str.contains("END CERTIFICATE"), "Has certificate data");

            // Try again with RSA
            request = new CertificateSigningRequest.CertificateSigningRequestBuilder()
                .setKeyPairGenerator(new KeyPairGenerator("RSA", services))
                .setCommonName("test2@example.com")
                .setEmail("test14@encr.us")
                .build();
            str = request.writePemString();
            System.out.println(str);
            assertTrue(str.contains("BEGIN CERTIFICATE") && str.contains("END CERTIFICATE"), "Has certificate data");

        } catch (OXException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testStream() {
        Security.addProvider(new BouncyCastleProvider());
        CertificateSigningRequest request;
        try {
            ServiceLookup services = new MockServiceLookup();
            request = new CertificateSigningRequest.CertificateSigningRequestBuilder()
                .setKeyPairGenerator(new KeyPairGenerator("RSA", services))
                .setCommonName("test2@example.com")
                .setEmail("test2@example.com")
                .setOrganization("org")
                .build();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            request.writePemToOutputStream(out);
            String str = new String(out.toByteArray());
            assertTrue(str.contains("BEGIN CERTIFICATE") && str.contains("END CERTIFICATE"), "Has certificate data");

        } catch (OXException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCRMF() {
        Security.addProvider(new BouncyCastleProvider());
        CRMFSigningRequest request;
        try {
            ServiceLookup services = new MockServiceLookup();
            final String email = "test2@example.com";
            KeyPair keys = new KeyPairGenerator("RSA", services).generateKeyPair();
            request = new CRMFSigningRequestBuilder().setKeyPair(keys).setCommon_name("test2@example.com").setEmail("test2@example.com").setOrganization("org").build();
            X500Name sender = new X500Name("CN=Sender");
            X500Name recip = new X500Name("CN=Destination");
            ProtectedPKIMessage msg = request.getPKIProtected("123".getBytes(), sender, recip, "secret");
            assertTrue(msg.hasPasswordBasedMacProtection(), "has mac protection");
            assertTrue(msg.getHeader().getSender().getName().toString().contains(sender.getRDNs(BCStyle.CN)[0].getFirst().getValue().toString()), "Sender matches");

            CertReqMsg[] certReqMsgs = CertReqMessages.getInstance(msg.getBody().getContent())
                .toCertReqMsgArray();
            CertReqMsg req = certReqMsgs[0];
            assertTrue(req.getCertReq().getCertTemplate().getSubject().toString().contains(email), "Subject contains email");
            Extension ext = req.getCertReq().getCertTemplate().getExtensions().getExtension(org.bouncycastle.asn1.x509.Extension.subjectAlternativeName);
            assertTrue((new String(((DEROctetString) ext.getExtnValue()).getOctets()).contains(email)), "Extension contains alternate subject of email");
        } catch (OXException e) {
            e.printStackTrace();
        }
    }
}
