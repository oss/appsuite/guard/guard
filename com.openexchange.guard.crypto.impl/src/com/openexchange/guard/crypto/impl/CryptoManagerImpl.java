/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.crypto.impl;

import java.util.concurrent.ConcurrentHashMap;
import com.openexchange.crypto.CryptoType;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.crypto.MimeSignatureVerificationService;
import com.openexchange.guard.crypto.PasswordManagementService;
import com.openexchange.guard.mime.services.MimeEncryptionService;

/**
 * {@link CryptoManagerImpl} Keeps list of Crypto based services, encryption, management services
 * Will search for registered services and return the one that handles the type of request
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CryptoManagerImpl implements CryptoManager {

    private final ConcurrentHashMap<CryptoType.PROTOCOL, PasswordManagementService> passwordServices = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<CryptoType.PROTOCOL, MimeEncryptionService> mimeEncryptionServices = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<CryptoType.PROTOCOL, MimeSignatureVerificationService> signatureServices = new ConcurrentHashMap<>();

    /**
     * Registers a {@link PasswordManagementService} to the registry
     *
     * @param service The {@link PasswordManagementService} to add
     */
    public void registerPasswordManagementService(PasswordManagementService service) {
        passwordServices.put(service.getCryptoType(), service);
    }

    /**
     * Removes the given {@link PasswordManagementService} from the registry
     *
     * @param service The {@link PasswordManagementService} to remove
     */
    public void removeKeyManagementService(PasswordManagementService service) {
        passwordServices.remove(service.getCryptoType());
    }

    @Override
    public MimeSignatureVerificationService getSignatureVerificationService(CryptoType.PROTOCOL type) {
        return signatureServices.get(type);
    }

    @Override
    public PasswordManagementService getPasswordManagementService(CryptoType.PROTOCOL type) {
        return passwordServices.get(type);
    }

    @Override
    public MimeEncryptionService getMimeEncryptionService(CryptoType.PROTOCOL type) {
        return mimeEncryptionServices.get(type);
    }

    /**
     * Registers a {@link MimeEncryptionService} to the registry
     *
     * @param service The {@link MimeEncryptionService} to add
     */
    public void registerMimeEncryptionService(MimeEncryptionService service) {
        mimeEncryptionServices.put(service.getCryptoType(), service);
    }

    /**
     * Removes the given {@link MimeEncryptionService} from the registry
     *
     * @param service The {@link MimeEncryptionService} to remove
     */
    public void removeMimeEncryptionService(MimeEncryptionService service) {
        mimeEncryptionServices.remove(service.getCryptoType());
    }

    /**
     * Registers a {@link MimeSignatureVerificationService} to the registry
     *
     * @param service The {@link MimeSignatureVerificationService} to add
     */
    public void registerMimeVerificationService(MimeSignatureVerificationService signatureVerificaionService) {
        signatureServices.put(signatureVerificaionService.getCryptoType(), signatureVerificaionService);
    }

    /**
     * Removes the given {@link MimeSignatureVerificationService} from the registry
     *
     * @param service The {@link MimeSignatureVerificationService} to remove
     */
    public void removeMimeVerificationService(MimeSignatureVerificationService signatureVerificaionService) {
        signatureServices.remove(signatureVerificaionService.getCryptoType());
    }
}
