
package com.openexchange.guard.mime.helpfiles.impl;

public class HelpAttach {

    private String language;
    private String translated;

    public HelpAttach(String lang, String trans) {
        language = lang;
        translated = trans;
    }

    public String getLanguage() {
        return (language);
    }

    public String getTranslated() {
        return (translated);
    }

}
