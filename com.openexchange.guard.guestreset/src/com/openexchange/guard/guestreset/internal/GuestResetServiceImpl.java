package com.openexchange.guard.guestreset.internal;

import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.guestreset.GuestResetService;
import com.openexchange.guard.guestreset.exceptions.GuardGuestResetExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.oxapi.sharing.EmailShareLink;
import com.openexchange.server.ServiceLookup;

public class GuestResetServiceImpl implements GuestResetService {

    ServiceLookup services;

    public GuestResetServiceImpl(ServiceLookup services) {
        this.services = services;
    }

    @Override
    public void reset(String email, String fromEmail, int fromId, int fromCid) throws OXException {
        GuardKeyService gKeyService = services.getServiceSafe(GuardKeyService.class);
        GuardKeys keys = gKeyService.getKeys(email);
        if (keys == null || keys.getContextid() > 0) {
            throw GuardGuestResetExceptionCodes.UNKNOWN_GUEST_ACCOUNT.create(email);
        }

        String language = keys.getLanguage();  // Get language preferred by guest
        if (fromEmail != null) {  // If fromEmail provided, use to get userId, cid
            GuardKeys fromKey = gKeyService.getKeys(fromEmail);
            if (fromKey != null && fromKey.getContextid() > 0) {
                fromId = fromKey.getUserid();
                fromCid = fromKey.getContextid();
            }
        }
        if (fromId == 0 || fromCid == 0) {
            throw GuardGuestResetExceptionCodes.UNABLE_TO_RESET.create("Missing ox user email or id/cid to originate share link.");
        }
        GuardConfigurationService guardConfigService = services.getServiceSafe(GuardConfigurationService.class);
        if (language == null) {
            language = guardConfigService.getProperty(GuardProperty.defaultLanguage, fromId, fromCid);
        }
        int templId = guardConfigService.getIntProperty(GuardProperty.templateID, fromId, fromCid);
        String resetLink = new EmailShareLink().createEmailShare(fromId, fromCid, email, "guestReset", language);
        if(resetLink == null) {
            throw GuardGuestResetExceptionCodes.UNABLE_TO_RESET.create("Unable to create reset link.");
        }
        MailCreatorService mailCreatorService = services.getServiceSafe(MailCreatorService.class);
        JsonObject mailJson = mailCreatorService.getGuestResetEmail(email, resetLink, language, templId, null, fromId, fromCid);
        GuardNotificationService guardNotificationService = services.getServiceSafe(GuardNotificationService.class);
        guardNotificationService.send(mailJson, fromCid, templId, null);

    }

}