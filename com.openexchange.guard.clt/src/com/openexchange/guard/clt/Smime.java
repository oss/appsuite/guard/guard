/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.clt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.management.MBeanException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import com.openexchange.auth.rmi.RemoteAuthenticator;
import com.openexchange.exception.OXException;
import com.openexchange.guard.management.maintenance.GuardMaintenanceMBean;

/**
 * {@link Smime}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class Smime extends AbstractGuardCLT {

    private final char[] optionsWithArgs = { 'a', 'c', 'e', 'g', 'i', 'm', 'x' };

    private final boolean requiresAuth;

    /**
     * @param args
     */
    public static void main(String[] args) {
        new Smime(args).execute(args);
    }

    /**
     * Initialises a new {@link Guard}.
     */

    public Smime(String[] args) {
        super();
        requiresAuth = needsAuth(args);
    }

    /**
     * Return true if ags includes action that requires authentication
     *
     * @param args
     * @return
     */
    private boolean needsAuth(String[] args) {
        for (String arg : args) {
            if (arg.trim().equals("-D") || arg.trim().equals("--delete")) {
                return true;
            }
            if (arg.trim().equals("-m") || arg.trim().equals("--import")) {
                return true;
            }
            if (arg.trim().equals("-a") || arg.trim().equals("--authority")) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void addOptions(Options options) {
        options.addOption("e", "reset", true, "Resets the specified e-mail address and sends a new password to the user\ne.g. smime --reset john@somewhere.com");
        options.addOption("a", "authority", true, "Add certificate Authority as a PEM file for a group ID.  guard --authority file --grpId groupId");
        options.addOption("g", "grpId", true, "Group Id");
        options.addOption("m", "import", true, "Import a Smime private key for user");
        options.addOption("c", "context", true, "Context of a user");
        options.addOption("i", "id", true, "ID of a user");
        options.addOption("k", "key_password", true, "Current key password");
        options.addOption("n", "new", true, "New password");
        options.addOption("x", "expiring", true, "Gets a csv list of users with certificates expiring within days specified.  smime --expiring 10");
    }

    @Override
    protected Void invoke(Options options, CommandLine cmd, String optRmiHostName) throws Exception {
        // Get the optional JMX params
        String jmxHost = "localhost";
        if (cmd.hasOption('s')) {
            jmxHost = cmd.getOptionValue('s');
        }
        String jmxPort = "9999";
        if (cmd.hasOption('p')) {
            jmxPort = cmd.getOptionValue('p');
        }
        String jmxUser = null;
        if (cmd.hasOption('j')) {
            jmxUser = cmd.getOptionValue('j');
        }
        String jmxPassword = null;
        if (cmd.hasOption('w')) {
            jmxPassword = cmd.getOptionValue('w');
        }

        try {
            if (cmd.hasOption('e')) {
                String email = cmd.getOptionValue('e');
                reset(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), email);
            } else if (cmd.hasOption("authority")) {
                if (cmd.getOptionValue("authority") == null) {
                    System.out.println("Missing authority.  Please specify a authority certificate file for this import");
                    System.exit(-1);
                }
                String filename = cmd.getOptionValue("authority");
                if (cmd.getOptionValue("grpId") == null) {
                    System.out.println("Missing grpId.  Please specify a group ID for this import");
                    System.exit(-1);
                }
                int grp = Integer.parseInt(cmd.getOptionValue("grpId"));
                importCA(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), filename, grp);
            } else if (cmd.hasOption("m")) {
                String filename = cmd.getOptionValue("m");
                if (filename == null) {
                    System.out.println("Missing filename");
                    System.exit(-1);
                }
                int id = Integer.parseInt(cmd.getOptionValue("i"));
                int cid = Integer.parseInt(cmd.getOptionValue("c"));
                String oldPass = cmd.getOptionValue("k");
                if (oldPass == null) {
                    System.out.println("Missing key password");
                    System.exit(-1);
                }
                String newPass = cmd.getOptionValue("n");
                if (newPass == null) {
                    newPass = oldPass;
                }
                importKey(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), filename, id, cid, oldPass, newPass);
            } else if (cmd.hasOption("x")) {
                int days = Integer.parseInt(cmd.getOptionValue("x"));
                getExpiring(createProxy(jmxHost, jmxPort, jmxUser, jmxPassword), days);
            } else {
                printHelp(options);
                System.exit(0);
            }
        } catch (Exception e) {
            if (e.getCause() instanceof OXException) {
                System.err.println(e.getCause().getMessage());
            } else {
                System.err.println("Operation failed.\n");
                e.printStackTrace();
            }
            System.exit(-1);
        }
        return null;
    }

    @Override
    protected void administrativeAuth(String login, String password, CommandLine cmd, RemoteAuthenticator authenticator) throws Exception {
        authenticator.doAuthentication(login, password);

    }

    @Override
    protected void checkOptions(CommandLine cmd) throws ParseException {
        boolean missingArg = false;

        for (char c : optionsWithArgs) {
            if (cmd.hasOption(c) && cmd.getOptionValue(c) == null) {
                missingArg = true;
            }
        }

        if (missingArg) {
            printHelp(options);
            System.exit(-1);
        }

    }

    @Override
    protected String getFooter() {
        return "Command line tool for OX Guard Smime ";
    }

    @Override
    protected String getName() {
        return "smime --option parameter [-j jmxUser] " + "[-w jmxPassword] [-x jmx.host] [-o <JMX_PORT>] [-p <RMI_PORT>] [--responsetimeout <TIMEOUT>]";
    }

    @Override
    protected Boolean requiresAdministrativePermission() {
        return Boolean.valueOf(requiresAuth);
    }

    //// Actions

    /**
     * Reads file into byte array
     *
     * @param filename to read
     * @return Byte array with data
     * @throws MBeanException If error
     */
    private byte[] readFile(String filename) throws MBeanException {
        try {
            return Files.readAllBytes(Paths.get(filename));
        } catch (IOException e) {
            throw new MBeanException(e);
        }
    }

    /**
     * ImportCA import a certificate authority
     *
     * @param proxy
     * @param filename Filename containing the ca certificate
     * @param grpId GroupID to associate the ca
     * @throws MBeanException
     */
    private void importCA(GuardMaintenanceMBean proxy, String filename, int grpId) throws MBeanException {
        if (filename == null) {
            System.out.println("Requires filename");
            return;
        }
        proxy.importCA(readFile(filename), grpId);
        System.out.println("Imported");
    }

    /**
     * Send password reset email
     *
     * @param proxy
     * @param email Email address to reset
     * @throws MBeanException
     */
    private void reset(GuardMaintenanceMBean proxy, String email) throws MBeanException {
        proxy.reset(email, "smime");
        System.out.println("Email with a new password has been sent.");
    }

    /**
     * Import a key for a user
     *
     * @param proxy
     * @param filename Filename of key to import
     * @param userId User UserId
     * @param cid User contextId
     * @param oldPass Password used to encrypt the file
     * @param newPass Password to use in Guard
     * @throws MBeanException
     */
    private void importKey(GuardMaintenanceMBean proxy, String filename, int userId, int cid, String oldPass, String newPass) throws MBeanException {
        if (filename == null) {
            System.out.println("Requires filename");
            return;
        }
        proxy.importKey("smime", readFile(filename), userId, cid, oldPass, newPass);
        System.out.println("Key imported");
    }

    /**
     * Returns list of users with keys expiring within specified days
     *
     * @param proxy
     * @param days Range for days in the future to check expiration
     */
    private void getExpiring(GuardMaintenanceMBean proxy, int days) throws MBeanException {
        System.out.println(proxy.getSmimeExpiring(days));
    }

}
