---
layout: null
---
{% capture scripts %}
  {% include js/jquery-3.2.0.min.js %}
  {% include js/bootstrap/affix.js %}
  {% include js/bootstrap/scrollspy.js %}
  {% include js/lightbox.js %}
  {% include js/prism.js %}

var site = {
    lightbox: function () {
        // lightbox support
        var lightbox = new Lightbox();

        $('.article').find('img').attr('data-jslghtbx', '');

        // support for oversized images
        lightbox.load({
            dimensions: true,
            maxImgSize: 2,
            hideOverflow: true,
            responsive: false
        });
    },
    headlineAnchors: function () {
        var hash = {};
        // Add anchors with icon to headlines
        $('.article').find('h1:not(.article-header), h2, h3, h4, h5').each(function (i, el) {
            var $el = $(el),
                id = $el.attr('id');
            if (!id) return;
            // fix duplicates (add counter suffix on collusion)
            var count = 1;
            if (hash[id]) {
                count++;
                id = id + count;
            }
            hash[id] = count;
            // append icon node
            $el.addClass('anchor').attr('id', id).append(
                $('<a>').attr('href', '#' + id).append(
                    $('<i class="fa fa-link" aria-hidden="true">')
                )
            );
        });
    },
    tableStyles: function () {
        $('table').addClass('table');
    },
    getListElement: function (data) {
        // auto numeration via css seems to work only when same parent
        return $('<li>').addClass(data.type + '-based')
            // used for manual scrollintoview
            .attr('data-anchor', data.id)
            .append(
                $('<a>').attr('href', '#' + data.id).text(data.label)
            );
    },
    affix: function () {
        // use only h1 and h2
        var list = $('.article').find('h1:not(.article-header), h2'),
            affix = $('#affix-nav');

        // disable for landing page
        if (!affix.length > 0 || !list.length > 0) return;

        // register scrollspy
        $('body').attr({
            'data-spy': 'scroll',
            'data-target': '.scrollspy',
            'data-offset': '100'
        });

        // hierarchy structure
        var sorted = [];
        $.each(list, function (index, node) {
            node = $(node);
            // ignore TOC
            if (node.closest('#toc-container').length) return;

            var type = node.get(0).tagName.toLowerCase(),
                target = (type === 'h1' ? sorted : sorted[sorted.length - 1].children);
            return target.push({
                id: node.attr('id'),
                label: node.text(),
                type: type,
                children: []
            });
        });
        // build h1-based list and h2-based sublist
        $.each(sorted, function (index, data) {
            var listelem = site.getListElement(data), sublist;
            affix.append(listelem);
            $.each(data.children, function (index, childdata) {
                if (index === 0) listelem.append(sublist = $('<ul class="nav children">'));
                sublist.append(site.getListElement(childdata));
            });
        });

        $('#affix-nav').affix({
            offset: {
                top: $('#affix-nav').offset().top,
                bottom: 44
            }
        });
    },
    navigationIndicator: function () {
        $('ul.nav a').filter(function (i, el) {
            return window.location.pathname.indexOf($(el).attr('href')) === 0;
        }).addClass('active');
    },
    adjustArticleHeight: function () {
        var topOffset = 50,
            bottomOffset = 44,
            height,
            width = (window.innerWidth > 0) ? window.innerWidth : window.screen.width;

        if (width < 768) topOffset = 100; // 2-row-menu

        // expand article to full height
        height = ((window.innerHeight > 0) ? window.innerHeight : window.screen.height);
        height = height - topOffset - bottomOffset;

        // sidebar height
        var sidebarheight = $('.sidebar-nav').height();
        height = height < sidebarheight ? sidebarheight : height;

        if (height < 1) height = 1;
        if (height > topOffset) $('.article').css('min-height', height);
    }
};

(function() {
    site.navigationIndicator()
    site.lightbox();
    site.headlineAnchors();
    site.tableStyles();
    site.affix();
    site.adjustArticleHeight();
})();

$(document).on('click', '.navbar-toggle', function () { $('.navbar-collapse').toggleClass('in'); });
$(window).on('resize', function () { site.adjustArticleHeight(); });

{% endcapture %}{{ scripts | uglify | strip }}
