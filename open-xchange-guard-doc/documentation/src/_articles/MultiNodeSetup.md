---
title: Multi Node Setup
description: OX Guard Multi Node Setup 
---

# Overview

If there are multiple installations of Guard and OX backends some additional configuration may be required to maximize the user experience.

When a Guard email is sent to a user not local to the Guard installation (not a member of the local OX installation), then Guard takes the following steps

- Search for any keys available using configured key servers
- Search for SRV records containing PGP information
- If no keys found, then create a Guest account

A Guest account creates a pair of PGP Public/Private keys, and sends the user a link to read the email using Guard's Guest reader.

Ideally, if the recipient is in a Guard installation, they should receive an encrypted email using their Guard keys rather than a Guest account link. That way, the experience is the same for them regardless if they received an email from a local user, or an external Guard account. 
 
# Configuring key servers

In the file */opt/open-xchange/etc/guard-core.properties* file, there is a configuration setting

```
com.openexchange.guard.publicPGPDirectory = hkp://keys.gnupg.net:11371, hkp://pgp.mit.edu:11371
```

If you know of other Guard installations that your users will be using, then it should be added here. 
The URLs can follow the standard *hkp://domain:11371*, or can specify a more detailed URL. 
If a more detailed URL, then it should end with a "?" at which point the standard hkp parameters will be added. 
If just the domain is given, then the standard *"/pks/lookup?"* is used.

Guard servers, if addressed directly, should have the URL http://address:port/pgp/lookup?

For example, if you are addressing another Guard server (say 10.10.10.100 using port 8080) directly, you would use the URL

```
com.openexchange.guard.publicPGPDirectory = http://10.10.10.100:8080/pgp/lookup?
```

If, on the other hand, you are using an Apache front end load balancer as configured in the installation directions, the URL would be

```
com.openexchange.guard.publicPGPDirectory = http://frontend
```

# Key Generation

Guard creates user keys on demand. Key generation, on the other hand, is very CPU intensive. 
Therefore, the public key lookup service does not, by default, create keys for users.

So, if sending to a user in another Guard installation that has a Guard server, but the user hasn't setup keys, the public key server will return "No keys" for the user.

Again, this is less than ideal, as we would like that user to have the full UI experience. 
To work around this, Guard has a whitelist setting for known Guard servers

```
# Comma delim CDIR notation or distinct IP.  eg "= 10.0.100.0/24, 192.168.10.3"
com.openexchange.guard.publicKeyWhitelist=
```

If the sending server is listed in this Whitelist, then the public key server WILL create keys for the user, and send that user a welcome message with a temporary password.

With the Whitelist parameter set, the experience for the sender and recipient is the same as if both users were local.

If there is a frontend used, the IP address of the sending Guard server might be masked, however. In this case, Guard respect the HTTP parameter *"X-Forwarded-For"*
    
# Testing

The response of a Guard server can be tested by using the URL

http://guardserver:8080/pgp/lookup?op=index&search=john@somewhere.com

Substitute the email address for a test user. If the user has set up keys already, their public keys will be listed. 
If they don't have keys yet, it will depend on the whitelist settings. If your computer is in the whitelist, then keys should be generated and the user should receive a welcome email.

Ideally, you should also check between two different nodes. Log into an account in node 1. Compose an email, enable encryption, then type the name of a user in node 
2. A PGP key icon should appear next to the name if proper lookup done. If a "green man" icon appears, then a Guest account would be created and the key lookup failed. 


# User Capabilities

We strongly encourage administrators to enable the capability

```
com.openexchange.capability.guard=true
```

for all users. This is the minimum Guard capability level. It allows the user to read an encrypted emails sent to them, and reply to those emails. 
This minimum level, though, does not allow the user to create new emails or encrypt files. We feel this is the best experience for the sender and recipients, and provides an excellent upsell opportunity. 
