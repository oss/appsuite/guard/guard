---
title: Certificate Ldap
description: Using Ldap for public key lookup of S/Mime certificates
---

# Overview

As of Guard version 4.1.0, Guard can be configured to lookup certificates for recipients using LDAP.  Multiple LDAP servers may be configured.  Guard will search the LDAP servers for the recipients email using the ldap attribute "mail" then check for any available certificates.

# Installation

There are no additional installation steps required for LDAP PKI support

# Configuration

## Enable

The bundle for LDAP PKI lookup must be enabled.  This is a system-wide configuration.

com.openexchange.guard.pki.ldap.enabled=true

## ClientIds

The ldap server configurations need to be set up the same way that the contacts ldap service is configured.  The ldap-client-config.yml file must have the server(s) configured and named.  

See [Contacts provider ldap](https://documentation.open-xchange.com/7.10.6/middleware/contacts/contacts_provider_ldap.html) for more details on setting up the contacts ldap provider.  Full setup is not required for the Guard ldap lookup (just the ldap-client-config.yml file).

Set up the clientIds to be searched using a comma delimited setting

com.openexchange.guard.pki.ldap.clientId=ldap1,ldap2,etc

## Mappings

By default, Guard will look at LDAP attributes for "userCertificate" and "userCertificate;binary".  If a different attribute should be searched, configure it for the specific clientId using the configuration

com.openexchange.guard.pki.ldap.[clientId].certificateMapping=xxxxxx

