---
title: Clustering
description: Scaling OX Guard
---

# IMPORTANT

__If nothing else, make sure the *oxguardpass* file is the same for all Guard servers. 
This is the local password file that is used to encrypt some database items. 
If this varies between machines, there will be failures with password recoveries and cached key creation!__

# Pre-Installation

- Please make sure that the database is accessible from all of the Guard servers (usernames allow for the Guard IPs)
- Set up a filestore that can be accessed from all Guard servers.  This may be a Networked File Store (NAS), or Amazon.

# Setup

## Configuration

###OX Backends

On each OX backend, make sure the packages *open-xchange-rest* and *open-xchange-guard-backend* are installed.
Edit the */opt/open-xchange/etc/server.properties* file to make sure the REST API username and password are configured. 
Please see the [Open-Xchange Middleware Configuration](./Installation.html#middleware-configuration-on-ox-guard-node) instructions for details.

### First machine

First, setup the *guard.properties* file to the needed settings.

- Make sure that the *com.openexchange.guard.storage.file.uploadDirectory* setting refers to the NAS
- Make sure the right username/password for the *REST API* are set
- Make sure all database settings are set for names that can be used from all guard servers (i.e. don't use localhost)
- If installing on the same server as an OX backend, it is possible to use localhost as the restAPIHostname, but if not, make sure goes through the same load balancer or routing as the users
    - Guard uses JESESSIONID, like the user interface, for routing
    - Guard uses the users cookie data to authorize against the backend to retrieve emails, etc.
    - If routed to a different backend than the user, there will be excess sessions created

Run the *./guard init* on __THE FIRST MACHINE ONLY__

Once init is done, the *oxguardpass* file will be created. This file will need to be copied to all other Guard machines.

### Other machines

Copy the *oxguardpass* file from the first installation into */opt/open-xchange/etc/*.
Make sure that the *guard-core.properties* file for that machine is configured with the same required settings as the first. 
You may be able to just copy the *guard-core.properties* file from the first depending on your setup.


## Startup

Once above is done, guard can be started. 
