---
title: Customization
description: Template customization
---

#Overview

Guard uses several templates for emails. 
These templates are fully customizable, and can be customized at the global level or at the context/user level.  Changing images, colors, and layout is easy. 
Changing the wording is also possible, though the translation tables will then need to be updated.

#Template ID

Guard uses a template ID for choosing the templates to use. The template ID can be chosen for a user or context using the configuration cascade.

```
com.openexchange.guard.templateID=x
```

For any template below, a customized template can be created with the name x-templatename where x is the integer value of the template ID for the user. 
For example, if you wanted a custom password template based on the template *"passwordtempl.html"* for a context of users, you could create a template 
*"2-passwordtempl.html"* and assign the value *com.openexchange.guard.templateID=2* to the context. 
Then, Guard will use any templates that start with "2-" for the context.

__NOTE__: If no template ID is specified, or if a file specified by the template ID is not found, then the default template is used. 
The default is the templates with no number prefix, i.e. *"passwordtempl.html"*


# Templates

## Emails

- *guesttempl.html* 

    Email template used when sending to a guest user (not an OX account)
    
- *passwordtempl.html*

    Template used to send a new password to a guest user. As of 2.8, only used if *newGuestsRequirePassword=true* is configured. 

- *resettempl.html* 

    Template used when sending a password reset
    
- *guestresettempl.html* 
    Template used when resetting guest account and password recovery is disabled
    
- *header.html*

    A header template that may be optionally added (see header/footer below)

- *footer.html*

    A footer template that may be optionally added


# Email Template GetText

In the HTML templates, wording is surrounded by a call to gettext, which will get the translation for the user. It is used in a HTML call *<$gettext("text here")>*. Example:

```
<$gettext("You have received this email because $from has sent you a secure email message with OX Guard. You will receive a link to the secure message in a separate email.")>
```

# Variables
Some email templates have space for variables depending on their function. The variable name will begin with *$* such as the above example *$from*. 

The following variables are available:

- $year  :  Current year
- $time  :  Current time
- $datetime  : Current date and time
- $sender  : Sender of the email
- $from  : Essentially the same as sender
- $givenname  :  Given name of the sender
- $surname  :  Surname of the sender
- $plaintext : The greeting message sent by the sender that will remain in plaintext

In addition, custom variables may be created which will be configurable using configuration cascade.  These variables are defined in the setting com.openexchange.guard.templateVariables as a comma deliniated list.  Then each variable should be defined.  

Example:

```
com.openexchange.guard.templateVariables=support,country
com.openexchange.guard.templateVariables.support=support@example.com
com.openexchange.guard.templateVariables.country=USA
```



# Conditionals

There are 3 conditionals included with Guard

- #if(guestmessage) or #if(!guestmessage)

  Conditional if the sender included a guestmessage (referenced by $plaintext)

- #if(header) or #if(!header)

  If header template is to be included

- #if(footer) or #if(!footer)

  If footer template is to be included

Example:

```
#if(!footer)
  <div style="text-align: left">
     <$gettext("Please don't reply to this email.")>
  </div>
#endif
#if(footer)
#add(footer)
#endif
```



# Header and Footer

Custom header/footers may be used for branding or other customization.  This can be configured for users through the configuration cascade

```
com.openexchange.guard.templateAddHeader=true/false
com.openexchange.guard.templateAddFooter=true/false
```



If true, then Guard will attempt to pull the template in the same manner as the main template.  It will look for the template based on the templateId first, and if not found, then use the default.  For example, if the users templateId was 3, and the templateAddHeader is true, then Guard would first search for "3-header.html", and if not found, then it would use "header.html"



The templates should include the instruction "add" then the template subtype.  Conditionals may be used as above.

```
#add(header) 
....
#add(footer)
```



# Guard Product Name Customization 

## Overview

The Guard product name can be configured by general setting for smaller deployments, by configuration cascade, or by URL

## Product name by configuration.

The configuration

*com.openexchange.guard.productName*

can be defined in the guard-core.properties. This product name will be passed to the UI.

This value can also be configured at the configuration cascade level

## Product name by URL

By editing the file yml located in */opt/open-xchange/etc/as-config.yml* the property

*guard.productName*

can be defined based on the browser URL/IP used to address the OX backend. This product name will the be passed to the UI to be displayed by the user. 
