feature_name: Basic OX Guard configuration
properties:
    - key: com.openexchange.guard.dns.allowUnsignedSRVRecords
      description: |
        OX Guard performs a SRV lookup on a recipient's domain in order to query the related key server.
        This option controls whether or not OX Guard trusts unsigned SRV DNS records.
        If set to "false", OX Guard will discard unsigned DNS SRV records. 
        Other DNS requests than SRV are not affected.
      defaultValue: true
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["HKP Client", "WKS Client", "DNS"]
    - key: com.openexchange.guard.apiUserAgent
      description: |
        Defines the API user agent.
      defaultValue: "Open-Xchange Guard Server"
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["REST API"]
    - key: com.openexchange.guard.aesKeyLength
      description: |
        Specify key length for symmetric AES encryption.

        Note:
        AES Key length of 256 is preferred, but not supported on all systems. 
        May need to have the Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction 
        Policy Files installed. 
      defaultValue: 256
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.attachHelpFile
      description: |
        Option to attach help files to outgoing PGP emails. The help files will not
        be displayed in PGP enabled email clients, rather they will only be displayed
        for clients that don't have PGP, and will explain to them that this is a PGP email
        and that they should log onto the UI to read the email. See related attachHelpFileAll
      defaultValue: true
      version: 2.8.0
      reloadable: false
      configcascadeAware: true
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.attachHelpFileAll
      description: |
        Only valid if attachHelPFile true.  attachHelpFileAll true will cause the helpfile to be
        attached to all outgoing PGP emails.  If false, then only users that had Guard created
        keys will have the attach file added.
      defaultValue: true
      version: 8.26.0
      reloadable: false
      configcascadeAware: true
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.authLifeTime
      description: |
        Maximum lifetime of an OX Guard user session.

        Deletion of expired authentication tokens is scheduled once a day (see [[com.openexchange.guard.cronHour]]).

        Can contain units of measurement: D(=days) W(=weeks) H(=hours) M(=minutes).
      defaultValue: 1W
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.guard.cronHour
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Session handling"]
    - key: com.openexchange.guard.backendSSL
      description: |
        Per default the connection between the Guard backend and the configured Open-Xchange
        REST API host is unencrypted. You can optionally encrypt the whole communication between
        those two components by using SSL. Please note: Enabling SSL might decrease performance
        and/or create more system load due to additional encoding of the HTTP streams.
      defaultValue: false
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.guard.cronHour
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Network", "Security"]
    - key: com.openexchange.guard.badMinuteLock
      description: |
        Defines how long (minutes) someone will be locked out after entering a wrong password several times attempts.
      defaultValue: 10
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.guard.badPasswordCount
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Session handling", "Security"]
    - key: com.openexchange.guard.badPasswordCount
      description: |
        Defines how many times a person can attempt to unlock an encrypted item before being locked out.
      defaultValue: 5
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      related: com.openexchange.guard.badMinuteLock
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Session handling", "Security"]
    - key: com.openexchange.guard.connectionTimeout
      description: |
        The timeout for all HTTP(S) connections in ms
      defaultValue: 10000
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.cronHour
      description: |
         At what hour of the day should the Guard service execute the
         internal maintenance cron jobs? Possible values are: 0 - 23
      defaultValue: 2
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Tasks"]
    - key: com.openexchange.guard.databasePassword
      description: |
        The password for the databases
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Credentials"]
    - key: com.openexchange.guard.databaseUsername 
      description: |
        The username to access the OX Backend and Guard database.
        This user needs to have select, create, lock, insert, update privileges.
        Guard database user also should have alter (for updates), drop, index. 
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Credentials"]
    - key: com.openexchange.guard.dbSchemaBaseName
      description: |
        Specifies the base name for the Guard databases. On initialisation
        Guard will create a database with the baseName, then additional
        Guest shards with the name baseName_#.
      defaultValue: oxguard
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database"]
    - key: com.openexchange.guard.defaultLanguage
      description: |
        The default language to use.
      defaultValue: en_US
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["i18n"]
    - key: com.openexchange.guard.defaultAdvanced
      description: |
        Specify whether or not users should be treated as default users by default.
      defaultValue: false
      version: 2.4.0
      reloadable: false
      configcascadeAware: true
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["User Settings"]
    - key: com.openexchange.guard.demo
      description: |
        This is for development- and testing-environments only.

        <b>
        WARNING: 

        If this property is set to "true", various Guard functions can be accessed without authentication needed!

        Make sure this is set to "false" for a productive setup.
        </b>

      defaultValue: false
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Testing"]
    - key: com.openexchange.guard.exposedKeyDurationInHours
      description: |
        Guard's Support-API allows to expose deleted, but backed-up keys for download.
        This property specifies the amount of hours how long a deleted key will be
        marked as "exposed" (downloadable), or 0 for disabling automatic reset of exposed keys.

        Note: Resetting is scheduled once a day (see [[com.openexchange.guard.cronHour]]).
      defaultValue: 168
      related: com.openexchange.guard.cronHour
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.externalEmailURL
      description: |
        When Guard sends an encrypted eMail to members, they may not be using the webmail UI
        to read the email. If [[com.openexchange.guard.attachHelpFile]] is set to true,
        a help file is attached, and a link will be provided to log into
        their webmail to read the encrypted item. This setting is used to point to a generic
        log in for the webmail system. Sent to multiple recipients, so not customised to
        the individual recipient. OK domain:port. HTTPS will always be added
      defaultValue: example.com
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.externalReaderPath
      description: |
        When Guard sends an eMail to external recipients those recipients will be able to
        access the encrypted content by opening a link in that eMail. The description and
        the link of that eMail are not encrypted and always readable by the recipient. The
        link points to the Guard reader for external recipients, a servlet to decrypt and
        display the encrypted eMail content. Specify which domain and path should be used
        The Https link will be created dynamically by Guard.
        This value will be used as the default unless over-written by cascade value
        'com.openexchange.guard.externalReaderURL'.

        <b>This property is deprecated in 2.10.0 and may be removed in further releases.</b>

      defaultValue: example.com/guard/reader/reader.html
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.failForMissingMDC 
      description: |
       If the PGP message does not have MDC tags attached, either 1) message is rejected
       or 2) a warning message is displayed to the user and the email is converted to
       plaintext. False displays the message. Setting to true
       will result in emails being rejected if the MDC data doesn't exist.
      defaultValue: false
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail", "Security"]
    - key: com.openexchange.guard.fileUploadDirectory
      description: |
        The directory used for buffering uploaded data.
        If empty: falls back to use "java.io.tmpdir"

        <b>NOTE: Guard does only buffer non sensitive or encrypted data to disk.</b>
      defaultValue: 10240
      version: 2.8.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.fileUploadBufferThreshhold
      description: |
        The threshold in bytes at which uploaded data will be buffered to disk.

        <b>NOTE: Guard does only buffer non sensitive or encrypted data to disk.</b>

      defaultValue: 10240
      version: 2.8.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.fromEmail
      description: |
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.guestCaching
      description: |
        Enables local caching for Guest users/data.
      defaultValue: true
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Database", "Guests"]
    - key: com.openexchange.guard.guestCleanedAfterDaysOfInactivity
      description: |
        Guest email accounts that aren't used after this number of days are cleaned.  
        The mail items are removed, and guest scheduled for removal.
      defaultValue: 365
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests"]
    - key: com.openexchange.guard.guestSMTPPassword
      description: |
        Specifies the SMTP server's password which is used for sending guest-emails.
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail", "Credentials"]
    - key: com.openexchange.guard.guestSMTPPort
      description: |
        Specifies the SMTP server's port which is used for sending guest-emails.
      defaultValue: 25
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail"]
    - key: com.openexchange.guard.guestSMTPServer
      description: |
        Specifies the SMTP server information for replies of external recipients. Those recipients
        are able to decrypt, display and reply to eMails they receive via the guest interface.
        The SMTP server is also used for sending password reset e-mails
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail"]
    - key: com.openexchange.guard.guestSMTPUsername
      description: |
        Specifies the SMTP username.
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail", "Credentials"]
    - key: com.openexchange.guard.guestSMTPMailFrom
      description: |
        Specifies email address to use in MAIL FROM
      version: 2.8.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail"]
    - key: com.openexchange.guard.keyCacheCheckInterval
      description: |
        This property specifies the interval in seconds to check the RSA cache and re-populate if 
        less than [[com.openexchange.guard.rsaCacheCount]].
        Only applies if [[com.openexchange.guard.rsaCache]] is enabled. 
      defaultValue: 30
      version: 2.4.0
      related: [com.openexchange.guard.rsaCache, com.openexchange.guard.rsaCacheCount]
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.keyValidDays
      description: |
        PGP Keys can have an expiration date. Set the number of days the keys will be valid for. 
        The user will have to create new keys after this date. Set to 0 for no expiration date.
      defaultValue: 3650
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.mailIdDomain
      description: |
        Outgoing e-mails all get assigned a mailID. This is usually in a format of
        a random ID followed by a domain. If empty, this domain will be
        the AppSuite domain from [[com.openexchange.guard.externalEmailURL]]
        Other domain can be configured here
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Guests", "E-Mail"]
    - key: com.openexchange.guard.mailResolverUrl
      description: |
        Mail resolver URL. Guard needs to be able to lookup an email
        address against the list of OX users. By default, it will
        try to do this against the OX backend. If there is a custom
        mail resolver, set it here. The email address will be appended to
        the end of the URL.

        More details here: [[http://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver]]
      version: 2.4.0
      related: [com.openexchange.guard.mailResolverUrl.basicAuthUsername, com.openexchange.guard.mailResolverUrl.basicAuthPassword]
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Mail-Resolver", "E-Mail"]
    - key: com.openexchange.guard.mailResolverUrl.basicAuthUsername
      description: |
        Specifies the username for the basic HTTP authentication used for accessing the mail resolver.
        If no username is specified here, [[com.openexchange.guard.restApiUsername]] will be used as fallback (default).

        More details here: [[http://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver]]
      related: [com.openexchange.guard.mailResolverUrl]
      version: 2.8.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Mail-Resolver", "E-Mail", "Credentials"]
    - key: com.openexchange.guard.mailResolverUrl.basicAuthPassword
      description: |
        Specifies the password for the basic HTTP authentication used for accessing the mail resolver.
        If no password is specified here, [[com.openexchange.guard.restApiPassword]] will be used as fallback (default)

        More details here: [[http://oxpedia.org/wiki/index.php?title=AppSuite:GuardMailResolver]]
      related: [com.openexchange.guard.mailResolverUrl]
      version: 2.8.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Mail-Resolver", "E-Mail", "Credentials"]
    - key: com.openexchange.guard.maxHttpConnections
      description: |
        Max number of connections to same URL (route).
      defaultValue: 50
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.maxTotalConnections
      description: |
        Max number of connections from Guard to backends.
      defaultValue: 50
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.minPasswordLength
      description: |
        Minimum password length for Guard PGP keys.
      defaultValue: 6
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.newPassLength
      description: |
        Length of the randomly generated passwords when a user resets password.
      defaultValue: 10
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.newGuestsRequirePassword
      description: |
        When creating a new Guest, option to send them a first password
        in a seperate email, or let them assign password on first login.
      defaultValue: false
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.noDeletePrivate
      description: |
        If the user should not be able to delete the private keys, only retract, select true here.
        Can also set configuration cascade value 'com.openexchange.capability.guard-nodeleteprivate=true'.
        If either is true, then the user won't be able to delete his/her keys
      defaultValue: false
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.noDeleteRecovery
      description: |
        Option to deny users the ability to delete the password recovery.
        There will be no way to retrieve the password if the recovery is deleted and the user forgets
        their password. Users, though, may want to delete the recovery for security reasons.

        Can also set configuration cascade value 'com.openexchange.capability.guard-nodeleterecovery=true'.
        If either is true, then the user will not be able to delete their password recovery
      defaultValue: false
      related: [com.openexchange.guard.noRecovery]
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.noRecovery
      description: |
        Enables or disables password recovery. 
        
        If password recovery is disabled, then there will be no way to recover passwords. 
        This will increase the security of the system, but will result in complete data loss if password
        is lost. 

        Can also set configuration cascade value 'com.openexchange.capability.guard-norecovery=true'.
        If either is true, then no recovery will be available.
      defaultValue: false
      related: [com.openexchange.guard.noDeleteRecovery]
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Security"]
    - key: com.openexchange.guard.oxBackendIdleTime
      description: |
        HTTP connections to the backend are kept open for faster response. 
        This is the timeout setting that will close idle connections.
      defaultValue: 60
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.oxBackendPath
      description: |
        URL used to communicated directly with the OX backend.
      defaultValue: "/ajax/"
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Network"]
    - key: com.openexchange.guard.oxBackendPort
      description: |
        Port for communicating with the OX Backend/REST API.
      defaultValue: 8009
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["REST API"]
    - key: com.openexchange.guard.oxGuardDatabaseHostname
      description: |
        Specify the hostname / IP address of the Guard database.
      defaultValue: localhost
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Database"]
    - key: com.openexchange.guard.oxGuardDatabaseInitConnectionTimeout
      description: |
        Defines the connection timeout used for init connections to the guard database.
      defaultValue: 15000
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Database"]
    - key: com.openexchange.guard.oxGuardDatabaseRead
      description: |
        Optional read-only IP/name for the OX Guard database that might be used in Master-Slave setups. 
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database"]
    - key: com.openexchange.guard.oxGuardShardDatabase
      description: |
        Specify the hostname / IP address of Guard guest shards.
        This is for the database shards used when sending to Guest users.
        Defines where the NEXT shard will go when created.
        Stores the Guest keys.
      defaultValue: localhost
      version: 2.4.0
      reloadable: false
      related: "com.openexchange.guard.shardSize"
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Guests"]
    - key: com.openexchange.guard.oxGuardShardDatabaseInitConnectionTimeout
      description: |
        Defines the connection timeout used for init connections to the shard database
      defaultValue: 15000
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Guests"]
    - key: com.openexchange.guard.oxGuardShardRead
      description: |
        Optional read-only IP/name for Guest database shards that might be used in Master-Slave setups.
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Guests"]
    - key: com.openexchange.guard.passwordFromAddress
      description: |
        Specifies the sender e-mail address for the password reset e-mail.
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["E-Mail", "Keymanagement"]
    - key: com.openexchange.guard.passwordFromName
      description: |
        Specifies the display name for the password reset e-mail.
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["E-Mail", "Keymanagement"]
    - key: com.openexchange.guard.pgpCacheDays
      description: |
        PGP Key's fetched from remote servers are stored in a cache for a set period of time
        before the remote servers are queried again. Set the time in days for the cache here.
      defaultValue: 7
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "HKP Client", "WKS Client"]
    - key: com.openexchange.guard.maxRemoteKeySize
      description: |
        Max allowed size for remote keys found using HKP or WKS.  Defaults to 150000 bytes
      defaultValue: 150000
      version: 2.10.5
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties 
      packageName: open-xchange-guard
      tags: ["Keymanagement", "HKP Client", "WKS Client"]
    - key: com.openexchange.guard.pinEnabled
      description: |
        Enables or disables PIN based 2FA authentication for guest emails.
        The sender assigns a randomly generated PIN to the new Guest account, and must
        convey this number to the recipient however they choose (phone, sms, etc).
        PIN is required for the recipient to open any encrypted items or change password.

        Note: 
        The capability com.openexchange.capability.guard-pin=true should be assigned
        to any user/context that you want the pin functionality enabled. 
        Available in guard-api.properties file.
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["E-Mail", "Guests", "2FA"]
    - key: com.openexchange.guard.productName
      description: |
        For customizing the product name, you can redefine the name here.
        This name will be used like "Guided tour for Name" or "Name Security Settings"
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Customizing"]
    - key: com.openexchange.guard.upgrader.version
      description: |
        Defines the version of the 'Upgrader'
      defaultValue: 7
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Upgrade"]
    - key: com.openexchange.guard.remoteKeyLookupTimeout
      description: |
        Total amount of time that Guard should search for remote keys before giving up in ms
      defaultValue: 10000
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement", "HKP Client", "WKS Client"]
    - key: com.openexchange.guard.restApiHostname
      description: |
        Specify the hostname of the Open-Xchange REST API server. 
        The REST API is a service on the Open-Xchange backend. Use localhost in case that the Guard service
        is deployed on the same system as the Open-Xchange backend. In case that the REST
        API is deployed on a separate system ensure that Guard can connect, see clustering
        documentation for Guard for more details.
      defaultValue: localhost
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["REST API"]
    - key: com.openexchange.guard.restApiPassword
      description: |
        Specify the authentication password for the basic HTTP authentication
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["REST API", "Credentials"]
    - key: com.openexchange.guard.restApiUsername
      description: |
        Specify the authentication username for the basic HTTP authentication
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["REST API", "Credentials"]
    - key: com.openexchange.guard.rsaCache
      description: |
        This property controlls if RSA keys are pre-generated in the background, encrypted, and
        stored for future user keys. RSA key generation is the most time consuming function and 
        the RSA cache significantly improves new user creation time.
      defaultValue: true
      related: [com.openexchange.guard.keyCacheCheckInterval, com.openexchange.guard.rsaCacheCount]
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.rsaCacheCount
      description: |
        Number of RSA keys to pre-generate if [[com.openexchange.guard.rsaCache]] is enabled.
      defaultValue: 100
      related: [com.openexchange.guard.keyCacheCheckInterval, com.openexchange.guard.rsaCache]
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.rsaCertainty
      description: |
        Bit certainty for RSA key generation. Higher numbers assure the number is in fact prime 
        but time consuming. Lower is much faster. May need to be lower if not using cache
      defaultValue: 256
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.rsaKeyLength
      description: |
        Specify encryption strength and length of OX Guard genereated PGP keys.
      defaultValue: 2048
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.secureReply
      description: |
        Specifies if a replay to an encrypted E-Mail must also be encrypted.
      defaultValue: true
      version: 2.4.0
      reloadable: false
      configcascadeAware: true
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["E-Mail"]
    - key: com.openexchange.guard.shardSize
      description: |
        Guest users data are placed in databases oxguard_x. After set number of users, another database shard is created
      defaultValue: 10000
      version: 2.4.0
      related: ["com.openexchange.guard.dbSchemaBaseName","com.openexchange.guard.oxGuardShardDatabase"]
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Database", "Guests"]
    - key: com.openexchange.guard.useStartTLS
      description: |
        Use TLS when delivering to the SMTP server when available.
      defaultValue: true
      version: 2.4.0
      related: com.openexchange.guard.guestSMTPServer
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Network", "E-Mail", "Security"]
    - key: com.openexchange.guard.templateID
      description: |
        Defines the template identifier.
      defaultValue: 0
      version: 2.4.0
      reloadable: false
      configcascadeAware: true
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Customizing"]
    - key: com.openexchange.guard.templatesDirectory
      description: |
        Specifies the path for the templates.
      defaultValue: "/opt/open-xchange/templates/guard"
      version: 2.4.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Customizing"]
    - key: com.openexchange.guard.keySources.trustThreshold
      description: |
        A threshold value to define which key sources are trusted.
        Every key source with a trust value equals or higher than this value is considered to be marked as "trusted".
        The trustlevel can give a hint to the client/enduser if a fetched recipient's key could be trustable.
      defaultValue: 4
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Key trust"]
    - key: com.openexchange.guard.keySources.trustLevelGuard
      description: |
        The trust level for keys created by OX Guard.
      defaultValue: 5
      related: com.openexchange.guard.keySources.trustThreshold
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Key trust"]
    - key: com.openexchange.guard.keySources.trustLevelGuardUserUploaded
      description: |
        The trust level for keys uploaded by a user.
      defaultValue: 4
      related: com.openexchange.guard.keySources.trustThreshold
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Key trust"]
    - key: com.openexchange.guard.keySources.trustLevelGuardUserShared
      description: |
        The trust level for keys uploaded by a user and shared among users in the same context.
      defaultValue: 3
      related: com.openexchange.guard.keySources.trustThreshold
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement", "Key trust"]
    - key: com.openexchange.guard.baseDomain
      description: |
        Specify the base domain for generated links, such as used in support.  Used with uiWebPath to construct URL links.  Example https://baseDomain/uiWebPath/link
      version: 2.10.0
      reloadable: false
      configcascadeAware: false
      related:
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Link", "Domain"]
    - key: com.openexchange.guard.masterKeyIndex
      description: |
        Specify the master key index to use for newly created keys.  This key is used to encrypt database entries for the user.  The master keys must already exist 
      version: 2.10.5
      defaultValue: 0
      reloadable: false
      configcascadeAware: true
      related:
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.masterKeyPath
      description: |
        Specify the path for the oxguardpass files. Default location is /opt/open-xchange/guard 
      version: 2.10.5
      defaultValue: /opt/open-xchange/guard
      reloadable: false
      configcascadeAware: false
      related:
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["Keymanagement"]
    - key: com.openexchange.guard.storage.file.fileStorageType
      description: |
        Needs to be set in order to specify the filestorage type. Possible values are "file,s3,sproxyd".
      version: 2.10.1
      reloadable: false
      configcascadeAware: false
      related: [com.openexchange.guard.storage.s3.s3FileStore,com.openexchange.guard.storage.file.uploadDirectory]
      file: guard-core.properties
      packageName: open-xchange-guard
      tags: ["File Storage","S3 Storage"]
      
      
