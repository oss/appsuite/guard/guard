---
title: OX Guard documenation
classes: no-counting
---

# General Information

OX Guard is a fully integrated security add-on to OX App Suite that provides end users with a flexible email and file encryption solution. OX Guard is a highly scalable, multi server, feature rich solution that is so simple-to-use that end users will actually use it

These files contain configuration settings available for OX Guard

