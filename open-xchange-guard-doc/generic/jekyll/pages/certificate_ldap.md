---
title: Certificate LDAP 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "CertificateLdap"
---

The configuration options for certificate lookup in ldap

