---
title: File Storage 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "File-Storage"
---

The configuration options for Guard file storage for Guest emails

