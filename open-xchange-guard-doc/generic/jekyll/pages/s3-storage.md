---
title: S3 Storage 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "S3-Storage"
---

The configuration options for S3 Storage for Guard Guest emails

