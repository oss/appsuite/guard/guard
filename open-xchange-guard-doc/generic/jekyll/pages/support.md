---
title: Support API 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "Support-API"
---

The configuration options for the Support API

