---
title: WKS Client 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "WKSClient"
---

The configuration options for configuring the WKS Client

