---
title: Backend 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "Backend-Plugin"
---

The configuration options for Backend-Plugin which must be bundled with the core MW

