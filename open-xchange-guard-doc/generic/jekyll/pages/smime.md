---
title: S/Mime 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "Smime"
---

The configuration options for S/Mime

