---
title: Autocrypt 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "AutoCrypt"
---

The configuration options for Autocrypt

