---
title: Templates 
icon: fa-cog
tags: Properties
layout: guard_properties
properties_file: "Templates"
---

The configuration options for configuring the email templates used when sending Guard mails

