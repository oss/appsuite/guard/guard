/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.mime.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MimeParser {

    private static Logger LOG = LoggerFactory.getLogger(MimeParser.class);
    private static final String BEGIN_PGP_MESSAGE_MARKER = "-----BEGIN PGP MESSAGE-----";
    private final MimeMessage message;

    public MimeParser(final InputStream inputStream) throws MessagingException {
        final Session s = Session.getDefaultInstance(new Properties());
        message = new MimeMessage(s, inputStream);
    }

    public MimeParser(final MimeMessage message) {
        this.message = message;
    }

    public MimeMessage getMessage() {
        return (message);
    }

    public boolean isPGPMime() throws MessagingException {
        if (message.getContentType() != null && message.getContentType().toString().toLowerCase().contains("multipart/mixed")) {
            Multipart mp;
            try {
                mp = (Multipart) message.getContent();
                final int count = mp.getCount();
                for (int i = 0; i < count; i++) {
                    final BodyPart bp = mp.getBodyPart(i);
                    if (bp.getContentType().toLowerCase().contains("pgp-encrypted")) {
                        return true;
                    }
                }
            } catch (IOException e) {
                LOG.error("Problem parsing email to check if Mime message", e);
            }
        }
        // This should not really ever return positive, as PGP MIME by standards should be multipart/mixed.  ? Return false always
        return message.getContentType() != null && message.getContentType().toString().toUpperCase().contains("APPLICATION/PGP-ENCRYPTED");
    }

    public boolean isPGPInline() throws IOException, MessagingException {

        byte[] peekedData = new byte[1024];
        int read = message.getInputStream().read(peekedData);
        if(read > 0) {
            String peekedContent = new String(peekedData, StandardCharsets.UTF_8);
            return peekedContent.contains(BEGIN_PGP_MESSAGE_MARKER);
        }

        return false;
    }

    /**
     * Get the first MIME boundry/border in a mime message
     *
     * @param msg
     * @return null if no boarder specified in content-type
     */
    public static String getBoundry(final MimeMessage msg) {
        try {
            final String[] content = msg.getHeader("Content-Type");
            if (content != null && content.length > 0) {
                final int i = content[0].indexOf("boundary");
                if (i > 0) {
                    String bndry = content[0].substring(i);
                    if (bndry.contains(";")) {
                        bndry = bndry.substring(0, bndry.indexOf(";"));
                    }
                    return (bndry.substring(bndry.indexOf("\"")).replace("\"", ""));
                }
            }
        } catch (final Exception e) {
            LOG.error("Error finding mime border ", e);
        }
        return (null);
    }

    /**
     * Function to parse out first section of mime body using the border
     *
     * @param mime The mime body
     * @param border The mime section border
     * @return
     */
    public static String getMimeSection(final String mime, String border) {
        if (border == null) {
            return (mime);
        }
        try {
            border = "--" + border;
            final int j = mime.indexOf(border);
            final int k = mime.indexOf(border, j + 10);
            if (j > 0 && k > 0) {
                final String data = mime.substring(j + 2 + border.length(), k - 2);
                return (data);
            }
        } catch (final Exception e) {
            LOG.error("Error parsing mime section for email");
        }

        return (mime);
    }
}
