package com.openexchange.guard.encryption.internal;


public class EncryptedItemsSql {

    public static final String INSERT_STMT = "INSERT INTO og_encrypted_items (Id, Owner, Expiration, Type, XML, Salt, Owner_cid) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE Type = Type";
    public static final String SELECT_BY_ID_STMT = "SELECT Id,Owner, Owner_cid, Expiration, Type, XML, Salt from og_encrypted_items WHERE Id = ?";
}
