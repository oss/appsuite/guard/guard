
package com.openexchange.guard.encryption.internal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.openexchange.exception.OXException;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.database.utils.DBUtils;
import com.openexchange.guard.database.utils.GuardConnectionWrapper;
import com.openexchange.guard.encryption.EncryptedItem;
import com.openexchange.guard.encryption.EncryptedItemsStorage;
import com.openexchange.guard.encryption.osgi.Services;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;

/**
 * {@link EncryptedItemsStorageImpl} - Rest API implementation of {@link EncryptedItemsStorage}
 */
public class EncryptedItemsStorageImpl implements EncryptedItemsStorage {

    @Override
    public void insertForRecipient(EncryptedItem item, int recipientId, int recipientContextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getWritable(recipientId, recipientContextId, 0);

        PreparedStatement stmt = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(EncryptedItemsSql.INSERT_STMT);
            stmt.setString(1, item.getItemId());
            stmt.setInt(2, item.getOwnerId());
            stmt.setLong(3, item.getExpiration());
            stmt.setInt(4, item.getType());
            stmt.setString(5, item.getXml());
            stmt.setString(6, item.getSalt());
            stmt.setInt(7, item.getOwnerCid());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(stmt);
            guardDatabaseService.backWritable(connectionWrapper);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.database.storage.ogEncryptedItems.EncryptedItemsStorage#insertForRecipientTruncated(java.lang.String, int, int, int, int, long, int, java.lang.String, java.lang.String)
     */
    @Override
    public void insertForRecipientTruncated(String itemId, int recipientId, int recipientCid, int owner, int ownerCid, long expirationDate, int type, String xml, String salt) throws OXException {
        // Store item. If multiple recips within the same context database, then there will be duplicates we can ignore
        if (xml.length() > 3072) {
            xml = xml.substring(0, 3071);
        }
        insertForRecipient(new EncryptedItem(itemId, owner, ownerCid, expirationDate, type, xml, salt), recipientId, recipientCid);
    }

    @Override
    public EncryptedItem getById(String id, int ownerId, int ownerContextId) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(ownerId, ownerContextId, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            stmt = connectionWrapper.getConnection().prepareStatement(EncryptedItemsSql.SELECT_BY_ID_STMT);
            stmt.setString(1, id);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new EncryptedItem(resultSet.getString("Id"), resultSet.getInt("Owner"), resultSet.getInt("Owner_cid"), resultSet.getLong("Expiration"), resultSet.getInt("Type"), resultSet.getString("XML"), resultSet.getString("Salt"));
            }
            return null;
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
    }



    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.database.storage.ogEncryptedItems.EncryptedItemsStorage#getXML(int, int, java.lang.String)
     */
    @Override
    public String getXML(int userid, int cid, String itemid) throws OXException {
        GuardDatabaseService guardDatabaseService = Services.getService(GuardDatabaseService.class);

        GuardConnectionWrapper connectionWrapper = guardDatabaseService.getReadOnly(userid, cid, 0);

        PreparedStatement stmt = null;
        ResultSet resultSet = null;
        try {
            String command = "SELECT XML FROM og_encrypted_items WHERE Id = ? AND Owner = ? AND Owner_cid = ?";
            stmt = connectionWrapper.getConnection().prepareStatement(command);
            stmt.setString(1, itemid);
            stmt.setInt(2, userid);
            stmt.setInt(3, cid);

            resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString("XML");
            }
        } catch (SQLException e) {
            throw GuardCoreExceptionCodes.SQL_ERROR.create(e, e.getMessage());
        } finally {
            DBUtils.closeSQLStuff(resultSet, stmt);
            guardDatabaseService.backReadOnly(connectionWrapper);
        }
        return null;
    }



}
