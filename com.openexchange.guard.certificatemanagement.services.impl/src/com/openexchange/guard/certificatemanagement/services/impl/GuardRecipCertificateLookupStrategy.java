/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.certificatemanagement.services.impl;

import java.util.List;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.RecipCertificateLookupStrategy;
import com.openexchange.guard.certificatemanagement.commons.RecipCertificate;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.keymanagement.commons.trust.KeySource;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link GuardRecipCertificateLookupStrategy} searches for a {@link @RecipCertificate} through regular OX Guard users
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class GuardRecipCertificateLookupStrategy implements RecipCertificateLookupStrategy {

    private static KeySource DEFAULT_KEYSOURCE = new KeySource("GUARD_CERTIFICATES", Integer.MAX_VALUE);

    private final ServiceLookup services;

    /**
     * Initializes a new {@link GuardRecipCertificateLookupStrategy}.
     *
     * @param services The {@link ServiceLookup}
     */
    public GuardRecipCertificateLookupStrategy(ServiceLookup services) {
        this.services = services;
    }

    @Override
    public RecipCertificate lookup(int userId, int contextId, String email, int timeout) throws OXException {
        SmimeKeyService pubService = services.getServiceSafe(SmimeKeyService.class);
        List<SmimeKeys> keys = pubService.getPublicKeys(email);
        if (keys != null && keys.size() > 0) {
            SmimeKeys validKey = PreferredPublic.findPreferredEncrypting(keys, services, userId, contextId);
            if (validKey != null) {
                return new RecipCertificate(validKey, DEFAULT_KEYSOURCE);
            }
        }
        return null;
    }
}
