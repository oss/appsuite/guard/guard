/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets;


import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;


/**
 * {@link GuardErrorResponseRendererTest}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class GuardErrorResponseRendererTest {

    /**
     * @throws java.lang.Exception
     */
    @BeforeEach
    public void setUp() throws Exception {}

    /**
     * Test method for {@link com.openexchange.guard.servlets.GuardErrorResponseRenderer#createErrorJson(com.openexchange.exception.OXException, java.util.Locale, boolean)}.
     */
    @Test
    public void bug44308Test_noLogArgs_doNotThrowNPE() {
        OXException oxException = new OXException();

        JsonObject createErrorJson = new GuardErrorResponseRenderer(HttpServletResponse.SC_OK).createErrorJson(oxException, new Locale("en"), true);

        assertNotNull(createErrorJson);
        JsonElement jsonElement = createErrorJson.get("error_params");
        assertNotNull(jsonElement);
        JsonArray asJsonArray = jsonElement.getAsJsonArray();
        assertTrue(asJsonArray.size() == 0);
    }
}
