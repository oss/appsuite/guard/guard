/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.ping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.servlets.AbstractGuardServlet;

/**
 * Test servlet for checking if OX Guard and the backend's REST API is available
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class PingServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = -341701020579115246L;
    private static final String ACTION_PING = "ping";
    private static Logger logger = LoggerFactory.getLogger(PingServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        logger.info(String.format("Ping command sent from IP %s", ServletUtils.getClientIP(request)));
        doAction(request,response,ACTION_PING,new PingAction());
    }
}
