/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.OXGuardSessionAuthenticationHandler;
import com.openexchange.guard.servlets.keymanagement.actions.AddUserIDAction;
import com.openexchange.guard.servlets.keymanagement.actions.CreateKeyPairAction;
import com.openexchange.guard.servlets.keymanagement.actions.DeleteExternalPublicKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.DeleteKeyPairAction;
import com.openexchange.guard.servlets.keymanagement.actions.DownloadKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetContactKeysAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetExternalPublicKeysAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetKeysAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetRecipKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.GetSignaturesAction;
import com.openexchange.guard.servlets.keymanagement.actions.HasKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.ImportAutocryptKeys;
import com.openexchange.guard.servlets.keymanagement.actions.PutAutoCryptKey;
import com.openexchange.guard.servlets.keymanagement.actions.RevokeKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.SetCurrentKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.SetExternalKeyInlineModeAction;
import com.openexchange.guard.servlets.keymanagement.actions.SetExternalKeyShareModeAction;
import com.openexchange.guard.servlets.keymanagement.actions.StartAutoCryptKeyImport;
import com.openexchange.guard.servlets.keymanagement.actions.TransferAutoCryptKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.UploadExternalPublicKeyAction;
import com.openexchange.guard.servlets.keymanagement.actions.UploadKeysAction;
import com.openexchange.guard.servlets.keymanagement.actions.VerifyAutoCryptKeyAction;
import com.openexchange.server.ServiceLookup;

/**
 * {@link KeymanagementServlet}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class KeymanagementServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = -1711171810438527067L;
    private static final String HAS_KEY_ACTION = "hasKey";
    private static final String GET_KEYS_ACTION = "getKeys";
    private static final String GET_KEY_ACTION = "getKey";
    private static final String DOWNLOAD_KEY_ACTION = "downloadKey";
    private static final String SET_CURRENT_KEY_ACTION = "setCurrentKey";
    private static final String CREATE_ACTION = "create";
    private static final String UPLOAD_ACTION = "upload";
    private static final String GET_RECIP_KEY_ACTION = "getRecipKey";
    private static final String DELETE_KEY_ACTION = "delete";
    private static final String GET_EXTERNAL_PUBLIC = "getExternalPublicKeys";
    private static final String GET_CONTACT_KEYS = "getContactKeys";
    private static final String UPLOAD_EXTERNAL_PUBLIC = "uploadExternalPublicKey";
    private static final String DELETE_EXTERNAL_PUBLIC = "deleteExternalPublicKey";
    private static final String SHARE_EXTERNAL_PUBLIC = "shareExternalPublicKey";
    private static final String INLINE_EXTERNAL_PUBLIC = "inlineExternalPublicKey";
    private static final String VERIFY_AUTOCRYPT_PUBLIC = "verifyAutoCryptKey";
    private static final String START_AUTOCRYPT_IMPORT = "startAutoCryptImport";
    private static final String IMPORT_AUTOCRYPT_KEY = "importAutoCryptKeys";
    private static final String ADD_USER_ID = "addUserId";
    private static final String REVOKE_KEY = "revoke";
    private static final String GET_SIGNATURES = "getSignatures";
    private static final String PUT_AUTOCRYPT = "putautocrypt";
    private static final String POST_AUTOCRYPT_TRANSFER = "autocrypttransfer";
    private transient final HashMap<String, GuardServletAction> getActions;
    private transient final HashMap<String, GuardServletAction> postActions;
    private transient final HashMap<String, GuardServletAction> putActions;
    private transient final HashMap<String, GuardServletAction> deleteActions;

    /**
     * Initializes a new {@link KeymanagementServlet}.
     */
    public KeymanagementServlet() {

        OXGuardSessionAuthenticationHandler sessionAuthenticationHandler = new OXGuardSessionAuthenticationHandler();

        //GET
        getActions = new HashMap<String, GuardServletAction>();
        getActions.put(HAS_KEY_ACTION, new HasKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_RECIP_KEY_ACTION, new GetRecipKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_KEYS_ACTION, new GetKeysAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_EXTERNAL_PUBLIC, new GetExternalPublicKeysAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_CONTACT_KEYS, new GetContactKeysAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_SIGNATURES, new GetSignaturesAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(START_AUTOCRYPT_IMPORT, new StartAutoCryptKeyImport().setAuthenticationHandler(sessionAuthenticationHandler));

        //POST
        postActions = new HashMap<String, GuardServletAction>();
        postActions.put(CREATE_ACTION, new CreateKeyPairAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(GET_KEY_ACTION, new GetKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(DOWNLOAD_KEY_ACTION, new DownloadKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(UPLOAD_EXTERNAL_PUBLIC, new UploadExternalPublicKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(UPLOAD_ACTION, new UploadKeysAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(DELETE_KEY_ACTION, new DeleteKeyPairAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(REVOKE_KEY, new RevokeKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(ADD_USER_ID, new AddUserIDAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(IMPORT_AUTOCRYPT_KEY, new ImportAutocryptKeys().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(POST_AUTOCRYPT_TRANSFER, new TransferAutoCryptKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));

        //PUT
        putActions = new HashMap<String, GuardServletAction>();
        putActions.put(SHARE_EXTERNAL_PUBLIC, new SetExternalKeyShareModeAction().setAuthenticationHandler(sessionAuthenticationHandler));
        putActions.put(INLINE_EXTERNAL_PUBLIC, new SetExternalKeyInlineModeAction().setAuthenticationHandler(sessionAuthenticationHandler));
        putActions.put(SET_CURRENT_KEY_ACTION, new SetCurrentKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
        putActions.put(PUT_AUTOCRYPT, new PutAutoCryptKey().setAuthenticationHandler(sessionAuthenticationHandler));
        putActions.put(VERIFY_AUTOCRYPT_PUBLIC, new VerifyAutoCryptKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));

        //DELETE
        deleteActions = new HashMap<String, GuardServletAction>();
        deleteActions.put(DELETE_EXTERNAL_PUBLIC, new DeleteExternalPublicKeyAction().setAuthenticationHandler(sessionAuthenticationHandler));
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, getActions);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, postActions);
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, deleteActions);
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, putActions);
    }
}
