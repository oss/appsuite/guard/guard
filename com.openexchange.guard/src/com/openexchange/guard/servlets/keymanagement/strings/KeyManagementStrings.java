/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.strings;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link KeyManagementStrings}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.2
 */
public class KeyManagementStrings implements LocalizableStrings {

    // PGP Signature Type:  Generic certification of a user's ID
    public static final String SIGNATURE_TYPE_GENERIC = "Generic certification of a User ID";

    // PGP Signature Type:  Certification of a persona
    public static final String SIGNATURE_TYPE_PERSONA = "Persona certification of a User ID";

    // PGP Signature Type: Casual certification of a user's ID
    public static final String SIGNATURE_TYPE_CASUAL = "Casual certification of a User ID";

    // PGP Signature Type: A positive (definite) certification of a user's ID
    public static final String SIGNATURE_TYPE_POSITIVE = "Positive certification of a User ID";

    // PGP Signature Type: A signature of a subkey
    public static final String SIGNATURE_TYPE_SUBKEY = "Subkey Binding Signature";

    // PGP Signature Type: A signature of the primary key
    public static final String SIGNATURE_TYPE_PRIMARY = "Primary Key Binding Signature";

    // PGP Signature Type: A direct signature of a key.
    public static final String SIGNATURE_TYPE_DIRECT = "Signature directly on a key";

    // PGP Signature Type: A revocation signature.
    public static final String SIGNATURE_TYPE_REVOKE = "Key revocation signature";

    // PGP Signature Type: A revocation of a subkey
    public static final String SIGNATURE_TYPE_SUBKEY_REVOKE = "Subkey revocation signature";

    // PGP Signature Type: Certification of a revocation signature
    public static final String SIGNATURE_TYPE_CERT_REVOKE = "Certification revocation signature";

    // PGP Signature Type: Unknown type of signature
    public static final String SIGNATURE_TYPE_UNKNOWN = "unknown signature type";

    public KeyManagementStrings () {
        super();
    }

}
