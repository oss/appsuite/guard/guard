/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_CREATE_IF_MISSING;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_EMAIL;
import static com.openexchange.guard.servlets.keymanagement.actions.Parameters.PARAM_KEYID;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.pgp.GetPGPRecipKeys;
import com.openexchange.guard.servlets.keymanagement.responses.RecipKeyInfoResponse;

/**
 * {@link GetRecipKeyAction} searches for other user's public keys and is also able to create new keys for users.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class GetRecipKeyAction extends GuardServletAction {



    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        Map<String, String[]> params = getHttpParameters(request);
        final String email = ServletUtils.getStringParameter(params, PARAM_EMAIL, false);
        if (email != null) {
            GuardRatifierService ratifier = Services.getService(GuardRatifierService.class);
            ratifier.validate(email);
        }
        final Long keyId = ServletUtils.getLongParameter(params, PARAM_KEYID, false);
        final boolean createIfMissing = ServletUtils.getBooleanParameter(params, PARAM_CREATE_IF_MISSING, false);

        //If a keyID was specified it has to be a valid long value
        if (keyId == null && ServletUtils.hasParameter(params, PARAM_KEYID)) {
            throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create(PARAM_KEYID);
        }

        //We need either key id or email
        if (keyId == null && Strings.isEmpty(email)) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create(PARAM_KEYID);
        }

        //It does not make sense to specify the create parameter while searching for a specific key ID
        if (keyId != null && createIfMissing) {
            throw GuardCoreExceptionCodes.PARAMETER_MISMATCH.create(PARAM_KEYID, PARAM_CREATE_IF_MISSING);
        }
        RecipKeyInfoResponse recip = GetPGPRecipKeys.getKeys(email, keyId, createIfMissing, userSession.getUserId(), userSession.getContextId());
        ServletUtils.sendObject(response, recip);
    }



}
