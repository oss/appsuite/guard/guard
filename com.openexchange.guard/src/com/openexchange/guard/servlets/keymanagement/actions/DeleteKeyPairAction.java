/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.actions;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.keymanagement.pgp.DeletePGPKeyPair;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link DeleteKeyPairAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class DeleteKeyPairAction extends GuardServletAction {


    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws OXException {
        //Brute Force protect the key deletion if the key ring has a password protected private key.
        final UserIdentity userIdentity = getUserIdentityFrom(request, userSession.getUserId(), userSession.getContextId());

        final Map<String, String[]> parms = getHttpParameters(request);
        final String keyIdString = getParameter(parms, "keyid");
        if (keyIdString == null) {
            throw OXException.mandatoryField("keyid");
        }
        AntiAbuseWrapper antiAbuse = getAntiAbuseWrapper(request, userSession, userIdentity == null ? keyIdString : new String(userIdentity.getPassword()));
        try {
            String actionResponse = antiAbuse.doAction(new AntiAbuseAction<String>() {

                @Override
                public String doAction() throws Exception {
                    return DeletePGPKeyPair.doPGPDeletePGPKeyPair(Long.parseLong(keyIdString), userSession.getUserId(), userSession.getContextId(), userSession.getGuardUserId(), userSession.getGuardContextId(),
                        userIdentity == null ? null : new String(userIdentity.getPassword()));
                }
            });
            ServletUtils.sendObject(response, actionResponse);
            return;
        } catch (Exception e) {
            if (e instanceof OXException) {
                throw (OXException) e;
            } else {
                throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e.getMessage());
            }
        }
    }
}
