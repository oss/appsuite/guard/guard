/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.keymanagement.responses;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.bouncycastle.bcpg.attr.ImageAttribute;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPUserAttributeSubpacketVector;
import com.openexchange.pgp.keys.tools.PGPKeysUtil;

/**
 * {@link PublicKeyInfoResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class PublicKeyInfoResponse {

    private final PGPPublicKey key;
    private final boolean hasPrivateaKey;
    private final boolean usedForEncryption;

    /**
     * Initializes a new {@link PublicKeyInfoResponse}.
     * @param key
     * @param usedForEncryption Whether or not Guard is considering using this key for encryption.
     */
    public PublicKeyInfoResponse(PGPPublicKey key, boolean usedForEncryption) {
        this(key, false, usedForEncryption);
    }

    /**
     * Initializes a new {@link PublicKeyInfoResponse}.
     * @param key
     * @param hasPrivateaKey Whether or not this key has a corresponding private key
     * @param usedForEncryption Whether or not Guard is considering using this key for encryption.
     */
    public PublicKeyInfoResponse(PGPPublicKey key, boolean hasPrivateaKey, boolean usedForEncryption) {
        this.key = key;
        this.hasPrivateaKey = hasPrivateaKey;
        this.usedForEncryption = usedForEncryption;
    }

    public String getId() {
        return Long.toString(this.key.getKeyID());
    }

    public boolean isMasterKey() {
        return this.key.isMasterKey();
    }

    public String getFingerPrint() {
        return PGPKeysUtil.getFingerPrint(this.key.getFingerprint());
    }

    public Date getCreationTime () {
        return key.getCreationTime();
    }

    public long getValidSeconds() {
        return key.getValidSeconds();
    }

    public String[] getUserIds() {
        ArrayList<String> userIds = new ArrayList<String>();
        Iterator iter = key.getUserIDs();
        while(iter.hasNext()) {
            userIds.add((String)iter.next());
        }
        return userIds.toArray(new String[userIds.size()]);
    }

    public boolean isEncryptionKey() {
        return PGPKeysUtil.isEncryptionKey(key);
    }

    public boolean isUsedForEncryption() {
       return this.usedForEncryption;
    }

    public boolean isRevoked() {
        return key.hasRevocation();
    }

    public boolean isExpired() {
        return PGPKeysUtil.isExpired(key);
    }

    public boolean getHasPrivateKey() {
        return hasPrivateaKey;
    }

    public PublicKeyImageResponse[] getImages() {
        ArrayList<PublicKeyImageResponse> ret = new ArrayList<PublicKeyImageResponse>();
        Iterator<PGPUserAttributeSubpacketVector> userAttributes = this.key.getUserAttributes();
        while (userAttributes.hasNext()) {
            ImageAttribute imageAttribute = userAttributes.next().getImageAttribute();
            if (imageAttribute != null && imageAttribute.getType() == 1) {
                ret.add(new PublicKeyImageResponse(imageAttribute));
            }
        }
        return ret.toArray(new PublicKeyImageResponse[ret.size()]);
    }
}