/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.callback;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;

/**
 * Handles callbacks from the OX backend
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class CallbackServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = 6518623347823371079L;
    private static final String PUT_USERDELETED_CALLBACK_ACTION = "user_delete";
    private static final String PUT_USER_CHANGED = "user_update";
    private static final String PUT_CONTEXTDELETED_CALLBACK_ACTION = "context_delete";
    private static final String CHECK_GUEST = "check_guest";

    private transient final HashMap<String, GuardServletAction> putActions;
    private transient final HashMap<String, GuardServletAction> getActions;
    private transient final HashMap<String, GuardServletAction> postActions;

    private static final Logger logger = LoggerFactory.getLogger(CallbackServlet.class);

    /**
     * Initializes a new {@link CallbackServlet}.
     * @throws OXException
     */
    public CallbackServlet() throws OXException {
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);

        //Protecting the servlet's actions with BASIC-AUTH
        String restApiUsername = configService.getProperty(GuardProperty.restApiUsername);
        String restApiPassword = configService.getProperty(GuardProperty.restApiPassword);
        GuardAuthenticationHandler basicAuthServletAuthentication = new BasicAuthServletAuthenticationHandler(restApiUsername, restApiPassword);
        putActions = new HashMap<String, GuardServletAction>();
        getActions = new HashMap<String, GuardServletAction>();
        postActions = new HashMap<String, GuardServletAction>();
        if(!Strings.isEmpty(restApiUsername) && !Strings.isEmpty(restApiPassword)){

            //PUT
            putActions.put(PUT_USERDELETED_CALLBACK_ACTION, new UserDeletedCallbackAction().setAuthenticationHandler(basicAuthServletAuthentication));
            putActions.put(PUT_CONTEXTDELETED_CALLBACK_ACTION, new ContextDeletedCallbackAction().setAuthenticationHandler(basicAuthServletAuthentication));
            putActions.put(PUT_USER_CHANGED, new UserUpdateCallbackAction().setAuthenticationHandler(basicAuthServletAuthentication));

            //GET - This actions are used to retrieval documents for the OX Documentconverter
            getActions.put(CHECK_GUEST, new CheckGuestAction().setAuthenticationHandler(basicAuthServletAuthentication));
        }
        else{
            logger.error("Denied OX Guard callback servlet initialization due to unset Basic-Auth configuration. Please set properties 'com.openexchangeguard.restApiUsername' and 'com.openexchangeguard.restApiPassword' appropriately.");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, putActions);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, getActions);
    }

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, postActions);
    }
}
