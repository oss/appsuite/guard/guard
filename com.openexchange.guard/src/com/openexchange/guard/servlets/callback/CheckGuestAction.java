/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.callback;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.openexchange.guard.caching.FileCacheItem;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.guest.GuestLookupService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.storage.cache.FileCacheStorage;

/**
 * {@link CheckGuestAction}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class CheckGuestAction extends GuardServletAction {

    /* Simple action to check if the user exists and if has email items in file cache
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        int contextId =  ServletUtils.getIntParameter(request, "context_id", true);
        int userId = ServletUtils.getIntParameter(request, "user_id", true);
        GuestLookupService lookupService = Services.getService(GuestLookupService.class);
        Email guest = lookupService.lookupGuardGuest(userId, contextId);
        boolean found = false;
        if (guest != null) {
            if (guest.getContextId() > 0) {  // Upgrade guest to appsuite user.  No cleanup for customer
                found = true;
            } else {
                FileCacheStorage fs = Services.getService(FileCacheStorage.class);
                List<FileCacheItem> items = fs.getForUser(guest.getUserId(), guest.getContextId(), 0, 2);  // Check if any items exist
                if (!items.isEmpty()) found = true;
            }
        }
        JsonObject resp = new JsonObject();
        resp.addProperty("hasItems", found);
        ServletUtils.sendJsonOK(response, resp);
    }



}
