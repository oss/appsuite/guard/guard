/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.demo;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;


/**
 * Provides helper functionality useful for demonstration or testing environments
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class DemoServlet extends AbstractGuardServlet {

    private static final Logger logger = LoggerFactory.getLogger(DemoServlet.class);
    private static final long serialVersionUID = -3685385753773851617L;
    private static final String RESET_ACTION = "reset";
    private static final String RESET_ALL_ACTION = "resetall";
    private static final String RESET_BAD_ACTION = "resetAntiAbuse";

    private transient final HashMap<String,GuardServletAction> servletGetActions;

    public DemoServlet() throws OXException{
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        if (configService.getBooleanProperty(GuardProperty.demo)) {
            logger.warn("WARNING: The property 'com.openexchange.guard.demo' is set to true, which allows unauthorized deletion of userkeys. Only enble this property for testing purpose!");
        }
        servletGetActions = new HashMap<String,GuardServletAction>();
        servletGetActions.put(RESET_ACTION, new ResetAction());
        servletGetActions.put(RESET_ALL_ACTION, new ResetAllAction());
        servletGetActions.put(RESET_BAD_ACTION, new ResetAntiAbuseAction());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        doAction(request, response, servletGetActions);
    }
}
