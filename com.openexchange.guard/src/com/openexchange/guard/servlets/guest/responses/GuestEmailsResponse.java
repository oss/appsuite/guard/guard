/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest.responses;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;
import com.openexchange.guard.guest.GuardGuestEmail;

/**
 * {@link GuestEmailsResponse}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class GuestEmailsResponse implements Closeable {

    private final Collection<GuardGuestEmail> emails;

    /**
     * Initializes a new {@link GuestEmailsResponse}.
     *
     * @param emails The emails for the response
     */
    public GuestEmailsResponse(Collection<GuardGuestEmail> emails) {
        this.emails = Objects.requireNonNull(emails, "emails must not be null");
    }

    /**
     * Gets a list of email
     *
     * @return The list of emails
     */
    public GuardGuestEmailResponse[] getEmails() {
        return emails.stream().map( e -> new GuardGuestEmailResponse(e)).toArray(GuardGuestEmailResponse[]::new);
    }

    /* (non-Javadoc)
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        for(GuardGuestEmail email : emails) {
            try {
                email.close();
            }
            catch(IOException e) { e.printStackTrace(); }
        }
    }
}
