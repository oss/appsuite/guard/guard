/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.UserIdentityPasswordlessGuestParser;
import com.openexchange.guard.user.OXUserService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuardGuestServletAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public abstract class GuardGuestServletAction extends GuardServletAction {

    /**
     * Parses a {@link UserIdentity} for a guest from the given {@link JSONObject} with just containing the email and no password
     *
     * @param json the json object to parse the guest identity from
     * @return The parsed {@link UserIdentity} with just the email set and no password, or null if no such guest user exists
     * @throws OXException
     */
    protected UserIdentity getPasswordLessGuestUserIdentityFrom(JSONObject json) throws OXException {
        return new UserIdentityPasswordlessGuestParser(Services.getService(OXUserService.class), json).parse(0, 0);  // No password, so 0,0
    }

    /**
     * Parses a {@link UserIdentity} for a guest from the given {@link JSONObject} just containing the email and no password,
     * or throws an exception if the request does not contain enough information to parse the guest, or the guest does not exists.
     *
     * @param json the json object to parse the guest identity from
     * @return The parsed {@link UserIdentity} with just the email set and no password.
     * @throws OXException
     */
    protected UserIdentity requirePasswordLessGuestUserIdentityFrom(JSONObject json, int userid, int cid) throws OXException {
        UserIdentity userIdentity = new UserIdentityPasswordlessGuestParser(Services.getService(OXUserService.class), json).parse(userid, cid);
        final boolean require = true;
        return requireOrReturn(userIdentity, require);
    }

    /**
     * Tries to get a valid guest {@link UserIdentity} from the given json object
     * <br>
     * This tries to return a {@link UserIdentity} for a valid guard authentication token provided in the given json.
     * <br>
     * If the json does not contain a valid authentication token then this method will try to determine a password-less guest by email
     * <br>
     *
     * @param json The json to parse the identity from
     * @return The identity for an auth-token provided in the json, or a passwordless guest identity for a given email, or null if no identity could be obtained.
     * @throws OXException
     * @throws JSONException
     */
    protected UserIdentity getAnyGuestIdentityFrom(JSONObject json) throws OXException, JSONException {
        UserIdentity guestIdentity = null;
        if (json.has("auth")) {
            guestIdentity = getUserIdentityFrom(json.getJSONObject("auth"));
            if (guestIdentity == null) {
                guestIdentity = getPasswordLessGuestUserIdentityFrom(json.getJSONObject("auth"));
            }
        }
        return guestIdentity;
    }

    /**
     * Tries to get a valid guest {@link UserIdentity} from the given json object
     * <br>
     * This tries to return a {@link UserIdentity} for a valid guard authentication token provided in the given json.
     * <br>
     * If the json does not contain a valid authentication token then this method will try to determine a a password-less guest by email, or throw an exception if no identity could be obtained.
     * <br>
     *
     * @param json The json to parse the identity from
     * @return The identity for an auth-token provided in the json, or a passwordless guest identity for a given email, or null if no no identity could be obtained.
     * @throws OXException
     * @throws JSONException
     */
    protected UserIdentity requireAnyGuestIdentityFrom(JSONObject json) throws OXException, JSONException {
        UserIdentity anyGuestIdentityFrom = getAnyGuestIdentityFrom(json);
        final boolean require = true;
        return requireOrReturn(anyGuestIdentityFrom, require);
    }
}
