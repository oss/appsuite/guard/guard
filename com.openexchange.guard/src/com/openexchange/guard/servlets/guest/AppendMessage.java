/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.guest.GuardGuestEmail;
import com.openexchange.guard.guest.GuardGuestService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.servlets.guest.responses.GuestEmailsResponse;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link AppendMessage}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class AppendMessage extends GuardGuestServletAction {

    private static final String MESSAGE_FIELD_NAME = "message";
    private static final String JSON_AUTH_FIELD_NAME = "json";
    private static final String JSON_APPENED_DATA_FIELD_NAME = "append";

    public static class AppenedData{

        private String folderId;

        public String getFolderId() {
            return folderId;
        }

        public void setFolderId(String folderId) {
            this.folderId = folderId;
        }
    }

    /**
     * Internal method to parse the request data
     *
     * @param json The JSON to parse the data from
     * @return The parsed request data
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     * @throws JSONException
     */
    private AppenedData parseAppendData(JSONObject json) throws JsonParseException, JsonMappingException, IOException, JSONException {
        JSONObject jsonAppend = json.getJSONObject(JSON_APPENED_DATA_FIELD_NAME);
        return new ObjectMapper().readValue(jsonAppend.toString(), AppenedData.class);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        final GuardGuestService guardGuestService = Services.getService(GuardGuestService.class);

        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        final JSONObject json = jsonFileUploadHandler.getJsonFrom(items,JSON_AUTH_FIELD_NAME, false);
        UserIdentity guestUser = requireAnyGuestIdentityFrom(json);
        AppenedData appendData = parseAppendData(json);
        List<GuardGuestEmail> appendedMessages = new ArrayList<GuardGuestEmail>();
        for(FileItem item : items) {
            if(!item.getFieldName().equals(JSON_AUTH_FIELD_NAME)) {
                try(InputStream messageStream = item.getInputStream();){
                    //Append the email message to the specified folder
                    appendedMessages.add(guardGuestService.appendMessage(guestUser, messageStream, appendData.getFolderId()));
                }
            }
        }
        ServletUtils.sendObject(response, new GuestEmailsResponse(appendedMessages));
    }
}