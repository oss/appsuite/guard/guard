/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.guest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.guest.GuestConverterService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.servlets.guest.exceptions.GuestServletExceptionCodes;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link GuestUpgrader}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GuestUpgrader extends GuardGuestServletAction {

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        GuestConverterService guestUpgrader = Services.getService(GuestConverterService.class);
        TokenAuthenticationService tokenService = Services.getService(TokenAuthenticationService.class);
        if (guestUpgrader == null || tokenService == null) {
            throw OXException.general("Missing required services for Guest upgrade");
        }

        // Get required parameters
        String authToken = ServletUtils.getStringParameter(request, "auth", true);
        String itemId = ServletUtils.getStringParameter(request, "item", true);
        String language = ServletUtils.getStringParameter(request, "lang");

        // Verify token valid
        UserIdentity userIdentity = tokenService.decryptUserIdentity(userSession.getSessionId(), authToken);
        if (userIdentity == null) {
            throw GuestServletExceptionCodes.BAD_AUTHENTICATION.create();
        }

        // Do upgrade
        String redirect = guestUpgrader.doUpgrade(itemId, userIdentity, language);

        // Send response
        JSONObject json = new JSONObject();
        JSONObject redir = new JSONObject();
        redir.put("redirect", redirect);
        json.put("data", redir);
        ServletUtils.sendOK(response, "application/json", json.toString());
    }

}
