/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets;

import javax.servlet.ServletException;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;
import com.openexchange.guard.servlets.authentication.GuardAuthenticationHandler;
import com.openexchange.guard.servlets.callback.CallbackServlet;
import com.openexchange.guard.servlets.certificatemanagement.CertificatemanagementServlet;
import com.openexchange.guard.servlets.crypto.CryptoServlet;
import com.openexchange.guard.servlets.demo.DemoServlet;
import com.openexchange.guard.servlets.guest.GuestServlet;
import com.openexchange.guard.servlets.keymanagement.KeymanagementServlet;
import com.openexchange.guard.servlets.keyretrieval.KeyRetrievalServlet;
import com.openexchange.guard.servlets.login.LoginServlet;
import com.openexchange.guard.servlets.mail.MailServlet;
import com.openexchange.guard.servlets.pgpcore.PGPCoreServlet;
import com.openexchange.guard.servlets.pgpmail.PGPMailServlet;
import com.openexchange.guard.servlets.ping.PingServlet;
import com.openexchange.guard.servlets.supportApi.SupportServlet;
import com.openexchange.server.ServiceLookup;

/**
 * Handles registration and de-registration of OX Guard servlets
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardServlets {

    private static final Logger LOG = LoggerFactory.getLogger(GuardServlets.class);

    private static final String GUARD_LOGIN_SERVLET_PATH = "/oxguard/login";
    private static final String GUARD_PING_SERVLET_PATH = "/oxguard/ping";
    private static final String GUARD_KEY_MANAGEMENT_SERVLET_PATH = "/oxguard/keys";
    private static final String GUARD_CERTIFICATE_MANAGEMENT_SERVLET_PATH = "/oxguard/certificates";
    private static final String GUARD_PGP_CORE_SERVLET_PATH = "/oxguard/pgpcore";
    private static final String GUARD_PGPMAIL_SERVLET_PATH = "/oxguard/pgpmail";
    private static final String GUARD_CRYPTO_SERVLET_PATH = "/oxguard/crypto";
    private static final String GUARD_MAIL_SERVLET_PATH = "/oxguard/mail";;
    private static final String GUARD_GUEST_SERVLET_PATH = "/oxguard/guest";
    private static final String GUARD_KEY_RETRIEVAL_SERVLET_PATH = "/oxguard/retrieve";
    private static final String GUARD_DEMO_SERVLET_PATH = "/oxguard/demo";
    private static final String GUARD_CALLBACK_SERVLET_PATH = "/guardadmin";
    public static final String GUARD_SUPPORT_SERVLET_PATH = "/guardsupport";
    private static final String GUARD_GUEST_READER_PATH = "/oxguard/reader";


    /**
     * Registers OX Guard servlets to the given service
     *
     * @param httpService The service where to register the servlets
     * @throws NamespaceException
     * @throws ServletException
     * @throws OXException
     */
    public static void registerServlets(ServiceLookup services) throws ServletException, NamespaceException, OXException {

        HttpService httpService = services.getServiceSafe(HttpService.class);
        GuardConfigurationService configService = services.getServiceSafe(GuardConfigurationService.class);
        // Create authentication handlers
        // Rest API Authentication
        String restApiUsername = configService.getProperty(GuardProperty.restApiUsername);
        String restApiPassword = configService.getProperty(GuardProperty.restApiPassword);
        GuardAuthenticationHandler basicRestAuthServletAuthentication = null;
        if (restApiPassword != null && restApiUsername != null) {
            basicRestAuthServletAuthentication = new BasicAuthServletAuthenticationHandler(restApiUsername, restApiPassword);
        }

        httpService.registerServlet(GUARD_PING_SERVLET_PATH, new PingServlet(), null, null);
        httpService.registerServlet(GUARD_KEY_MANAGEMENT_SERVLET_PATH, new KeymanagementServlet(), null, null);
        httpService.registerServlet(GUARD_CERTIFICATE_MANAGEMENT_SERVLET_PATH, new CertificatemanagementServlet(), null, null);
        httpService.registerServlet(GUARD_PGP_CORE_SERVLET_PATH, new PGPCoreServlet(), null, null);
        httpService.registerServlet(GUARD_PGPMAIL_SERVLET_PATH, new PGPMailServlet(), null, null);
        httpService.registerServlet(GUARD_MAIL_SERVLET_PATH, new MailServlet(), null, null);
        httpService.registerServlet(GUARD_LOGIN_SERVLET_PATH, new LoginServlet(services, basicRestAuthServletAuthentication), null, null);
        httpService.registerServlet(GUARD_GUEST_SERVLET_PATH, new GuestServlet(), null, null);
        httpService.registerServlet(GUARD_KEY_RETRIEVAL_SERVLET_PATH, new KeyRetrievalServlet(), null, null);
        httpService.registerServlet(GUARD_SUPPORT_SERVLET_PATH, new SupportServlet(), null, null);
        httpService.registerServlet(GUARD_DEMO_SERVLET_PATH, new DemoServlet(), null, null);
        httpService.registerServlet(GUARD_CALLBACK_SERVLET_PATH, new CallbackServlet(), null, null);
        httpService.registerServlet(GUARD_CRYPTO_SERVLET_PATH, new CryptoServlet(), null, null);

    }

    /**
     * Unregisters OX Guard servlets from the given service
     *
     * @param httpService The service to unregister the servlets from
     */
    public static void unregisterServlets(HttpService httpService) {
        if (httpService == null) {
            LOG.debug("Unable to unregister guard servlets. HttpService is null!");
            return;
        }
        httpService.unregister(GUARD_PING_SERVLET_PATH);
        httpService.unregister(GUARD_PGP_CORE_SERVLET_PATH);
        httpService.unregister(GUARD_PGPMAIL_SERVLET_PATH);
        httpService.unregister(GUARD_MAIL_SERVLET_PATH);
        httpService.unregister(GUARD_LOGIN_SERVLET_PATH);
        httpService.unregister(GUARD_GUEST_SERVLET_PATH);
        httpService.unregister(GUARD_KEY_RETRIEVAL_SERVLET_PATH);
        httpService.unregister(GUARD_SUPPORT_SERVLET_PATH);
        httpService.unregister(GUARD_DEMO_SERVLET_PATH);
        httpService.unregister(GUARD_CALLBACK_SERVLET_PATH);
        httpService.unregister(GUARD_GUEST_READER_PATH);
    }
}
