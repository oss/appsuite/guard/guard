/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.supportApi.request.analyzer;

import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.addToMap;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.getFirstParameterValue;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseBodyParameters;
import static com.openexchange.request.analyzer.utils.RequestAnalyzerUtils.parseRequestParameters;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.java.Strings;
import com.openexchange.guard.database.GuardDatabaseService;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.servlets.GuardServlets;
import com.openexchange.request.analyzer.AnalyzeResult;
import com.openexchange.request.analyzer.BodyData;
import com.openexchange.request.analyzer.RequestAnalyzer;
import com.openexchange.request.analyzer.RequestData;
import com.openexchange.request.analyzer.RequestURL;
import com.openexchange.request.analyzer.UserInfo;
import com.openexchange.request.analyzer.utils.RequestAnalyzerUtils;
import com.openexchange.segment.SegmentMarker;
import com.openexchange.tools.functions.ErrorAwareSupplier;

/**
 * {@link SupportApiRequestAnalyzer}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 */
public class SupportApiRequestAnalyzer implements RequestAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SupportApiRequestAnalyzer.class);

    private static final String CONTEXT_ID_PARAM = "cid";
    private static final String EMAIL_PARAM = "email";
    private static final String ACTION_PARAM = "action";

    private ErrorAwareSupplier<? extends GuardDatabaseService> guardDbServiceSupplier;
    private ErrorAwareSupplier<? extends EmailStorage> emailStorage;

    public SupportApiRequestAnalyzer(ErrorAwareSupplier<? extends GuardDatabaseService> guardDbServiceSupplier, ErrorAwareSupplier<? extends EmailStorage> emailStorage) {
        super();
        this.guardDbServiceSupplier = guardDbServiceSupplier;
        this.emailStorage = emailStorage;
    }

    @Override
    public Optional<AnalyzeResult> analyze(RequestData data) throws OXException {
        String method = data.getMethod();
        if (Strings.isEmpty(method) || !"POST".equals(method)) {
            return Optional.empty();
        }

        RequestURL url = data.getParsedURL();
        if (url.getPath().isEmpty() || !RequestAnalyzerUtils.isQualifiedPath(url.getPath().get(), Collections.singleton(GuardServlets.GUARD_SUPPORT_SERVLET_PATH))) {
            return Optional.empty();
        }

        boolean hasActionParam = url.hasParameter(ACTION_PARAM);
        if (!hasActionParam) {
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        Map<String, String[]> parametersMap = new TreeMap<String, String[]>(String.CASE_INSENSITIVE_ORDER);
        Optional<BodyData> requestBody = data.optBody();
        addParamsToMap(url, parametersMap, requestBody);

        String email = getFirstParameterValue(parametersMap, EMAIL_PARAM);
        String action = url.optParameter(ACTION_PARAM).get();
        if (action.equalsIgnoreCase("reset_password")) {
            return handleResetPasswordAction(email);
        } else if (action.equalsIgnoreCase("delete_user") || action.equalsIgnoreCase("expose_key") || action.equalsIgnoreCase("upgrade_guest")) {
            String contextId = getFirstParameterValue(parametersMap, CONTEXT_ID_PARAM);
            if ((contextId == null) && Strings.isEmpty(email) && (!requestBody.isPresent())) {
                return Optional.of(AnalyzeResult.MISSING_BODY);
            }
            return handleOtherActions(contextId, email);
        }
        LOGGER.debug("Did not retrieve any usable information. Cannot answer appropriately.");
        return Optional.of(AnalyzeResult.UNKNOWN);
    }

    private void addParamsToMap(RequestURL url, Map<String, String[]> parametersMap, Optional<BodyData> requestBody) {
        addToMap(parseRequestParameters(url), parametersMap);
        if (requestBody.isPresent()) {
            addToMap(parseBodyParameters(requestBody.get()), parametersMap);
        }
    }

    private Optional<AnalyzeResult> handleOtherActions(String contextId, String email) throws OXException {
        if (contextId != null) {
            try {
                Optional<AnalyzeResult> schemaAndBuildAnalyzeResult = handleWithContextId(Integer.parseInt(contextId));
                if (schemaAndBuildAnalyzeResult.isPresent()) {
                    return schemaAndBuildAnalyzeResult;
                }
            } catch (NumberFormatException e) {
                LOGGER.debug("The value '{}' set as contextId could not be parsed as an integer.", contextId, e);
                return Optional.of(AnalyzeResult.UNKNOWN);
            }
        }
        return handleWithEmail(email);
    }

    private Optional<AnalyzeResult> handleResetPasswordAction(String email) throws OXException {
        if (email == null) {
            return Optional.of(AnalyzeResult.UNKNOWN);
        }
        return handleWithEmail(email);
    }

    private Optional<AnalyzeResult> handleWithEmail(String email) throws OXException {
        Optional<AnalyzeResult> handleWithEmail = resolveSchemaWithEmail(email);
        if (handleWithEmail.isPresent()) {
            return handleWithEmail;
        }
        return Optional.of(AnalyzeResult.UNKNOWN);
    }

    private Optional<AnalyzeResult> resolveSchemaWithEmail(String email) throws OXException {
        Email resolve = emailStorage.get().getByEmail(email);
        if (resolve == null) {
            return Optional.empty();
        }
        int contextId = resolve.getContextId();
        return handleWithContextId(contextId);
    }

    private Optional<AnalyzeResult> handleWithContextId(int contextId) {
        if (contextId <= 0) {
            return Optional.empty();
        }
        try {
            String schemaName = getSchemaName(contextId);
            return buildAnalyzeResult(schemaName, contextId);
        } catch (OXException e) {
            LOGGER.debug("No schema for context '{}' found.", contextId, e);
            return Optional.empty();
        }
    }

    private String getSchemaName(int contextId) throws OXException {
        return guardDbServiceSupplier.get().getDatabaseService().getSchemaName(contextId);
    }

    private Optional<AnalyzeResult> buildAnalyzeResult(String schemaName, int contextId) {
        return Optional.of(new AnalyzeResult(SegmentMarker.of(schemaName), UserInfo.builder(contextId).build()));
    }
}
