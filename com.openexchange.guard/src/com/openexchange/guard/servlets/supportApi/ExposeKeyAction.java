/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.supportApi;

import java.net.URI;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.inputvalidation.EmailValidator;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.support.SupportService;
import com.openexchange.guard.support.SupportServiceImpl;

/**
 * Action to expose a deleted, but backed up, key for downloading.
 *
 * If a deleted key is marked as "exposed" it is possible to download the key using com.openexchange.guard.server.servlets.keyretrieval
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since 2.4.0
 */
public class ExposeKeyAction extends GuardServletAction {

    private final SupportService supportService;

    public ExposeKeyAction() {
        supportService = new SupportServiceImpl();
    }

    @Override
    public void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {

        //Parsing parameters
        final String email = new EmailValidator().assertInput(ServletUtils.getStringParameter(request, "email", true /* mandatory */), "email");
        int cid = ServletUtils.getIntParameter(request, "cid", true /* mandatory */);

        //exposing the user's keys
        URI exposeUri = supportService.exposeKey(cid, email);
        ServletUtils.sendOK(response, "text/plain", exposeUri.toString());
    }
}
