/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.crypto;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.exception.OXException;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardErrorResponseRenderer;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.BasicAuthServletAuthenticationHandler;

/**
 * {@link CryptoServlet}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.7
 */
public class CryptoServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = 369986366047440120L;
    
    private transient final HashMap<String, GuardServletAction> postActions;
    private transient final HashMap<String, GuardServletAction> putActions;
    private transient final HashMap<String, GuardServletAction> getActions;
    private static final String POST_VERIFY_ACTION = "verify";
    private static final String POST_PROCESS_INCOMING_MIME = "process_mime";
    private static final String POST_DECRYPT_MIME_MESSAGE = "decrypt";

    public CryptoServlet() throws OXException {
        super(new GuardErrorResponseRenderer(HttpServletResponse.SC_INTERNAL_SERVER_ERROR));

        BasicAuthServletAuthenticationHandler authHandler = createBasicAuthHandler();
        //GET
        getActions = new HashMap<String, GuardServletAction>();

        //POST
        postActions = new HashMap<String, GuardServletAction>();
        postActions.put(POST_VERIFY_ACTION, new VerifyMimeEmailAction().setAuthenticationHandler(authHandler));
        postActions.put(POST_PROCESS_INCOMING_MIME,
            new MimeEmailAction()
                .setAuthenticationHandler(authHandler)
                .setErrorResponseRenderer(new GuardErrorResponseRenderer(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)));
        postActions.put(POST_DECRYPT_MIME_MESSAGE,
            new DecryptMimeEmailAction()
                .setAuthenticationHandler(authHandler)
                .setErrorResponseRenderer(new GuardErrorResponseRenderer(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)));

        //PUT
        putActions = new HashMap<String, GuardServletAction>();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        super.doAction(request, response, postActions);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, putActions);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, getActions);
    }

}
