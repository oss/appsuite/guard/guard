/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.crypto;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.AntiAbuseUtils;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseAction;
import com.openexchange.guard.keymanagement.commons.antiabuse.AntiAbuseWrapper;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.crypto.Exceptions.CryptoServletExceptionCodes;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link DecryptMimeEmailAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.4
 */
public class DecryptMimeEmailAction extends GuardServletAction {

    private static final String JSON_DATA_FIELD_NAME = "json";
    private static final String JSON_AUTH_FIELD_NAME = "guardAuth";
    private static final String MESSAGE_FIELD_NAME = "file";

    /**
     * Parses a {@link GuardParsedMimeMessage} from the given message
     *
     * @param request The request
     * @param json The JSON containing meta information
     * @param message The message to parse
     * @param userId The ID of the user
     * @param contextId The ID of the context
     * @return The parsed {@link GuardParsedMimeMessage}
     * @throws MessagingException
     * @throws JSONException
     * @throws OXException
     * @throws IOException
     */
    public static GuardParsedMimeMessage parseMimeMessage(HttpServletRequest request, JSONObject json, InputStream message, int userId, int contextId) throws MessagingException, JSONException, OXException, IOException{
    	final boolean draft = ServletUtils.getBooleanParameter(request, "draft", false);
    	String host = null;
        if (request.getHeader("X-Host-Name") != null) {
            host = request.getHeader("X-Host-Name");
        }
    	return new GuardParsedMimeMessage(message, json, userId, contextId, null, draft, host, null, false);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(final HttpServletRequest request, final HttpServletResponse response, GuardUserSession userSession) throws Exception {
        RangeInputValidator<Integer> positiveValidator = new RangeInputValidator<Integer>(I(0), I(Integer.MAX_VALUE));

        final boolean mandatory = true;
        final int userId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "user", mandatory), "user"));
        final int contextId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "context", mandatory), "context"));
    	final boolean draft = ServletUtils.getBooleanParameter(request, "draft", false);
        final String host = request.getHeader("X-Host-Name");

        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        try (InputStream messageStream = fileUploadHandler.getFileItemStreamFrom(items, MESSAGE_FIELD_NAME, true);) {

            final JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, false);
            final JSONObject guardAuth = jsonFileUploadHandler.getJsonFrom(items, JSON_AUTH_FIELD_NAME, false);

            //Extracting user identity from the authenticationToken
            UserIdentity userIdentity = getUserIdentityFrom(guardAuth);
            if (userIdentity == null) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }

            //This abuse wrapper does protect from wrong passwords inside of a valid auth-Token. Not from non-valid auth-tokens.
            AntiAbuseWrapper antiAbuseWrapper = getAntiAbuseWrapper(AntiAbuseUtils.getAllowParameters(request, userId, contextId, new String(userIdentity.getPassword())));
            final UserIdentity closureUserIdentity = userIdentity;
            antiAbuseWrapper.doAction(new AntiAbuseAction<Void>() {

                @SuppressWarnings("synthetic-access")
                @Override
                public Void doAction() throws Exception {
                    final GuardParsedMimeMessage parsedMimeMessage = new GuardParsedMimeMessage(messageStream, json, userId, contextId, null, draft, host, null, false);
                    doDecryption(request, response, parsedMimeMessage, userId, contextId, closureUserIdentity);
                    return null;
                }
            });
        }
    }

    @SuppressWarnings("resource")
    private static void doDecryption(HttpServletRequest request, HttpServletResponse response, GuardParsedMimeMessage parsedMimeMessage, int userId, int contextId, UserIdentity userIdentity) throws OXException, IOException {
        // Check handled
        CryptoManager cryptoManager = Services.getService(CryptoManager.class);
        MimeEncryptionService mimeEncryptor = cryptoManager.getMimeEncryptionService(getCryptoType(request, parsedMimeMessage));
        if (mimeEncryptor != null) {
            mimeEncryptor.doDecryption(parsedMimeMessage, userId, contextId, userIdentity, response.getOutputStream());
        } else {
            throw CryptoServletExceptionCodes.UNKNOWN_CRYPTO_TYPE.create();
        }
    }
}
