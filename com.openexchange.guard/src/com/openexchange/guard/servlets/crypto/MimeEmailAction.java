/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.crypto;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.json.JSONException;
import org.json.JSONObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.crypto.CryptoManager;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.inputvalidation.RangeInputValidator;
import com.openexchange.guard.mime.services.GuardParsedMimeMessage;
import com.openexchange.guard.mime.services.MimeEncryptionService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.crypto.Exceptions.CryptoServletExceptionCodes;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.servlets.fileupload.JsonFileUploadHandler;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link MimeEmailAction} - Encrypt an email
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.4.0
 */
public class MimeEmailAction extends AbstractCryptoServletAction {

	private static final String JSON_DATA_FIELD_NAME = "json";
	private static final String JSON_AUTH_FIELD_NAME = "guardAuth";
	private static final String MESSAGE_FIELD_NAME = "file";

    private GuardParsedMimeMessage parseMimeMessage(HttpServletRequest request, JSONObject json, InputStream message, int userId, int contextId, String fromName, String fromEmail, boolean isGuest) throws MessagingException, JSONException, OXException, IOException {
        return parse(request, json, message, userId, contextId, fromName, fromEmail, isGuest);
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        RangeInputValidator<Integer> positiveValidator = new RangeInputValidator<Integer>(I(0), I(Integer.MAX_VALUE));

        //We trust the userId and contextId, because it comes from a backend callback
        final boolean mandatory = true;
        final int userId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "user", mandatory), "user"));
        final int contextId = i(positiveValidator.assertInput(ServletUtils.getIntParameter(request, "context", mandatory), "context"));
        final String fromName = ServletUtils.getStringParameter(request, "fromName", mandatory);
        final String senderEmail = ServletUtils.getStringParameter(request, "email", mandatory);
        final boolean senderIsGuest =  ServletUtils.getBooleanParameter(request, "isGuest");

        final boolean encryptedFileUpload = true;
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class), encryptedFileUpload);
        JsonFileUploadHandler jsonFileUploadHandler = new JsonFileUploadHandler(fileUploadHandler);
        Collection<FileItem> items = fileUploadHandler.parseItems(request);
        try (InputStream messageStream = fileUploadHandler.getFileItemStreamFrom(items, MESSAGE_FIELD_NAME, true)) {

            JSONObject json = jsonFileUploadHandler.getJsonFrom(items, JSON_DATA_FIELD_NAME, true);
            JSONObject guardAuthJson = jsonFileUploadHandler.getJsonFrom(items, JSON_AUTH_FIELD_NAME, false);

            UserIdentity signingIdentity = guardAuthJson != null ? getUserIdentityFrom(guardAuthJson) : null;
            if (guardAuthJson != null && signingIdentity == null) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }

            //Extracting user identity from the authenticationToken
            String password = signingIdentity != null ? new String(signingIdentity.getPassword()) : null;

            final GuardParsedMimeMessage parsedMimeMessage = parseMimeMessage(request, json, messageStream, userId, contextId, fromName, senderEmail, senderIsGuest);
            CryptoManager manager = Services.getService(CryptoManager.class);

            MimeEncryptionService mimeEncryptor = manager.getMimeEncryptionService(getCryptoType(request, parsedMimeMessage));
            if (mimeEncryptor == null) {
                throw CryptoServletExceptionCodes.UNKNOWN_CRYPTO_TYPE.create();
            }
            mimeEncryptor.doEncryption(parsedMimeMessage, userId, contextId, password, response.getOutputStream());
        }
    }
}
