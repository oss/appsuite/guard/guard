/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.certificatemanagement;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.openexchange.guard.servlets.AbstractGuardServlet;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.authentication.OXGuardSessionAuthenticationHandler;
import com.openexchange.guard.servlets.certificatemanagement.actions.CreateCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.DeleteCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.DownloadCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.GetCertificatesAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.GetRecipCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.HasCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.SetCurrentCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.UploadCertificateAction;
import com.openexchange.guard.servlets.certificatemanagement.actions.UploadRecipCertificateAction;

/**
 * {@link CertificatemanagementServlet}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.7
 */
public class CertificatemanagementServlet extends AbstractGuardServlet {

    private static final long serialVersionUID = -1921986068926804371L;

    private static final String GET_HAS_CERTIFICATE_ACTION = "hasCertificate";
    private static final String GET_RECIP_CERTIFICATE_ACTION = "getRecipCertificate";
    private static final String GET_CERTIFICATES_ACTION = "getCertificates";

    private static final String PUT_SET_CURRENT_CERTIFICATE_ACTION = "setCurrentCertificate";

    private static final String POST_DELETE_CERTIFICATE_ACTION = "delete";
    private static final String POST_UPLOAD_CERTIFICATE_ACTION = "upload";
    private static final String POST_UPLOAD_RECIP_CERTIFICATE_ACTION = "uploadRecipCertificate";
    private static final String DOWNLOAD_CERTIFICATE_ACTION = "download";
    private static final String CREATE_CERTIFICATE_ACTION = "create";

    private transient final HashMap<String, GuardServletAction> getActions;
    private transient final HashMap<String, GuardServletAction> putActions;
    private transient final HashMap<String, GuardServletAction> postActions;

    public CertificatemanagementServlet() {

        OXGuardSessionAuthenticationHandler sessionAuthenticationHandler = new OXGuardSessionAuthenticationHandler();

        getActions = new HashMap<>();
        getActions.put(GET_HAS_CERTIFICATE_ACTION, new HasCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_RECIP_CERTIFICATE_ACTION, new GetRecipCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        getActions.put(GET_CERTIFICATES_ACTION, new GetCertificatesAction().setAuthenticationHandler(sessionAuthenticationHandler));

        putActions = new HashMap<>();
        putActions.put(PUT_SET_CURRENT_CERTIFICATE_ACTION, new SetCurrentCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));

        postActions = new HashMap<>();
        postActions.put(POST_DELETE_CERTIFICATE_ACTION, new DeleteCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(POST_UPLOAD_CERTIFICATE_ACTION, new UploadCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(POST_UPLOAD_RECIP_CERTIFICATE_ACTION, new UploadRecipCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(DOWNLOAD_CERTIFICATE_ACTION, new DownloadCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
        postActions.put(CREATE_CERTIFICATE_ACTION, new CreateCertificateAction().setAuthenticationHandler(sessionAuthenticationHandler));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, getActions);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, putActions);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doAction(request, response, postActions);
    }
}
