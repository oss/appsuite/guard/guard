/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.certificatemanagement.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus.Status;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.certificatemanagement.pki.PKIService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.certificatemanagement.responses.CertificateEnrollmentResponse;
import com.openexchange.guard.servlets.certificatemanagement.responses.CertificateResponse;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.guard.user.UserIdentity;

/**
 * {@link CreateCertificateAction} Action to create certificate if able using a PKI service
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0
 */
public class CreateCertificateAction extends GuardServletAction {

    private static final Logger LOG = LoggerFactory.getLogger(CreateCertificateAction.class);

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        final UserIdentity userIdentity = requireUserIdentityFrom(request, userSession);
        PKIService pkiService = Services.getService(PKIService.class);
        if (pkiService == null || !pkiService.canCreateKeys(userSession.getUserId(), userSession.getContextId())) {
            LOG.error("Unable to create keys for user due to pki service unavailable or disabled for user");
            ServletUtils.sendError(response, "unable to create keys");
            return;
        }
        CertificateEnrollmentStatus enrollment = pkiService.createKeys(userSession.getUserId(), userSession.getContextId(), userIdentity.getPassword());
        // If good, done
        if (enrollment.getStatus().equals(Status.Done)) {
            SmimeKeyService smimeKeyService = Services.getService(SmimeKeyService.class);
            if (smimeKeyService == null) {
                throw OXException.general("Missing SmimeKeyService");
            }
            SmimeKeys newKeys = enrollment.getSmimeKeys();
            smimeKeyService.updateRecovery(newKeys, new String(userIdentity.getPassword()), userSession.getUserId(), userSession.getContextId());
            smimeKeyService.storeKey(newKeys, userSession.getUserId(), userSession.getContextId(), false);
            CertificateResponse resp = new CertificateResponse(enrollment.getSmimeKeys());
            ServletUtils.sendObject(response, resp);
            return;
        }
        // Pending or failed
        ServletUtils.sendObject(response, new CertificateEnrollmentResponse(enrollment));

    }

}
