/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
package com.openexchange.guard.servlets.certificatemanagement.actions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.servlets.GuardServletAction;
import com.openexchange.guard.servlets.certificatemanagement.responses.CertificatesResponse;
import com.openexchange.guard.servlets.fileupload.FileUploadHandler;
import com.openexchange.guard.smime.SmimeKeyService;
import com.openexchange.java.Streams;

/**
 * {@link UploadRecipCertificateAction} allows to upload a S/MIME recipient certificate
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.1
 */
public class UploadRecipCertificateAction extends GuardServletAction {

    private static final Logger LOG = LoggerFactory.getLogger(UploadRecipCertificateAction.class);

    private static final int MAX_KEY_FILE_UPLOAD_SIZE = 1000000; /* 1 MB */
    private static final String CERT_PREFIX = "cert";


    @SuppressWarnings("resource")
    public List<SmimeKeys> importCertificates(Collection<FileItem> items, GuardUserSession userSession) throws OXException {
        InputStream certificateInputStream = null;
        String filename;
        List<SmimeKeys> imported = new ArrayList<SmimeKeys>();
        try {
            SmimeKeyService keyService = Services.getService(SmimeKeyService.class);
            for (final FileItem item : items) {
                if (item.getFieldName().startsWith(CERT_PREFIX)) {
                    certificateInputStream = item.getInputStream();
                    filename = item.getName();
                    if (filename != null) {
                        SmimeKeys key = keyService.importRecipKey(certificateInputStream, userSession.getUserId(), userSession.getContextId(), filename);
                        if (key != null) {
                            imported.add(key);
                        }
                    } else {
                        throw GuardCoreExceptionCodes.INVALID_PARAMETER_VALUE.create("filename");
                    }
                }
            }
            if (certificateInputStream == null) {
                throw GuardCoreExceptionCodes.MULTIPART_UPLOAD_MISSING.create();
            }

        } catch (OXException e) {
            LOG.error("Error importing", e);
            throw e;
        } catch (final Exception e) {
            LOG.error("Error importing", e);
            throw OXException.general("Problem parsing request", e);
        } finally {
            Streams.close(certificateInputStream);
        }
        return imported;
    }

    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        FileUploadHandler fileUploadHandler = new FileUploadHandler(Services.getService(GuardConfigurationService.class));
        List<SmimeKeys> importedCertificate = importCertificates(fileUploadHandler.parseItems(request, MAX_KEY_FILE_UPLOAD_SIZE), userSession);
        ServletUtils.sendObject(response, new CertificatesResponse(importedCertificate));
    }

}
