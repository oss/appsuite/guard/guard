/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

package com.openexchange.guard.servlets.certificatemanagement.responses;

import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus;
import com.openexchange.guard.certificatemanagement.commons.CertificateEnrollmentStatus.Status;

/**
 * Response when enrolling a certificate fails or is delayed
 * If completed without issue, should just be a CertificateResponse
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class CertificateEnrollmentResponse {

    private int stage;
    private Status status;
    private String message;

    public CertificateEnrollmentResponse(CertificateEnrollmentStatus result) {
        this.stage = result.getStage();
        this.status = result.getStatus();
        this.message = result.getMessage();
    }

    /**
     * Gets the stage
     *
     * @return The stage
     */
    public int getStage() {
        return stage;
    }

    /**
     * Gets the status
     *
     * @return The status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Gets the message
     *
     * @return The message
     */
    public String getMessage() {
        return message;
    }

}
