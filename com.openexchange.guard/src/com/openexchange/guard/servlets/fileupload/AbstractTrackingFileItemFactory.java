/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.fileupload;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;

/**
 * {@link AbstractTrackingFileItemFactory} - Enhances {@link FileItemFactory} in order to provide symmetric encryption
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.4
 */
public abstract class AbstractTrackingFileItemFactory implements FileItemFactory {

    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(AbstractTrackingFileItemFactory.class);

    protected final Collection<FileItem> fileItems = Collections.synchronizedSet(new HashSet<FileItem>());

    public void cleanup() {
        for (FileItem item : fileItems) {
            try {
                item.delete();
            } catch (Exception e) {
                LOG.error("Error deleting temporary file", e);
            }
        }
    }

}
