/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */
/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 *
 * Contributor(s):
 *
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.openexchange.guard.servlets.fileupload;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.mail.internet.SharedInputStream;
import org.apache.commons.codec.binary.Base64InputStream;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link EncryptedSharedFileInputStream} - Is a {@link SharedInputStream} which reads encrypted items from a {@link File}.
 *
 * @author Bill Shannon
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.10.3
 */
public class EncryptedSharedFileInputStream extends FilterInputStream implements SharedInputStream {

    private final GuardCipherAlgorithm algorithm;
    private final EncryptedSharedFileInputStream parent;
    private final File file;
    private final long start;
    private final long length;
    private final SecretKey secretKey;
    private final byte[] iv;

    private long position;
    private long markedPosition;

    private final List<EncryptedSharedFileInputStream> subStreams = new LinkedList<EncryptedSharedFileInputStream>();


    /**
     * Initializes a new {@link EncryptedSharedFileInputStream}.
     *
     * @param algorithm The algorithm to use for decryption
     * @param secretKey The secret key to tuse for decryption
     * @param iv The IV to use for decryption
     * @param file The file
     * @throws IOException
     */
    public EncryptedSharedFileInputStream(GuardCipherAlgorithm algorithm, SecretKey secretKey, byte[] iv, File file) throws IOException {
        this(algorithm, secretKey, iv, file, 0, file.length());
    }

    private EncryptedSharedFileInputStream(GuardCipherAlgorithm algorithm,
        SecretKey secretKey,
        byte[] iv,
        File file,
        long start,
        long length) throws IOException {

        super(new BufferedInputStream(new Base64InputStream(new CipherInputStream(new FileInputStream(file), createCipher(secretKey, iv, algorithm)))));

        this.algorithm = algorithm;
        this.secretKey = secretKey;
        this.iv = iv;
        this.parent = null;
        this.file = file;
        this.start = start;
        this.length = length;

        if (start != this.in.skip(start)) {
            throw new IOException("Invalid start parameter for inputstream");
        }
    }

    private EncryptedSharedFileInputStream(GuardCipherAlgorithm algorithm,
        SecretKey secretKey,
        byte[] iv,
        EncryptedSharedFileInputStream parent,
        long start,
        long length) throws IOException {

        super(new BufferedInputStream(new Base64InputStream(new CipherInputStream(new FileInputStream(parent.file), createCipher(secretKey, iv, algorithm)))));

        this.algorithm = algorithm;
        this.parent = parent;
        this.secretKey = parent.secretKey;
        this.iv = parent.iv;
        this.file = parent.file;
        this.start = start;
        this.length = length;

        if (start != this.in.skip(start)) {
            throw new IOException("Invalid start parameter for inputstream");
        }
    }

    private static Cipher createCipher(SecretKey key, byte[] iv, GuardCipherAlgorithm _algorithm) throws IOException {
        try {
            Cipher encryptionCipher = Cipher.getInstance(_algorithm.getTransformation(), _algorithm.getProvider());
            encryptionCipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
            return encryptionCipher;
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * Return the shared stream that represents the top most stream that
     * this stream inherits from.
     *
     * @return the base of the shared stream tree.
     */
    public EncryptedSharedFileInputStream getRoot() {
        if (parent != null) {
            return parent.getRoot();
        }

        return this;
    }

    /**
     * Close of this stream and any substreams that have been created from it.
     *
     * @throws IOException on problem closing the main stream.
     */
    public void dispose() throws IOException {
        Iterator<EncryptedSharedFileInputStream> it = subStreams.iterator();

        while (it.hasNext()) {
            try {
                it.next().dispose();
            } catch (IOException e) {/*ignore*/}
        }

        in.close();
    }

    @Override
    public long getPosition() {
        return position;
    }

    @Override
    public InputStream newStream(long start, long finish) {
        try {
            EncryptedSharedFileInputStream stream;

            if (finish < 0) {
                if (length > 0) {
                    stream = new EncryptedSharedFileInputStream(algorithm, secretKey, iv, this, this.start + start, length - start);
                } else if (length == 0) {
                    stream = new EncryptedSharedFileInputStream(algorithm, secretKey, iv, this, this.start + start, 0);
                } else {
                    stream = new EncryptedSharedFileInputStream(algorithm, secretKey, iv, this, this.start + start, -1);
                }
            } else {
                stream = new EncryptedSharedFileInputStream(algorithm, secretKey, iv, this, this.start + start, finish - start);
            }

            subStreams.add(stream);

            return stream;
        } catch (IOException e) {
            throw new IllegalStateException("unable to create shared stream: " + e);
        }
    }

    @Override
    public int read(byte[] buf) throws IOException {
        return this.read(buf, 0, buf.length);
    }

    @Override
    public int read(byte[] buf, int off, int len) throws IOException {
        int count = 0;

        if (len == 0) {
            return 0;
        }

        while (count < len) {
            int ch = this.read();

            if (ch < 0) {
                break;
            }

            buf[off + count] = (byte) ch;
            count++;
        }

        if (count == 0) {
            return -1;  // EOF
        }

        return count;
    }

    @Override
    public int read() throws IOException {
        if (position == length) {
            return -1;
        }

        position++;
        return in.read();
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public long skip(long n) throws IOException {
        long count;

        for (count = 0; count != n; count++) {
            if (this.read() < 0) {
                break;
            }
        }

        return count;
    }

    @Override
    public void mark(int readLimit) {
        markedPosition = position;
        in.mark(readLimit);
    }

    @Override
    public void reset() throws IOException {
        position = markedPosition;
        in.reset();
    }
}
