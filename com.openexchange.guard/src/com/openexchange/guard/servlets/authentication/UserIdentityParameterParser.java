/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.exceptions.GuardAuthExceptionCodes;
import com.openexchange.guard.handler.exceptions.GuardResponseHandlerExceptionCodes;
import com.openexchange.guard.keymanagement.commons.MasterKey;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.user.UserIdentity;

import static com.openexchange.java.Autoboxing.i;

/**
 * {@link UserIdentityParameterParser}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.10.0
 */
public class UserIdentityParameterParser implements UserIdentityParser {

    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_E_PASSWORD = "e_password";
    private static final String PARAM_KEY_INDEX = "keyIndex";

    private final HttpServletRequest request;

    /**
     * Initializes a new {@link UserIdentityParameterParser}.
     * @param request The request to parse the {@link UserIdentity} from
     */
    public UserIdentityParameterParser(HttpServletRequest request) {
        this.request = request;
    }

    private String decryptPassword(String e_password, Integer index, int userId, int cid) throws OXException {
        GuardCipherFactoryService cipherFactoryService = Objects.requireNonNull(Services.getService(GuardCipherFactoryService.class));
        GuardCipherService cipherService = cipherFactoryService.getCipherService(GuardCipherAlgorithm.RSA);
        MasterKeyService masterKeyService = Objects.requireNonNull(Services.getService(MasterKeyService.class));
        MasterKey mkey;
        if (index != null) {
            mkey = masterKeyService.getMasterKey(i(index), false);
        } else {
            mkey = masterKeyService.getMasterKey(userId, cid, false);
        }
        return cipherService.decrypt(e_password, masterKeyService.getDecryptedClientKey(mkey));
    }

    private String parsePassword(int userId, int cid) throws OXException {
        String password = request.getParameter(PARAM_PASSWORD);
        if(password != null) {
            return password;
        }

        String e_password = request.getParameter(PARAM_E_PASSWORD);
        if (e_password != null) {
            Integer index = null;
            if (request.getParameter(PARAM_KEY_INDEX) != null) {
                try {
                    index = Integer.valueOf(request.getParameter(PARAM_KEY_INDEX));
                } catch (@SuppressWarnings("unused") NumberFormatException e) {
                    throw GuardResponseHandlerExceptionCodes.BAD_RSA_ERROR.create();
                }
            }
            String decryptedPassword = decryptPassword(e_password, index, userId, cid);
            if (decryptedPassword == null) {
                throw GuardAuthExceptionCodes.BAD_PASSWORD.create();
            }
            return decryptedPassword;
        }
        return null;
    }


    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.authentication.UserIdentityParser#parse()
     */
    @Override
    public UserIdentity parse(int userId, int cid) throws OXException {
        if(request.getParameterMap().size() > 0) {
            String password = parsePassword(userId, cid);
            if(password != null) {
                return new UserIdentity(/*TODO*/null, password);
            }
        }
        return null;
    }

    @Override
    public UserIdentity parse() throws OXException {
        throw OXException.mandatoryField("userid and context required");
    }
}
