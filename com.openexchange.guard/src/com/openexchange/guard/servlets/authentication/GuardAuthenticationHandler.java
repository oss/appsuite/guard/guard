/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import javax.servlet.http.HttpServletRequest;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.servlets.GuardServletAction;

/**
 * Defines a mechanism for the authentication of a OX Guard request
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public interface GuardAuthenticationHandler {

    /**
     * Performs authentication for the given request
     * @param userSession the user session or null if the action is not performed in a user context
     * @param request the request to check authentication for
     * @param response
     * @return true, if the authentication has been granted, false otherwise
     * @throws Exception
     */
    boolean authenticate(GuardServletAction action, GuardUserSession userSession, HttpServletRequest request) throws Exception;

    /**
     * @return returns the value of the WWW-Authenticate header which will be set to the client's response in case the authentication was not successful
     */
    String getWWWAuthenticateHeader();
}
