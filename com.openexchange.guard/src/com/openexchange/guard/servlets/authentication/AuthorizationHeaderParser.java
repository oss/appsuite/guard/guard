/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpHeaders;

/**
 * {@link AuthorizationHeaderParser} parses a HTTP Authorization Header
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v7.8.3
 */
public class AuthorizationHeaderParser {

    /**
     * Gets specified string's ASCII bytes
     *
     * @param str The string
     * @return The ASCII bytes
     */
    private static byte[] toAsciiBytes(final String str) {
        if (null == str) {
            return null;
        }
        final int length = str.length();
        if (0 == length) {
            return new byte[0];
        }
        final byte[] ret = new byte[length];
        str.getBytes(0, length, ret, 0);
        return ret;
    }

    /**
     * Parses an authorization header
     *
     * @param request The request.
     * @return The authorization value of the first authorization header found.
     */
    public String parse(HttpServletRequest request) {
        return parse(null, request);
    }

    /**
     * Parses a specific Authorization mechanism (for example "Basic").
     *
     * @param authorizationMechanism The mechanism to parse
     * @param request The request
     * @return The authorization value of the authorization header with the given mechanism
     */
    public String parse(String authorizationMechanism, HttpServletRequest request) {
        Enumeration<?> headers = request.getHeaders(HttpHeaders.AUTHORIZATION);
        while (headers.hasMoreElements()) {
            String authorisationHeader = (String) headers.nextElement();
            String[] parsedHeader = null;
            if (authorisationHeader.contains(",")) {
                //We do also support a comma separated list of header-values in one header
                parsedHeader = authorisationHeader.split(",");
            } else {
                parsedHeader = new String[] { authorisationHeader };
            }
            for (String header : parsedHeader) {
                if (header != null) {
                    header = header.trim();
                    String authType = null;
                    String authValue = null;
                    if (header.contains(" ")) {
                        authType = header.substring(0, header.indexOf(" "));
                        authValue = header.substring(header.indexOf(" "));
                    }
                    if (authType != null && authValue != null) {
                        authType = authType.trim();
                        authValue = authValue.trim();
                        if (authorizationMechanism == null ||
                            authType.toLowerCase().equals(authorizationMechanism.toLowerCase())) {
                            byte[] decodedCredentials = org.apache.commons.codec.binary.Base64.decodeBase64(toAsciiBytes(authValue));
                            return new String(decodedCredentials, StandardCharsets.UTF_8);
                        }
                    }
                }
            }
        }
        return null;
    }
}
