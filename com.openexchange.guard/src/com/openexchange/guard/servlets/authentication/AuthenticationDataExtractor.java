/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.authentication;

import javax.servlet.http.HttpServletRequest;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.internal.UserData;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.session.GuardSessionService;

/**
 * {@link AuthenticationDataExtractor}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class AuthenticationDataExtractor {

    private static final String PARAM_E_PASSWORD = "epassword";
    private static final String PARAM_E_PASSWORD_ALTERNATIVE = "epass";
    private static final String PARAM_AUTH_TOKEN = "auth";
    private static final String PARAM_PLAIN_TEXT_PASSWORD = "password";

    private static String decodeEPass(String epass, String sessionId) throws OXException {
        GuardSessionService sessionService = Services.getService(GuardSessionService.class);
        String token = sessionService.getToken(sessionId);
        GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
        return (cipherService.decrypt(epass, token));
    }

    private String getPassword(String authToken, String epassword, String password, GuardUserSession userSession) throws OXException {
        //Try to parse the RSA encrypted authentication structure
        if (authToken != null) {
            if (authToken.length() > 20) {
                UserData data = new UserData(authToken, userSession);
                if ((data.getUserid() != -1) && (data.getEncr_password() != null)) {
                    return data.getEncr_password();
                }
            }
        }

        //Try to parse the AES encrypted password
        if (epassword != null && !epassword.isEmpty()) {
            String sessionId = userSession.getGuardSession();
            if(sessionId != null && !sessionId.isEmpty()) {
                return decodeEPass(epassword, sessionId);
            }
        }

        if (password != null) {
            return password;
        }

        return null;
    }

    private String extractEpasswordFromRequest(HttpServletRequest request) {
        String epassword = ServletUtils.getStringParameter(request, PARAM_E_PASSWORD);
        if (epassword != null && !epassword.isEmpty()) {
            return epassword;
        }
        return ServletUtils.getStringParameter(request, PARAM_E_PASSWORD_ALTERNATIVE);
    }

    public String getPassword(HttpServletRequest request, GuardUserSession userSession) throws OXException {
        return getPassword(request.getParameter(PARAM_AUTH_TOKEN), extractEpasswordFromRequest(request), ServletUtils.getStringParameter(request, PARAM_PLAIN_TEXT_PASSWORD), userSession);
    }

    public String requirePassword(HttpServletRequest request, GuardUserSession userSession) throws OXException {
        String password = getPassword(request, userSession);
        if(password == null) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("Password or auth missing.");
        }
        return password;
    }

    public String getPassword(JsonObject json, GuardUserSession userSession) throws OXException {
        if (json != null) {
            String authToken = null;
            if (json.has(PARAM_AUTH_TOKEN) && !json.get(PARAM_AUTH_TOKEN).isJsonNull()) {
                authToken = json.get(PARAM_AUTH_TOKEN).getAsString();
            }
            String epassword = null;
            if (json.has(PARAM_E_PASSWORD) && !json.get(PARAM_E_PASSWORD).isJsonNull()) {
                epassword = json.get(PARAM_E_PASSWORD).getAsString();
            }

            String password = null;
            if (json.has(PARAM_PLAIN_TEXT_PASSWORD) && !json.get(PARAM_PLAIN_TEXT_PASSWORD).isJsonNull()) {
                password = json.get(PARAM_PLAIN_TEXT_PASSWORD).getAsString();
            }
            return getPassword(authToken, epassword, password, userSession);
        }
        return null;
    }

    public String requirePassword(JsonObject json, GuardUserSession userSession) throws OXException {
        String password = getPassword(json, userSession);
        if(password == null) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("Password or auth missing.");
        }
        return password;
    }


    public String getPassword(HttpServletRequest request, JsonObject json, GuardUserSession userSession) throws OXException {
        String password = getPassword(request, userSession);
        if (password == null || password.isEmpty() /* UI seems to send empty password in some cases */) {
            password = getPassword(json, userSession);
        }
        return password;
    }

    public String requirePassword(HttpServletRequest request, JsonObject json, GuardUserSession userSession) throws OXException {
        String password = getPassword(request, json, userSession);
        if(password == null) {
            throw GuardCoreExceptionCodes.PARAMETER_MISSING.create("Password or auth missing.");
        }
        return password;
    }
}
