/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.servlets.pgpcore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.gson.JsonObject;
import com.openexchange.guard.common.servlets.utils.ServletUtils;
import com.openexchange.guard.common.session.GuardUserSession;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.pgpcore.services.TokenAuthenticationService;
import com.openexchange.guard.servlets.GuardServletAction;


/**
 * {@link DeleteAuthenticationTokenAction}
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.2
 */
public class DeleteAuthenticationTokenAction extends GuardServletAction {

    private static final String SESSION_PARAMETER_NAME = "session";

    /* (non-Javadoc)
     * @see com.openexchange.guard.servlets.GuardServletAction#doActionInternal(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, com.openexchange.guard.servlets.authentication.GuardUserSession)
     */
    @Override
    protected void doActionInternal(HttpServletRequest request, HttpServletResponse response, GuardUserSession userSession) throws Exception {
        String sessionIdentifier = ServletUtils.getStringParameter(request, SESSION_PARAMETER_NAME);
        if(sessionIdentifier == null) {
            JsonObject json = Services.getService(ResponseHandler.class).getJson(request);
            if(json != null) {
                if(json.has(SESSION_PARAMETER_NAME)){
                    sessionIdentifier = json.get(SESSION_PARAMETER_NAME).getAsString();
                }
            }
        }
        TokenAuthenticationService authService = Services.getService(TokenAuthenticationService.class);
        authService.deleteAuthenticationToken(sessionIdentifier);
    }
}
