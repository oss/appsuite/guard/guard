/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.handler.impl;

import java.io.BufferedReader;
import java.security.Key;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.common.util.JsonUtil;
import com.openexchange.guard.handler.ResponseHandler;
import com.openexchange.guard.handler.exceptions.GuardResponseHandlerExceptionCodes;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.server.ServiceLookup;

/**
 * {@link ResponseHandlerImpl}
 *
 * @author <a href="mailto:martin.schneider@open-xchange.com">Martin Schneider</a>
 * @since v2.4.0
 */
public class ResponseHandlerImpl implements ResponseHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ResponseHandlerImpl.class);
    private final ServiceLookup services;

    public ResponseHandlerImpl(ServiceLookup services) {
        this.services = services;
    }

     @Override
     public JsonObject getJsonAndDecodeEncryptedPasswords(HttpServletRequest request, int userId, int cid) throws OXException {
         return decodeEncryptedPassword(getJson(request), userId, cid);
    }

    @Override
    public JsonObject getJson(HttpServletRequest request) throws OXException{
        String postBody = getPostBody(request);
        if (postBody == null) {
            LOG.warn("Provided post body is empty. Cannot return decoded password.");
            return null;
        }
        return JsonUtil.parseAsJsonObject(postBody);
    }

    /**
     * Get the string contents of a request
     *
     * @param request The request to read from
     * @return the request's body as string
     */
    private static String getPostBody(HttpServletRequest request) {
        StringBuilder sb = null;
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                if (sb == null) {
                    sb = new StringBuilder();
                }
                sb.append(line);
            }
            reader.close();
        } catch (Exception e) {
            LOG.error("Error reading from request", e);
        }
        if (sb == null || sb.length() == 0) {
            return null;
        }
        return sb.toString();
    }

    /**
     * Parse JSOn and decode all encrypted passwords
     *
     * @param result
     * @return
     * @throws OXException
     */
    private JsonObject decodeEncryptedPassword(JsonObject result, int userId, int cid) throws OXException {
        if (result == null) {
            return (null);
        }
        result = decr(result, "e_password", "password", userId, cid);
        result = decr(result, "e_encr_password", "encr_password", userId, cid);
        return result;
    }

    private JsonObject decr(JsonObject result, String key, String newkey, int userId, int cid) throws OXException {
        if (result.has(key)) {
            String encr = result.get(key).getAsString();
            GuardCipherFactoryService cipherFactoryService = services.getServiceSafe(GuardCipherFactoryService.class);
            MasterKeyService mKeyService = services.getServiceSafe(MasterKeyService.class);
            GuardCipherService cipherService = cipherFactoryService.getCipherService(GuardCipherAlgorithm.RSA);
            Key decrKey = mKeyService.getDecryptedClientKey(mKeyService.getMasterKey(userId, cid, false));
            String decoded = cipherService.decrypt(encr, decrKey);
            if (decoded != null) {
                result.addProperty(newkey, decoded);
            } else {
                if (encr.length() > 2) {
                    throw GuardResponseHandlerExceptionCodes.BAD_RSA_ERROR.create();
                }
            }
        }
        return result;
    }
}
