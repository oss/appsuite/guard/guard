/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.oxuser;

import javax.servlet.http.HttpServletRequest;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.oxapi.Api;
import com.openexchange.guard.oxapi.OxCookie;

/**
 * {@link GetUserServiceImpl}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v2.10.0
 */
public class GetUserServiceImpl implements GetUserService {

    Api api;

    public GetUserServiceImpl(HttpServletRequest request) {
        api = new Api(request);
    }

    public GetUserServiceImpl(OxCookie cookie, String session, String userAgent) {
        api = new Api(cookie, session, userAgent);
    }

    /* (non-Javadoc)
     * @see com.openexchange.guard.oxuser.GetUserService#getUser(int)
     */
    @Override
    public OxUserResult getUser(int userid) throws OXException {
        JsonObject user = api.getUserData(userid);
        if (user != null && user.has("data")) {
            JsonObject userData = user.get("data").getAsJsonObject();
            if (userData.has("email1")) {
                OxUserResult result = new OxUserResult(userData.get("email1").getAsString(),
                    userData.has("guest_created_by"));
                return result;
            }

        }
        return null;
    }

}
