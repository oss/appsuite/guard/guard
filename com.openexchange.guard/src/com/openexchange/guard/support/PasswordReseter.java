
package com.openexchange.guard.support;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.cipher.GuardCipherAlgorithm;
import com.openexchange.guard.cipher.GuardCipherFactoryService;
import com.openexchange.guard.cipher.GuardCipherService;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.keymanagement.services.MasterKeyService;
import com.openexchange.guard.keymanagement.storage.KeyTableStorage;
import com.openexchange.guard.mailcreator.MailCreatorService;
import com.openexchange.guard.notification.GuardNotificationService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.ratifier.GuardRatifierService;

public class PasswordReseter {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordReseter.class);

    public enum EmailTarget {
        PRIMARY,
        SECONDARY
    }

    /**
     * @param userid
     * @param cid
     * @return
     * @throws OXException
     */
    public GuardKeys getCurrentKey(int userid, int cid) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        List<GuardKeys> keysForUser = ogKeyTableStorage.getKeysForUser(userid, cid);
        if (keysForUser.size() > 0) {
            return keysForUser.get(0);
        }
        throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(userid, cid);
    }

    /**
     * @param userid
     * @param cid
     * @return
     */
    private int getTemplateId(int userid, int cid) {
        int templid = 0;
        try {
            GuardConfigurationService guardConfigService = Services.getService(GuardConfigurationService.class);
            templid = guardConfigService.getIntProperty(GuardProperty.templateID, userid, cid);
        } catch (Exception e) {
            LOG.error("problem getting template id for reset password email");
        }
        return templid;
    }

    /**
     * Resets the password of a user's current key and send the new password to the given email address
     *
     * @param userid the ID of the user to change the password for
     * @param cid the context ID of the user
     * @param language the language used within the email sent to the user
     * @param email the email address to send the new password to
     * @throws OXException
     */
    public void resetPassword(int userid, int cid, String language, String email) throws OXException {
        int templid = getTemplateId(userid, cid);
        GuardKeys key = getCurrentKey(userid, cid);

        reset(userid, cid, language, email, templid, key.getEmail());
    }

    protected void reset(int userid, int cid, String language, String secondaryEmail, int templid, String emailToRest) throws OXException {
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        boolean recoveryEnabled = keyService.isRecoveryEnabled(userid, cid);
        if (!recoveryEnabled) {
            LOG.info("Failed to reset password for email address {} as recovery is not enabled/available.", emailToRest);
            throw GuardCoreExceptionCodes.DISABLED_ERROR.create("reset password");
        }

        String newpass = keyService.generatePassword(userid, cid);

        MailCreatorService mailCreatorService = Services.getService(MailCreatorService.class);
        JsonObject mail = mailCreatorService.getResetEmail(secondaryEmail, mailCreatorService.getFromAddress(emailToRest, emailToRest, userid, cid), newpass, language, templid, "", userid, cid);

        GuardNotificationService guardNotificationService = Services.getService(GuardNotificationService.class);
        guardNotificationService.send(mail, userid, cid, null);

        keyService.resetPassword(emailToRest, newpass);
    }

    /**
     * Resets the password of a user's current key and send the new password to either the user's secondary email address, or the primary email address
     *
     * @param userid the ID of the user to change the password for
     * @param cid the context ID of the user
     * @param language the language used within the email sent to the user
     * @throws OXException
     */
    public EmailTarget resetPassword(int userid, int cid, String language) throws OXException {
        KeyTableStorage ogKeyTableStorage = Services.getService(KeyTableStorage.class);
        List<GuardKeys> keysForUser = ogKeyTableStorage.getKeysForUser(userid, cid);

        if (keysForUser.size() > 0) {
            GuardKeys key = keysForUser.get(0);

            String encr_answer = key.getAnswer();
            GuardCipherService cipherService = Services.getService(GuardCipherFactoryService.class).getCipherService(GuardCipherAlgorithm.AES_CBC);
            boolean secondary = true;

            //First check if the user has secondary email address wich we can use
            String email = null;
            if (key.getQuestion() != null && key.getQuestion().equals("e")) {
                String rc = Services.getService(MasterKeyService.class).getMasterKey(userid, cid).getRC();
                email = cipherService.decrypt(encr_answer, rc, "e");
            }

            //Fall back to primary email address if no secondary email address was found for the user
            if (email == null || email.isEmpty()) {
                email = key.getEmail();
                secondary = false;
            }

            GuardRatifierService validatorService = Services.getService(GuardRatifierService.class);
            validatorService.validate(email);

            resetPassword(userid, cid, language, email);

            return secondary ? EmailTarget.SECONDARY : EmailTarget.PRIMARY;
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_IDS_ERROR.create(userid, cid);
        }
    }
}
