
package com.openexchange.guard.support;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.utils.URIBuilder;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.keymanagement.commons.DeletedKey;
import com.openexchange.guard.keymanagement.storage.DeletedKeysStorage;
import com.openexchange.guard.osgi.Services;

/**
 * Exposes a private key of a deleted user for downloading
 *
 * When user's get deleted we backup their keys for a certain amount of time.
 * In that time-span an administrator can enable download functionality for
 * a user to ensure the user can get his key, even if his account was already
 * deleted.
 */
public class DeletedKeyExposer {

    /**
     * Internal method to create an URI for accessing an exposed key
     *
     * @param deletedKey The deleted key to create an URI for
     * @return An URI from which a user can download an exposed key
     * @throws OXException
     */
    private URI buildGetExposedKeyUri(DeletedKey deletedKey) throws OXException {

        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);

        String base = configService.getProperty(GuardProperty.externalEmailURL, deletedKey.getUserId(), deletedKey.getCid());
        final String baseDomain = configService.getProperty(GuardProperty.baseDomain, deletedKey.getUserId(), deletedKey.getCid());
        if(base.isEmpty() && baseDomain.isEmpty()) {
            throw GuardCoreExceptionCodes.CONFIGURATION_ERROR.create("baseDomain", "Missing");
        }
        if (!baseDomain.isEmpty()) {
            base = baseDomain;  // BaseDomain over-rides the value of the externalEmailURL
        }
        final String uiWebPath = configService.getProperty("com.openexchange.UIWebPath", deletedKey.getUserId(), deletedKey.getCid(), "MISSING");
        if(uiWebPath.isEmpty()) {
            throw GuardCoreExceptionCodes.CONFIGURATION_ERROR.create("com.openexchange.UIWebPath", "Missing");
        }

        StringBuilder sb = new StringBuilder();
        sb.append(uiWebPath);
        if (!uiWebPath.endsWith("/")) {
            sb.append("/");
        }
        sb.append("api/oxguard/retrieve");
        String path = sb.toString();

        String scheme = "http" + (configService.getBooleanProperty(GuardProperty.backendSSL) ? "s" : "");
        URIBuilder uri = new URIBuilder().setScheme(scheme).setHost(base).setPath(path).
            setParameter("action", "getExposedKeys").
            setParameter("userId", String.valueOf(deletedKey.getUserId())).
            setParameter("contextId", String.valueOf(deletedKey.getCid()));
        try {
            return uri.build();
        } catch (URISyntaxException e) {
            throw GuardCoreExceptionCodes.UNEXPECTED_ERROR.create(e, "Could not construct redirect location for URI '" + uri.toString() + "'");
        }
    }

    /**
     * Exposes all deleted keys for the given user
     *
     * @param email the user's email
     * @param cid the context id
     * @throws OXException On error during exposing the key
     * @return an URI for accessing deleted but exposed keys for the user
     */
    public URI exposeKey(String email, int cid) throws OXException {
        DeletedKeysStorage deletedKeyStorage = Services.getService(DeletedKeysStorage.class);

        //Checking if the user has deleted keys and get the salt of the first found key
        DeletedKey firstDeletedKey = deletedKeyStorage.getFirstForEmail(email, cid);
        if (firstDeletedKey == null) {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_MAIL_ERROR.create(email, cid);
        }

        //Creating URI for accessing deleted,exposed keys
        URI uri = buildGetExposedKeyUri(firstDeletedKey);

        //Exposing the keys of the given email in order to be available using the URI
        deletedKeyStorage.setExposedForEmail(email, cid);

        return uri;
    }
}
