/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.support;

import java.net.URI;
import com.openexchange.exception.OXException;
import com.openexchange.guard.email.storage.ogEmail.Email;
import com.openexchange.guard.email.storage.ogEmail.EmailStorage;
import com.openexchange.guard.exceptions.GuardCoreExceptionCodes;
import com.openexchange.guard.guestupgrade.GuestUpgradeService;
import com.openexchange.guard.internal.GuardMaintenanceExceptionCodes;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.secondfactor.SecondFactorService;
import com.openexchange.guard.support.PasswordReseter.EmailTarget;

/**
 * Default implementation of the SupportService
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class SupportServiceImpl implements SupportService {

    @Override
    public URI exposeKey(int contextId, String email) throws OXException {
        return new DeletedKeyExposer().exposeKey(email, contextId);
    }

    @Override
    public EmailTarget resetPassword(String email, String language) throws OXException {
        //finding user name and context id for the given email
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email mapping = ogEmailStorage.getByEmail(email);

        if (mapping != null) {
            return new PasswordReseter().resetPassword(mapping.getUserId(), mapping.getContextId(), language);
        } else {
            throw GuardCoreExceptionCodes.KEY_NOT_FOUND_FOR_MAIL_ERROR.create(email, 0);
        }
    }

    @Override
    public void deleteUser(int contextId, int userId) throws OXException {
        new UserDeleter().deleteWithBackup(userId, contextId);
    }

    @Override
    public void deleteUser(String email) throws OXException {
        if (email == null || email.isEmpty()) {
            throw new IllegalArgumentException("Missing parameter email");
        }
        //Getting user-ID and CID by the user's by email
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email ogEmail = ogEmailStorage.getByEmail(email);
        if (ogEmail != null) {
            deleteUser(ogEmail.getContextId(), ogEmail.getUserId());
        } else {
            throw new IllegalArgumentException("no such user");
        }
    }

    @Override
    public void removeSecondary(String email) throws OXException {
        if (email == null || email.isEmpty()) {
            throw new IllegalArgumentException("Missing parameter email");
        }
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        GuardKeys key = keyService.getKeys(email);
        if (key == null) {
            throw GuardMaintenanceExceptionCodes.ACCOUNT_NOT_FOUND.create(email);
        }
        if (!keyService.storeQuestion(key.getUserid(), key.getContextid(), "e", "")) {
            throw OXException.general("Error writing to database");
        }
    }

    @Override
    public void removePin(String email) throws OXException {
        // Pre 2.10, can be removed soon
        GuardKeyService keyService = Services.getService(GuardKeyService.class);
        keyService.removePin(email);
        // 2.10 and post
        EmailStorage ogEmailStorage = Services.getService(EmailStorage.class);
        Email ogEmail = ogEmailStorage.getByEmail(email);
        if (ogEmail == null) {
            throw GuardMaintenanceExceptionCodes.ACCOUNT_NOT_FOUND.create(email);
        }
        SecondFactorService secondFactor = Services.getService(SecondFactorService.class);
        secondFactor.removeSecondFactor(ogEmail.getUserId(), ogEmail.getContextId());

    }

    @Override
    public void upgradeGuestAccount(String email, String userid, String cid) throws OXException {
        GuestUpgradeService guestUpgradeService = Services.getService(GuestUpgradeService.class);
        guestUpgradeService.upgrade(email, userid, cid);
    }
}
