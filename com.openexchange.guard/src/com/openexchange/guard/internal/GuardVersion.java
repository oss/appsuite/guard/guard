/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.internal;

import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link GuardVersion} - Extracting Version information from the Manifest file
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.4.0
 */
public class GuardVersion {

    private static final Logger logger = LoggerFactory.getLogger(GuardVersion.class);
    private static final String UNKNOWN_VERSION_STRING = "n/a";
    private static final String MANIFEST_VERSION = "Version";
    private static final String MANIFEST_REVISION = "Revision";
    private static final String MANIFEST_BUILD_DATE = "BuildDate";

    private static String guardVersion;

    /**
     * Internal method to construct the version string from the manifest attributes
     *
     * @param manifestAttributes the manifest attributes
     * @return the version string
     */
    private static String createVersionString(Attributes manifestAttributes) {
        return manifestAttributes.getValue(MANIFEST_VERSION) +
            "-rev" +
            manifestAttributes.getValue(MANIFEST_REVISION) +
            ":" +
            manifestAttributes.getValue(MANIFEST_BUILD_DATE);
    }

    /**
     * Loads the guard version from the manifest file
     */
    public static void loadGuardVersion() {
        try {
            String location = GuardVersion.class.getProtectionDomain().getCodeSource().getLocation().toString();
            location = location.replace("file:", "");
            if (location.contains(".jar")) {
                try (JarFile jarFile = new JarFile(location)) {
                    Manifest manifest = jarFile.getManifest();
                    if (manifest == null) {
                        guardVersion = "unknown";
                    } else {
                        Attributes attributes = manifest.getMainAttributes();
                        String version = createVersionString(attributes);
                        guardVersion = version;
                        logger.info("OX Guard Version: " + guardVersion);
                    }
                }
            } else {
                guardVersion = null;
            }
        } catch (Exception ex) {
            logger.error("Problem getting OX Guard version ", ex);
        }
    }

    /**
     * @return the guard version defined in the bundle's manifest file
     */
    public static String getGuardVersion() {
        return (guardVersion == null || guardVersion.isEmpty()) ? UNKNOWN_VERSION_STRING : guardVersion;
    }
}
