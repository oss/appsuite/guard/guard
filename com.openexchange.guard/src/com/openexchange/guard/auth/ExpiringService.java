/*
* @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/
package com.openexchange.guard.auth;

import static com.openexchange.java.Autoboxing.I;
import static com.openexchange.java.Autoboxing.L;
import java.util.Calendar;
import java.util.Date;
import com.google.gson.JsonObject;
import com.openexchange.exception.OXException;
import com.openexchange.guard.certificatemanagement.commons.SmimeKeys;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.commons.GuardKeys;
import com.openexchange.guard.keymanagement.services.GuardKeyService;
import com.openexchange.guard.osgi.Services;
import com.openexchange.guard.smime.SmimeKeyService;

/**
 * {@link ExpiringService}
 * Service to get list of expired and expiring keys
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v8.28
 */
public class ExpiringService {

    /**
     * Returns a JSONObject containing the guardkey id and expiration date
     *
     * @param key
     * @return
     */
    private static JsonObject guardKeyObject(GuardKeys key) {
        JsonObject json = new JsonObject();
        json.addProperty("id", Long.toString(key.getKeyid()));
        json.addProperty("expiration", L(key.getExpiration().getTime()));
        return json;
    }

    /**
     * Return a JSONObject containing the smime id and expiration date
     *
     * @param key
     * @return
     */
    private static JsonObject smimeObject(SmimeKeys key) {
        JsonObject json = new JsonObject();
        json.addProperty("id", key.getSerial().toString());
        json.addProperty("expiration", L(key.getExpires().getTime()));
        return json;
    }

    /**
     * Checks the S/Mime and Guard Keys marked current to see if they are expired
     * or expiring soon. Returns JSON listing the ones expiring or expired
     *
     * @param userId
     * @param contextId
     * @return JSON containing "current" keys/certs expiring or expired
     * @throws OXException
     */
    public static JsonObject getExpiringJson(int userId, int contextId) throws OXException {
        SmimeKeys smimeKeys = null;
        GuardKeys guardKeys = null;
        JsonObject expiring = new JsonObject();
        final SmimeKeyService smimeKeyService = Services.getService(SmimeKeyService.class);
        final GuardKeyService guardKeyService = Services.getService(GuardKeyService.class);
        if (smimeKeyService != null) {
            smimeKeys = smimeKeyService.getCurrentKey(userId, contextId);
        }
        if (guardKeyService != null) {
            guardKeys = guardKeyService.getKeys(userId, contextId);
        }
        if (smimeKeys == null && guardKeys == null) { // Services not loaded or no keys
            return expiring;
        }
        GuardConfigurationService configService = Services.getService(GuardConfigurationService.class);
        int days = configService.getIntProperty(GuardProperty.expiringCertificateWarningDays);
        if (days == 0)  // Disabled
            return expiring;
        expiring.addProperty("daysWarning", I(days));

        Date current = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(current);
        cal.add(Calendar.DATE, days);
        
        if (guardKeys != null) {
            Date expires = guardKeys.getExpiration();
            if (expires != null) {
                if (expires.before(current)) {
                    expiring.add("guardExpired", guardKeyObject(guardKeys));
                } else {
                    if (expires.before(cal.getTime())) {
                        expiring.add("guardExpiringSoon", guardKeyObject(guardKeys));
                    }
                }
            }
        }

        if (smimeKeys != null) {
            Date expires = smimeKeys.getExpires();
            if (expires.before(current)) {
                expiring.add("smimeExpired", smimeObject(smimeKeys));
            } else {
                if (expires.before(cal.getTime())) {
                    expiring.add("smimeExpiringSoon", smimeObject(smimeKeys));
                }
            }
        }
        return expiring;
    }
}
