# SPDX-FileCopyrightText: 2014, 2016, 2019, 2024 Ivar Snaaijer <ivar@workshop-chapina.com>
msgid ""
msgstr ""
"Project-Id-Version: OX Guard templates\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-03-25 07:58-0400\n"
"PO-Revision-Date: 2024-03-30 23:42+0100\n"
"Last-Translator: Ivar Snaaijer <ivar@workshop-chapina.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.1\n"

#. Autocrypt Setup message for transferring keys
#: ../templates/guard/autocryptTransfer.html:2
msgid "Autocrypt Setup Message"
msgstr "Autocrypt Configuratie Bericht"

#: ../templates/guard/autocryptTransfer.html:8
msgid "Your $productName key transfer email"
msgstr "Uw $productName sleutel overzet e-mail"

#: ../templates/guard/autocryptTransfer.html:12
msgid ""
"This email contains the information required to transfer your keys securely "
"to another email client."
msgstr ""
"Dit e-mailbericht bevat de informatie die nodig is om uw sleutel veilig over "
"te zetten naar een ander e-mailprogramma."

#: ../templates/guard/autocryptTransfer.html:13
msgid ""
"To begin, open this email in the new client and follow the instructions.  "
"You will need the multi-digit passcode provided to you when this email was "
"created."
msgstr ""
"Om te beginnen opent u dit e-mailbericht in het nieuwe programma en volgt u "
"de instructies. U moet de meercijferige pincode opgeven die u is gegeven "
"toen deze e-mail is gemaakt."

#: ../templates/guard/footer.html:13
#: ../templates/guard/guestresettempl.html:289
#: ../templates/guard/guesttempl.html:357
#: ../templates/guard/passwordtempl.html:395
#: ../templates/guard/resettempl.html:306
#| msgid ""
#| "Please do not reply to this email. If you want to contact us please click"
msgid ""
"Please don't reply to this email. If you want to contact us, you can write "
"to $support"
msgstr ""
"Beantwoord dit e-mailbericht alstublieft niet. Als u contact met ons op wil "
"nemen stuur dan naar $support"

#: ../templates/guard/guestresettempl.html:1
msgid "$productName Account Reset"
msgstr "$productName Account Herstel"

#: ../templates/guard/guestresettempl.html:19
#: ../templates/guard/guesttempl.html:17
#: ../templates/guard/passwordtempl.html:19
#: ../templates/guard/resettempl.html:20
msgid "$productName - Your secure email link"
msgstr "$productName - Uw beveiligde e-maildienst"

#: ../templates/guard/guestresettempl.html:182
#| msgid ""
#| "Clicking the link below will result in your account being reset, allowing "
#| "you to choose a password for your new account.  This will not reset the "
#| "password for previous emails, rather only apply to future emails secured "
#| "using $productName."
msgid ""
"Click the link below to reset your account. You will then be able to choose "
"a password for new secure messages.  This will not reset the password for "
"previous emails, rather the new password will only apply to future emails "
"secured using $productName."
msgstr ""
"Klik op de onderstaande link om uw account te herstellen. U kan dan een"
" wachtwoord kiezen voor nieuwe beveiligde berichten. Dit zal niet het "
"wachtwoord voor oude e-mailberichten herstellen, maar wel toegepast worden op"
" nieuwe berichten die beveiligd zijn met $productName."

#: ../templates/guard/guestresettempl.html:183
msgid ""
"If you did not send this request, ignore this email and no changes will "
"occur to your account."
msgstr ""
"Als u geen verzoek heeft gedaan kan u dit e-mailbericht negeren en worden "
"geen wijzigingen gemaakt aan uw account."

#: ../templates/guard/guestresettempl.html:214
msgid "RESET ACCOUNT"
msgstr "ACCOUNT HERSTELLEN"

#: ../templates/guard/guesttempl.html:227
msgid ""
"$from has shared a secure email with you to keep your conversation private."
msgstr ""
"$from heeft een beveiligd e-mailbericht met u gedeeld om de conversatie privé"
" te houden."

#: ../templates/guard/guesttempl.html:247
#| msgid "Please click the button below to read the secure $productName email:"
msgid "Click the button below to read the secure $productName email:"
msgstr ""
"Klik op de knop hieronder om het beveiligde $productName e-mailbericht te"
" lezen:"

#: ../templates/guard/guesttempl.html:281
msgid "READ MESSAGE"
msgstr "LEES BERICHT"

#: ../templates/guard/passwordtempl.html:1
msgid "Welcome to $productName"
msgstr "Welkom bij $productName"

#: ../templates/guard/passwordtempl.html:181
#| msgid ""
#| "You have received this email because $from has sent you a secure email "
#| "message with $productName. You will receive a link to the secure message "
#| "in a separate email. $from added a personalized message to this invite:"
msgid ""
"You have received this email because $from has sent you a secure email "
"message with $productName. You will receive a link to the message in a "
"separate email. $from added a personalized message to this invite:"
msgstr ""
"U heeft dit e-mailbericht ontvangen omdat $from u een beveiligd e-"
"mailbericht heeft gestuurd met $productName. U ontvangt een link naar het "
"beveiligde bericht in een apart e-mailbericht. $from heeft het volgende "
"persoonlijke bericht toegevoegd aan deze uitnodiging:"

#: ../templates/guard/passwordtempl.html:184
#| msgid ""
#| "You have received this email because $from has sent you a secure email "
#| "message with $productName. You will receive a link to the secure message "
#| "in a separate email."
msgid ""
"You have received this email because $from has sent you a secure email "
"message with $productName. You will receive a link to the message in a "
"separate email."
msgstr ""
"U heeft dit e-mailbericht ontvangen omdat $from u een beveiligd e-"
"mailbericht heeft gestuurd met $productName. U ontvangt een link naar het "
"beveiligde bericht in een apart e-mailbericht."

#: ../templates/guard/passwordtempl.html:264
#| msgid ""
#| "$productName created the following temporary password in order for you to "
#| "access the secure email message:"
msgid ""
"$productName created the following temporary password in order for you to "
"access your message:"
msgstr ""
"$productName heeft het volgende tijdelijke wachtwoord aangemaakt zodat u "
"toegang kan krijgen tot uw beveiligde e-mailbericht:"

#: ../templates/guard/passwordtempl.html:324
#| msgid ""
#| "For extra security you will be asked to change your temporary password "
#| "once you access your secure email message for the first time."
msgid ""
"For extra security you will be asked to change your temporary password once "
"you access your email message for the first time."
msgstr ""
"Voor extra zekerheid moet u het tijdelijke wachtwoord veranderen als u voor"
" de "
"eerste keer de beveiligde e-mailberichten gebruikt."

#. Password reset in subject line
#: ../templates/guard/resettempl.html:2
msgid "Password Reset"
msgstr "Wachtwoord herstel"

#: ../templates/guard/resettempl.html:181
msgid "Your $productName password has been reset. Your new password is:"
msgstr "Uw $productName wachtwoord is hersteld. Uw nieuwe wachtwoord is:"

#: ../templates/guard/upsell_example.html:3
msgid ""
"You have received an email protected with Guard Security.  If you would like "
"to find out how you can send fully protected emails, click on the following "
"link:"
msgstr ""
"U heeft een e-mailbericht ontvangen welke is beveiligd met Guard Security. "
"Als u meer wilt weten hoe u volledig beveiligde e-mailberichten kan "
"verzenden klikt u op de volgende link:"

#: ../templates/guard/upsell_example.html:5
msgid "Upgrade"
msgstr "Upgrade"

#~ msgid "Request received to reset your <strong>$productName</strong> account"
#~ msgstr ""
#~ "Er is een verzoek ontvangen om uw <strong>$productName</strong> account "
#~ "te herstellen"

#~ msgid ""
#~ "If this does not work you can copy and paste the link below into your "
#~ "browser."
#~ msgstr ""
#~ "Als dit niet werkt kan u de onderstaande link kopiëren en in uw "
#~ "webbrowser plakken."

#~ msgid "here"
#~ msgstr "hier"

#~ msgid "You have received a secure <strong>$productName</strong> email"
#~ msgstr ""
#~ "U heeft een beveiligde <strong>$productName</strong> e-mail ontvangen"

#~ msgid "$productName - Your secure email password"
#~ msgstr "$productName - Uw beveiligde e-mail wachtwoord"

#~ msgid "Welcome to <strong>$productName</strong>"
#~ msgstr "Welkom bij <strong>$productName</strong>"

#~ msgid "$productName - Your $productName password has been reset"
#~ msgstr "$productName - Uw $productName wachtwoord is hersteld"

#~ msgid "<strong>$productName</strong> password reset"
#~ msgstr "<strong>$productName</strong> wachtwoord hersteld"

#~ msgid ""
#~ "For extra security please change this password in your $productName "
#~ "account."
#~ msgstr ""
#~ "Voor extra zekerheid moet u dit wachtwoord veranderen in uw $productName "
#~ "account."

#~ msgid "OX Guard - Your secure email link"
#~ msgstr "OX Guard - Uw beveiligde e-maildienst"

#~ msgid "You have received a secure <strong>OX Guard</strong> email"
#~ msgstr "U heeft een beveiligde <strong>OX Guard</strong> e-mail ontvangen"

#~ msgid "Please click the button below to read the secure OX Guard email:"
#~ msgstr ""
#~ "Klik alstublieft op de knop hieronder om het beveiligde OX Guard e-"
#~ "mailbericht te lezen:"

#~ msgid "Welcome to OX Guard"
#~ msgstr "Welkom bij OX Guard"

#~ msgid "OX Guard - Your secure email password"
#~ msgstr "OX Guard - Uw beveiligde e-mail wachtwoord"

#~ msgid "Welcome to"
#~ msgstr "Welkom bij"

#~ msgid "OX Guard"
#~ msgstr "OX Guard"

#~ msgid ""
#~ "You have received this email because $from has sent you a secure email "
#~ "message with OX Guard. You will receive a link to the secure message in a "
#~ "separate email."
#~ msgstr ""
#~ "U heeft dit e-mailbericht ontvangen omdat $from u een beveiligd e-"
#~ "mailbericht heeft gestuurd met OX Guard. U ontvangt een link naar het "
#~ "beveiligde bericht in een apart e-mailbericht."

#~ msgid ""
#~ "OX Guard created the following temporary password in order for you to "
#~ "access the secure email message:"
#~ msgstr ""
#~ "OX Guard heeft het volgende tijdelijke wachtwoord aangemaakt zodat u "
#~ "toegang kan krijgen tot uw beveiligde e-mailbericht:"

#~ msgid ""
#~ "Please click the link below to read the secure OX Guard email using your "
#~ "webmail:"
#~ msgstr ""
#~ "Klik alstublieft op de link hieronder om het beveiligde OX Guard e-"
#~ "mailbericht te lezen vanuit uw webmail:"

#~ msgid ""
#~ "If this does not work you can copy and paste the link below into your "
#~ "browser or log into your webmail directly"
#~ msgstr ""
#~ "Als dit niet werkt kan u de onderstaande link kopiëren en in uw "
#~ "webbrowser plakken of uzelf aan te melden in uw webmail."

#~ msgid ""
#~ "Please click the link below to read the secure $productName email using "
#~ "your webmail:"
#~ msgstr ""
#~ "Klik alstublieft op de link hieronder om het beveiligde $productName e-"
#~ "mailbericht te lezen vanuit uw webmail:"

#~ msgid ""
#~ "You have received this email because $from has sent you a secure email "
#~ "message with OX Guard. You will receive a link to the secure message in a "
#~ "separate email. $from added a personalized message to this invite:"
#~ msgstr ""
#~ "U heeft dit e-mailbericht ontvangen omdat $from u een beveiligd e-"
#~ "mailbericht heeft gestuurd met OX Guard. U ontvangt een link naar het "
#~ "beveiligde bericht in een apart e-mailbericht. $from heeft het volgende "
#~ "persoonlijke bericht toegevoegd aan deze uitnodiging:"

#~ msgid "OX Guard - Your OX Guard password has been reset"
#~ msgstr "OX Guard - Uw OX Guard wachtwoord is hersteld"

#~ msgid "<strong>OX Guard </strong> password reset"
#~ msgstr "<strong>OX Guard </strong> wachtwoord herstel"

#~ msgid "Your OX Guard password has been reset. Your new password is:"
#~ msgstr "Uw OX Guard wachtwoord is hersteld. Uw nieuwe wachtwoord is:"

#~ msgid ""
#~ "For extra security please change this password in your OX Guard account."
#~ msgstr ""
#~ "Voor extra zekerheid moet u dit wachtwoord veranderen in uw OX Guard "
#~ "account."

