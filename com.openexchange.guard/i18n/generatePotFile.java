/*
* @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
* @license AGPL-3.0
*
* This code is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
*
* You should have received a copy of the GNU Affero General Public License
* along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
*
* Any use of the work other than as authorized under this license or copyright law is prohibited.
*
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v4.0.0
 */
public class generatePotFile {

    private static final String POT_HEADER = "# Open-Xchange POT File.\n"
        + "# copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>\n"
        + "# license AGPL-3.0\n"
        + "msgid \"\"\n"
        + "msgstr \"\"\n"
        + "\"MIME-Version: 1.0\\n\"\n"
        + "\"Content-Type: text/plain; charset=CHARSET\\n\"\n"
        + "\"Content-Transfer-Encoding: 8bit\\n\"\n\n";

    /**
     * Only pull java files within the directory
     *
     * @param dir
     * @return
     * @throws IOException
     */
    private static Set<String> listFiles(String dir) throws IOException {
        try (Stream<Path> stream = Files.walk(Paths.get(dir), 20)) {
            return stream
                .filter(file -> file.toString().endsWith(".java"))
                .map(Path::toString)
                .collect(Collectors.toSet());
        }
    }

    /**
     * Create POT entry for a given string. Previous line may contain hint/remarks
     *
     * @param string String to translate
     * @param previous Line possibly containing hint/remarks
     * @return An entry for the POT file
     */
    private static String createPOTEntry(String string, String previous, int lineNum, String filename) {
        StringBuilder sb = new StringBuilder();
        if (previous != null) {
            previous = previous.trim();
            // Check if starts with remark
            if (previous.startsWith("/")) {
                previous = previous.substring(previous.indexOf(" "));
                previous = previous.trim();
                if (previous.endsWith("*/")) {
                    previous = previous.substring(0, previous.length() - 2);
                }
            } else {
                previous = "";
            }
        }
        if (previous != null && !previous.isEmpty()) {
            sb.append("#. ");
            sb.append(previous);
            sb.append("\n");
        }
        sb.append("#: ");
        // Append filename, but get rid of git location
        if (filename.contains("com.openexchange")) {
            filename = filename.substring(filename.indexOf("com.openexchange"));
        }
        sb.append(filename);
        sb.append(":");
        sb.append(lineNum);
        sb.append("\n");
        sb.append("msgid \"");
        sb.append(string);
        sb.append("\"\n");
        sb.append("msgstr \"\"");
        sb.append("\n\n");
        return sb.toString();
    }

    /**
     * Assemble the POT entries for a given file
     *
     * @param fileString
     * @return List of POT entries if any for a file
     * @throws IOException
     */
    private static String getPOTEntries(String fileString) throws IOException {

        File file = new File(fileString);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder sb = new StringBuilder();
        sb.append(POT_HEADER);
        String line, previousLine = null;
        int lineNum = 0;
        boolean translating = false;
        while ((line = br.readLine()) != null) {
            lineNum++;
            if (translating) {
                if (line.contains("String") && line.contains("=")) {
                    int start = line.indexOf("\"") + 1;
                    int end = line.lastIndexOf("\"");
                    if (start > 0 && end > 0) {
                        String toTranslate = line.substring(start, end);
                        sb.append(createPOTEntry(toTranslate, previousLine, lineNum, fileString));
                    }
                }
            } else {
                if (line.contains("interface") || line.contains("abstract")) {
                    return null;
                }
                if (line.contains("class")) {
                    if (line.contains("implements LocalizableStrings")) {
                        translating = true;
                    } else {
                        return null;
                    }
                }
            }
            previousLine = line;
        }
        return sb.toString();

    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 2 || !args[0].equals("-d")) {
            System.out.println("Missing parameters.");
            System.out.println("Mandatory: use -d followed by upper directory containing all code for pot file");
            System.out.println("Option: -o followed by output file name.  i.e. ../i18n/guard.pot");
            System.exit(1);
        }
        String directory = args[1];
        Path path = Paths.get(directory);
        if (!Files.exists(path) || !Files.isDirectory(path)) {
            System.out.println("Invalid directory");
            System.exit(1);
        }
        String outputLocation = "guard.pot";
        if (args.length >= 4) {
            if (args[2] == "-d") {
                outputLocation = args[3];
            }
        }

        try {
            StringBuilder sb = new StringBuilder();
            Set<String> files = listFiles(directory);
            Iterator<String> fileList = files.iterator();
            while (fileList.hasNext()) {
                String newEntries = getPOTEntries(fileList.next());
                if (newEntries != null && !newEntries.isEmpty()) {
                    sb.append(newEntries);
                }
            }
            System.out.println(System.getProperty("user.dir"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputLocation));
            writer.write(sb.toString());
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
