//-------------------------------------------------------------------------------------------------
//Jenkinsfile / declarative pipeline job to build gettext-POT files from the OX Guard sources.
//-------------------------------------------------------------------------------------------------
//GIT
def guardRepo =  'code.open-xchange.com/git/guard'
def guardRepoURI = 'https://' + guardRepo
def automationRepoURI = 'https://code.open-xchange.com/git/automation'
def backendRepoURI = 'https://code.open-xchange.com/git/wd/backend'
//-------------------------------------------------------------------------------------------------
//Ant build target 
def antTarget = 'create-server-pot'
def antPotBuildFile = './automation/backendI18N/guardPot.xml'
//-------------------------------------------------------------------------------------------------
//Buil POT parameters
def baseDir= '/automation/backendI18N/'
def potDir='/com.openexchange.guard/i18n'
def productToGenerate='guard'
//-------------------------------------------------------------------------------------------------
//The branch to use for the automation repository
def branch = 'develop'
//-------------------------------------------------------------------------------------------------

pipeline {

    agent{
        label 'gettext&&git&&java8'
    }

    triggers {
        cron('develop' == env.BRANCH_NAME ? 'H H(20-23) * * 1-5' : '')
    }

    stages{

        stage('init'){
            steps{
                cleanWs()
            }
        }

        stage("SCM checkout"){
            steps{
                checkout scm
                checkout([$class: 'GitSCM', branches: [[name: branch ]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'automation']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: '2cb98438-7e92-4038-af72-dad91b4ff6be', url: automationRepoURI]]])
            }	
        }

        stage("build POT"){
            steps{
                withAnt(installation: 'ant') {
                    withEnv(["baseDir=" + baseDir,
                            "antPotBuildFile=" + antPotBuildFile,
                            "branch=" + branch,
                            "potDir=" + potDir, 
                            "productToGenerate=" + productToGenerate,
                            "antTarget=" + antTarget]) {
                        sh'''
                            ant -v -file ${antPotBuildFile} -Dbranch=${branch} -DcheckoutDir="${WORKSPACE}" -DproductToGenerate=guard -DpotDir="${WORKSPACE}${potDir}" -Dbasedir="${WORKSPACE}${baseDir}" ${antTarget}
                        '''   
                    }
                }    
            } 
        }

        stage("Commit POT changes to SCM"){
            steps{
                withEnv(['GUARD_REPO=' + guardRepo, 'GUARD_BRANCH=' + env.BRANCH_NAME]) {
                    withCredentials([usernameColonPassword(credentialsId: '2cb98438-7e92-4038-af72-dad91b4ff6be', variable: 'GIT_CREDENTIALS')]) {
                        dir('guard'){
                            sh'''
                                URI="https://${GIT_CREDENTIALS}@${GUARD_REPO} ${GUARD_BRANCH}"
                                LINES=$(git status --porcelain --untracked-files=no | wc -l)
                                if [ $LINES -ne 0 ]
                                    then
                                        # Push the detected changes.
                                        # Make sure jenkins/git is not operating in detached HEAD mode:
                                        # Enable additional git behaviour "Check out to matching local branch" in the jenkins job configuration
                                        git add ../com.openexchange.guard/i18n/guard.pot
                                        git commit -m "Automatic POT generation"
                                        git push ${URI}
                                fi
                            '''   
                        }
                    }
                }
            }
        }
    }
}
