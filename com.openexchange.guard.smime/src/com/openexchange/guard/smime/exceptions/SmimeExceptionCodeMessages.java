/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.smime.exceptions;

import com.openexchange.i18n.LocalizableStrings;

/**
 * {@link SmimeExceptionCodeMessages}
 *
 * @author <a href="mailto:greg.hill@open-xchange.com">Greg Hill</a>
 * @since v7.10.4
 */
public class SmimeExceptionCodeMessages implements LocalizableStrings {

    // User tried to upload a duplicate private SMIME certificate
    public static final String DUPLICATE_PRIVATE = "This appears to be a duplicate SMIME private certificate";
    // Certificates in the chain could not be verified as trusted
    public static final String VERIFICATION_FAILED = "The certificate chain could not be fully verified as trusted";
    // Key wasn't found
    public static final String KEY_NOT_FOUND = "The key can not be found '%1$s'";
    // User is not authorized to perform this action
    public static final String NOT_AUTHORIZED = "Not authorized to perform this action";
    // Unusual smime exception error
    public static final String SMIME_ERROR = "Error processing smime message '%1$s'";
    // A cryptographic error occurred doing this SMIME process
    public static final String CRYPTO_GRAPH_ERROR = "Cryptographic error occurred performing this SMIME action: '%1$s'";
    // Key wasn't found that cold be used for signing
    public static final String KEY_NOT_FOUND_FOR_SIGNING = "A valid key for signing can not be found.";
    // Password recovery function is disabled
    public static final String RECOVERY_DISABLED = "Password recovery function is not available";
    // Unable to decrypt this message.  If the user has multiple keys, possibly the wrong password was used for the key.  Possibly due to missing key.
    public static final String UNABLE_TO_DECRYPT = "In case of multiple keys, it is possible that the wrong password for the key was given.  It is possible that a key is missing.";
    // Unable to find an encryption certificate for the sender.
    public static final String UNABLE_TO_FIND_SENDER = "Unable to find encryption certificate for sender";
    // The certificate was marked as revoked
    public static final String REVOKED = "Certificate has been revoked";
    // Another account already has this private key, and it cannot be uploaded
    public static final String PRIVATE_KEY_ALREADY_EXISTS = "Another account already has this private key, and it cannot be uploaded";
    // Another user has a private key with this email address
    public static final String PRIVATE_KEY_OTHER_USER = "A private key with this email exists with another user account";
    // The certificate has expired (past the valid date)
    public static final String CERTIFICATE_EXPIRED = "The certificate has expired.";

}
