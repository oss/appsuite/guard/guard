/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.keysources;

import java.security.KeyPair;
import org.bouncycastle.openpgp.PGPKeyPair;
import com.openexchange.exception.OXException;
import com.openexchange.guard.keymanagement.services.KeyPairSource;

/**
 * {@link CompositeKeyPairSource} consists of various {@link KeyPairSource} implementations.
 * <p>
 * It returns a key pair from the first source which is able to create a key pair (if not empty).
 * </p>
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class CompositeKeyPairSource implements KeyPairSource {

    private final KeyPairSource[] sources;

    /**
     * Initializes a new {@link CompositeKeyPairSource}.
     *
     * @param sources A set of sources to use
     */
    public CompositeKeyPairSource(KeyPairSource... sources) {
        this.sources = sources;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyPairCreationStrategy#createKeyPair()
     */
    @Override
    public PGPKeyPair get() throws OXException {
        for (KeyPairSource source : sources) {
            PGPKeyPair keyPair = source.get();
            if (keyPair != null) {
                return keyPair;
            }
        }
        return null;
    }

    @Override
    public KeyPair getKeyPair() throws OXException {
        for (KeyPairSource source : sources) {
            KeyPair keyPair = source.getKeyPair();
            if (keyPair != null) {
                return keyPair;
            }
        }
        return null;
    }
}
