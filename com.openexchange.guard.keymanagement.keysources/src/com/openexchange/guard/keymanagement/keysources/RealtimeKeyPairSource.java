/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.guard.keymanagement.keysources;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Date;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;
import com.openexchange.exception.OXException;
import com.openexchange.guard.configuration.GuardConfigurationService;
import com.openexchange.guard.configuration.GuardProperty;
import com.openexchange.guard.keymanagement.keysources.exceptions.KeySourcesExceptionCodes;
import com.openexchange.guard.keymanagement.services.KeyPairSource;

/**
 * {@link RealtimeKeyPairSource} implements a {@link KeyPairSource} which will create a {@link PGPKeyPair} on demand in realtime.
 *
 * @author <a href="mailto:benjamin.gruedelbach@open-xchange.com">Benjamin Gruedelbach</a>
 * @since v2.6.0
 */
public class RealtimeKeyPairSource implements KeyPairSource {

    private final int rsaKeyLength;
    private final int rsaCertainty;
    private final SecureRandom secureRandom;

    /**
     * Initializes a new {@link RealtimeKeyPairSource}.
     *
     * @param rsaKeyLength The RSA key length to use for new keys
     * @param rsaCertainty The RSA key certainty to use for new keys
     */
    public RealtimeKeyPairSource(int rsaKeyLength, int rsaCertainty) {
        this.rsaKeyLength = rsaKeyLength;
        this.rsaCertainty = rsaCertainty;
        this.secureRandom = new SecureRandom();
    }

    /**
     * Initializes a new {@link RealtimeKeyPairSource}.
     *
     * @param guardConfigService The {@link GuardConfigurationService} to get the required RSA configurations from.
     */
    public RealtimeKeyPairSource(GuardConfigurationService guardConfigService) {
        this(guardConfigService.getIntProperty(GuardProperty.rsaKeyLength), guardConfigService.getIntProperty(GuardProperty.rsaCertainty));
    }

    /*
     * (non-Javadoc)
     *
     * @see com.openexchange.guard.keymanagement.services.KeyPairCreationStrategy#createKeyPair()
     */
    @Override
    public PGPKeyPair get() throws OXException {

        try {
            RSAKeyPairGenerator rsaKeyPairGenerator = new RSAKeyPairGenerator();
            rsaKeyPairGenerator.init(new RSAKeyGenerationParameters(BigInteger.valueOf(0x10001), secureRandom, rsaKeyLength, rsaCertainty));
            return new BcPGPKeyPair(PGPPublicKey.RSA_GENERAL, rsaKeyPairGenerator.generateKeyPair(), new Date());
        } catch (PGPException e) {
            throw KeySourcesExceptionCodes.KEY_CREATION_ERROR.create(e, e.getMessage());
        }
    }

    @Override
    public KeyPair getKeyPair() throws OXException {
        RSAKeyPairGenerator kpgen = new RSAKeyPairGenerator();
        kpgen.init(new RSAKeyGenerationParameters(BigInteger.valueOf(0x10001), secureRandom, rsaKeyLength, rsaCertainty));
        AsymmetricCipherKeyPair kp = kpgen.generateKeyPair();
        RSAPrivateCrtKeyParameters rsapckp = (RSAPrivateCrtKeyParameters) kp.getPrivate();

        RSAKeyParameters rsakp = (RSAKeyParameters) kp.getPublic();

        KeyFactory fact;
        try {
            fact = KeyFactory.getInstance("RSA", "BC");
            RSAPrivateCrtKeySpec prvKeySpecs = new RSAPrivateCrtKeySpec(rsapckp.getModulus(), rsapckp.getPublicExponent(), rsapckp.getExponent(), rsapckp.getP(), rsapckp.getQ(), rsapckp.getDP(), rsapckp.getDQ(), rsapckp.getQInv());

            PrivateKey privateKey = fact.generatePrivate(prvKeySpecs);

            RSAPublicKeySpec pubKeySpecs = new RSAPublicKeySpec(rsakp.getModulus(), rsakp.getExponent());
            PublicKey publicKey = fact.generatePublic(pubKeySpecs);

            return new KeyPair(publicKey, privateKey);
        } catch (InvalidKeySpecException | NoSuchProviderException | NoSuchAlgorithmException e) {
            throw KeySourcesExceptionCodes.KEY_CREATION_ERROR.create(e, e.getMessage());
        }
    }
}
